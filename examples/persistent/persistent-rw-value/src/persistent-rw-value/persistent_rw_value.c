/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "persistent_rw_value.h"
#include "persistent_mem_api.h"
#include "system_utils.h"
#include <string.h>

#define TAG	"persistent_rw_value"

#define USER_DATA_KEY	"persist_data"

char user_data[50] = "Hello! User";

void persistent_rw_value_init() {
	EM_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
}

void persistent_rw_value_loop() {
	char temp_data[50] = { 0 };
	// clear the temp_dummy_persistent_data
	bzero(temp_data, sizeof(temp_data));

	// Going to read data from persistent memory
	em_err res = persistent_read_data_by_key(USER_DATA_KEY, temp_data,
			sizeof(user_data));
	if (res == EM_OK) {
		EM_LOGI(TAG, "");
		EM_LOGI(TAG, "Data read from persistent memory ========> %s", temp_data);
		EM_LOGI(TAG, "");
	} else if (res == EM_FAIL) {
		EM_LOGE(TAG, "Failed to read data from persistent memory against the KEY %s", USER_DATA_KEY);
		res = persistent_write_data_by_key(USER_DATA_KEY, user_data, sizeof(user_data));
		if (res != EM_OK) {
			EM_LOGE(TAG, "Failed to write user data into persistent memory");
		} else {
			EM_LOGI(TAG, "");
			EM_LOGI(TAG, "Successfully written into persistent memory ========> %s", user_data);
			EM_LOGI(TAG, "");
		}
	}

	TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	// going to restart the system
	core_system_restart();
}
