/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef PERSISTENT_RW_VALUE_H_
#define PERSISTENT_RW_VALUE_H_

#include "emmate.h"
#include "thing.h"

/**
 *
 * */
void persistent_rw_value_init();

/**
 * */
void persistent_rw_value_loop();

#endif	/* PERSISTENT_RW_VALUE_H_ */
