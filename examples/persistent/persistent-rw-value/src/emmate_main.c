
#include "persistent-rw-value/persistent_rw_value.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling persistent_rw_value_init() in your_module.c in your-module directory ...");
	persistent_rw_value_init();
	EM_LOGI(TAG, "Returned from persistent_rw_value_init()");

	while(1){
		persistent_rw_value_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
