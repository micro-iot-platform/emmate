#include "json-creation-and-post/json_creation_and_post.h"
#include "conn.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	json_creation_and_post_init();

	while (1) {
		while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {
			TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
		}
		json_creation_and_post_loop();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
