/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   http_get.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The HTTP Post methods.
 *
 *
 *
 */

#ifndef SIMPLE_HTTP_POST_H_
#define SIMPLE_HTTP_POST_H_

#include "emmate.h"
#include "thing.h"

#define EXAMPLE_HOST				"httpbin.org"
#define EXAMPLE_URL					"http://"EXAMPLE_HOST"/post"	// This actually means "http://httpbin.org/post"
#define EXAMPLE_PORT				8080
#define EXAMPLE_ROOTCA				NULL
#define EXAMPLE_HTTP_USER_AGENT		"emmate/2.0"
#if CONFIG_PLATFORM_SIMULATOR
#define EXAMPLE_CONTENT_TYPE 	"Content-Type: application/json"
#else
#define EXAMPLE_CONTENT_TYPE 	"application/json"
#endif
#define EXAMPLE_POST_DATA			"\{\"example-key\": \"example-val\"}"
#define EXAMPLE_RESPONSE_LEN		2048

/**
 * @brief	Init function for http_post module
 *
 */
void simple_http_post_init();

/**
 * @brief	Execution function for http_post module
 *
 */
void simple_http_post_loop();


#endif /* SIMPLE_HTTP_POST_H_ */
