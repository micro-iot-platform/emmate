/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   http_get.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The HTTP Post method
 *
 *
 *
 */

#include "simple_http_post.h"

// Emmate Headers
#include "http_client_api.h"
#include "http_constant.h"

// Standard Headers
#include <string.h>

#define TAG "simple_http"

void simple_http_post_init() {
	EM_LOGI(TAG, "In http_post_init");
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);
}

void simple_http_post_loop() {
	em_err ret = EM_FAIL;
	EM_LOGI(TAG, "################################################################################");

	// store the Example POST data into the *json_buf
	char *json_buf = EXAMPLE_POST_DATA;

	// calculate the length of the *json_buf data
	int json_len = strlen(json_buf);

	// a char* variable to store the response of the HTTP Post Operation
	char http_resp[EXAMPLE_RESPONSE_LEN] = {'\0'};

	// get the Response data length
	size_t http_resp_len = 0;

	// Log print of String which used for Demonstrate the HTTP Post Operation
	EM_LOGI(TAG, "Posting Data: %s", json_buf);

	/* Do http operation */
	uint16_t http_stat = 0;

	ret = do_http_operation(EXAMPLE_URL, EXAMPLE_PORT, EXAMPLE_ROOTCA, HTTP_CLIENT_METHOD_POST, EXAMPLE_HOST,
								EXAMPLE_HTTP_USER_AGENT, EXAMPLE_CONTENT_TYPE, json_buf, json_len, http_resp,
									&http_resp_len, EXAMPLE_RESPONSE_LEN, &http_stat);

	EM_LOGW(TAG, "HTTP Status: %d", http_stat);
	if (ret == EM_OK) {
		if (http_resp_len > 0) {
			EM_LOGW(TAG, "Response: %.*s", http_resp_len, http_resp);
		} else {
			EM_LOGW(TAG, "No Response!");
			ret = EM_FAIL;
		}
	} else {
		ret = EM_FAIL;
	}

	EM_LOGI(TAG, "################################################################################\n");
}
