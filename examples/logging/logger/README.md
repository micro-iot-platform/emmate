# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so. 

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/somthing-logging.png" width="500">

###For ESP32 DevKit-C V4

<img src="res/fritzing/logging.png" width="500">

[//]: ![image](res/fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `logging` module APIs to change the log levels at run time

#### Example specific configurations
The 'Logging' configuration is internally enabled. So, for this example, no other configuration is needed.

<img src="res/logger-config.png" width="500">


This example shows:

- The different log levels and how they displays in the console
- How to change log levels of each module at runtime
