
#include "simple-migcloud-postdata/simple_migcloud_postdata.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	simple_migcloud_postdata_init();

	while (1) {
		simple_migcloud_postdata_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
