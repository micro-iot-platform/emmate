/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef SIMPLE_MIGCLOUD_POSTDATA_H_
#define SIMPLE_MIGCLOUD_POSTDATA_H_

#include "emmate.h"
#include "thing.h"

#define MAX_JSON_SIZE	512

/**
 *
 * */
void simple_migcloud_postdata_init();

/**
 * */
void simple_migcloud_postdata_loop();

#endif	/* SIMPLE_MIGCLOUD_POSTDATA_H_ */
