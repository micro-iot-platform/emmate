/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */
#include <string.h>
#include "simple_migcloud_postdata.h"

// Emmate Headers
#include "parson.h"
#include "apppostdata_helper.h"
#include "conn.h"

#define TAG	"migcloud_postdata"

static void create_json(char *json_buf, int val1, int val2) {
	// Store JSON string into this variable.
	char *serialized_string = NULL;

	// Creating JSON root value and object
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);

	// Set values against keys
	json_object_set_number(root_object, "val1", val1);
	json_object_set_number(root_object, "val2", val2);

	// Get the JSON in serialized format
	serialized_string = json_serialize_to_string(root_value);
	size_t serialized_string_len = json_serialization_size(root_value);

	// copy the serialized json to passed buffer
	strcpy(json_buf, serialized_string);

	// cleanup JSON memory allocation
	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
}

void simple_migcloud_postdata_init() {
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);
}

void simple_migcloud_postdata_loop() {
	// Declare JSON variables
	char json_buf[MAX_JSON_SIZE] = {'\0'};
	int json_buf_len = 0;

	// Declare val1 and val2 static variables
	static int val1 = 100;
	static int val2 = 0;

	// Increment & Decrement val1 and val2
	if(--val1 == 0) val1 = 100;
	if(++val2 == 0) val2 = 0;

	// Create the JSON
	memset(json_buf, 0x00, sizeof(json_buf));
	create_json(json_buf, val1, val2);

	EM_LOGW(TAG, "");
	EM_LOGW(TAG, "==================================================================");
	EM_LOGW(TAG, "Posting JSON: %s", json_buf);

	// Check for network
	if ((get_network_conn_status() & NETCONNSTAT_CONNECTED)	== NETCONNSTAT_CONNECTED) {

		// Post Data to migcloud
		em_err res = migcloud_post_app_json_http(json_buf);

		if (res == EM_OK) {
			EM_LOGW(TAG, "AppPostData Publish Process Success");
		} else {
			EM_LOGE(TAG, "AppPostData Publish Process Failed");
		}
	}
	EM_LOGW(TAG, "==================================================================");
	EM_LOGW(TAG, "");
}
