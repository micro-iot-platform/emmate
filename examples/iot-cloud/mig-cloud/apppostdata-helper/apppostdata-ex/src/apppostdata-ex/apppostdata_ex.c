/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */
#include <string.h>
#include "apppostdata_ex.h"

#include "apppostdata_helper.h"
#include "app_json_utils.h"

#if CONFIG_USE_INTERNET_CHECKER
#include "internet_checker.h"
#endif

#include "conn.h"

#include "app_json_utils.h"

#define TAG	"apppostdata_ex"

#define POST_DATA_MSG	"Hello-User..."

static APPLICATION_PUBLISH_DATA app_data;

void apppostdata_ex_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	// Clear the post data's message array
	bzero(app_data.msg, sizeof(app_data.msg));

	// populate the post data's message array with the message;
	strcpy(app_data.msg, POST_DATA_MSG);
	app_data.post_data_count = 1;

	EM_LOGI(TAG, "Returning from %s", __func__);
}

void apppostdata_ex_loop() {
	EM_LOGD(TAG, "In %s", __func__);

	char* post_data_json_buff = NULL;
	size_t post_data_json_buff_len = 0;

	// create Application Post Data Json
	em_err res = apppostdata_publish_json(&app_data, &post_data_json_buff, &post_data_json_buff_len);

	if (res == EM_OK) {
		EM_LOGI(TAG, "### AppPostData Publish Start #############################################\n");
		// print the Application post data json
		EM_LOGI(TAG, "\r\n-: AppPostData Publish JSON :-\r\n %s\r\n", post_data_json_buff);

		// checking for WiFi connection
#if CONFIG_USE_INTERNET_CHECKER
		if (get_internet_stat() == INTERNET_AVAILABLE) {
#else
		if ((get_network_conn_status() & NETCONNSTAT_CONNECTED)	== NETCONNSTAT_CONNECTED) {
#endif
			// start app post operation
			em_err res = migcloud_post_app_json_http(post_data_json_buff);
			if (res == EM_OK) {
				EM_LOGI(TAG, "AppPostData Publish Process Success");

				// increment the post-data count on the success of post operation
				app_data.post_data_count++;

			} else if (res == EM_ERR_NO_MEM) {
				EM_LOGE(TAG, "AppPostData Publish Process Failed because of low memory");
			} else {
				EM_LOGE(TAG, "AppPostData Publish Process Failed");
			}

		}
		EM_LOGI(TAG, "### AppPostData Publish Finish #############################################\n\n");
	}

	// free the apppost data allocated memory
	free(post_data_json_buff);

}
