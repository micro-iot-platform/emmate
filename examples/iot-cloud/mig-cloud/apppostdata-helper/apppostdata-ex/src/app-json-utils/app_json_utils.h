/*
 * File Name: app_json_utils.h
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#ifndef APP_JSON_UTILS_H_
#define APP_JSON_UTILS_H_

#include "apppostdata_ex.h"

/*
 * Creating Application POST Json String:
 *
 *	{
 *		"msg" : "Hello-User..."
 *		"post_data_count" : 20
 *	}
 *
 */
em_err apppostdata_publish_json(APPLICATION_PUBLISH_DATA *publish_data, char **ppbuf, size_t *plen);


#endif /* APP_JSON_UTILS_H_ */
