/*
 * File Name: app_json_utils.c
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#include "app_json_utils.h"

#include "parson.h"
#include "core_utils.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#define TAG "app_json_utils"

/*
 * Creating Application POST Json String:
 *
 *	{
 *		"msg" : "Hello-User..."
 *		"post_data_count" : 20
 *	}
 *
 */
em_err apppostdata_publish_json(APPLICATION_PUBLISH_DATA *publish_data, char **ppbuf, size_t *plen) {
	em_err ret = EM_FAIL;

	char *serialized_string = NULL;

	if (NULL != publish_data) {
		// root obj
		JSON_Value *root_value = json_value_init_object();
		JSON_Object *root_object = json_value_get_object(root_value);

		json_object_set_string(root_object, GET_VAR_NAME(publish_data->msg, "->"), publish_data->msg);

		json_object_set_number(root_object, GET_VAR_NAME(publish_data->post_data_count, "->"), publish_data->post_data_count);

//		serialized_string = json_serialize_to_string(root_value);
//		size_t len = json_serialization_size(root_value);

		serialized_string = json_serialize_to_string_pretty(root_value);
		size_t len = json_serialization_size_pretty(root_value);

		//printf("%s\r\n", serialized_string);

		len = len + 1;  // since json_serialization_size returns size + 1
		//printf("JSON Len = %d\r\n", len);

		char *ptemp = (char*) calloc(len, sizeof(char));
		if (ptemp == NULL) {
			EM_LOGE(TAG, "Failed to create application's Post Json for Low Memory");
			*ppbuf = NULL;
			*plen = 0;

			json_value_free(root_value);
			if (serialized_string != NULL) {
				json_free_serialized_string(serialized_string);
			}

			return EM_ERR_NO_MEM;
		}

		memset(ptemp, 0x00, len);
		memcpy(ptemp, serialized_string, len);
		*plen = len;
		*ppbuf = ptemp;

		json_value_free(root_value);
		if (serialized_string != NULL) {
			json_free_serialized_string(serialized_string);
		}
		ret = EM_OK;
	} else {
		ret = EM_ERR_INVALID_ARG;
	}
	return ret;

}

