
#include "apppostdata-ex/apppostdata_ex.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling apppostdata_ex_init() in your_module.c in your-module directory ...");
	apppostdata_ex_init();
	EM_LOGI(TAG, "Returned from apppostdata_ex_init()");

	while(1){
		EM_LOGD(TAG, "Calling apppostdata_ex_loop() in your_module.c in your-module directory ...");
		apppostdata_ex_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
