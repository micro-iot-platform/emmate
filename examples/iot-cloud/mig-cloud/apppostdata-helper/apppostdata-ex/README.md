# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing-logging.png" width="500">

### For ESP32 DevKit-C V4

<img src="res/fritzing/logging.png" width="500">

[//]: ![ESP32-DevKit-C](fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

#### Example specific configurations
This example is configured ..

<img src="res/apppostdata-helper-example-config.png" width="500">

---
Note: The following pre-requisites must be met:

- This example must be run with **migCloud server** to work. Please visit the [migCloud website](https://mig.iquesters.com/?s=cloud) to create a account. After creating an account you must follow the steps provided in the portal to register and create a SoMThing. Please visit the server to know this in detail. 

- This example uses Wi-Fi as the network connectivity option, so you must have a stable Wi-Fi connection.

- This example uses BLE as the device configuration option, so please visit the [EmMate Website](https://mig.iquesters.com/?s=embedded) and download the EmMate Device Configuration mobile application.

- After flashing this example, please configure the device using the EmMate Device Configuration Application. This will enable your application to connect to a Wi-Fi AP. 

---

With the help of this example you will learn how to post data from an EmMate application's configuration to the migCloud IoT server. It uses EmMate's `system/apppostdata-helper` APIs.

The migCloud IoT server provides rich features for working with any IoT project. You can easily create and link your IoT devices with the migCloud. EmMate provides full scale APIs for connectivity with the migCloud server. Please visit the [migCloud website](https://mig.iquesters.com/?s=cloud) to know more.

This example does the following things:

- Creates a JSON string

- Calls the function `post_application_json_via_http()` to post the json data to the migCloud server.

