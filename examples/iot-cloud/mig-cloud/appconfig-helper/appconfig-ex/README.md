# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing-logging.png" width="500">

### For ESP32 DevKit-C V4

<img src="res/fritzing/logging.png" width="500">

[//]: ![ESP32-DevKit-C](res/fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

#### Example specific configurations
This example is configured ..

<img src="res/appconfig-helper-example-config.png" width="500">

---
Note: The following pre-requisites must be met:

- This example must be run with **migCloud server** to work. Please visit the [migCloud website](https://mig.iquesters.com/?s=cloud) to create a account. After creating an account you must follow the steps provided in the portal to register and create a SoMThing. Please visit the server to know this in detail. 

- This example uses Wi-Fi as the network connectivity option, so you must have a stable Wi-Fi connection.

- This example uses BLE as the device configuration option, so please visit the [EmMate Website](https://mig.iquesters.com/?s=embedded) and download the EmMate Device Configuration mobile application.

- After flashing this example, please configure the device using the EmMate Device Configuration Application. This will enable your application to connect to a Wi-Fi AP. 

---

With the help of this example you will learn how to change an EmMate application's configuration from the migCloud IoT server. It uses EmMate's `system/appconfig-helper` APIs.

By changing a configuration the behaviour of a IoT device can be changed remotely. For example, for a simple temperature monitoring IoT device the frequency of data acquisition can be altered remotely. The Application Configuration Module deals with this feature. Be sure to register to the [migCloud IoT server](https://mig.iquesters.com/?s=cloud) to enjoy these features.  

The migCloud IoT server provides rich features for working with any IoT project. You can easily create and link your IoT devices with the migCloud. EmMate provides full scale APIs for connectivity with the migCloud server. Please visit the [migCloud website](https://mig.iquesters.com/?s=cloud) to know more.


This example does the following things:

- Registers a callback function that will be called if any configuration is changed in the migCloud server. The callback is registered using `register_app_configuration_function()`

- Once the callback is called, the application parses the configurations and prints the new values.

- In a real application, these values can be used to modify the behaviour of the IoT device.
