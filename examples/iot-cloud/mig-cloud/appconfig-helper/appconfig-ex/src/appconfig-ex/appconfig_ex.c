/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "appconfig_ex.h"
#include "app_json_utils.h"
#include "appconfig_helper.h"

#define TAG	"appconfig_ex"

APPLICATION_CONFIGURATION app_conf;

/**
 *	app-configuration call-back function
 *	Whenever an application configuration is received from the migcloud server,
 *	this function will get called
 */
static em_err app_configuration_process(char * app_config, size_t app_config_len) {
	em_err res = EM_FAIL;
	EM_LOGI(TAG, "Application Configuration %s\n", app_config);
	if (app_config != NULL) {
		res = parse_config_json(app_config, &app_conf);
		if (res != EM_OK) {
			EM_LOGE(TAG, "Failed to parse App Configuration");
		}else{
			EM_LOGI(TAG,"###################### AppConfig Data");
			EM_LOGI(TAG,"app_config_value1 = %d", app_conf.app_config_value1);
			EM_LOGI(TAG,"app_config_value2 = %f", app_conf.app_config_value2);
			EM_LOGI(TAG,"###################### AppConfig Data\n");
		}
	}
	return res;
}

void appconfig_ex_init() {
	/* Do all necessary initializations here */

	/* register application configuration function with the migcloud library */
	migcloud_register_app_config_function(app_configuration_process);
}

void appconfig_ex_loop() {
	/* Nothing to do in this example ...*/
}
