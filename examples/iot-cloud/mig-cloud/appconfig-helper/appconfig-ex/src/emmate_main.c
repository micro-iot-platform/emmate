
#include "appconfig_ex.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	appconfig_ex_init();

	while(1){
		appconfig_ex_loop();
		TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
	}
}
