# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing-logging.png" width="500">

### For ESP32 DevKit-C V4

<img src="res/fritzing/logging.png" width="500">

[//]: ![ESP32-DevKit-C](../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example
This example demonstrates how to use the `input-processor` APIs to create a nested JSON. The EmMate Framework internally uses `parson` json library.

#### Example specific configurations
This example is configured ..

<img src="res/input-processor-example-config.png" width="500">

In this example you will learn:

- How to create `JSON_Value` & `JSON_Object` and set string, boolean and number
- How to create a nested json
- How to serialize a `JSON_Value` into a string
- How to serialize a `JSON_Value` into a pretty string