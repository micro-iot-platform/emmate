#include "simple-json-create/simple_json_create.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	simple_json_create_init();

	while (1) {
		// Execute My module
		simple_json_create_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
