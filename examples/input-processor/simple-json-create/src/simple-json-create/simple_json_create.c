/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   json_create.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a JSON using structure.
 *
 *
 *
 */

#include "simple_json_create.h"
#include "core_utils.h"
// Include JSON Library
#include "parson.h"

#define TAG	"simple_json"

/********************************************** Module's Static Functions **********************************************************************/
static void create_json() {
	// Temporary variables to store JSON values
	char my_name[50] = "emmate";
	int my_marks = 75;

	// Store JSON string into this variable.
	char *serialized_string = NULL;
	char *serialized_string_pretty = NULL;

	// Creating JSON root value and object
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);

	// Set values against keys
	json_object_set_string(root_object, "name", my_name);
	json_object_set_number(root_object, "marks", my_marks);

	// Get the JSON in serialized format
	serialized_string = json_serialize_to_string(root_value);
	size_t serialized_string_len = json_serialization_size(root_value);

	// Get The JSON in serialized pretty format
	serialized_string_pretty = json_serialize_to_string_pretty(root_value);
	size_t serialized_string_pretty_len = json_serialization_size_pretty(
			root_value);

	// Print of Serialized String
	if (serialized_string != NULL) {
		EM_LOGI(TAG, "Serialized String:");
		EM_LOGI(TAG, "Length: %d", serialized_string_len);
		EM_LOGW(TAG, "JSON:\n%s\r\n", serialized_string);
	}

	// Print of Serialized String Pretty
	if (serialized_string_pretty != NULL) {
		EM_LOGI(TAG, "Serialized String Pretty:");
		EM_LOGI(TAG, "Length: %d", serialized_string_pretty_len);
		EM_LOGW(TAG, "JSON:\n%s", serialized_string_pretty);
	}

	/* cleanup JSON memory allocation*/
	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	json_free_serialized_string(serialized_string_pretty);
}

/*******************************************************************************************************************/
void simple_json_create_init() {
	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
}

void simple_json_create_loop() {
	EM_LOGI(TAG,
			"==================================================================");

	// Create and print the simple JSON
	create_json();

	EM_LOGI(TAG,
			"==================================================================\n\n");
}
