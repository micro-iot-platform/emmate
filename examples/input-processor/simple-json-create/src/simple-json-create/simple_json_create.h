/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   json_create.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a JSON using structure.
 *
 *
 *
 */

#ifndef SIMPLE_JSON_CREATE_H_
#define SIMPLE_JSON_CREATE_H_

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for json_create module
 *
 */
void simple_json_create_init();

/**
 * @brief	Execution function for json_create module
 *
 */
void simple_json_create_loop();

#endif	/* SIMPLE_JSON_CREATE_H_ */
