Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing_adc_lm35.png" width="600">

### For ESP32 DevKit-C V4

<img src="res/fritzing/adc-lm35.png" width="500">

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the DHT22 module APIs to read temperature and humidity from a DHT22 sensor.

#### Example specific configurations
This example is configured ..

<img src="res/external-ics-lm35-example-config.png" width="500">

Please see the functions `dht22_set_dout_gpio()`, `dht22_read()`, `dht22_get_temperature()` and `dht22_get_humidity()` for more info.
