#include "dht22-sensor/dht22_sensor.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void *param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	dht22_sensor_init();

	while (1) {
		dht22_sensor_loop();
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	}
}
