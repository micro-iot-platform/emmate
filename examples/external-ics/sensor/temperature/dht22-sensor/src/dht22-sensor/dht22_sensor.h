/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef DHT22_SENSOR_H_
#define DHT22_SENSOR_H_

#include "emmate.h"
#include "thing.h"

/**
 * @brief  Initialize DHT22
 * @return
 *
 **/
void dht22_sensor_init();
/**
 * @brief Read temperature and humidity from DHT22
 *   print value in degree Celsius
 *
 * @return
 *
 **/
void dht22_sensor_loop();

#endif	/* DHT22_SENSOR_H_ */
