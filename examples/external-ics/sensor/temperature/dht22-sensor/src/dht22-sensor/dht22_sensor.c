/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */
#include "dht22_sensor.h"
#include "dht22.h"

#define TAG	"DHT22_sensor"

void dht22_sensor_init() {
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	dht22_set_dout_gpio(DHT22_GPIO);
}

void dht22_sensor_loop() {
	float temperature = 0, humidity = 0;

	em_err ret = dht22_read();
	if (ret == EM_OK) {
		temperature = dht22_get_temperature();
		humidity = dht22_get_humidity();

		EM_LOGI(TAG, "Temperature = %0.2f C\tRelative Humidity = %0.2f%", temperature, humidity);
	} else {
		dht22_error_handler(ret);
	}
}
