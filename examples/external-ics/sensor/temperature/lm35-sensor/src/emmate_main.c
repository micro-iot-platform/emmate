
#include "lm35-sensor/lm35_sensor.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	lm35_sensor_init();

	while(1){
		lm35_sensor_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
