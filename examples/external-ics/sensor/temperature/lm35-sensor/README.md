Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing_adc_lm35.png" width="600">

### For ESP32 DevKit-C V4

<img src="res/fritzing/adc-lm35.png" width="500">

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the LM32 module APIs to read temperature data from a LM35 sensor.

#### Example specific configurations
This example is configured ..

<img src="res/external-ics-lm35-example-config.png" width="500">

Please see the functions `lm35_sensor_init()` and `lm35_sensor_loop()` for more info.
