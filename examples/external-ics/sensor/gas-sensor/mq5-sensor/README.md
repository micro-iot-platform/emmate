# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below steps to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing-mq5.png" width="600">

## About this example

This example demonstrates how to use the MQ5 module APIs to read Gas Level Data from a MQ-5 sensor.

#### Example specific configurations
This example is configured ..

<img src="res/config.png" width="500">

Please see the functions `mq5_sensor_app_init()` and `mq5_sensor_app_loop()` for more info.
