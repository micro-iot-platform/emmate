#include "tm-diff-now/tm_diff_now.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	tm_diff_now_init();

	while (1) {
		// Execute My module
		tm_diff_now_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
