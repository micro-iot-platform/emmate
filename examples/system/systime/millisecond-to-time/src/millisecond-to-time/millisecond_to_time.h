/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   millisecond_to_time.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * MilliSecond  to Time conversion
 *
 *
 *
 */
#ifndef MILLISECOND_TO_TIME_H_
#define MILLISECOND_TO_TIME_H_

#include "emmate.h"
#include "thing.h"

#define CURRENT_TIME_MILLIS		1560259909111	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief set the time value and time zone
 *
 *
 */
void millisecond_to_time_init();
/**
 * @brief Pass the unix time in convert_millis_to_tm()
 *     Get time in structure form from convert_millis_to_tm()
 *     print time
 *
 */
void millisecond_to_time_loop();

#endif	/* MILLISECOND_TO_TIME_H_ */

