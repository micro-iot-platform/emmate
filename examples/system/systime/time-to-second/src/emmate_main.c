#include "time-conversion-to-sec/time_conversion_to_sec.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	time_to_sec_init();

	while (1) {
		time_to_sec_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
