/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   time_set_get.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The time set date/time into the system RTC and get date/time.
 *
 *
 *
 */


#include "emmate.h"
#include "thing.h"

#define CURRENT_TIME_MILLIS		1560259909111	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief	Init function for time_set_get module
 *
 */
void time_set_get_init();

/**
 * @brief	Execution function for time_set_get module
 *
 */
void time_set_get_loop();


