#include "time-set-get/time_set_get.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	time_set_get_init();

	while (1) {
		// Execute My module
		time_set_get_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
