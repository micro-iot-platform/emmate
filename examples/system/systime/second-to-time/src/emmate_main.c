#include "second-to-time/second_to_time.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	second_to_time_init();

	while (1) {
		second_to_time_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
