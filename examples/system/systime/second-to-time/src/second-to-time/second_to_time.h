/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   second_to_time.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * Second  to Time conversion
 *
 *
 *
 */
#ifndef SECOND_TO_TIME_H_
#define SECOND_TO_TIME_H_

#include "emmate.h"
#include "thing.h"


#define CURRENT_TIME_SECONDS		1560259909	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief set the time value and time zone
 *
 *
 */
void second_to_time_init();
/**
 * @brief Pass the unix time in convert_seconds_to_tm()
 *     Get time in structure form from convert_seconds_to_tm()
 *     print time
 *
 */
void second_to_time_loop();

#endif	/* SECOND_TO_TIME_H_ */

