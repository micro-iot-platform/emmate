/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   second_to_time.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * Second  to Time conversion
 *
 *
 *
 */

#include "second_to_time.h"
#include "systime.h"
#include <string.h>

#define TAG "second_to_time"

void second_to_time_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
#if 0
	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = 1560259909111;// UTC Time/Date : Tue Jun 11 2019 11:46:47

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec =(milisec_now % 1000) * 1000;

	/* Set the system time */
	if(core_settimeofday(&now_tv, NULL) != 0) {
		EM_LOGE(TAG, "Failed to set System time...");
	} else {
		EM_LOGI(TAG, "Successfully set the System time...");
	}
#endif
}

void second_to_time_loop() {
	EM_LOGI(TAG, "In %s", __func__);
	uint64_t second_to_convert = CURRENT_TIME_SECONDS;
	struct tm converted_time;

	// get the Current system time
	convert_seconds_to_tm(second_to_convert, &converted_time);

	char strftime_buf[64];
	// make time string from current-time structure data, in human readable format.
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &converted_time);

	EM_LOGI(TAG, "Seconds to convert: %lld\n", second_to_convert);
	EM_LOGI(TAG, "Converted Time: %s\n", strftime_buf);
}

