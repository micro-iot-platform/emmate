# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/somthing-logging.png" width="500">

### For ESP32 DevKit-C V4

<img src="res/fritzing/logging.png" width="500">

[//]: ![image](res/fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates, how to set and get system time using the systime module APIs.

#### Example specific configurations
This example is configured ..

<img src="res/systime-example-config.png" width="500">

See the functions `system_time_init() and system_time_loop()` for better understanding.

To set a desired current time as the system time open the file `system-time/system_time.h` and change the macro `CURRENT_TIME_MILLIS`