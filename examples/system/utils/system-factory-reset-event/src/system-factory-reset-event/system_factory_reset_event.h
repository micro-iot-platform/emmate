/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef SYSTEM_FACTORY_RESET_EVENT_H_
#define SYSTEM_FACTORY_RESET_EVENT_H_

#include "emmate.h"
#include "thing.h"

/**
 *
 * */
void system_factory_reset_event_init();

/**
 * */
void system_factory_reset_event_loop();

#endif	/* SYSTEM_FACTORY_RESET_EVENT_H_ */
