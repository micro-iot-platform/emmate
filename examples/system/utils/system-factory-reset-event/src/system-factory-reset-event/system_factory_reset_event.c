/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "system_factory_reset_event.h"
#include "system_utils.h"

#define TAG	"system_factory_reset_event"

/**
 *	declear application's factory reset event callback function
 */
void applications_factory_reset_event_cb(){
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "######################## Application's Factory Reset Event Callback ####\n");

	EM_LOGI(TAG, "Hello!! User, system's factory reset event Occure");
	EM_LOGI(TAG, "Complete your unfinished process and leave from this callback function");
	EM_LOGI(TAG, "Good Bye!!! User, leaving from the application's factory reset event callback functions\n");

	EM_LOGI(TAG, "######################## Application's Factory Reset Event Callback ####");
	EM_LOGI(TAG,"");

}

void system_factory_reset_event_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */
	// Registern application's factory-reset event's drive by system when factory-reset event occre
	register_factory_reset_event_function(applications_factory_reset_event_cb);

	EM_LOGI(TAG, "Returning from %s", __func__);
}

void system_factory_reset_event_loop() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "Now press and Hold the reset button to execute the system reset event.....\n");
}
