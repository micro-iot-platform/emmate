
#include "system-factory-reset-event/system_factory_reset_event.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling system_factory_reset_event_init() in system_factory_reset_event.c in system-factory-reset-event directory ...");
	system_factory_reset_event_init();
	EM_LOGI(TAG, "Returned from system_factory_reset_event_init()");

	while(1){
		EM_LOGD(TAG, "Calling system_factory_reset_event_loop() in system_factory_reset_event.c in system-factory-reset-event directory ...");
		system_factory_reset_event_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
