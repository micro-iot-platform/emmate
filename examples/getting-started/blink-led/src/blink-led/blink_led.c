/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   blink_led.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Blink of a LED
 *
 *
 *
 */

#include "blink_led.h"
#include "gpio_helper_api.h"

#define TAG "blink_led"

#define BLINK_LED_GPIO BOARD_LED_1

/********************************************** Module's Static Functions **********************************************************************/

/*******************************************************************************************************************/

void blink_led_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* 	Configure the gpio for BLINK_LED_GPIO as an Output Pin with Floating state [No PullUp or PullDown or Both]*/
	em_err res = configure_gpio(BLINK_LED_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if(res != EM_OK){
		EM_LOGE(TAG, "Failed to configure gpio: BLINK_LED_GPIO, which is an invalid gpio");
	}

}

void blink_led_loop() {
	EM_LOGI(TAG, "In %s", __func__);

	static uint32_t bit = 0;
	bit ^= 1;

	// set the GPIO pin value to Glow (1) or OFF (0) to the LED
	em_err res = set_gpio_value(BLINK_LED_GPIO, bit);
	if(res != EM_OK){
		EM_LOGE(TAG, "Failed to set value to gpio: BLINK_LED_GPIO, which is an invalid gpio");
	}
}
