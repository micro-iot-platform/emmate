# Example: Blink-Led

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing_led.png" width="600">

### For ESP32 DevKit-C V4

<img src="res/fritzing/blink_led.png" width="500">

[//]: ![image](fritzing/blink_led.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates a LED blink operation with the help of the GPIO peripheral of the EmMate Framework.

#### Example specific configurations
This example is configured ..

<img src="res/gpio-example-config.png" width="500">

This example does the following things:

- Blinks the `SYSTEM_HMI_LED_MONO_RED` LED internally as a system notification. This must be configured properly in `src/thing/thing.h`
- Toggles the `CUSTOM_LED_1` LED at a 1 second interval. This must be configured properly in `src/thing/thing.h`
- Prints a log message every 1 second.
