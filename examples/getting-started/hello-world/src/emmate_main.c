#include "hello-world/hello_world.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	while (1) {
		hello_world();
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	}
}
