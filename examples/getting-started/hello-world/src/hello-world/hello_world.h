/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   hello_world.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to get started
 *
 */

#ifndef HELLO_WORLD_H_
#define HELLO_WORLD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Print Hello! World
 *
 */
void hello_world();

#ifdef __cplusplus
}
#endif

#endif	/* HELLO_WORLD_H_ */
