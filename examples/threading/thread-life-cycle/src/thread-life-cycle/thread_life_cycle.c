/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   thread_life_cycle.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the
 * task to create and run for 100time and then deleted.
 *
 *
 *
 */

#include "thread_life_cycle.h"

#include "threading.h"

#define TAG	"thread_life_cycle"

TaskHandle thread_handle;

/* Task1 with priority 3 */
static void task_1(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

		if (count > 100) {
			EM_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

void thread_life_cycle_init() {
	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a task-1 with priorities 3 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_1-1.....");
	}
}

void thread_life_cycle_loop() {
	//EM_LOGI(TAG, "In thread_life_cycle_loop");
}
