/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread_manager.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task management
 *
 *
 *
 */

#ifndef MULTI_THREAD_MANAGER_H_
#define MULTI_THREAD_MANAGER_H_

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for multi_thread_manager module
 *
 */
void multi_thread_manager_init();

/**
 * @brief	Execution function for multi_thread_manager module
 *
 */
void multi_thread_manager_loop();

#endif	/* MULTI_THREAD_MANAGER_H_ */
