/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread_manager.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task management
 *
 *
 *
 */

#include "multi_thread_manager.h"
#include "threading.h"

#define TAG	"multi_thread_manager"

TaskHandle thread_handle_1;
TaskHandle thread_handle_2;
TaskHandle thread_handle_3;

/********************************************** Module's Static Functions **********************************************************************/

/* Task1 with priority 3 */
static void task_1(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		// Task delay of 2 secs
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/* Task2 with priority 3 */
static void task_2(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		// Task delay of 1.5 secs
		TaskDelay((DELAY_1_SEC+DELAY_500_MSEC) / TICK_RATE_TO_MS);
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/* Task3 with priorities 3  */
static void task_3(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/* Start_tasks function used to execute for start the tasks */
static void start_tasks() {
	/* Create a task-1 with priorities 1 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_1, &thread_handle_1);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_1.....");
	}

	/* Create a task-2 with priorities 2 */
	xReturned = TaskCreate(task_2, "task_2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_2, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_2 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_2.....");
	}

	/* Create a task_3 with priorities 3 */
	xReturned = TaskCreate(task_3, "task_3", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_3);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_3 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_3.....");
	}
}

/**************************************************************************************************************************/

void multi_thread_manager_init() {
	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	start_tasks();
}

void multi_thread_manager_loop() {
	static uint8_t manage_step = 1;

	switch (manage_step) {
	case 1: {
		// stop the execution of Task_1 & Task 2
		EM_LOGI(TAG, "======================================= Suspend execution of the Task 1 & 2");
		if (thread_handle_1 != NULL && thread_handle_2 != NULL) {
			TaskSuspend(thread_handle_1);
			TaskSuspend(thread_handle_2);
		}
		manage_step++;
		break;
	}
	case 2: {
		// resume the execution of Task_1
		EM_LOGI(TAG, "======================================= Resume execution of the Task 1");
		if (thread_handle_1 != NULL) {
			TaskResume(thread_handle_1);
		}
		manage_step++;
		break;
	}
	case 3: {
		// resume the execution of Task_2
		EM_LOGI(TAG, "======================================= Suspend execution of the Task 2");
		if (thread_handle_2 != NULL) {
			TaskResume(thread_handle_2);
		}

		// stop the execution of Task 3
		EM_LOGI(TAG, "======================================= Suspend execution of the Task 3");
		if (thread_handle_3 != NULL) {
			TaskResume(thread_handle_3);
		}

		manage_step++;

		break;
	}
	case 4: {
		// resume the execution of the all tasks
		EM_LOGI(TAG, "======================================= Resume the execution of the all tasks");

		// resume the execution of Task_1
		EM_LOGI(TAG, "Resume execution of the Task 1");
		if (thread_handle_1 != NULL) {
			TaskResume(thread_handle_1);
		}

		// resume the execution of Task_2
		EM_LOGI(TAG, "Resume execution of the Task 2");
		if (thread_handle_2 != NULL) {
			TaskResume(thread_handle_2);
		}

		// resume the execution of Task_3
		EM_LOGI(TAG, "Resume execution of the Task 3");
		if (thread_handle_3 != NULL) {
			TaskResume(thread_handle_3);
		}

		manage_step++;
		break;
	}
	case 5: {
		// Delete the all running tasks
		EM_LOGI(TAG, "======================================= Delete the all running tasks");

		// Delete the execution of Task_1
		EM_LOGI(TAG, "Delete execution of the Task 1");
		if (thread_handle_1 != NULL) {
			TaskDelete(thread_handle_1);
		}
		// Delete the execution of Task_2
		EM_LOGI(TAG, "Delete execution of the Task 2");
		if (thread_handle_2 != NULL) {
			TaskDelete(thread_handle_2);
		}
		// Delete the execution of Task_3
		EM_LOGI(TAG, "Delete execution of the Task 3");
		if (thread_handle_3 != NULL) {
			TaskDelete(thread_handle_3);
		}
		manage_step++;

		break;
	}
	case 6: {
		// Again Start the all tasks
		EM_LOGI(TAG, "======================================= Again Start the all tasks");
		start_tasks();
		manage_step++;
		break;
	}
	default: {
		manage_step = 1;
		break;
	}
	}
}
