#include "multi-thread-manager/multi_thread_manager.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	multi_thread_manager_init();

	while (1) {
		// Execute My module
		multi_thread_manager_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
