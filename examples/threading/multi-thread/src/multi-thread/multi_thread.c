/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task
 *
 *
 *
 */

#include "multi_thread.h"
#include "threading.h"

#define TAG	"multi_thread"

TaskHandle thread_handle_1;
TaskHandle thread_handle_2;

/********************************************** Module's Static Functions **********************************************************************/

/* Task1 with priority 3 */
static void task_1(void * param) {
	uint8_t count = 0;
	while (1) {
		count++;
		EM_LOGI(TAG, "Hello %s running count %d..", __func__, count);
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

		if (count > 100) {
			EM_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/* Task2 with priority 3 */
static void task_2(void * param) {
	uint8_t count = 0;
	while (1) {
		count++;
		EM_LOGI(TAG, "Hello %s running count %d..", __func__, count);
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

		if (count > 150) {
			EM_LOGI(TAG, "Hello %s going to end after 150times print..", __func__);
			break;
		}
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/**************************************************************************************************************************/

void multi_thread_init() {
	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a task-1 with priorities 3 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_1);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_1.....");
	}

	/* Create a task-2 with priorities 3 */
	xReturned = TaskCreate(task_2, "task_2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_2 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_2.....");
	}

}

void multi_thread_loop() {
	//EM_LOGI(TAG, "In %s", __func__);
}
