/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread_switching.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task switching
 *
 *
 *
 */

#include "multi_thread_switching.h"
#include "threading.h"

#define TAG	"multi_thread_switching"

TaskHandle thread_handle_1;
TaskHandle thread_handle_2;
TaskHandle thread_handle_3;

/********************************************** Module's Static Functions **********************************************************************/

/* Task1 with priority 1 */
static void task_1(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		// Task delay of 2 secs
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);

		if (count > 100) {
			EM_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/* Task2 with priority 2 */
static void task_2(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		// Task delay of 1.5 secs
		TaskDelay((DELAY_1_SEC + DELAY_500_MSEC) / TICK_RATE_TO_MS);

		if (count > 100) {
			EM_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/* idle_task with priorities 0 means This task will run when other High priority task are in Idle state */
static void idle_task(void *param) {
	uint8_t count = 0;
	while (1) {
		EM_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);

		if (count > 100) {
			EM_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Deleting the task_1 */
	TaskDelete(NULL);
#endif
}

/**************************************************************************************************************************/

void multi_thread_switching_init() {
	EM_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a task-1 with priorities 1 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_1, &thread_handle_1);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_1.....");
	}

	/* Create a task-2 with priorities 2 */
	xReturned = TaskCreate(task_2, "task_2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_2, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "task_2 started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start task_2.....");
	}

	/* Create a idle_task with priorities 0 means This task will run when other High priority task are in Idle state */
	xReturned = TaskCreate(idle_task, "idle_task", TASK_STACK_SIZE_2K, NULL, 0, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		EM_LOGI(TAG, "idle_task started.....");
	} else {
		// failed to start the task
		EM_LOGE(TAG, "Failed to start idle_task.....");
	}
}

void multi_thread_switching_loop() {
	//EM_LOGI(TAG, "In %s", __func__);
}
