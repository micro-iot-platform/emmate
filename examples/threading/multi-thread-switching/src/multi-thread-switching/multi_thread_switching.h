/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread_switching.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task switching
 *
 *
 *
 */
#ifndef MULTI_THREAD_SWITCHING_H_
#define MULTI_THREAD_SWITCHING_H_

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for multi_thread_switching module
 *
 */
void multi_thread_switching_init();

/**
 * @brief	Init function for multi_thread_switching module
 *
 */
void multi_thread_switching_loop();

#endif	/* MULTI_THREAD_SWITCHING_H_ */
