#include "multi-thread-switching/multi_thread_switching.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	multi_thread_switching_init();

	while (1) {
		// Execute My module
		multi_thread_switching_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
