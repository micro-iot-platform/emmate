/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using Button
 *
 *
 *
 */

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for button_interaction module
 *
 */
void button_interaction_init();

/**
 * @brief	Execution function for button_interaction module
 *
 */
void button_interaction_loop();

