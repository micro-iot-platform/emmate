/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using Button
 *
 *
 *
 */

#include "button_long_interaction.h"
#include "button_helper.h"
#include "gpio_helper_api.h"
#include "gpio_core.h"

#define BLINK_GPIO 		BOARD_LED_1
#define SWITCH_GPIO		BOARD_BUTTON_2

#define TAG "button_interaction"

#define DELAY_TIME		DELAY_10_SEC

static uint32_t bit = 0;

/********************************************** Module's Static Functions **********************************************************************/

void btn_interaction_handler(uint8_t io_pin, BTN_INTERACTION_TYPE intact_type) {

	if (io_pin == SWITCH_GPIO && intact_type == LONG_PRESSED) {
		bit ^= 1;
		// set the GPIO pin value 1 for On & 0 for Off
		if (bit == 1) {
			EM_LOGI(TAG, "Button pressed LED On");
		} else {
			EM_LOGI(TAG, "Button pressed LED Off");
		}
		set_gpio_value(BLINK_GPIO, bit);
	}
}

/*******************************************************************************************************************/

void button_interaction_init() {
	em_err res = configure_gpio(BLINK_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize BLINK_GPIO");
	}

	/*
	 * Initialize the basic Buttons activity. (button details are in thing/thing.h)
	 */
	em_err ret = init_long_press_btn_interface(SWITCH_GPIO, GPIO_IO_FLOATING, btn_interaction_handler);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize the SWITCH_2");
	}
}

void button_interaction_loop() {
	/*
	 * In this example the Led will glow when the button pressed and again it will off when button pressed.
	 */

	// write down your code...
	TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
}
