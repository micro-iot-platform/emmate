/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_button.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using multiple buttons
 *
 *
 *
 */

#include "multi_button.h"
#include "button_helper.h"
#include "gpio_helper_api.h"
#include "gpio_core.h"

#define BLINK_GPIO_1 			BOARD_LED_1
#define BLINK_GPIO_2 		BOARD_LED_2
#define DOUBLE_PRESS_SW		BOARD_BUTTON_3
#define CUST_PRESS_SW		BOARD_BUTTON_4

#define TAG "multi_button"

#define DELAY_TIME		DELAY_10_SEC

static uint32_t bit = 0;
static uint32_t bit1 = 0;

/********************************************** Module's Static Functions **********************************************************************/

void btn_interaction_handler(uint8_t io_pin, BTN_INTERACTION_TYPE intact_type) {
	if (io_pin == DOUBLE_PRESS_SW && intact_type == DOUBLE_PRESSED) {
		bit ^= 1;
		if (bit == 1) {
			EM_LOGI(TAG, "Button DOUBLE_PRESSED LED On");
		} else {
			EM_LOGI(TAG, "Button DOUBLE_PRESSED LED Off");
		}
		set_gpio_value(BLINK_GPIO_1, bit);
	} else if (io_pin == CUST_PRESS_SW && intact_type == CUSTOM_PRESSED) {
		bit1 ^= 1;
		if (bit1 == 1) {
			EM_LOGI(TAG, "Button CUSTOM_PRESSED LED On");
		} else {
			EM_LOGI(TAG, "Button CUSTOM_PRESSED LED Off");
		}
		set_gpio_value(BLINK_GPIO_2, bit1);
	}
}

/*******************************************************************************************************************/

void multi_button_init() {

	EM_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	em_err res = configure_gpio(BLINK_GPIO_1, GPIO_IO_MODE_OUTPUT,
			GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize BLINK_GPIO");
	}
	res = configure_gpio(BLINK_GPIO_2, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize BLINK_GPIO");
	}

	/*
	 * Initialize the basic Buttons activity. (button details are in thing/thing.h)
	 */
	em_err ret = init_double_press_btn_interface(DOUBLE_PRESS_SW,
			GPIO_IO_FLOATING, btn_interaction_handler);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize the SWITCH_1");
	}

	ret = init_custom_press_btn_interface(CUST_PRESS_SW, GPIO_IO_FLOATING,
	DELAY_1_SEC, btn_interaction_handler);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize the SWITCH_2");
	}
}

void multi_button_loop() {
	/*
	 * In this example the Led will glow when the CUSTOM_BTN_1 button pressed
	 * And again it will off when CUSTOM_BTN_2 button pressed.
	 */

	// write down your code...
	TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);

}
