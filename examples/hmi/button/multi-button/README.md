# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/somthing_led_multi_button.png" width="600">

### For ESP32 DevKit-C V4

<img src="res/fritzing/multi-button.png" width="500">

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `hmi/button-helper` module APIs to initialize multiple Buttons and execute a LED on/off operation using those buttons.

#### Example specific configurations
This example is configured ..

<img src="res/button-example-config.png" width="500">