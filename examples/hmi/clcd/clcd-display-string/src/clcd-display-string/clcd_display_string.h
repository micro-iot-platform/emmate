/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using clcd display
 *
 *
 *
 */

#ifndef CLCD_DISPLAY_STRING_H_
#define CLCD_DISPLAY_STRING_H_

#include "emmate.h"
#include "thing.h"


/**
 * @brief	Init function for clcd_display_string module
 *
 */
void clcd_display_init();

/**
 * @brief	Execution function for clcd_display_string module
 *
 */
void clcd_display_string();

#endif	/* CLCD_DISPLAY_STRING_H_ */
