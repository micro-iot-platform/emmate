/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   button_interaction.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Human interaction with the system using clcd display
 *
 *
 *
 */

#ifndef YOUR_MODULE_H_
#define YOUR_MODULE_H_


#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for clcd_display_string module
 *
 */
void clcd_display_init();

/**
 * @brief	Execution function for clcd_display_string module
 *
 */
void clcd_display_string();

/**
 * @brief	Execution function for clcd_display_string module in loop
 *
 */
void clcd_display_string_loop();

#endif	/* YOUR_MODULE_H_ */
