/*
 * This file contains Pin Configurations for Team Thing Board v2.2
 * More Details about this board can be found at: https://mig.iquesters.com/?s=somthing&p=resources
 *
 * Please read the below comments carefully before using this file
 */

#ifndef THING_H_
#define THING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "som.h"
#define YOUR_THING_NAME	"CLCD MultiLine String"

/* ************************************** LED Button Selection ************************************
 * If the following LED(s) or Button(s) are to be used with the LED Helper or Button Helper module,
 * then do the following:
 * For LED Helper:
 *   - Change the value of the below MACRO LEDS_NUMBER to the desired number
 *   - Add the desired LED Macro Name(s) in the LEDS_LIST below
 * For Button Helper:
 *   - Change the value of the below MACRO BUTTONS_NUMBER to the desired number
 *   - Add the desired BUTTON Macro Name(s) in the BUTTONS_LIST below
 * */
#define BOARD_LED_1			SOM_PIN_43
#define BOARD_LED_2			SOM_PIN_73
#define BOARD_LED_3			SOM_PIN_97
#define BOARD_LED_4			SOM_PIN_55

#define BOARD_BUTTON_1		SOM_PIN_43
#define BOARD_BUTTON_2		SOM_PIN_73
#define BOARD_BUTTON_3		SOM_PIN_97
#define BOARD_BUTTON_4		SOM_PIN_55

/* ************************************** System LED Selection ************************************
 * The following LEDs are used by the EmMate Framework for showing various system specific notifications.
 * These are added to the LED Helper module. To add/remove more LEDs to the LED Helper modify the below
 * macros - LEDS_NUMBER & LEDS_LIST
 *
 * If the LED Helper and System HMI is enabled in the Menu Configuration tool, then system specific
 * notifications will be shown automatically, else these pins can be used for other purpose
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_100   /* Used by the EmMate Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_102   /* Used by the EmMate Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_104   /* Used by the EmMate Framework as BLUE LED */

#define LEDS_ACTIVE_STATE 	1	/*!< State at which a LED become active (High-1 or Low-0) */

/* Number of LEDs used for the LED Helper Module */
#define LEDS_NUMBER			3
/* List of LEDs to pass to the LED Helper Module. Must match the LEDS_NUMBER macro */
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

/* ************************************** System Button Selection ************************************
 * The following BUTTON is used by the EmMate Framework as a System/Factory Reset Button for completely erasing the
 * persistent memory. It is added to the Button Helper Module. To add/remove more Buttons to the Button Helper,
 * modify the below macros - BUTTONS_NUMBER & BUTTONS_LIST
 *
 * If the Button Helper and System HMI is enabled in the Menu Configuration tool, then the
 * Factory Reset feature will work, else this pin can be used for other purpose
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will
 */
#define SYSTEM_RESET_BUTTON		SOM_PIN_98		/* Used by the EmMate Framework as System/Factory Reset Button */

#define BUTTONS_ACTIVE_STATE	0	/* State at which a Button become active (High-1 or Low-0) */

/* Number of Buttons used for the Button Helper Module */
#define BUTTONS_NUMBER			1
/* List of Buttons to pass to the Button Helper Module. Must match the BUTTONS_NUMBER macro */
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

/* ************************************** SDMMC GPIO Selection ************************************
 * If using SDMMC module with SPI, the following macros must be defined else compilation error will occur
 * If not using the SDMMC feature, then these pins can be used for any other purpose
 */
#define SDMMC_CLK 	SOM_PIN_47
#define SDMMC_MISO 	SOM_PIN_49
#define SDMMC_MOSI 	SOM_PIN_51
#define SDMMC_CS 	SOM_PIN_53

/* ************************************** Other GPIO Selection ************************************
 * It is recommended to add other pins that might be required for an application in this file.
 * Please add other pins below.
 */

/*
 * @Note:
 * If using CLCD-Lib, the following defines must be made else compilation error will occur
 */

/*** MikroBus Port 1 ***/
// If CLCD-lib Enable for 4bit operation mode then Data pin D7-D4 use to send data to CLCD
#define LCD_D4		SOM_PIN_69
#define LCD_D5		SOM_PIN_86
#define LCD_D6		SOM_PIN_88
#define LCD_D7		SOM_PIN_92

/*
 * If CLCD 4bit Mode enable then mention below D3-D0 data pin will be deactivate
 * "CONFIG_CLCD_4BIT_MODE_ENABLE" is application config macro, which define in src/core_config.h
 */
#if !CONFIG_CLCD_4BIT_MODE_ENABLE
/*
 * If CLCD-lib Enable for 8bit operation mode then Data pin D7-D0 use to send data to CLCD.
 * So, all pin are activated
 */
/*** MikroBus Port 2 ***/
#define LCD_D0		SOM_PIN_43
#define LCD_D1		SOM_PIN_30
#define LCD_D2		SOM_PIN_105
#define LCD_D3		SOM_PIN_71

#endif

/*** MikroBus Port 4 ***/
// CLCD common control pins for CLCD 8bit & 4bit operation
#define LCD_RS		SOM_PIN_59
#define LCD_RW		SOM_PIN_73
#define LCD_EN		SOM_PIN_194

// Busy pin
#define LCD_BUSY		LCD_D7

#ifdef __cplusplus
}
#endif

#endif /* THING_H_ */
