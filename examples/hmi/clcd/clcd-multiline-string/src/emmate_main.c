#include "clcd-multi-string/clcd_display_multi_string.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void *param) {
	EM_LOGI(TAG,
			"==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG,
			"Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG,
			"==================================================================");

	//initialize My module
	clcd_display_init();

	// Execute My module
	clcd_display_string();

	while (1) {

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
