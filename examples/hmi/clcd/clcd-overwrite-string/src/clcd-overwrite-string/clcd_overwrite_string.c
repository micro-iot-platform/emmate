/**
 * This is an example c file of clcd display string  module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "clcd_overwrite_string.h"
#include "clcd.h"


#define TAG	"clcd_overwrite_string"

//Write your own string here
char *company_name = "Iquester Solution LLP";
char *platform_name = "EmMate Framework";

void clcd_display_init() {

	//Initialize clcd with max row and max column here we using 4x20 lcd display
	em_err ret = init_clcd(MAX_ROW, MAX_COL);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "%s init_clcd_lcd failed!", __func__);
		return;
	}
}

void clcd_display_string() {

	/*set string for displaying 1st row
	 *set string to  character,start row, end row, start column, end column,
	 *scroll option, scroll frequency(in ms), blink frequency(in ms),overwrite status(overwrite as per delay given in loop)
	 */
	em_err res = set_string_to_clcd(company_name, 0, 0, 0, 19, SCROLL_LEFT_TO_RIGHT, 250, 0,
			OVERWRITE);
	if (res == CONFLICT_STRING) {
		EM_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}

	TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);

	/*set string for displaying 2nd row
	 * set string to  character,start row(1), end row(1), start column(0), end column(19),
	 * scroll option(right to left),scroll frequency(100 ms),
	 * overwrite string after above delay time
	 */
	res = set_string_to_clcd(platform_name, 0, 0, 0, 19, SCROLL_RIGHT_TO_LEFT, 100, 0, OVERWRITE);
	if (res == CONFLICT_STRING) {
		EM_LOGE(TAG, " %s set_string failed!", __func__);
		return;
	}
}

