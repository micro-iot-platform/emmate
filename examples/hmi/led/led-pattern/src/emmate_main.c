#include "led-pattern-example/led_pattern_example.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	led_pattern_init();

	while (1) {
		led_pattern_loop();

		TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
	}
}
