#include "single-led/single_led.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	single_led_init();

	while (1) {
		// Execute My module
		single_led_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
