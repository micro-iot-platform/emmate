/*
 * unix_current_time.c
 * Created on: 16-May-2019
 *
 *  Author: Pawan
 */
#include "unix_current_time.h"
#include "led_helper.h"
#include "systime.h"

#define TAG  "unix_current_time"

#define num_leds 12
#define  TIME_PATTERN make_led_pattern(COLOR_MONO,10000,1, 1)

//NOs. of led blink as per unix hours/
static void blink_led_1() {    //1 led ON when time is 1 hr/
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 11, MONO_LED, BSP_LED_1, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 11, 1, TIME_PATTERN);
}

static void blink_led_2() {   //2 leds ON when time is 2  hr/
	blink_led_1();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 10, MONO_LED, BSP_LED_2, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 10, 1, TIME_PATTERN);
}

static void blink_led_3() {   //3 leds ON when time is 3  hr/
	blink_led_2();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 9, MONO_LED, BSP_LED_3, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 9, 1, TIME_PATTERN);
}

static void blink_led_4() {   //4 leds ON when time is 4  hr/
	blink_led_3();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 8, MONO_LED, BSP_LED_4, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 8, 1, TIME_PATTERN);
}

static void blink_led_5() {    //5 leds ON when time is 5  hr/
	blink_led_4();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 7, MONO_LED, BSP_LED_5, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 7, 1, TIME_PATTERN);
}

static void blink_led_6() {  //6 leds ON when time is 6  hr/
	blink_led_5();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 6, MONO_LED, BSP_LED_6, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 6, 1, TIME_PATTERN);
}

static void blink_led_7() {  //7 leds ON when time is 7  hr/
	blink_led_6();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 5, MONO_LED, BSP_LED_7, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 5, 1, TIME_PATTERN);
}

static void blink_led_8() {  //8 leds ON when time is 8  hr/
	blink_led_7();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 4, MONO_LED, BSP_LED_8, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 4, 1, TIME_PATTERN);
}

static void blink_led_9() {   //9 leds ON when time is 9  hr/

	blink_led_8();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 3, MONO_LED, BSP_LED_9, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 3, 1, TIME_PATTERN);
}

static void blink_led_10() {   //10 leds ON when time is 10  hr/

	blink_led_9();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 2, MONO_LED, BSP_LED_10, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 2, 1, TIME_PATTERN);
}

static void blink_led_11() {   //11 leds ON when time is 11  hr/

	blink_led_10();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds - 1, MONO_LED, BSP_LED_11, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds - 1, 1, TIME_PATTERN);
}

static void blink_led_12() {    //12 leds ON when time is 12  hr/

	blink_led_11();
	set_led_type_idx(LED_HELPER_MODE_APP, num_leds, MONO_LED, BSP_LED_12, 0, 0);
	map_patterns_to_led(LED_HELPER_MODE_APP, num_leds, 1, TIME_PATTERN);
}

static void set_system_time() {
	struct timeval tv;
	tv.tv_sec = 1558922107;
	tv.tv_usec = 0;
	/* Structure crudely representing a timezone.
	 This is obsolete and should never be used.  */
	struct timezone tz;
	tz.tz_dsttime = 0;
	tz.tz_minuteswest = 0;

	core_settimeofday(&tv, NULL);
}

static void get_system_time() {

	struct tm now;
	int t = get_systime(&now);

	int time_hr = now.tm_hour;

	if (time_hr > 12) {
		time_hr = time_hr - 12;
	}

	EM_LOGI(TAG, "Current Hour is: %d", time_hr);

	/* Set the LED notification depending upon the time hours */
	if (time_hr == 1) { // when time_hr =1 AM or PM
		blink_led_1();
	} else if (time_hr == 2) {  // when time_hr =2 AM or PM
		blink_led_2();
	} else if (time_hr == 3) {   // when time_hr =3 AM or PM
		blink_led_3();
	} else if (time_hr == 4) {  // when time_hr =4 AM or PM
		blink_led_4();
	} else if (time_hr == 5) {  // when time_hr =5 AM or PM
		blink_led_5();
	} else if (time_hr == 6) {  // when time_hr =6 AM or PM
		blink_led_6();
	} else if (time_hr == 7) {  // when time_hr =7 AM or PM
		blink_led_7();
	} else if (time_hr == 8) {  // when time_hr =8 AM or PM
		blink_led_8();
	} else if (time_hr == 9) {  // when time_hr =9 AM or PM
		blink_led_9();
	} else if (time_hr == 10) {  // when time_hr =10 AM or PM
		blink_led_10();
	} else if (time_hr == 11) {  // when time_hr =11 AM or PM
		blink_led_11();
	} else if (time_hr == 12) {  // when time_hr =12 AM or PM
		blink_led_12();
	}
}

void unix_current_time_led_init() {

	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	/* Initialize the basic LEDs activity. (LED pin details are in thing/thing.h) */
	em_err ret = init_leds(LED_HELPER_MODE_APP);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to initilize the leds");
		return;
	}

}


void unix_current_time_led_notification() {
	em_err ret;

	set_system_time();

	ret = init_led_notification(LED_HELPER_MODE_APP, num_leds, NOTIFY_TIME_FOREVER);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "init_led_notification failed!");
		return;
	}

	// Loop will check the update time after 60 sec
	while (1) {
		get_system_time();

		TaskDelay(DELAY_1_MIN / TICK_RATE_TO_MS); // 60 sec delay
	}

	// Start the led notification /
	start_led_notification(LED_HELPER_MODE_APP);
}
