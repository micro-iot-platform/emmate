
/*
 * unix_current_time.h
 *
 *  Created on: 27-May-2019
 *      Author: Pawan Kumar Kashyap
 */

#include "emmate.h"
#include "thing.h"

/**
 *
 **/
void unix_current_time_led_init();

/**
 * @brief This function start the led blink as per unix time
 *
 * @return
 *
 **/
void unix_current_time_led_notification();




