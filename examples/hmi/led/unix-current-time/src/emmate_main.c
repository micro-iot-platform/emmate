#include "unix-current-time/unix_current_time.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");
	
	unix_current_time_led_init();

	while (1) {
		unix_current_time_led_notification();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
