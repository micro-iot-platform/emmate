# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so. 

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing-unix-led.png" width="600">

### For ESP32 DevKit-C V4

<img src="res/fritzing/unix-led.png" width="500">

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `hmi/led-helper` module APIs to initialize a LED notification comprising of multiple LEDs.

#### Example specific configurations
This example is configured ..

<img src="res/led-example-config.png" width="500">

This example shows:

- How to initialize an LED notification using multiple LEDs
- How to create custom LED blinking patterns
- How to map 1 or more LED blinking pattern on to a LED
- How to start an LED notification
