# Getting Started with Examples

The best way to learn EmMate is to understand the examples. In the `examples` directory, there are numerous examples of each EmMate Module. The examples will provide a guide so that one can understand the EmMate project structure and its APIs.

If you are building a project based on EmMate one of the examples could be your starting point. It will provide you a template on which you can build upon.

Lets get started..

## Examples Directory Structure

The following directory structure provides an at-a-glance view of all the examples available in EmMate.

```
examples
    |-- conn
    |        |-- protocols
    |                |-- http
    |                        |-- http-get
    |                        |-- http-post
    |                        |-- https-post
    |                        |-- json-creation-and-post
    |
    |-- external-ics
    |        |-- sensor
    |                |-- gas-sensor
    |                |        |-- mq5-sensor
    |                |-- temperature
    |                         |-- lm35-sensor
    |
    |-- getting-started
    |        |-- blink-led
    |        |-- hello-world
    |
    |-- hmi
    |        |-- button
    |        |        |-- button-interaction
    |        |        |-- multi-button
    |        |        |-- btn-custom-long-interaction
    |        |        |-- btn-long-interaction
    |        |
    |        |-- clcd
    |        |        |-- clcd-display-string
    |        |        |-- clcd-multi-features
    |        |        |-- clcd-multiline-string
    |        |        |-- clcd-overwrite-string
    |        |        |-- clcd-scroll-string
    |        |
    |        |-- led
    |                |-- led-pattern
    |                |-- single-led
    |                |-- unix-current-time
    |
    |-- input-processor
    |        |-- json-array-create
    |        |-- json-create
    |        |-- nested-json-create
    |        |-- parser-json
    |
    |-- iot-cloud
    |        |-- mig-cloud
    |                |-- appconfig-helper
    |                |-- apppostdata-helper
    |
    |-- logging
    |        |-- logger
    |
    |-- peripherals
    |        |-- adc
    |        |        |-- adc-pot
    |        |        |-- lm35
    |        |
    |        |-- gpio
    |        |        |-- blink-led
    |        |        |-- gpio-intr
    |        |
    |        |-- i2c
    |        |        |-- epprom-module
    |        |        |-- i2c-master-slave
    |        |        |-- rtc-module
    |        |
    |        |-- pwm
    |        |        |-- led-pwm
    |        |                |-- led-fade-api-ex
    |        |                |-- led-fade-duty
    |        |                |-- led-pwm-with-rgb-led
    |        |                |-- vibgyor-rainbow-colour
    |        |
    |        |-- uart
    |                |-- uart-handshake
    |                |-- uart-loop-back
    |
    |-- persistent
    |        |-- persistent-rw-value
    |
    |-- system
    |        |-- systime
    |        |        |-- millisecond-to-time
    |        |        |-- second-to-time
    |        |        |-- system-time
    |        |        |-- time-diff
    |        |        |-- time-set-get
    |        |        |-- time-to-seconds
    |        |        |-- tm-diff-now
    |        |        |-- tm-to-sec
    |        |
    |        |-- utils
    |                 |-- system-factory-reset-event
    |
    |-- threading
            |-- data-exchange-into-threads
            |-- multi-thread
            |-- multi-thread-manager
            |-- multi-thread-switching
            |-- thread-life-cycle
```	

## Understanding the hello-world Example

Now lets understand one example among the above list. We choose the `hello-world` example.

### Project Directory Structure
Each EmMate example has the following directory structure. The `hello-world` example is shown below.

```
hello-world
     |
     |-- res --> (Optional) Project's resource directory to store files which are not directly impacting the build.
     |
     |-- setup --> (Mandatory) Directory to contain EmMate Project's setup utilities.
     |        |-- setup.bat        --> (Mandatory) EmMate setup utility for Windows 
     |        |-- setup_conf.bat    --> (Mandatory) EmMate setup configurations for Windows. Not important for user.
     |        |-- setup.sh            --> (Mandatory) EmMate setup utility for Ubuntu
     |        |-- setup.conf        --> (Mandatory) EmMate setup configurations for Ubuntu. Not important for user.
     |        |-- readme.txt        --> (Optional)    Setup utility readme file
     |
     |-- src --> (Mandatory) Directory to contain a EmMate Project's source files
     |        |-- hello-world    --> (Optional) User may create sub-directories inside the src dir for code modularity
     |        |        |-- hello_world.c --> (Optional) Application dependant source file
     |        |        |-- hello_world.h --> (Optional) Application dependant header file
     |        |
     |        |-- include --> (Mandatory) Must be present for every EmMate Project. No additional files should be kept here.
     |        |        |-- thing.h             --> (Mandatory) Board specific file. Read file contents for explanation.
     |        |        |-- app_version.conf --> (Mandatory) File to maintain project version.
     |        |        |-- CMakeLists.txt     --> (Mandatory) CMakeLists.txt file for this directory 
     |        |
     |        |-- emmate_main.c --> (Mandatory) An EmMate project's main file. User's code starts from here.
     |        |-- CMakeLists.txt --> (Mandatory) CMakeLists.txt file for src. User to add files and libs here
     |
     |-- emmate_config --> (Mandatory) EmMate project configuration file. Used by the EmMate Configuration GUI tool
     |-- README.md --> (Optional) File detailing the Project's functionality

```

### Important Points before you modify an example

1. The `src` directory must be present. Do not rename it.
2. Do not add or remove any file from the `include` directory. If your application requires an include directory then create a separate one. Do not rename the `include` directory.
3. If you need to add any file or directory in the project, you must add its entry in the `src/CMakeLists.txt` file. See below section **Understanding the CMakeLists.txt file**. 
4. If you need to add any dependancy library you must add its entry in the `src/CMakeLists.txt` file.
5. Do not add any source/header file outside the `src` directory

### Understanding the CMakeLists.txt file

The EmMate Framework uses CMake for managing its build process. CMake is a cross-platform free and open-source software tool for managing the build process of software using a compiler-independent method.

CMake makes use of CMakeLists.txt to understand what files must be built. There are many CMakeLists.txt files in EmMate. Here we will look at the file where an user's intervention is required. This file is the `src/CMakeLists.txt` file of your project. We will use the `hello-world` project's file.

The file contents are:

```
add_subdirectory(include)

# Add source files to the application
set(srcs
	emmate_main.c
	
	# Add your module's source files here.
	# If your source files are inside a directory like your_module.c, then add the full path
	hello-world/hello_world.c
	
	)

add_library(${CMAKE_PROJECT_NAME} STATIC ${srcs})

# List the libraries on which this application is dependent
list(APPEND EMMATE_LIBS 
					emmate_config
					${CMAKE_PROJECT_NAME}_inc
					# Add more libraries below this line
					)
target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE ${EMMATE_LIBS})

# Add include directories
target_include_directories(${CMAKE_PROJECT_NAME} 
							PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}
							# Add more include directories below this line
							
							)

```

**Code Walkthrough**

* `add_subdirectory(include)`

Adds a subdirectory of the parent directory to the build. There must be a `CMakeLists.txt` file in this subdirectory. If you plan to add subdirectories inside the `src` directory, then you must add a line `add_subdirectory(<your-dir>)` below this line.

* Add source files to the project

```
# Add source files to the application
set(srcs
	emmate_main.c
	
	# Add your module's source files here.
	# If your source files are inside a directory like your_module.c, then add the full path
	hello-world/hello_world.c
	
	)
```
Sets a variable called `srcs` which will contain many source files `.c/.cpp` which must compiled. If you add any source files to your project, make sure to add an entry here.

If your source file is inside a subdirectory, you may add its full path like `hello-world/hello_world.c`. In this case you do not need to add the line `add_subdirectory(<your-dir>)`. But if there are any header files in the subdirectory you need to provide full path when including them, like `#include "hello-world/hello_world.h"`

* `add_library(${CMAKE_PROJECT_NAME} STATIC ${srcs})`

This line tells the compiler to convert your project into a static library. This library is finally linked with the build process to make an executable.

* Link dependency libraries to the project

```
# List the libraries on which this application is dependent
list(APPEND EMMATE_LIBS 
					emmate_config
					${CMAKE_PROJECT_NAME}_inc
					# Add more libraries below this line
					)
target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE ${EMMATE_LIBS})
```
If your project is using EmMate modules such as `http`, `mig-cloud`, `fatfs-helper`, `external-ics` etc. then these modules have to be linked to your project separately.

By default few common libraries are already linked to your project. They are:

```
common
logging
peripherals
system
threading
utils
hmi
persistent
``` 
To link an EmMate library, just add the name below the line `# Add more libraries below this line`. EmMate module names and library names are same.

* Add include directories

```
# Add include directories
target_include_directories(${CMAKE_PROJECT_NAME} 
							PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}
							# Add more include directories below this line
							
							)

```
If you have header files in directories other than `src`, then you must add the include directory name separately. To add an include directory you must follow this standard: `PRIVATE <your include directory name>`. It must be done below the line `# Add more include directories below this line`

**For more information, please read the CMake Documentation**

## Setup, Build and Run Examples

Now you should have a fair understanding of the EmMate examples and its structure. So we can try them hands-on.

To setup, build and run any example, please read the section **Setup, Build & Run EmMate Projects** of [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=documentation)  

