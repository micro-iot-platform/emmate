#include "gpio-intr/gpio_intr.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling gpio_intr_init() in your_module.c in your-module directory ...");
	gpio_intr_init();
	EM_LOGI(TAG, "Returned from gpio_intr_init()");

	while (1) {
		gpio_intr_loop();
		TaskDelay(DELAY_250_MSEC/ TICK_RATE_TO_MS);
	}
}
