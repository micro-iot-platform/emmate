/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "gpio_intr.h"
#include "gpio_helper_api.h"

#define TAG	"gpio_intr"

#define APP_SW	BOARD_BUTTON_4
#define APP_LED	BOARD_LED_2

static bool sw_pressed = false;
static uint8_t toggle_bit = LOW;

/************************************ Modules IRS handler Function ***********************************/
void INTERRUPT_ATTRIBUTES isr_func(void* arg) {
	uint32_t gpio_num = (uint32_t) arg;

	// ISR handler body
	if(gpio_num == APP_SW) {
		// Switch Press detected
		sw_pressed = true;
	}

}

void gpio_intr_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* initializations Switch 2 GPIO */
	em_err res = configure_gpio(APP_SW, GPIO_IO_MODE_INPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize Switch-2");
	} else {
		// Add ISR handler function
		res = add_gpio_isr(APP_SW, GPIO_INTERRUPT_POSEDGE, isr_func, (void*)APP_SW);
		if (res != EM_OK) {
			EM_LOGE(TAG, "Failed to Add Interrupt");
		}
	}

	/* initializations LED 3 GPIO */
	res = configure_gpio(APP_LED, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize LED-3");
	}

	EM_LOGI(TAG, "Returning from %s", __func__);
}

void gpio_intr_loop() {
	EM_LOGD(TAG, "In %s", __func__);

	if (sw_pressed == true) {
		EM_LOGI(TAG, "Switch Pressed");
		toggle_bit ^= HIGH;
		// Toggle the LED 3
		set_gpio_value(APP_LED, toggle_bit);
		sw_pressed = false;
	}
}
