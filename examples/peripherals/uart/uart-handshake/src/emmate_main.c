
#include "uart-handshake/uart_handshake.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling uart_handshake_init() in your_module.c in your-module directory ...");
	uart_handshake_init();
	EM_LOGI(TAG, "Returned from uart_handshake_init()");

	while(1){
		EM_LOGD(TAG, "Calling uart_handshake_loop() in your_module.c in your-module directory ...");
		uart_handshake_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
