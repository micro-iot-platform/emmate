# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.
###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing_uart_loop_back.png" width="850">

### For ESP32 DevKit-C V4

<img src="res/fritzing/uart_loop_back.png" width="500">

[//]: ![image](fritzing/uart_loop_back.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the LoopBack **UART** communication between ESP32 and computer terminal, with the help of the UART peripheral of the EmMate Framework.

#### Example specific configurations
This example is configured ..

<img src="res/uart-example-config.png" width="500">

This example does the following:

- Show LoopBack comunication using the UART advanced APIs.
- The UART's Tx & Rx pin must be configured properly in `src/thing/thing.h`
- In LoopBack mode: Whatever is typed in the terminal will be looped back to the terminal through the system. So the user can see the typed keys printed on the terminal.