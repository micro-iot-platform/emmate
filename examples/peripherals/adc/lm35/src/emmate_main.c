#include "lm35/lm35.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	// initialize My module
	lm35_init();

	while (1) {

		// Execute My module
		lm35_loop();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
