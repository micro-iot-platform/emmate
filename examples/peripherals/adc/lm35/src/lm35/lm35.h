/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   lm35.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate 
 * The ambient temperature measurement using LM35
 *
 *
 *
 */

#include "emmate.h"
#include "thing.h"

/**
 * @brief	Init function for lm35 module
 *
 */
void lm35_init();

/**
 * @brief	Execution function for lm35 module
 *
 */
void lm35_loop();
