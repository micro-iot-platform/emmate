/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   eeprom_module.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * EEPROM
 *
 *
 *
 */
#include "eeprom_module.h"
#include "i2c_core.h"

#define TAG	"eeprom_module"

#define CLK_SPEED      		100000         	// clock speed 100kHz
#define SLAVE_ADDRESS     	0xA0			// Slave address of EEPROM-24C0xx 0b10100000
#define COMMAND_ADDRESS    	0x00        	// Command address

I2C_CORE_DRV_HANDLE i2c_drv_handle = NULL;

void eeprom_module_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	EM_LOGI(TAG, "Your thing name is: %s\r\n", YOUR_THING_NAME);

	em_err ret;

	uint8_t data_wr[47] = "EmMate Framework Iquesters Solution LLP";  // Data which write on the slave
	uint8_t size = 1;
	//printf("%s", data_wr);

	//printf("\n");

	i2c_drv_handle = init_i2c_core_master(I2C_CORE_NUM_1, SDA_PIN, SCL_PIN, CLK_SPEED, 1); // initialize I2C master peripheral
	if (i2c_drv_handle != NULL) {
		EM_LOGI(TAG, "I2C driver initialize complete, for EEPROM-24C0xx module");
		for (int i = 0; i <= 47; i++) {
			ret = i2c_core_master_write_slave(i2c_drv_handle, SLAVE_ADDRESS, COMMAND_ADDRESS + i, &data_wr[i], size); // Data write
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Failed to write '%c' data into EEPROM module", data_wr[i]);
			}
			TaskDelay(DELAY_10_MSEC / TICK_RATE_TO_MS);
		}
	} else {
		EM_LOGE(TAG, "Failed to initialize the I2C driver");
	}
	EM_LOGI(TAG, "Returning from your_module_init\r\n");
}

void eeprom_module_loop() {
	EM_LOGI(TAG, "In %s", __func__);
	em_err ret;
	uint8_t data_rd = 0;
	uint8_t size = 1;
	if (i2c_drv_handle != NULL) {
		EM_LOGI(TAG, "Read Data from EEPROM-24C0xx module:");
		for (int i = 0; i <= 47; i++) {
			ret = i2c_core_master_read_slave(i2c_drv_handle, SLAVE_ADDRESS, COMMAND_ADDRESS + i, &data_rd, size, 0); // Data read and store in array
			if (ret == EM_OK) {
				printf("%c", (char) data_rd);  // print the data
			} else {
				EM_LOGE(TAG, "\n");
				EM_LOGE(TAG, "Failed to read data from EEPROM module");
			}
		}
		printf("\n\n");
	} else {
		// Write Error Mesasges LOGs
		EM_LOGE(TAG, "I2C driver not initialize");
	}
}
