/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "i2c_master_slave.h"
#include "i2c_core.h"
#include "threading.h"

#define TAG	"i2c_master_slave"

#define I2C_MASTER_MODE	1

I2C_CORE_DRV_HANDLE slave_drv;
I2C_CORE_DRV_HANDLE master_drv;

#define SLAVE_ADDR		0x28
#define MASTER_SCL_FRQ	100000

#define LEN 128

SemaphoreMutexHandle print_mux = NULL;

/**
 * @brief test function to show buffer
 */
static void disp_buf(uint8_t *buf, int len) {
	int i;
	for (i = 0; i < len; i++) {
		printf("%02x ", buf[i]);
		if ((i + 1) % 16 == 0) {
			printf("\n");
		}
	}
	printf("\n");
}

static void i2c_test_task(void* param) {
	em_err res;
	uint8_t data_rd[LEN] = { 0 };
	uint8_t data_wr[LEN] = { 0 };
	uint32_t task_idx = (uint32_t) param;
	int i = 0;

	while (1) {

		bzero(data_rd, LEN);	// clear the Data read buffer array
		bzero(data_wr, LEN);	// clear the Data write buffer array

		// create the Data write buffer array
		for (i = 0; i < LEN; i++) {
			data_wr[i] = i + 10;
		}

		// Take the Semaphore.
		SemaphoreTakeMutex(print_mux, THREADING_MAX_DELAY);

		// Slave write data to the master
		res = i2c_core_slave_write_master(slave_drv, data_wr, LEN, DELAY_1_SEC);

		if (res == EM_ERR_TIMEOUT) {
			EM_LOGE(TAG, "I2C Timeout to Slave write");
		} else if (res == EM_OK) {
			// Master read data from the Slave
			res = i2c_core_master_read_slave_without_cmd(master_drv, SLAVE_ADDR, data_rd, LEN);
			printf("*********************************************************\n");
			printf("==== TASK[%d]: Slave write buffer data ====\n", task_idx);
			disp_buf(data_wr, LEN);
			printf("==== TASK[%d]: Master read buffer data ====\n", task_idx);
			disp_buf(data_rd, LEN);
		} else {
			EM_LOGW(TAG, "Master read from slave error, IO not connected...%s \n", esp_err_to_name(res));
		}

		// Free the Semaphore.
		SemaphoreGiveMutex(print_mux);
		vTaskDelay((DELAY_2_SEC * (task_idx + 1)) / TICK_RATE_TO_MS);

		/**************************************************************************************************/

		bzero(data_rd, LEN);
		bzero(data_wr, LEN);
		for (i = 0; i < LEN; i++) {
			data_wr[i] = i;
		}

		// Take the Semaphore.
		SemaphoreTakeMutex(print_mux, THREADING_MAX_DELAY);

		// Master write data to the slave
		res = i2c_core_master_write_slave_without_cmd(master_drv, SLAVE_ADDR, data_wr, LEN);
		if (res == EM_ERR_TIMEOUT) {
			ESP_LOGE(TAG, "I2C Timeout to Master write");
		} else if (res == EM_OK) {
			// Slave read data from the Master
			res = i2c_core_slave_read_master(slave_drv, data_rd, LEN, DELAY_500_MSEC);
			printf("*********************************************************\n");
			printf("---- TASK[%d]: Master write buffer data ----\n", task_idx);
			disp_buf(data_wr, LEN);
			printf("---- TASK[%d]: Slave read buffer data ----\n", task_idx);
			disp_buf(data_rd, LEN);
		} else {
			EM_LOGW(TAG, "Master write to slave error, IO not connected....%s \n", esp_err_to_name(res));
		}

		// Free the Semaphore.
		SemaphoreGiveMutex(print_mux);
		vTaskDelay((DELAY_2_SEC * (task_idx + 1)) / TICK_RATE_TO_MS);
	}

}

/********************************************************************************************************************/

void i2c_master_slave_init() {
	EM_LOGI(TAG, "In %s", __func__);

	EM_LOGI(TAG, "Accessing your thing from thing.h in thing directory ...");
	EM_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* Do all necessary initializations here */

	// initialize the I2C master port
	master_drv = init_i2c_core_master(I2C_CORE_NUM_1, MASTER_SDA_GPIO, MASTER_SCL_GPIO, MASTER_SCL_FRQ, true);

	// initialize the I2C slave port
	slave_drv = init_i2c_core_slave(I2C_CORE_NUM_0, SLAVE_SDA_GPIO, SLAVE_SCL_GPIO, false, SLAVE_ADDR, true);

	// Create Semaphore
	print_mux = SemaphoreCreateMutex();

	// Create 2task of "i2c_test_task"
	TaskCreate(i2c_test_task, "i2c_test_task_0", 1024 * 2, (void * )0, 10, NULL);
	TaskCreate(i2c_test_task, "i2c_test_task_1", 1024 * 2, (void * )1, 10, NULL);

	EM_LOGI(TAG, "Returning from %s", __func__);
}

void i2c_master_slave_loop() {
	EM_LOGI(TAG, "In your_module_loop");

}
