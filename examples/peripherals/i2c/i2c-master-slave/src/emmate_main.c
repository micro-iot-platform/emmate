
#include "i2c-master-slave/i2c_master_slave.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	i2c_master_slave_init();

	while(1){
		i2c_master_slave_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
