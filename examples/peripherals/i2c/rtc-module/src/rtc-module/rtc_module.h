/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   rtc_module.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * Real Time Clock
 *
 *
 *
 */
#ifndef RTC_MODULE_H_
#define RTC_MODULE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
/**
 * @brief initialize I2C master peripheral
 *    Set SDA pin , SCL pin and clock speed in init_i2c_core_master()
 *    Set Slave address, Command address, data ( which write in slave ) and size of data in i2c_core_master_write_slave()
 *    print the set time
 * @return
 *
 **/
void rtc_module_init();
/**
 * @brief read the data from Real time clock using i2c_core_master_read_slave()
 * print the current time
 * @return
 *
 **/
void rtc_module_loop();

#endif	/* RTC_MODULE_H_ */
