#include "rtc-module/rtc_module.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	rtc_module_init();

	while (1) {
		rtc_module_loop();
		TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
	}
}
