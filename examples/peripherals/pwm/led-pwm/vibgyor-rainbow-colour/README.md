# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

###[For Team Thing v2.2](https://mig.iquesters.com/?s=somthing&p=resources#peripherals)

<img src="res/fritzing/teamthing-rgb-led.png" width="600">

### For ESP32 DevKit-C V4

<img src="res/fritzing/vibgyor-rainbow-colour.png" width="500">

[//]: ![image](fritzing/vibgyor-rainbow-colour.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates the **VIBGYOR RAINBOW COLOUR** using RGB LED with the help of the LED_PWM peripheral of the EmMate Framework.

#### Example specific configurations
This example is configured ..

<img src="res/led-pwm-example-config.png" width="500">

This example does the following:

- Show **VIBGYOR RAINBOW COLOUR** transition by RGB LED. RGB Led's Red, Green, Blue GPIO must be configured properly in `src/thing/thing.h`
- Gradient Transition of the **VIBGYOR RAINBOW COLOUR** (VIBGYOR is an acronym for Violet, Indigo, Blue, Green, Yellow, Orange and Red)
- Prints a log message every 3 second.