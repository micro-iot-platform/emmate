
#include "vibgyor-rainbow-colour/vibgyor_rainbow_colour.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling vibgyor_rainbow_colour_init() in your_module.c in your-module directory ...");
	vibgyor_rainbow_colour_init();
	EM_LOGI(TAG, "Returned from vibgyor_rainbow_colour_init()");

	while(1){
		vibgyor_rainbow_colour_loop();
		TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);
	}
}
