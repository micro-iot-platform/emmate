/**
 * This is an example header file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#ifndef LED_FADE_DUTY_H_
#define LED_FADE_DUTY_H_

#include "emmate.h"
#include "thing.h"

/**
 *
 * */
void led_pwm_init();

/**
 * */
void led_pwm_loop();

#endif	/* LED_FADE_DUTY_H_ */
