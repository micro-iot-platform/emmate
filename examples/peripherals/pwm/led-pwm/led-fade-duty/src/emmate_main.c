
#include "led-fade-duty/led_fade_duty.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling led_pwm_init() in led_pwm.c in led_pwm directory ...");
	led_pwm_init();
	EM_LOGI(TAG, "Returned from led_pwm_init()");

	while(1){
		led_pwm_loop();
		TaskDelay(DELAY_20_MSEC/ TICK_RATE_TO_MS);
	}
}
