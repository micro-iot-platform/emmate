
#include "led-fade-api-ex/led-fade-api-ex.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling led_fade_pwm_init() in your_module.c in your-module directory ...");
	led_fade_pwm_init();
	EM_LOGI(TAG, "Returned from led_fade_pwm_init()");

	while(1){
		led_fade_pwm_loop();
		TaskDelay(DELAY_10_MSEC/ TICK_RATE_TO_MS);
	}
}
