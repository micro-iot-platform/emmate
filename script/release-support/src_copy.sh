#!/bin/bash

echo
echo "#####################################################################"

# setting the extglob
shopt -s extglob

echo

release_pkg_prefix="emmate-v"

echo Reading version conf file...
export VER_CONF_FILE=$SRC_DIR/version/core_version.conf

#reading the existing version numbers from conf file
source $VER_CONF_FILE

source $CONFIG_PATH

# checking requested platforms
echo Checking requested platforms
echo PLATFORM = $PLATFORM

platform_count=0

if [ ! -z "$PLATFORM" ]
then
	platform_count=${#PLATFORM[@]}
	echo Number of requested platforms = $platform_count
else
	echo No platforms were requested
fi

if [ $platform_count -le 0 ]
then
	echo Setting all platforms
	cd $SRC_DIR/platform
	echo */
	PLATFORM=(*/)
	echo PLATFORM = ${PLATFORM[@]}
	platform_count=${#PLATFORM[@]}
	echo Generating release for $platform_count platforms
fi

if [ $platform_count -gt 0 ]
then
	for ((i=0; i<$platform_count; i++))
	do
		echo
#		echo .....................................................................
		echo "#####################################################################"

		THIS_PLATFORM=${PLATFORM[$i],,}
		echo Generating release package for $THIS_PLATFORM...
		
		#checking the availability of the mentioned platform
		#goto platform source directory ie, src/platform/this_platform
		PLATFORM_SRC_DIR=$SRC_DIR/platform/$THIS_PLATFORM
		{
			cd $PLATFORM_SRC_DIR
		} || {
			echo platform src directory NOT FOUND for $THIS_PLATFORM
			echo $PLATFORM_SRC_DIR NOT FOUND
			continue
		}
		echo
		echo
		
		################################################################################################
		
		release_pkg=$release_pkg_prefix$CORE_VERSION_MAJOR.$CORE_VERSION_MINOR-$THIS_PLATFORM
		
		DIST_PLATFORM_DIR=$ACTUAL_DIST_DIR/$release_pkg
		echo "src_copy: DIST_PLATFORM_DIR = $DIST_PLATFORM_DIR"
		
		#create the distribution directory
		mkdir -p $DIST_PLATFORM_DIR
		
		# First copying the root release-root files
		echo Copying Release PKG Root Root Additional files from $PROJ_DIR/res/release-root to $DIST_PLATFORM_DIR
		cp -v -u -r $PROJ_DIR/res/release-root/* $DIST_PLATFORM_DIR

		#platform src directory found
		echo $PLATFORM_SRC_DIR FOUND
		################################################################################################
		
		#copying the core src files
		DIST_SRC_DIR=$DIST_PLATFORM_DIR/src

		#creating the distribution src directory
		mkdir -p $DIST_SRC_DIR

		#goto source directory
		cd $SRC_DIR
		
		echo Copying src files from $SRC_DIR to $DIST_SRC_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			if [[ $OS_TYPE == "Linux" ]]
			then
				rsync -av -u -r . $DIST_SRC_DIR --exclude platform --exclude core_config.h
				#cp -v -u -r !(platform) $DIST_SRC_DIR
			elif [[ $OS_TYPE == "Windows" ]]
			then
				#read -p "Press [Enter] key to continue"
				cp -v -u -r `ls -A | grep -v "core_config.h\|platform"` $DIST_SRC_DIR 
				
				# failed to exclude core_config.h while copying.
				#so, at the end of copy delete that core_config.h
				rm -f -v $DIST_SRC_DIR/emmate_config/core_config.h
			fi
			echo $OS_TYPE $CURRENT_DATE $CURRENT_TIME
			#echo hello...
		else
			if [[ $OS_TYPE == "Linux" ]]
			then
				rsync -av -r . $DIST_SRC_DIR --exclude platform --exclude core_config.h
				#cp -v -r !(platform) $DIST_SRC_DIR
			elif [[ $OS_TYPE == "Windows" ]]
			then
				cp -v -r `ls -A | grep -v "core_config.h\|platform"` $DIST_SRC_DIR 
				
				# failed to exclude core_config.h while copying.
				#so, at the end of copy delete that core_config.h
				rm -f -v $DIST_SRC_DIR/emmate_config/core_config.h
			fi
		fi

		echo
		echo .....................................................................
		################################################################################################
		
		#copying the platform specific src files
		DIST_PLATFORM_SRC_DIR=$DIST_PLATFORM_DIR/platform

		#creating the distribution platform directory
		mkdir -p $DIST_PLATFORM_SRC_DIR

		cd $PLATFORM_SRC_DIR

		echo Copying platform src files from $PLATFORM_SRC_DIR to $DIST_PLATFORM_SRC_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_PLATFORM_SRC_DIR
		else
			cp -v -r . $DIST_PLATFORM_SRC_DIR
		fi

		echo .....................................................................
		echo
		################################################################################################
		
		#copying the simulator specific src files
		DIST_SIMULATOR_SRC_DIR=$DIST_PLATFORM_DIR/simulator

		#creating the distribution platform directory
		mkdir -p $DIST_SIMULATOR_SRC_DIR
		
		SIMULATOR_SRC_DIR=$SRC_DIR/platform/simulator
		cd $SIMULATOR_SRC_DIR

		echo Copying simulator src files from $SIMULATOR_SRC_DIR to $DIST_SIMULATOR_SRC_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_SIMULATOR_SRC_DIR
		else
			cp -v -r . $DIST_SIMULATOR_SRC_DIR
		fi

		echo .....................................................................
		echo
		################################################################################################
		
		DIST_RESOURCE_FILES_DIR=$DIST_PLATFORM_DIR/resource
		
		#copying the setup related files
		DIST_SETUP_FILES_DIR=$DIST_RESOURCE_FILES_DIR/setup

		#creating the distribution setup directory
		mkdir -p $DIST_SETUP_FILES_DIR

		#goto setup directory
		cd $PROJ_DIR/res/setup

		echo Copying src files from $PROJ_DIR/res/setup to $DIST_RESOURCE_FILES_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_RESOURCE_FILES_DIR
			echo
			echo "Moving \"*.*\" src files except  \"getOSPlatform.py\"  from $DIST_RESOURCE_FILES_DIR to $DIST_SETUP_FILES_DIR"
			mv -f -v `ls $DIST_RESOURCE_FILES_DIR/*.* | grep -v "getOSPlatform.py"` $DIST_SETUP_FILES_DIR
		else
			cp -v -r . $DIST_RESOURCE_FILES_DIR
			echo
			echo "Moving \"*.*\" src files except  \"getOSPlatform.py\"  from $DIST_RESOURCE_FILES_DIR to $DIST_SETUP_FILES_DIR"
			mv -f -v `ls $DIST_RESOURCE_FILES_DIR/*.* | grep -v "getOSPlatform.py"` $DIST_SETUP_FILES_DIR
		fi

		echo
		echo .....................................................................
		################################################################################################	
		
		echo
		rm -rfv $DIST_RESOURCE_FILES_DIR/files/emmate-docs
		
		echo
		echo Generating platform.conf file in $DIST_SETUP_FILES_DIR
		PLATFORM_CONF_FILE=$DIST_RESOURCE_FILES_DIR/files/emmate-sample-proj/build/build-support/platform.conf
		echo "# This is a Auto-Generated file. DO NOT TOUCH" > $PLATFORM_CONF_FILE
		echo "PLATFORM="$THIS_PLATFORM >> $PLATFORM_CONF_FILE

		echo .....................................................................
		echo
		
		echo .....................................................................
		echo
		################################################################################################		
		
		#copying the platform-resources related files
		#DIST_PLATFORM_RESOURCES_FILES_DIR=$DIST_PLATFORM_DIR/platform-resources
		
		#creating the distribution platform-resources directory
		#mkdir -p $DIST_PLATFORM_RESOURCES_FILES_DIR
		
		#goto platform-resources/platform directory
		cd $PROJ_DIR/res/platform-resources
		
		echo Copying src files from $PROJ_DIR/res/platform-resources to $DIST_RESOURCE_FILES_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u *.* $DIST_RESOURCE_FILES_DIR
		else
			cp -v *.* $DIST_RESOURCE_FILES_DIR
		fi
		
		echo
		echo .....................................................................
		################################################################################################
		
		#copying the examples related files
		DIST_EXAMPLES_FILES_DIR=$DIST_PLATFORM_DIR/examples

		#creating the distribution examples directory
		mkdir -p $DIST_EXAMPLES_FILES_DIR

		#goto examples directory
		cd $PROJ_DIR/examples

		echo Copying src files from $PROJ_DIR/examples to $DIST_EXAMPLES_FILES_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_EXAMPLES_FILES_DIR
		else
			cp -v -r . $DIST_EXAMPLES_FILES_DIR
		fi

		echo .....................................................................
		echo
		################################################################################################
		
		#copying the getting started doc files
		DIST_GETTING_STARTED_DOC_DIR=$DIST_PLATFORM_DIR/getting-started

		#creating the distribution's getting started doc directory
		mkdir -p $DIST_GETTING_STARTED_DOC_DIR
		
		#goto doc/getting-started directory
		cd $PROJ_DIR/doc/getting-started
		
		echo Copying getting-started files from $PROJ_DIR/doc/getting-started to $DIST_GETTING_STARTED_DOC_DIR
		cp -v -u -r . $DIST_GETTING_STARTED_DOC_DIR
		
	done
fi

echo "#####################################################################"
echo
