#!/bin/bash

echo
echo "#####################################################################"

# setting the extglob
shopt -s extglob

echo

release_pkg_prefix="emmate-v"

echo Reading version conf file...
export VER_CONF_FILE=$SRC_DIR/version/core_version.conf

#reading the existing version numbers from conf file
source $VER_CONF_FILE

source $CONFIG_PATH

# checking requested platforms
echo Checking requested platforms
echo PLATFORM = $PLATFORM

platform_count=0

if [ ! -z "$PLATFORM" ]
then
	platform_count=${#PLATFORM[@]}
	echo Number of requested platforms = $platform_count
else
	echo No platforms were requested
fi

if [ $platform_count -le 0 ]
then
	echo Setting all platforms
	cd $SRC_DIR/platform
	echo */
	PLATFORM=(*/)
	echo PLATFORM = ${PLATFORM[@]}
	platform_count=${#PLATFORM[@]}
	echo Generating release for $platform_count platforms
fi

if [ $platform_count -gt 0 ]
then
	for ((i=0; i<$platform_count; i++))
	do
		echo
#		echo .....................................................................
		echo "#####################################################################"

		THIS_PLATFORM=${PLATFORM[$i],,}
		echo Copying resources for $THIS_PLATFORM...
		
		if [ $THIS_PLATFORM == "esp32" ]
		then
			
			release_pkg=$release_pkg_prefix$CORE_VERSION_MAJOR.$CORE_VERSION_MINOR-$THIS_PLATFORM
		
			DIST_PLATFORM_DIR=$ACTUAL_DIST_DIR/$release_pkg
		
			echo "resource_copy: DIST_PLATFORM_DIR = $DIST_PLATFORM_DIR"
			echo
			
			if [ $BUILD_MODE == "dev" ]
			then
				# Copying Platform SDK file(s)
				echo Copying Platform SDK files from $PLATFORM_SDK_DIR_PATH to $DIST_PLATFORM_DIR/sdk/esp-idf
				mkdir -p $DIST_PLATFORM_DIR/sdk/esp-idf
				cp -v -u -r $PLATFORM_SDK_DIR_PATH/* $DIST_PLATFORM_DIR/sdk/esp-idf
					
			else
				# Copying Platform SDK .zip file(s)
				echo Copying Platform SDK .zip files from $PLATFORM_SDK_ZIP_PATH to $DIST_PLATFORM_DIR/sdk
				mkdir -p $DIST_PLATFORM_DIR/sdk
				cp -v -u -r $PLATFORM_SDK_ZIP_PATH $DIST_PLATFORM_DIR/sdk/
				
				echo 
				mkdir -p $DIST_PLATFORM_DIR/tools
				
				echo
				# Copying Emmate Tool zip/files(s)
				
				######### Copying Python3 zips
				# echo Copying Python3_x64 .zip files from $PYTHON3_x64_TOOLS_ZIP_PATH to $DIST_PLATFORM_DIR/tools
				# cp -v -u -r $PYTHON3_x64_TOOLS_ZIP_PATH $DIST_PLATFORM_DIR/tools/
				
				# echo Copying Python3_x86 .zip files from $PYTHON3_x86_TOOLS_ZIP_PATH to $DIST_PLATFORM_DIR/tools
				# cp -v -u -r $PYTHON3_x86_TOOLS_ZIP_PATH $DIST_PLATFORM_DIR/tools/
				
				######### Copying 7zip files
				mkdir -p $DIST_PLATFORM_DIR/tools/7zip_x64
				echo Copying 7z_x64 files from $UNZIP_7z_x64_TOOL_DIR_PATH/* to $DIST_PLATFORM_DIR/tools/7zip_x64
				cp -v -u -r $UNZIP_7z_x64_TOOL_DIR_PATH/* $DIST_PLATFORM_DIR/tools/7zip_x64
				
				mkdir -p $DIST_PLATFORM_DIR/tools/7zip_x86
				echo Copying 7z_x86 files from $UNZIP_7z_x86_TOOL_DIR_PATH/* to $DIST_PLATFORM_DIR/tools/7zip_x86
				cp -v -u -r $UNZIP_7z_x86_TOOL_DIR_PATH/* $DIST_PLATFORM_DIR/tools/7zip_x86
				
				
				######### Copying MSYS2 Zip file
				echo Copying MSYS2 .zip file from $MSYS2_x86_X64_TOOL_ZIP_PATH to $DIST_PLATFORM_DIR/tools/dist
				mkdir -p $DIST_PLATFORM_DIR/tools/dist
				cp -v -u -r $MSYS2_x86_X64_TOOL_ZIP_PATH $DIST_PLATFORM_DIR/tools/dist
				
			fi
		fi
	done
fi

echo "#####################################################################"
echo
