
import os
import sys
import json
import subprocess
from datetime import datetime
import getpass
import time

# current date and time
now = datetime.now()

username = getpass.getuser()

#global emmate_path 
#emmate_path = ""
emmate_path = os.getenv('EMMATE_PATH')

#emmate_path = sys.argv[1]

tempFilePath = "/home/"+username+"/emmateTemp/"
#stream = os.popen("echo $PWD")
pwd = os.getcwd() 
print(pwd)
#input()

outFileName=pwd+"/testres.csv"
print(outFileName)
#input()


def _clearall():
    os.system("rm -rfv "+tempFilePath)
    
def _readJsonFile():
    print(emmate_path)
    with open(emmate_path+"/examples/examples.json") as fpt:
        global exampleJsonDt
        exampleJsonDt = json.load(fpt)
        #print(exampleJsonDt)


def _executeExample(dictValue, argPath=""):
    exampleListItem = list(dictValue.keys())
    exampleListItem.sort()
    
    for key in exampleListItem:
        #print(key)
        keyDt = dictValue[key]
        #print(type(keyDt))
        argPath=argPath+"/"+key
        if isinstance(keyDt, dict):
            #print(key +" is a Dict\n")
            _executeExample(keyDt, argPath)
            #print(argPath+"...")
            argPath=argPath.replace("/"+key, "")
        elif isinstance(keyDt, list):
            #print(key +" is a list\n")
            #print(argPath)
            for item in keyDt:
                print("####################  Execute "+item)
                path = tempFilePath+"/examples/"+argPath+"/"+item
                print(path)
                
                try:
                     # execute setup script
                     os.chdir(path);
                     print(os.getcwd())
                     os.chdir("./setup")
                     os.system("./setup.sh")
                     
                     
                     
                     # execute build script
                     os.chdir(path);
                     os.chdir("./build")
                     print(os.getcwd())
                     #input()
                     
                     #change the build mode in cli-config
                     #read input file
                     fpt = open("build.conf", "rt")
                     #read file contents to string
                     content = fpt.read() 
                     #replace all occurrences of the required string
                     content = content.replace('gui', 'cli')
                     #close the input file
                     fpt.close()
                     #open the input file in write mode
                     fin = open("build.conf", "wt")
                     #overrite the input file with the resulting data
                     fin.write(content)
                     #close the file
                     fin.close()
        
                     
                     #os.system("./build.sh -make")
                     returncode = os.system("./build.sh -make")
                     
                     now = datetime.now()
                     resDt=""
                     resDt=str(now)+","+item+","+path+","+str(returncode)
                     print(resDt+".....\n\n")
                     #input()
                     with open(outFileName,'at',) as fpt:
                          fpt.write(resDt+"\n")
                          
                     time.sleep(5)
                except:
                    now = datetime.now()
                    resDt=""
                    resDt=str(now)+","+item+","+path+","+str("project not found")
                    print(resDt+".....\n\n")
                    with open(outFileName,'at',) as fpt:
                          fpt.write(resDt+"\n")
                    
                    time.sleep(5)
                    
            argPath=argPath.replace("/"+key, "")
            
        else:
            print(key +" is a None\n")
        
        

    
        
    

def _main():
    print("example test file")
    # print(emmate_path)
    _clearall()
    
    os.system("mkdir -p "+tempFilePath)
    os.system("cp -r -v -u "+emmate_path+"/examples"+" "+tempFilePath)
    _readJsonFile()
    #print("\n\n" + str(exampleJsonDt))
    
    
    with open(outFileName,'wt',) as fpt:
        fpt.write("Timestamp, Example Name, Example Path, Build Status\n")
        
    _executeExample(exampleJsonDt, "")
    
    print("\n\n")
    print("###################################################")
    print("CSV File Path: "+outFileName)
    print("###################################################")
    
    #_clearall()

if __name__ == '__main__':
    _main()
    pass

