/*
 * File Name: conn.c
 * File Path: /emmate/src/conn/conn.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include "conn.h"
#include "conn_platform.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"
//#include "system.h"
#include "system_utils.h"
//#include "event_group_core.h"
#if CONFIG_HAVE_SYSTEM_HMI
#include "system_hmi_led_notification.h"
#endif
#include "tcpip_adapter_core.h"
#include "module_thread_priorities.h"
#include "netifinfo.h"

#if CONFIG_USE_INTERNET_CHECKER
#include "internet_checker.h"
#endif

#include <string.h>

#define TAG	LTAG_CONN

static ConnData _conndata;

//em_err init_network(DeviceConfig *dev_cfg) {
static em_err init_network() {
	em_err ret = EM_FAIL;

	conn_global_init();

#if CONFIG_USE_WIFI
//	ret = init_wifi_sta_mode(&dev_cfg->wifi_cfg);
	ret = init_wifi_sta_mode();
#endif

	return ret;
}

//em_err connect_to_network(DeviceConfig *dev_cfg) {
static em_err connect_to_network() {
	em_err ret = EM_FAIL;
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
	ret = connect_wifi();
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif
	return ret;
}

NETWORK_CONN_STATUS get_network_conn_status() {
#if CONFIG_PLATFORM_SIMULATOR
	/* Check system's network connectivity */
	char ip[50] = {'\0'};
	memset(ip, 0x00, strlen(ip));
	em_err ret = conn_get_public_ip(ip);
	EM_LOGD(TAG, "Public IP: %s", ip);

	if (ret == EM_OK) {
		_conndata.netstat = NETCONNSTAT_CONNECTED;
	} else if (ret == EM_FAIL) {
		_conndata.netstat = NETCONNSTAT_DISCONNECTED;
	}

#else

#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
	WIFI_CONNECTION_STATUS stat = get_wifi_connection_status();
	switch (stat) {
	case WIFI_CONNECTING:
		_conndata.netstat = NETCONNSTAT_CONNECTING;
		break;
	case WIFI_CONNECTED:
		_conndata.netstat = NETCONNSTAT_CONNECTED;
		break;
	case WIFI_NOT_CONNECTED:
		_conndata.netstat = NETCONNSTAT_DISCONNECTED;
		break;
	}
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif

#endif /* CONFIG_PLATFORM_SIMULATOR */

#if CONFIG_USE_INTERNET_CHECKER
	if (_conndata.netstat == NETCONNSTAT_CONNECTED) {
		INTERNET_STATUS internet_stat = get_internet_stat();
		switch (internet_stat) {
		case INTERNET_NOT_AVAILABLE:
			_conndata.netstat = NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_NOT_AVAILABLE
					| NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE;
			break;
		case INTERNET_AVAILABLE:
			_conndata.netstat = NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_AVAILABLE
					| NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE;
			break;
		case INTERNET_CLOUD_AVAILABLE:
			_conndata.netstat =
					NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_AVAILABLE | NETCONNSTAT_IOT_CLOUD_AVAILABLE;
			break;
		default:
			break;
		}
	}
#endif

	return _conndata.netstat;
}

#if CONFIG_PLATFORM_SIMULATOR
static TaskRet internet_stat_thread(void *params) {
	em_err ret = EM_FAIL;
	/* Init platform specific networking */
	init_network();

	show_network_connecting_notification();

	/* Check system's network connectivity */
	char ip[50] = {'\0'};
	do {
		TaskDelay(DELAY_250_MSEC / TICK_RATE_TO_MS);
		memset(ip, 0x00, strlen(ip));
		ret = conn_get_public_ip(ip);
	} while(ret == EM_FAIL);

	event_group_set_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT);

	show_network_connected_notification();

	while (1) {
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}
}

#else

static TaskRet network_connection_thread(void *params) {
//	DeviceConfig *dev_cfg = (DeviceConfig*) params;

	/* 1. Initialize the network */
//	em_err ret = init_network(dev_cfg);
	em_err ret = init_network();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize the network");
		goto kill_task;
	}

	/* 2. Connect to network using configured network interface */
//	ret = connect_to_network(dev_cfg);
	ret = connect_to_network();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to connect to network");
		goto kill_task;
	}

	show_network_connecting_notification();

	EventBits ebits_in = (CONN_CONNECTING_BIT | CONN_GOT_IP_BIT | CONN_GOT_IP_FAILED | INTERNET_AVAILABLE_BIT
			| INTERNET_NOT_AVAILABLE_BIT);
	EventBits ebits_out;

	/* 3. Wait until we get an IP */
	ebits_out = event_group_wait_bits(get_system_evtgrp_hdl(), ebits_in, false, false, EventMaxDelay);

	/* Either we will get an IP or fail to associate with network, so we check for both conditions */
	if ((ebits_out & ebits_in) == CONN_CONNECTING_BIT) {
		EM_LOGD(TAG, "1. Network is connecting");
		event_group_clear_bits(get_system_evtgrp_hdl(), CONN_CONNECTING_BIT);
		show_network_connecting_notification();
	} else if ((ebits_out & ebits_in) == CONN_GOT_IP_BIT) {
		EM_LOGD(TAG, "show_network_connected_notification");
		show_network_connected_notification();

		/* Get the IP from the active interface and print the IP address */
		TcpipAdapterIpInfo_Core ip_info;
		if (is_tcpip_core_netif_up(CORE_TCPIP_ADAPTER_IF_STA)) {
			ret = get_tcpip_ip_info(CORE_TCPIP_ADAPTER_IF_STA, &ip_info);
			if (ret == EM_OK)
				EM_LOGI(TAG, "1. WiFi connected, IP: %s", ip4addr_ntoa(&ip_info.ip));
		} else if (is_tcpip_core_netif_up(CORE_TCPIP_ADAPTER_IF_ETH)) {
			ret = get_tcpip_ip_info(CORE_TCPIP_ADAPTER_IF_ETH, &ip_info);
			if (ret == EM_OK)
				EM_LOGI(TAG, "Eth connected, IP: %s", ip4addr_ntoa(&ip_info.ip));
		}

	} else if ((ebits_out & ebits_in) == CONN_GOT_IP_FAILED) {
		EM_LOGD(TAG, "show_network_notconnected_notification\r\n");
		show_network_connecting_notification();
		/* Network could not connect to specified configurations, so we need to do CONFIG_MODE_L1 again */
		EM_LOGI(TAG, "1. Could not get IP from network!\r\n");
	}

	while (1) {
		ebits_out = event_group_wait_bits(get_system_evtgrp_hdl(), ebits_in, false, false,
				EventMaxDelay/TICK_RATE_TO_MS);

		/* Either we will get an IP or fail to associate with network, so we check for both conditions */
		if ((ebits_out & ebits_in) == CONN_CONNECTING_BIT) {
			EM_LOGD(TAG, "2. Network is connecting");
			//TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			event_group_clear_bits(get_system_evtgrp_hdl(), CONN_CONNECTING_BIT);
		} else if ((ebits_out & ebits_in) == CONN_GOT_IP_BIT) {
			EM_LOGD(TAG, "2. Network is connected");
			show_network_connected_notification();
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			//event_group_clear_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT);
		} else if ((ebits_out & ebits_in) == CONN_GOT_IP_FAILED) {
			EM_LOGD(TAG, "2. Network connection failed!");
			show_network_connecting_notification();
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			event_group_clear_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_FAILED);
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
			/* Try to reconnect the wifi */
			reconnect_wifi();
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif
		}

#if CONFIG_USE_INTERNET_CHECKER
		else if ((ebits_out & ebits_in) == INTERNET_AVAILABLE_BIT) {
			EM_LOGD(TAG, "3. Internet Available !");
			if (get_internet_stat() == INTERNET_AVAILABLE) {
				show_internet_available_notification();
			} else if (get_internet_stat() == INTERNET_CLOUD_AVAILABLE) {
				show_internet_iot_cloud_available_notification();
			}

//			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			event_group_clear_bits(get_system_evtgrp_hdl(), INTERNET_AVAILABLE_BIT);

		} else if ((ebits_out & ebits_in) == INTERNET_NOT_AVAILABLE_BIT) {
			event_group_clear_bits(get_system_evtgrp_hdl(), INTERNET_NOT_AVAILABLE_BIT);
			EM_LOGD(TAG, "3. Internet Not Available !");
			show_internet_not_available_notification();
		}
#endif
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}

	kill_task:
//	if (ret == EM_OK)
//		show_network_connected_notification();
//	else
//		show_network_notconnected_notification();

#if defined (CONFIG_PLATFORM_ESP_IDF)
	/* Kill the task */
	TaskDelete(NULL);
#endif
}
#endif	/* CONFIG_PLATFORM_SIMULATOR */

//em_err start_network_connection(DeviceConfig *dev_cfg) {
em_err start_network_connection() {
	em_err ret = EM_FAIL;

	_conndata.netstat = NETCONNSTAT_DISCONNECTED;
	BaseType thread_stat;

#if CONFIG_PLATFORM_SIMULATOR
	/* In case of the Simulator the network connection is managed by the underlying OS (Windows, Linux, MAC)
	 * So here we create a thread only to show the Internet availability statuses
	 * */
	thread_stat = TaskCreate(internet_stat_thread, "net-conn", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_CONN,
			NULL);
#else
	/* Create a thread to perform network connection */
	thread_stat = TaskCreate(network_connection_thread, "net-conn", TASK_STACK_SIZE_3K, NULL, THREAD_PRIORITY_CONN,
			NULL);
#endif	/* CONFIG_PLATFORM_SIMULATOR */
	if (thread_stat == false) {
		EM_LOGE(TAG, "Failed to create network connection thread!");
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}

	return ret;
}

em_err conn_prerequisite_setup() {

	/* Initialize NetIF Info Receive */
	netifinfo_init();

#if CONFIG_USE_INTERNET_CHECKER

	/* Initialize Ping Task */
	init_internet_checker();

#endif

	return EM_OK;
}
