/*
 * File Name: http_core.c
 * File Path: /emmate/src/conn-proto/http-client/http_client_core.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include "http_client_core.h"
#include "http_constant.h"
#include "conn.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"

#include <string.h>
#include <stdlib.h>
#include "gll.h"

#define TAG LTAG_CONNPROTO_HTTPCLI

static http_res_recv_handle_cb http_recv_handler;
static gll_t *http_ctrl_list = NULL;
static SemaphoreMutexHandle http_cli_mutex = NULL;

static HttpCtrl *find_http_ctrl_node(HttpClientHandle client, size_t *node_pos) {
	HttpCtrl *http_ctrl = NULL;
	EM_LOGV(TAG, "find_http_ctrl_node");
	if (http_ctrl_list != NULL) {
		size_t list_size = http_ctrl_list->size;
		EM_LOGD(TAG, "List Size = %d", list_size);

		size_t count = 0;
		bool search_flag = false;

		/* Adding to Linked List */
		SemaphoreTakeMutex(http_cli_mutex, THREADING_MAX_DELAY);

		for (count = 0; count < list_size; count++) {
			http_ctrl = (HttpCtrl*) gll_get(http_ctrl_list, count);
			if (http_ctrl->client == client) {
				*node_pos = count;
				search_flag = true;
				break;
			} else {
				EM_LOGD(TAG, "http_ctrl->client != evt->client");
			}
		}

		SemaphoreGiveMutex(http_cli_mutex);

		if (search_flag)
			return http_ctrl;
	}
	*node_pos = -1;
	return NULL;
}

static em_err http_event_handler(HttpClientEvent *evt) {
	EM_LOGV(TAG, "http_event_handler entry");
	size_t node_pos = -1;
//	HttpCtrl *http_ctrl = find_http_ctrl_node(evt->client, &node_pos);
	switch (evt->event_id) {
	case HTTP_CLIENT_EVENT_ERROR: {
		EM_LOGV(TAG, "HTTP_CLIENT_EVENT_ERROR, Node: %d", node_pos);
		break;
	}
	case HTTP_CLIENT_EVENT_ON_CONNECTED: {
		EM_LOGV(TAG, "HTTP_CLIENT_EVENT_ON_CONNECTED, Node: %d", node_pos);
		break;
	}
	case HTTP_CLIENT_EVENT_HEADER_SENT: {
		EM_LOGV(TAG, "HTTP_CLIENT_EVENT_HEADER_SENT, Node: %d", node_pos);
		break;
	}
	case HTTP_CLIENT_EVENT_ON_HEADER: {
		EM_LOGV(TAG, "HTTP_CLIENT_EVENT_ON_HEADER, Node: %d", node_pos);
		break;
	}
	case HTTP_CLIENT_EVENT_ON_DATA: {
		HttpCtrl *http_ctrl = find_http_ctrl_node(evt->client, &node_pos);
		if (http_ctrl != NULL) {
			uint16_t http_stat = http_client_get_status_code(http_ctrl->client);
			EM_LOGD(TAG, "HTTP_CLIENT_EVENT_ON_DATA, Node: %d, len: %d, HTTP Stat: %d", node_pos, evt->data_len,
					http_stat);
			if (http_stat == HTTP_OK_RES_CODE) {
				if (http_ctrl->client_api_mode == _API_MODE_SINGLE_STEP) {
					if ((http_ctrl->response_data != NULL)) {
						int resp_size = *http_ctrl->response_len + evt->data_len;
						if (resp_size <= http_ctrl->max_response_len) {
							memcpy((http_ctrl->response_data + *http_ctrl->response_len), evt->data, evt->data_len);
							*http_ctrl->response_len += evt->data_len;
						} else {
							EM_LOGE(TAG, "Max bytes reached!");
						}
					} else {
						EM_LOGD(TAG, "response_data is NULL");
					}
				} else if (http_ctrl->client_api_mode == _API_MODE_MULTI_STEP) {
					if (http_ctrl->http_recv_handler != NULL)
						http_ctrl->http_recv_handler(evt->event_id, (char*) evt->data, evt->data_len);
				}
			} else {
				EM_LOGD(TAG, "HTTP_CLIENT_EVENT_ON_DATA, Node: %d, HTTP Stat: %d", node_pos, http_stat);
			}
		} else {
			EM_LOGD(TAG, "http_ctrl = NULL");
		}
		break;
	}
	case HTTP_CLIENT_EVENT_ON_FINISH: {
		EM_LOGD(TAG, "HTTP_CLIENT_EVENT_ON_FINISH, Node: %d", node_pos);
		HttpCtrl *http_ctrl = find_http_ctrl_node(evt->client, &node_pos);

		if (http_ctrl != NULL) {
			if (http_ctrl->client_api_mode == _API_MODE_SINGLE_STEP) {
				http_ctrl->event_stat = HTTP_CLIENT_EVENT_ON_FINISH;
			} else if (http_ctrl->client_api_mode == _API_MODE_MULTI_STEP) {
				http_ctrl->http_recv_handler(evt->event_id, (char*) NULL, 0);
			}
		}
		break;
	}
	case HTTP_CLIENT_EVENT_DISCONNECTED: {
		EM_LOGD(TAG, "HTTP_CLIENT_EVENT_DISCONNECTED");
		break;
	}
	}
	EM_LOGV(TAG, "http_event_handler exit");
	return EM_OK;
}

em_err init_http_client(HttpClientData *http_cli_data) {
	em_err ret = EM_FAIL;

#if CONFIG_USE_WIFI
	if (get_wifi_connection_status() != WIFI_CONNECTED) {
		return EM_ERR_INVALID_STATE;
	}
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif

	if (http_ctrl_list == NULL) {
		http_ctrl_list = gll_init();
	}

	if (http_cli_mutex == NULL) {
		http_cli_mutex = SemaphoreCreateMutex();
	}

	HttpClientConfig http_cli_config;
	bzero(&http_cli_config, sizeof(HttpClientConfig));

	/* Set Configure HTTP Client's URL */
	if (http_cli_data->http_cnf_data.url == NULL) {
		EM_LOGE(TAG, "HttpClientData->url == null");
		return ret;
	}
	http_cli_config.url = http_cli_data->http_cnf_data.url;

	/* Set Configure HTTP Client's Certificate */
	if (http_cli_data->http_cnf_data.cert_pem == NULL) {
		EM_LOGD(TAG, "HttpClientData->cert_pem == null");
	}
	http_cli_config.cert_pem = http_cli_data->http_cnf_data.cert_pem;

	/* Set Configure HTTP Client Event Handler */
	http_cli_config.event_handler = http_event_handler;

	/* Set Configure HTTP Client's Authentication Data */
//	http_cli_cfg.username = http_cli_data->username;
//	http_cli_cfg.password = http_cli_data->password;

	/* Set Configure HTTP Socket's Port */
	http_cli_config.port = http_cli_data->http_cnf_data.port;

	/* Set Configure HTTP Client's Server Authentication Certificates */
//	http_cli_cfg.client_cert_pem = http_cli_data->client_cert_pem;
//	http_cli_cfg.client_key_pem = http_cli_data->client_key_pem;

	/* Set Configure HTTP Client's Server Connection Timeout */
//	http_cli_cfg.timeout_ms = http_cli_data->timeout_ms;

	http_cli_data->http_ctrl.client = http_client_init(&http_cli_config);

	if (http_cli_data->http_ctrl.client != NULL) {
		/* Adding to Linked List */
		SemaphoreTakeMutex(http_cli_mutex, THREADING_MAX_DELAY);

		if (gll_add(http_ctrl_list, &http_cli_data->http_ctrl, 0) == C_OK) {
			ret = config_http_client(http_cli_data);
		}
		SemaphoreGiveMutex(http_cli_mutex);

	} else {
		EM_LOGE(TAG, "HttpClientHandle is NULL!");
		ret = EM_FAIL;
	}

	return ret;
}

em_err config_http_client(HttpClientData *http_cli_data) {
	em_err ret = EM_FAIL;

	/* set the Client WEB Host URL */
	if (http_cli_data->http_cnf_data.url != NULL) {
		ret = http_client_set_url(http_cli_data->http_ctrl.client, http_cli_data->http_cnf_data.url);
		if (ret != EM_OK) {
			return ret;
		}
	} else {
		return EM_FAIL;
	}

	/* set the HTTP Method */
	ret = http_client_set_method(http_cli_data->http_ctrl.client, http_cli_data->http_cnf_data.method);
	if (ret != EM_OK) {
		return ret;
	}
#if CONFIG_PLATFORM_SIMULATOR
	/* set the HTTP Client Content Type */
	if (http_cli_data->http_cnf_data.content_type != NULL) {
		ret = http_client_set_header(http_cli_data->http_ctrl.client, "Content-Type",
				http_cli_data->http_cnf_data.content_type);
		if (ret != EM_OK) {
			return ret;
		}
	} else {
		// TODO: Nothing to do.
	}
#else
	/* set the HTTP Header */
	if (http_cli_data->http_cnf_data.host_server != NULL) {
		ret = http_client_set_header(http_cli_data->http_ctrl.client, "Host", http_cli_data->http_cnf_data.host_server);
		if (ret != EM_OK) {
			return ret;
		}
	} else {
		// TODO: Nothing to do.
	}

	/* set the HTTP Client User agent */
	if (http_cli_data->http_cnf_data.user_agent != NULL) {
		ret = http_client_set_header(http_cli_data->http_ctrl.client, "User-Agent",
				http_cli_data->http_cnf_data.user_agent);
		if (ret != EM_OK) {
			return ret;
		}
	} else {
		// TODO: Nothing to do.
	}

	/* set the HTTP Client Content Type */
	if (http_cli_data->http_cnf_data.content_type != NULL) {
		ret = http_client_set_header(http_cli_data->http_ctrl.client, "Content-Type",
				http_cli_data->http_cnf_data.content_type);
		if (ret != EM_OK) {
			return ret;
		}
	} else {
		// TODO: Nothing to do.
	}
#endif
	/* set the HTTP Client Post Data */
	if (http_cli_data->http_cnf_data.method == HTTP_CLIENT_METHOD_POST) {
		ret = http_client_set_post_field(http_cli_data->http_ctrl.client, http_cli_data->http_cnf_data.post_data,
				http_cli_data->http_cnf_data.post_data_len);
		if (ret != EM_OK) {
			return ret;
		}
	} else {
		// TODO: Nothing to do.
	}

	return ret;
}

em_err perform_http_client_process(HttpClientData *http_cli_data) {
	em_err ret = EM_FAIL;
	EM_LOGV(TAG, "perform_http_client_process entry");
	if (http_cli_data->http_ctrl.client == NULL) {
		EM_LOGE(TAG, "Client handle is NULL");
	} else {
		EM_LOGD(TAG, "Client handle is OK");
		ret = http_client_perform(http_cli_data->http_ctrl.client);
	}
	EM_LOGV(TAG, "perform_http_client_process exit");
	return ret;
}

uint16_t get_http_status_code(HttpClientData *http_cli_data) {
	return http_client_get_status_code(http_cli_data->http_ctrl.client);
}

size_t get_http_client_response(HttpClientData *http_cli_data, char* res_buff, size_t len) {
	size_t ret = -1;
	ret = http_client_read(http_cli_data->http_ctrl.client, res_buff, len);
	return ret;
}

uint16_t get_http_client_event_stat(HttpClientData *http_cli_data) {
	size_t node_pos = -1;
	HttpCtrl *http_ctrl = find_http_ctrl_node(http_cli_data->http_ctrl.client, &node_pos);

	if (http_ctrl != NULL) {
		return http_ctrl->event_stat;
	} else {
		return 0;
	}
}

em_err deinit_http_client(HttpClientData *http_cli_data) {
	size_t node_pos = -1;
	EM_LOGV(TAG, "deinit_http_client entry");
	HttpCtrl *http_ctrl = find_http_ctrl_node(http_cli_data->http_ctrl.client, &node_pos);
	if (http_ctrl != NULL && node_pos != -1) {
		/* Removing from Linked List */
		SemaphoreTakeMutex(http_cli_mutex, THREADING_MAX_DELAY);
		gll_remove(http_ctrl_list, node_pos);
		SemaphoreGiveMutex(http_cli_mutex);
	} else {
		EM_LOGD(TAG, "http_ctrl->client != evt->client");
	}

	em_err ret = http_client_cleanup(http_cli_data->http_ctrl.client);
	http_recv_handler = NULL;
	EM_LOGV(TAG, "deinit_http_client exit");
	return ret;
}
