/*
 * File Name: http_constant.h
 * File Path: /emmate/src/conn-proto/http-client/http_constant.h
 * Description:
 *
 *  Created on: 02-May-2019
 *      Author: Noyel Seth
 */

#ifndef HTTP_CONSTANT_H_
#define HTTP_CLIENT_HTTP_CONSTANT_H_


/**
 * HTTP Content Types
 */
#if CONFIG_PLATFORM_SIMULATOR
#define CONTENT_TYPE_APPLICATION_JSON 	"Content-Type: application/json"
#else
#define CONTENT_TYPE_APPLICATION_JSON 	"application/json"
//Add when needed
#endif

/**
 * HTTP User agent
 */
#define HTTP_USER_AGENT		"EmMate/2.0"

/**
 * HTTP Status Codes
 */
/** 1xx Informational response **/
#define HTTP_CONTINUE_RES_CODE					100			/**< Continue Response */
#define HTTP_SWITCH_PROTOCOLS_RES_CODE			101			/**< Switch Protocol Response */
#define HTTP_PROCESSING_RES_CODE				102			/**< Processing Response */
#define HTTP_EARLY_HINTS_RES_CODE				103			/**< Early Hints Response */


/** 2xx Informational response **/
#define HTTP_OK_RES_CODE						200			/**< OK / Success Response */
#define HTTP_CREATED_RES_CODE					201			/**< Created Response */
#define HTTP_ACCEPTED_RES_CODE					202			/**< Accepted Response */
#define HTTP_NON_AUTHORITATIVE_RES_CODE			203			/**< Non authoritative Response */
#define HTTP_NO_CONTENT_RES_CODE				204			/**< No Content Response */
#define HTTP_RESET_CONTENT_RES_CODE				205			/**< Reset Content Response */
#define HTTP_PARTIAL_CONTENT_RES_CODE			206			/**< Partial Content Response */
#define HTTP_MULTI_STATUS_RES_CODE				207			/**< Multi status Response */
#define HTTP_ALREADY_REPORTED_RES_CODE			208			/**< Already reported Response */
#define HTTP_IM_USER_RES_CODE					226			/**< IM User Response */


/** 3xx Informational response **/
#define HTTP_MULTIPLE_CHOICES_RES_CODE			300			/**< Multiple Choices Response */
#define HTTP_MOVED_PERMANENTLY_RES_CODE			301			/**< Moved Permanently Response */
#define HTTP_FOUND_RES_CODE						302			/**< Found Response */
#define HTTP_SEE_OTHER_RES_CODE					303			/**< See Other Response */
#define HTTP_NOT_MODIFIED_RES_CODE				304			/**< not Modified Response */
#define HTTP_USE_PROXY_RES_CODE					305			/**< Use Proxy Response */
#define HTTP_SWITCH_PROXY_RES_CODE				306			/**< Switch Proxy Response */
#define HTTP_TEMPORARY_REDIRECT_RES_CODE		307			/**< Temporary Redirect Response */
#define HTTP_PERMANENT_REDIRECT_RES_CODE		308			/**< Permanent Redirect Response */


/** 4xx Informational response **/
#define HTTP_BAD_REQUEST_RES_CODE								400		/**< Bad Request Response */
#define HTTP_UNAUTHORIZED_RES_CODE								401		/**< Unauthorized Response */
#define HTTP_PAYMENT_REQUIRED_RES_CODE							402		/**< Payment Required Response */
#define HTTP_FORBIDDEN_RES_CODE									403		/**< Forbidden Response */
#define HTTP_NOT_FOUND_RES_CODE									404		/**< Not Found Response */
#define HTTP_MATHOD_NOT_ALLOWED_RES_CODE						405		/**< MATHOD_NOT_ALLOWED Response */
#define HTTP_NOT_ACCEPTABLE_RES_CODE							406		/**< Not Acceptable Response */
#define HTTP_PROXY_AUTHENTICATION_REQUIRED_RES_CODE				407		/**< Proxy Authentication Required Response */
#define HTTP_REQUEST_TIMEOUT_RES_CODE							408		/**< Request Timeout Response */
#define HTTP_CONFLICT_RES_CODE									409		/**< conflict Response */
#define HTTP_GONE_RES_CODE										410		/**< Gone Response */
#define HTTP_LENGTH_REQUIRED_RES_CODE							411		/**< Length Required Response */
#define HTTP_PRECONDITION_FAILED_RES_CODE						412		/**< Precondition Failed Response */
#define HTTP_PAYLOAD_TOO_LARGE_RES_CODE							413		/**< Payload Too Large Response */
#define HTTP_URI_TOO_LONG_RES_CODE								414		/**< URI Too Long Response */
#define HTTP_UNSUPPORTED_MEDIA_TYPE_RES_CODE					415		/**< Unsupported Media Type Response */
#define HTTP_RANGE_NOT_SATISFIABLE_RES_CODE						416		/**< Range Not Satisfiable Response */
#define HTTP_EXPECTATION_FAILED_RES_CODE						417		/**< Expectation Failed Response */
#define HTTP_IM_A_TEAPOT_RES_CODE								418		/**< IM A Teapot Response */
#define HTTP_MISDIRECTED_REQUEST_RES_CODE						421		/**< Misdirected Request Response */
#define HTTP_UNPROCESSABLE_ENTITY_RES_CODE						422		/**< Unprocessable Entity Response */
#define HTTP_LOCKED_RES_CODE									423		/**< Locked Response */
#define HTTP_FAILED_DEPENDENCY_RES_CODE							424		/**< Failed Dependency Response */
#define HTTP_TOO_EARLY_RES_CODE									425		/**< Too Early Response */
#define HTTP_UPGRADE_REQUIRED_RES_CODE							426		/**< Upgrade Required Response */
#define HTTP_PRECONDITION_REQUIRED_RES_CODE						428		/**< Precondition Required Response */
#define HTTP_TOO_MANY_REQUESTS_RES_CODE							429		/**< Too Many Requests Response */
#define HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE_RES_CODE			431		/**< Request Header Fields Too Large Response */
#define HTTP_UNAVAILABLE_FOR_LEGAL_REASONS_RES_CODE				451		/**< Unavailable For Legal Reasons Response */


/** 5xx Informational response **/
#define HTTP_INTERNAL_SERVER_ERROR_RES_CODE						500		/**< Internal Server Error Response */
#define HTTP_NOT_IMPLEMENTED_RES_CODE							501		/**< Not Implemented Response */
#define HTTP_BAD_GATEWAY_RES_CODE								502		/**< BAD Gateway Response */
#define HTTP_SERVICE_UNAVAILABLE_RES_CODE						503		/**< Service Unavailable Response */
#define HTTP_GATEWAY_TIMEOUT_RES_CODE							504		/**< Gateway Timeout Response */
#define HTTP_HTTP_VERSION_NOT_SUPPORTED_RES_CODE				505		/**< HTTP Version Not Supported Response */
#define HTTP_VARIANT_ALSO_NEGOTIATE_RES_CODE					506		/**< Variant Also Negotiate Response */
#define HTTP_INSUFFICIENT_STORAGE_RES_CODE						507		/**< Insufficient Storage Response */
#define HTTP_LOOP_DETECTED_RES_CODE								508		/**< Loop Detected Response */
#define HTTP_NOT_EXTENDED_RES_CODE								510		/**< Not Extended Response */
#define HTTP_NETWORK_AUTHENTICATION_REQUIRED_RES_CODE			511		/**< Network Authentication Required Response */


#endif /* HTTP_CONSTANT_H_ */
