/*
 * File Name: http_client_core.h
 * File Path: /emmate/src/conn-proto/http-client/http_client_core.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef HTTP_CORE_H_
#define HTTP_CORE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "http_client_platform.h"

#define HTTP_CLIENT_ERR_HTTP_BASE				HTTP_CLIENT_PF_ERR_HTTP_BASE				/*!< Starting number of HTTP error codes */
#define HTTP_CLIENT_ERR_HTTP_MAX_REDIRECT		HTTP_CLIENT_PF_ERR_HTTP_MAX_REDIRECT		/*!< The error exceeds the number of HTTP redirects */
#define HTTP_CLIENT_ERR_HTTP_CONNECT			HTTP_CLIENT_PF_ERR_HTTP_CONNECT				/*!< Error open the HTTP connection */
#define HTTP_CLIENT_ERR_HTTP_WRITE_DATA			HTTP_CLIENT_PF_ERR_HTTP_WRITE_DATA			/*!< Error write HTTP data */
#define HTTP_CLIENT_ERR_HTTP_FETCH_HEADER		HTTP_CLIENT_PF_ERR_HTTP_FETCH_HEADER		/*!< Error read HTTP header from server */
#define HTTP_CLIENT_ERR_HTTP_INVALID_TRANSPORT	HTTP_CLIENT_PF_ERR_HTTP_INVALID_TRANSPORT	/*!< There are no transport support for the input scheme */
#define HTTP_CLIENT_ERR_HTTP_CONNECTING			HTTP_CLIENT_PF_ERR_HTTP_CONNECTING			/*!< HTTP connection hasn't been established yet */
#define HTTP_CLIENT_ERR_HTTP_EAGAIN				HTTP_CLIENT_PF_ERR_HTTP_EAGAIN				/*!< Mapping of errno EAGAIN to esp_err_t */

/**
 * @brief   HTTP Client events id
 */
typedef enum {
	HTTP_CLIENT_EVENT_ERROR = HTTP_CLIENT_PF_EVENT_ERROR, /*!< This event occurs when there are any errors during execution */
	HTTP_CLIENT_EVENT_ON_CONNECTED = HTTP_CLIENT_PF_EVENT_ON_CONNECTED, /*!< Once the HTTP has been connected to the server, no data exchange has been performed */
	HTTP_CLIENT_EVENT_HEADER_SENT = HTTP_CLIENT_PF_EVENT_HEADER_SENT, /*!< After sending all the headers to the server */
	HTTP_CLIENT_EVENT_ON_HEADER = HTTP_CLIENT_PF_EVENT_ON_HEADER, /*!< Occurs when receiving each header sent from the server */
	HTTP_CLIENT_EVENT_ON_DATA = HTTP_CLIENT_PF_EVENT_ON_DATA, /*!< Occurs when receiving data from the server, possibly multiple portions of the packet */
	HTTP_CLIENT_EVENT_ON_FINISH = HTTP_CLIENT_PF_EVENT_ON_FINISH, /*!< Occurs when finish a HTTP session */
	HTTP_CLIENT_EVENT_DISCONNECTED = HTTP_CLIENT_PF_EVENT_DISCONNECTED, /*!< The connection has been disconnected */
} HTTP_CLIENT_EVENT_ID;

/**
 * @brief HTTP method
 */
typedef enum {
	HTTP_CLIENT_METHOD_GET = HTTP_CLIENT_PF_METHOD_GET, /*!< HTTP GET Method */
	HTTP_CLIENT_METHOD_POST = HTTP_CLIENT_PF_METHOD_POST, /*!< HTTP POST Method */
	HTTP_CLIENT_METHOD_PUT = HTTP_CLIENT_PF_METHOD_PUT, /*!< HTTP PUT Method */
	HTTP_CLIENT_METHOD_PATCH = HTTP_CLIENT_PF_METHOD_PATCH, /*!< HTTP PATCH Method */
	HTTP_CLIENT_METHOD_DELETE = HTTP_CLIENT_PF_METHOD_DELETE, /*!< HTTP DELETE Method */
	HTTP_CLIENT_METHOD_HEAD = HTTP_CLIENT_PF_METHOD_HEAD, /*!< HTTP HEAD Method */
	HTTP_CLIENT_METHOD_NOTIFY = HTTP_CLIENT_PF_METHOD_NOTIFY, /*!< HTTP NOTIFY Method */
	HTTP_CLIENT_METHOD_SUBSCRIBE = HTTP_CLIENT_PF_METHOD_SUBSCRIBE, /*!< HTTP SUBSCRIBE Method */
	HTTP_CLIENT_METHOD_UNSUBSCRIBE = HTTP_CLIENT_PF_METHOD_UNSUBSCRIBE,/*!< HTTP UNSUBSCRIBE Method */
	HTTP_CLIENT_METHOD_OPTIONS = HTTP_CLIENT_PF_METHOD_OPTIONS, /*!< HTTP OPTIONS Method */
	HTTP_CLIENT_METHOD_MAX = HTTP_CLIENT_PF_METHOD_MAX,
} HTTP_CLIENT_METHOD;

/* Wrapper typedefs for abstracting the platform specific names */

/**
 * @brief HTTP Client Config
 **/
typedef HttpClientPfConfig HttpClientConfig;

/**
 * @brief HTTP Client Handle
 **/
typedef HttpClientPfHandle HttpClientHandle;

/**
 * @brief HTTP Client Methods
 **/
typedef HttpClientPfMethod HttpClientMethod;

/**
 * @brief HTTP Client EventHandle
 **/
typedef HttpClientPfEvent HttpClientEvent;

/* Wrapper functions for abstracting the platform specific names */
/**
 * @brief      Start a HTTP session
 *             This function must be the first function to call,
 *             and it returns a HttpClientHandle that you must use as input to other functions in the interface.
 *             This call MUST have a corresponding call to esp_http_client_cleanup when the operation is complete.
 *
 * @param[in]  config   The configurations, see `HttpClientConfig`
 *
 * @return
 *     - `HttpClientHandle`
 *     - NULL if any errors
 */
#define http_client_init(config)						http_client_pf_init(config)

/**
 * @brief      Set URL for client, when performing this behavior, the options in the URL will replace the old ones
 *
 * @param[in]  client  The esp_http_client handle
 * @param[in]  url     The url
 *
 * @return
 *  - EM_OK
 *  - EM_FAIL
 */
#define http_client_set_url(client, url)				http_client_pf_set_url(client, url)

/**
 * @brief      Set http request method
 *
 * @param[in]  client  The HttpClientHandle
 * @param[in]  method  The method
 *
 * @return     EM_OK
 */
#define http_client_set_method(client, method)			http_client_pf_set_method(client, method)

/**
 * @brief      Set http request header, this function must be called after http_client_init and before any
 *             perform function
 *
 * @param[in]  client  The HttpClientHandle
 * @param[in]  key     The header key
 * @param[in]  value   The header value
 *
 * @return
 *  - EM_OK
 *  - EM_FAIL
 */
#define http_client_set_header(client, key, value)		http_client_pf_set_header(client, key, value)

/**
 * @brief      Set post data, this function must be called before `http_client_perform`.
 *             Note: The data parameter passed to this function is a pointer and this function will not copy the data
 *
 * @param[in]  client  The HttpClientHandle
 * @param[in]  data    post data pointer
 * @param[in]  len     post length
 *
 * @return
 *  - EM_OK
 *  - EM_FAIL
 */
#define http_client_set_post_field(client, data, len)	http_client_pf_set_post_field(client, data, len)

/**
 * @brief      Invoke this function after `http_client_init` and all the options calls are made, and will perform the
 *             transfer as described in the options. It must be called with the same esp_http_client_handle_t as input as the esp_http_client_init call returned.
 *             esp_http_client_perform performs the entire request in either blocking or non-blocking manner. By default, the API performs request in a blocking manner and returns when done,
 *             or if it failed, and in non-blocking manner, it returns if EAGAIN/EWOULDBLOCK or EINPROGRESS is encountered, or if it failed. And in case of non-blocking request,
 *             the user may call this API multiple times unless request & response is complete or there is a failure. To enable non-blocking esp_http_client_perform(), `is_async` member of esp_http_client_config_t
 *             must be set while making a call to esp_http_client_init() API.
 *             You can do any amount of calls to esp_http_client_perform while using the same esp_http_client_handle_t. The underlying connection may be kept open if the server allows it.
 *             If you intend to transfer more than one file, you are even encouraged to do so.
 *             esp_http_client will then attempt to re-use the same connection for the following transfers, thus making the operations faster, less CPU intense and using less network resources.
 *             Just note that you will have to use `esp_http_client_set_**` between the invokes to set options for the following esp_http_client_perform.
 *
 * @note       You must never call this function simultaneously from two places using the same client handle.
 *             Let the function return first before invoking it another time.
 *             If you want parallel transfers, you must use several esp_http_client_handle_t.
 *             This function include `esp_http_client_open` -> `esp_http_client_write` -> `esp_http_client_fetch_headers` -> `esp_http_client_read` (and option) `esp_http_client_close`.
 *
 * @param      client  The esp_http_client handle
 *
 * @return
 *  - EM_OK 							on successful
 *  - EM_FAIL 						on error
 *  - HTTP_CLIENT_ERR_HTTP_CONNECT		Error opening the HTTP connection
 */
#define http_client_perform(client)						http_client_pf_perform(client)

/**
 * @brief      Get http response status code, the valid value if this function invoke after `http_client_perform`
 *
 * @param[in]  client  The HttpClientHandle
 *
 * @return     Status code
 */
#define http_client_get_status_code(client)				http_client_pf_get_status_code(client)

/**
 * @brief      Get http response content length (from header Content-Length)
 *             the valid value if this function invoke after `http_client_perform`
 *
 * @param[in]  client  The HttpClientHandle
 *
 * @return
 *     - (-1) Chunked transfer
 *     - Content-Length value as bytes
 */
#define http_client_get_content_length(client)			http_client_pf_get_content_length(client)

/**
 * @brief      Read data from http stream
 *
 * @param[in]  client  The HttpClientHandle
 * @param      buffer  The buffer
 * @param[in]  len     The length
 *
 * @return
 *     - (-1) if any errors
 *     - Length of data was read
 */
#define http_client_read(client, buffer, len)			http_client_pf_read(client, buffer, len)

/**
 * @brief      This function must be the last function to call for an session.
 *             It is the opposite of the http_client_init function and must be called with the same handle as input that a http_client_init call returned.
 *             This might close all connections this handle has used and possibly has kept open until now.
 *             Don't call this function if you intend to transfer more files, re-using handles is a key to good performance with HttpClientHandle.
 *
 * @param[in]  client  The HttpClientHandle
 *
 * @return
 *     - EM_OK
 *     - EM_FAIL
 */
#define http_client_cleanup(client)						http_client_pf_cleanup(client)

/**
 * @brief	Http Client's response receive event callback function pointer
 *
 *
 **/
typedef void (*http_res_recv_handle_cb)(uint8_t event_id, char* data, size_t data_len);

/**
 * @brief	The Http Client module has 2 types of modes of operation:
 *
 * 			1. 	SINGLE STEP - The application is required to call only one function, i.e. do_http_operation()
 * 				and the required task will be performed. This function is blocking
 *
 * 			2. 	MULTI STEP - The application is required to call a set of functions to do a complete HTTP operation.
 * 				Here the application also needs to implement a callback function where response data will be sent.
 * 				The functions to be called in this mode are: init_http_client(), perform_http_client_process(),
 * 				get_http_status_code() and deinit_http_client(). See each function's documentation for more insights
 *
 * 			These modes are implicitly set by the module and the application **must not** set these modes explicitly
 */
typedef enum {
	_API_MODE_SINGLE_STEP, /**< Used to enable Single Step mode of operation */
	_API_MODE_MULTI_STEP /**< Used to enable Multi Step mode of operation */
} HTTP_CLIENT_API_MODE;

/**
 * @brief HTTP Control Data
 */
typedef struct {
	size_t list_pos; /*!< HTTP Client Node position */
	http_res_recv_handle_cb http_recv_handler; /*!< HTTP Response callback handler */
	char* response_data; /*!< HTTP Response buffer, to be populated and returned (Used in SINGLE STEP mode only) */
	size_t* response_len; /*!< HTTP Response buffer length, to be populated and returned (Used in SINGLE STEP mode only)*/
	size_t max_response_len; /*!< HTTP Response max length. Maximum number of bytes in this response (Used in SINGLE STEP mode only)*/
	uint16_t event_stat; /*!< HTTP event code (Used in SINGLE STEP mode only)*/
	HttpClientHandle client; /*!< HTTP Client handle */
	uint8_t client_api_mode; /*!< Mode of operation (HTTP_CLIENT_API_MODE). Set internally */
} HttpCtrl;

/**
 * @brief HTTP Config Data
 */
typedef struct {
	char* url; /*!< HTTP URL, the information on the URL is most important, it overrides the other fields below, if any */
	int port; /*!< Port to connect, default depend on esp_http_client_transport_t (80 or 443) */
//	char *username; 			/*!< Using for HTTP authentication */
//	char *password; 			/*!< Using for HTTP authentication */
	char *cert_pem; /*!< SSL server certification, PEM format as string, if the client requires to verify server */
//	char *client_cert_pem; 	/*!< SSL client certification, PEM format as string, if the server requires to verify client */
//	char *client_key_pem; 	/*!< SSL client key, PEM format as string, if the server requires to verify client */
//	int timeout_ms; /*!< Network timeout in milliseconds */
	HTTP_CLIENT_METHOD method; /*!< HTTP Method */
	char* host_server; /*!< HTTP Host name */
	char* user_agent; /*!< HTTP User-agent details*/
	char* content_type; /*!< HTTP Content type */
	char* post_data; /*!< HTTP Post data  */
	size_t post_data_len; /*!< HTTP Post data legnth */
} HttpConfigData;

/**
 * @brief HTTP Client Data
 */
typedef struct {
	HttpConfigData http_cnf_data; /*!< HTTP Config Data */
	HttpCtrl http_ctrl; /*!< HTTP Control Data */
} HttpClientData;

/**
 * @brief	initialize the HTTP with the configured HTTP client data.
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @return
 * 		- EM_OK 					on sucess
 *		- EM_FAIL 				on fail
 *		- EM_ERR_INVALID_STATE	returned if no network connection is available
 **/
em_err init_http_client(HttpClientData *http_cli_data);

/**
 * @brief		Re-configure the HTTP client with new HTTP config data, without close the current HTTP client connection
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
em_err config_http_client(HttpClientData *http_cli_data);

/**
 * @brief		Execute the HTTP Client Process
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
em_err perform_http_client_process(HttpClientData *http_cli_data);

/**
 * @brief		Get the HTTP status conde after 'perform_http_client_process()'
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @return
 *		- HTTP Response Codes
 **/
uint16_t get_http_status_code(HttpClientData *http_cli_data);

/**
 * @brief		Get the HTTP response length after 'perform_http_client_process'
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @param[out]	res_buff		function populate the response based on the request Length
 *
 * @param[in]	len 			request response length
 *
 * @return
 *     - (-1) if any errors
 *     - Length of data was read
 **/
size_t get_http_client_response(HttpClientData *http_cli_data, char* res_buff, size_t len);

/**
 * @brief		Get the HTTP event code for this particular client
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @return
 * 				uint16_t event status code
 **/
uint16_t get_http_client_event_stat(HttpClientData *http_cli_data);

/**
 * @brief		de-initialize the HTTP with the configured HTTP client data.
 *
 * @param[in]	http_cli_data	pass the configured HttpClientData
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
em_err deinit_http_client(HttpClientData *http_cli_data);

#ifdef __cplusplus
}
#endif

#endif /* HTTP_CORE_H_ */

