/*
 * File Name: sntp_client_platform.c
 * File Path: /emmate/src/conn-proto/sntp-client/sntp_client_core.c
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#include "sntp_client_core.h"

void initialize_sntp_core(void) {
	initialize_sntp_platform(CONFIG_SNTP_SERVER_URL);
}
