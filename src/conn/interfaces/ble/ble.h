/*
 * ble.h
 *
 *	File Path: /emmate/src/conn/ble/ble.h
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

#ifndef SRC_CONN_BLE_BLE_H_
#define SRC_CONN_BLE_BLE_H_

#include "core_config.h"
#include "core_error.h"
#include "ble_platform.h"
#include "ble_constant.h"

typedef struct {
	uint8_t ble_mac[BLE_MAC_LEN+1];
} BLEInfo;

/**
 * @brief	Set the BLE Peripheral's name which advertise when BLE activate
 *
 * @param[in]	pble_adv	Name of the BLE device
 *
 * @attention
 * 			pble_adv's length can not be more then BLE_NAME_MAX_LENGTH
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 *
 **/
em_err set_ble_adv_name(char* pble_adv);


/**
 * @brief	Initialize the  BLE Peripheral
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 *
 **/
em_err init_ble() ;


/**
 * @brief	De-initialize BLE Peripheral
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 *
 **/
em_err deinit_ble();


/**
 * @brief	Enabling BLE Peripheral
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 *
 **/
em_err enable_ble();

/**
 * @brief	Disabling BLE Peripheral
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 *
 **/
em_err disable_ble();

/**
 * @brief	Get Connection Status
 *
 * @return
 * 		- BLE_NOT_CONNECTED	if BLE not connected
 * 		- BLE_CONNECTED		if BLE connected
 *
 **/
BLE_CONNECTION_STATUS isbleconnected();

/*
 * @brief		Get the Bluetooth interface's MAC address
 *
 * @param[out]	bt_mac[6]	Bluetooth interface's MAC address (6 bytes) returned as an out parameter
 *
 * @return
 * 				- EM_OK: succeed
 * 				- EM_FAIL on fail
 * */
void get_ble_mac(uint8_t *ble_mac);

typedef void (*BLEIFInfoRecvHandler) (void* ifinfo);


/**
  * @brief     Register a function to receive BLE interface's information
  *
  * @param handler	BLEIFInfoRecvHandler Function handler where BLE information will be received
  *
  * @return
  * 	EM_OK: succeed (if handler != NULL)
  * 	EM_FAIL
  */
em_err register_ble_info_recv_cb_handler(BLEIFInfoRecvHandler handler);

#endif /* SRC_CONN_BLE_BLE_H_ */
