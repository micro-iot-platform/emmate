/*
 * ble_config.h
 *
 *  Created on: 15-Apr-2019
 *      Author: iqubuntu02
 */

#ifndef SRC_CONN_BLE_BLE_CONFIG_H_
#define SRC_CONN_BLE_BLE_CONFIG_H_

#include "core_error.h"
#include "threading.h"
#include "ble.h"


typedef enum {
	BLE_AUTH_NOT_DONE = 0,				//!< BLE authentication not done yet
	BLE_AUTH_SUCCESS,					//!< BLE authentication successful
	BLE_AUTHENTICATED,					//!< BLE authentication done
	BLE_AUTH_FAILED,					//!< BLE authentication failed
} BLE_AUTH_STATUS;

typedef enum {
	BLE_DATA_PROCEESS_NOT_DONE = 0,			//!< BLE data processing not yet start
	BLE_METADATA_RECV_DONE,					//!< Metadata received complete
	BLE_BYTEDATA_RECV_DONE,					//!< ByteData received complete
	BLE_BYTEDATA_RECV_FAILED,				//!< ByteData failed receive
	BLE_SEND_RES_DEVCFG_DONE,				//!< Device config response send success
	BLE_SEND_RES_DEVCFG_FAILED,				//!< Device config response send failed
} BLE_DATA_PROCESS_STATUS;

typedef enum {
	BLE_CONFIGURATION_ON_IDLE_STATE = 0,		//!< BLE Configuration on Idle state
	BLE_CONFIGURATION_IN_PROCESS,				//!< BLE Configuration executing
	BLE_CONFIGURATION_COMPLETE,					//!< BLE Configuration execution complete
} BLE_CONFIGURATION_PROCESS_STATUS;

typedef struct {
	BLE_AUTH_STATUS ble_auth_status;								//!< BLE_AUTH_STATUS variable for store the authentication status
	BLE_CONFIGURATION_PROCESS_STATUS ble_config_process_status;		//!< BLE_CONFIGURATION_PROCESS_STATUS variable for store the BLE process status
	BLE_DATA_PROCESS_STATUS ble_data_process_stat;					//!< BLE_DATA_PROCESS_STATUS variable for store the BLE data process status
	bool is_anonymous_ble_app;
} BLE_PROCESS_DATA;


/**
 * @brief	start and activate the BLE.
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 *
 **/
em_err init_ble_cfg(QueueHandle* recv_queue, char* ble_adv_name, char* ble_authid);


/**
 * @brief	start the BLE Configuration task
 *
 * @return
 *		- EM_OK on sucess
 *		- EM_FAIL on fail to create the BLE config Task
 **/
em_err start_ble_cfg();


/**
 * @brief	set the current BLE Authentication condition
 * 			by default it set as BLE_AUTH_NOT_DONE
 *
 * @return
 *
 **/
void set_ble_auth_stat(BLE_AUTH_STATUS stat);

/**
 * @brief	Return back BLE authentication status.
 *
 * @return
 * 		- BLE_AUTH_NOT_DONE			If BLE authentication not yet done
 * 		- BLE_AUTH_SUCCESS			If BLE authentication success
 * 		- BLE_AUTHENTICATED			If BLE authentication successfully completed
 * 		- BLE_AUTH_FAILED			If BLE authentication failed
 *
 **/
BLE_AUTH_STATUS get_ble_auth_stat();

/**
 * @brief set the current BLE_CONFIGURATION_PROCESS_STATUS
 *
 * @param[in] stat current BLE_CONFIGURATION_PROCESS_STATUS
 *
 * @return
 *
 **/
void set_ble_ongoing_process_stat(BLE_CONFIGURATION_PROCESS_STATUS stat);

/**
 * @brief	get the current BLE_DATA_PROCESS_STATUS
 *
 * @return  the current running BLE_DATA_PROCESS_STATUS
 *
 **/
BLE_CONFIGURATION_PROCESS_STATUS get_ble_ongoing_process_stat();

/**
 * @brief	set the current BLE_DATA_PROCESS_STATUS
 *
 * @param[in]	status current BLE_DATA_PROCESS_STATUS
 *
 * @return
 *
 **/
void set_ble_data_process_stat(BLE_DATA_PROCESS_STATUS status);


#endif /* SRC_CONN_BLE_BLE_CONFIG_H_ */
