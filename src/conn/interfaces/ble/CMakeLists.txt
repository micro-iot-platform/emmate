set(srcs
	ble.c
	ble_config.c
	ble_platform.c
	gatt_service.c
	gatt_utils.c
	)

add_library(ble STATIC ${srcs})

# Private Linkages
list(APPEND EXTRA_LIBS emmate_config)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

if(CONFIG_USE_THREADING)
	list(APPEND EXTRA_LIBS threading)
endif()

if(CONFIG_USE_HMI)
	list(APPEND EXTRA_LIBS hmi)
endif()

if(CONFIG_USE_SYSTEM)
	list(APPEND EXTRA_LIBS system)
endif()
		
# Public Linkages

# Link the libraries
target_link_libraries(ble PRIVATE ${EXTRA_LIBS})

# Platform Linkages
if(CONFIG_PLATFORM_ESP_IDF)
	list(APPEND IDF_LIBS
				idf::bt
			)
	target_link_libraries(ble PRIVATE ${IDF_LIBS})
endif()

target_include_directories(ble 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)