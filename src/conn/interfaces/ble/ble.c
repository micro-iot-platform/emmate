/*
 * ble.c
 *
 *	File Path: /emmate/src/conn/ble/ble.c
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

//#include "ble_platform.h"
#include "ble.h"
#include "hmi.h"
#include "system_hmi_led_notification.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define TAG LTAG_CONN_BLE

static BLEInfo ble_info;
static BLEIFInfoRecvHandler ble_info_hdl=NULL;


static void ble_connect_event_handler(BLE_CONNECTION_STATUS status) {
	if (status == BLE_CONNECTED) {
		EM_LOGI(TAG, "BLE is connected");
		show_ble_connected_notification();
	} else {
		EM_LOGI(TAG, "BLE is disconnected");
		show_ble_advertising_notification();
	}
}

/*******************************************************************************************/

/**
 * @brief
 *
 * @return
 *
 **/
em_err set_ble_adv_name(char* pble_adv) {
	em_err err = EM_FAIL;
	if (NULL != pble_adv) {
		if (strlen(pble_adv) > ble_platform_get_ble_max_name_size()) {
			return EM_ERR_INVALID_SIZE;
		} else {
			err = ble_platform_set_ble_adv_name(pble_adv);
		}
	}
	return err;
}

/**
 * @brief	Initialize the  BLE Peripheral
 *
 * @return
 *
 **/
em_err init_ble() {
	em_err err = EM_FAIL;
	err = ble_platform_config_ble();
	ble_platform_set_connect_event_handler(ble_connect_event_handler);
	show_ble_advertising_notification();
	return err;
}

/**
 * @brief	De-initialize BLE Peripheral
 *
 * @return
 *
 **/
em_err deinit_ble() {
	em_err err = EM_FAIL;
	err = ble_platform_deconfig_ble();
	return err;
}

/**
 * @brief	Enabling BLE Peripheral
 *
 * @return
 *
 **/
em_err enable_ble() {
	em_err err = EM_FAIL;
	err = ble_platform_enable_ble(ble_info.ble_mac);
	if (err == EM_OK) {
		if (ble_info_hdl != NULL) {
			ble_info_hdl(&ble_info);
		}
	}
	return err;
}

/**
 * @brief	Disabling BLE Peripheral
 *
 * @return
 *
 **/
em_err disable_ble() {
	em_err err = EM_FAIL;
	err = ble_platform_disable_ble();
	return err;
}

/**
 * @brief	Get Connection Status
 *
 * @return
 *
 **/
BLE_CONNECTION_STATUS isbleconnected() {
	return ble_platform_isble_connected();
}

void get_ble_mac(uint8_t *ble_mac) {
	if (ble_info.ble_mac != NULL) {
		memcpy(ble_mac, ble_info.ble_mac, BLE_MAC_LEN);
	} else {
		EM_LOGE(TAG, "Unable to get BLE MAC");
		memset(ble_mac, 0, BLE_MAC_LEN);
	}
}

/**
 *
 */
em_err register_ble_info_recv_cb_handler(BLEIFInfoRecvHandler handler) {
	if (handler != NULL) {
		ble_info_hdl = handler;
		return EM_OK;
	}
	return EM_FAIL;
}
