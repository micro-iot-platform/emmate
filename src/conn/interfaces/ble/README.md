# BLE Module (Under Development!)
Currently the BLE module can only be together with the Device Configurator System Module. It can be used as a device configuration interface using the EmMate's Device Configuration Android Application.

Currently an application cannot use the BLE as a data transfer interface. It is currently under development and will be available in future releases.