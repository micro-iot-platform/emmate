/*
 * File Name: conn.h
 * File Path: /emmate/src/conn/conn.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef CONN_H_
#define CONN_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"


#if CONFIG_USE_BLE
#include "ble.h"
#endif
#if CONFIG_USE_WIFI
#include "wifi.h"
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif


//#if CONFIG_USE_DEVCONFIG
//#include "device_config.h"
//#endif

#if CONFIG_USE_INTERFACES
#include "core_interfaces.h"
#endif

typedef uint32_t NETWORK_CONN_STATUS;

//#define	NETCONNSTAT_CONNECTING 					0x00	/**< Connecting to a network interface */
//#define	NETCONNSTAT_CONNECTED					0x01	/**< Connected to a network interface */
//#define NETCONNSTAT_DISCONNECTED				0x02	/**< Not connected to any network interface */
//#define	NETCONNSTAT_INTERNET_NOT_AVAILABLE		0x04	/**< Internet is not available */
//#define	NETCONNSTAT_INTERNET_AVAILABLE			0x40	/**< Internet is available */
//#define	NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE		0x08	/**< Configured IoT cloud server is not available */
//#define	NETCONNSTAT_IOT_CLOUD_AVAILABLE			0x80	/**< Configured IoT cloud server is available */

#define	NETCONNSTAT_CONNECTING 					0b00000001	/**< Connecting to a network interface */
#define	NETCONNSTAT_CONNECTED					0b00000010	/**< Connected to a network interface */
#define NETCONNSTAT_DISCONNECTED				0b00000100	/**< Not connected to any network interface */
#define	NETCONNSTAT_INTERNET_NOT_AVAILABLE		0b00001000	/**< Internet is not available */
#define	NETCONNSTAT_INTERNET_AVAILABLE			0b00010000	/**< Internet is available */
#define	NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE		0b00100000	/**< Configured IoT cloud server is not available */
#define	NETCONNSTAT_IOT_CLOUD_AVAILABLE			0b01000000	/**< Configured IoT cloud server is available */


typedef struct {
	NETWORK_CONN_STATUS netstat;
} ConnData;

/**
 * @brief initialize the configured Network interface
 *
 * @param dev_cfg	pass DeviceConfig data to config the Network interface.
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
//em_err init_network(DeviceConfig *dev_cfg);


/**
 * @brief make a connection between network interface and the network
 *
 * @param dev_cfg	pass DeviceConfig data to connection network interface with the network
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
//em_err connect_to_network(DeviceConfig *dev_cfg);

/**
 * @brief 			Returns the network connection and internet statuses
 *
 * @note			The statuses returned from this function are bits which are ORed together
 * 					The user can use these bits to make one or more conditions if required
 *
 * 					Example:
 *
 *					\code{.c}
 * 					uint32_t stat = get_network_conn_status();
 *
 * 					if ((stat & NETCONNSTAT_CONNECTED) == NETCONNSTAT_CONNECTED) {
 * 						// Do something;
 * 					} else if ((stat & NETCONNSTAT_DISCONNECTED) == NETCONNSTAT_DISCONNECTED) {
 * 						// Do something else;
 * 					} else if ((stat & (NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_AVAILABLE)) ==
 * 								(NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_AVAILABLE)) {
 * 						// Do something else;
 * 					} else if ((stat & (NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_NOT_AVAILABLE)) ==
 * 								(NETCONNSTAT_CONNECTED | NETCONNSTAT_INTERNET_NOT_AVAILABLE)) {
 * 						// Do something else;
 * 					}
 * 					\endcode
 *
 * @return
 * 			NETWORK_CONN_STATUS
 **/
NETWORK_CONN_STATUS get_network_conn_status();

/**
 * @brief 			Starts the network connection thread
 *
 * @param dev_cfg	pass DeviceConfig data for connecting to appropriate network
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
//em_err start_network_connection(DeviceConfig *dev_cfg);
em_err start_network_connection();


/**
 * @brief 			prerequisite setup/initialize for connection Interfaces.
 * Note: 	call this function before initialize any connection interfaces.
 *
 * @return
 * 		- EM_OK 	on sucess
 *		- EM_FAIL on fail
 **/
em_err conn_prerequisite_setup();

#ifdef __cplusplus
}
#endif

#endif /* CONN_H_ */
