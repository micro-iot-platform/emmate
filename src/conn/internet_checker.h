/*
 * File Name: ping.h
 * File Path: /emmate/src/conn/internet_checker.h
 * Description:
 *
 *  Created on: 03-Apr-2020
 *      Author: Noyel Seth
 */

#ifndef INTERNET_CHECKER_H_
#define INTERNET_CHECKER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"

#if CONFIG_USE_INTERNET_CHECKER


#define GOOGLE_PING_HOST			"google.com"

#define GOOGLE_PING_URL		"http://"GOOGLE_PING_HOST
#define GOOGLE_PING_PORT 		80

#define INTERNET_CHECK_INTERVAL		CONFIG_INTETNET_CHECKING_INTERVAL_TIME * DELAY_1_SEC




typedef enum {
	INTERNET_NOT_AVAILABLE = 0,
	INTERNET_AVAILABLE,
	INTERNET_CLOUD_AVAILABLE,
} INTERNET_STATUS;

typedef void (*InternetStatRecvCallBackHandler) (INTERNET_STATUS internet_stat);

/**
 *
 */
em_err init_internet_checker();

/**
 *
 */
void deinit_internet_checker();

/**
 *
 */
INTERNET_STATUS get_internet_stat();

/**
  * @brief     Register a function to receive Ping status
  *
  * @param handler	PingStatRecvCallBackHandler Function handler where Ping status will be received
  *
  * @return
  * 	EM_OK: succeed (if handler != NULL)
  * 	EM_FAIL
  */
em_err register_internet_stat_recv_cb_handler(InternetStatRecvCallBackHandler hdl);

#endif /* CONFIG_USE_INTERNET_CHECKER */

#ifdef __cplusplus
}
#endif

#endif /* INTERNET_CHECKER_H_ */
