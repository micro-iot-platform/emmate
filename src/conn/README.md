# Connectivity Module

## Overview

The connectivity module helps the application to connect to a network. This module features easy to use APIs to initialize, connect and disconnect to/from a network. The connectivity module supports multiple network interfaces and network protocols for an application to use.

## Features

The connectivity module supports the following Network Interfaces and Protocols

#### Supported Network Interfaces

1. Wi-Fi - For more details see [Details of Wi-Fi Module](interfaces/wifi/README.md)
2. Ethernet - *(not yet implemented)*
3. Bluetooth Low Energy (BLE) - For more details see [Details of BLE Module](interfaces/ble/README.md)
4. GSM - *(not yet implemented)*
5. NB-IoT - *(not yet implemented)*

**For more details of each interface please see its Readme, API Documentation and Examples**

#### Supported Protocols

1. HTTP(S) Client - For more details see [Details of HTTP Module](protocols/http/README.md)
2. SNTP Client - For more details see [Details of SNTP Module](protocols/sntp/README.md)

**For more details of each protocol please see its Readme, API Documentation and Examples**


## How to use the Connectivity Module from an Application

The APIs exposed from the connectivity module are provided in `conn.h`

#### Header(s) to include

```
conn.h
```

#### Module Specific Configurations

Since this module is used by the System Module, most of the basic functionality can be set from the Menuconfig Application.

If the following configurations are enabled, then the connectivity module is initialized and started.

1. Dev-Config Module (see image below)
2. Sys-heartbeat Module (see image below)

![](img/conn-config-0.png)

** If the System Module is enabled then by default the selected network interface is Wi-Fi and the network protocol is HTTP**

However is the System Module is not used, the following configurations has to be set before using the APIs.

1. Enable Connection Module as shown below:

![](img/conn-config-1.png)

2. Currently EmMate Framework only supports WiFi interface for data transfer, so it is enabled by default. See the image below.

3. For enabling a network protocol select the appropriate check boxes as shown below. Please read each sub-module's documentation for better understanding.

![](img/conn-config-2.png)


#### Module Specific APIs

1. Init and start the network connection:

```c
DeviceConfig dev_cfg;

/* Currently Wi-Fi is only supported, so configure the SSID & Password */
strcpy(dev_cfg.wifi_cfg.wifi_ssid, "MY-SSID");
strcpy(dev_cfg.wifi_cfg.wifi_pwd, "PASSWORD");

core_err ret = start_network_connection(&dev_cfg);
if (ret != EM_OK) {
	EM_LOGE(TAG, "Failed to initialize and start the network");
	return ret;
}

/* Wait untill the network connection is successful */
while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {
	TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
}

EM_LOGI(TAG, "Network connection successful");
```  
