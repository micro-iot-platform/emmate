/*
 * File Name: board_ids.c
 * File Path: /emmate/src/boards/board_ids.c
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */


#include "board_ids.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_WIFI
#include "wifi.h"
#endif
#if CONFIG_USE_BLE
#include "ble.h"
#endif
#if IFACE_ETH
#endif
#if IFACE_GSM
#endif

#if CONFIG_USE_EEPROM_24AA02UID
#include "eeprom_24AA02UID.h"
#endif

#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#if CONFIG_USE_MIGCLOUD
#include "migcloud_storage.h"
#endif
#endif

#include "core_utils.h"
#include <string.h>

#if (CONFIG_PLATFORM_SIMULATOR)
#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"
#endif
#include "input_processor.h"
#include "threading.h"

#define TAG	LTAG_BOARDS

static char core_somthing_id[SOMTHING_ID_LEN+1] = {0};

em_err read_save_somthing_id() {
	em_err err = EM_FAIL;

	memset(core_somthing_id, 0, (SOMTHING_ID_LEN+1));

	// TODO: Checking for hard-wired SOM ID has to be made dynamic instead of compile time
#if CONFIG_USE_SILICON_ID_CHIP
#if CONFIG_USE_EEPROM_24AA02UID
	uint8_t uid[8] = {0};
	err = eeprom_24AA02UID_init();
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize EEPROM 24AA02UID");
		return EM_FAIL;
	}

	err = eeprom_24AA02UID_read_64bit_uid_code(uid);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to read 64 bit UID ID from EEPROM 24AA02UID");
		return EM_FAIL;
	}

	err = eeprom_24AA02UID_deinit();
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to de-initialize EEPROM 24AA02UID");
		return EM_FAIL;
	}
	err = hexbytearray_to_str(uid, sizeof(uid), core_somthing_id);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to convert HEX UID to String");
		return EM_FAIL;
	}
#endif /* CONFIG_USE_EEPROM_24AA02UID */
#else

#if CONFIG_USE_PERSISTENT

#if CONFIG_USE_MIGCLOUD
	err = read_somthing_id_from_persistent_mem(core_somthing_id);
	if (err != EM_OK) {
		memcpy(core_somthing_id, NO_HW_SOM_ID, strlen(NO_HW_SOM_ID));
	}
#endif /* CONFIG_USE_MIGCLOUD */

#else
	memcpy(core_somthing_id, NO_HW_SOM_ID, strlen(NO_HW_SOM_ID));
#endif /* CONFIG_USE_PERSISTENT */

#endif /* CONFIG_USE_SILICON_ID_CHIP */

	err = EM_OK;
	return err;
}

em_err get_somthing_id(char *somthing_id) {
	em_err err = EM_FAIL;

	if (somthing_id == NULL)
		return EM_FAIL;

	/* If NO_SOM_ID is found, then return suitable error code */
	if (strcmp(core_somthing_id, NO_HW_SOM_ID) == 0) {
		return EM_ERR_NOT_FOUND;
	}

	strcpy(somthing_id, core_somthing_id);
//	memcpy(som_id, core_som_id, SOM_ID_LEN);

	EM_LOGD(TAG, "SOM ID: %s", somthing_id);
//	EM_LOGI(TAG, "SOM ID: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", som_id[0], som_id[1], som_id[2], som_id[3], som_id[4],
//			som_id[5]);

	err = EM_OK;
	return err;
}

em_err set_virtual_somthing_id(char *v_somthing_id) {
	if (v_somthing_id == NULL)
		return EM_FAIL;

	strcpy(core_somthing_id, v_somthing_id);

	return EM_OK;
}

bool is_hw_somthing_id_present() {
	/* If NO_SOM_ID is found, then return suitable error code */
	if (strcmp(core_somthing_id, NO_HW_SOM_ID) == 0) {
		return false;
	}
	return true;
}

#if CONFIG_USE_WIFI
em_err get_board_wifi_sta_mac(uint8_t *wifi_sta_mac) {
	em_err err = EM_FAIL;

	if (wifi_sta_mac == NULL)
		return EM_FAIL;

	err = get_wifi_sta_mac(wifi_sta_mac);
	EM_LOGD(TAG, "Wi-Fi STA MAC: %02x:%02x:%02x:%02x:%02x:%02x", wifi_sta_mac[0], wifi_sta_mac[1], wifi_sta_mac[2],
			wifi_sta_mac[3], wifi_sta_mac[4], wifi_sta_mac[5]);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to get Wi-Fi STA MAC");
		err = EM_FAIL;
	}
	return err;
}
#endif

#if CONFIG_USE_BLE
em_err get_board_bt_mac(uint8_t *bt_mac) {
	em_err err = EM_FAIL;

	if (bt_mac == NULL)
		return EM_FAIL;

	get_ble_mac(bt_mac);
	EM_LOGD(TAG, "Bluetooth MAC: %02x:%02x:%02x:%02x:%02x:%02x", bt_mac[0], bt_mac[1], bt_mac[2], bt_mac[3], bt_mac[4],
			bt_mac[5]);
	err = EM_OK;
	return err;
}
#endif

#if IFACE_ETH
em_err get_board_eth_mac(uint8_t *eth_mac) {
	em_err err = EM_FAIL;

	if (eth_mac == NULL) return EM_FAIL;

	err = // TODO:
	EM_LOGD(TAG, "Ethernet MAC: %02x:%02x:%02x:%02x:%02x:%02x", eth_mac[0], eth_mac[1], eth_mac[2], eth_mac[3],
			eth_mac[4], eth_mac[5]);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to get Ethernet MAC");
		err = EM_FAIL;
	}
	return err;
}
#endif

#if IFACE_GSM
em_err get_board_gsm_imei(long *gsm_imei) {
	em_err err = EM_FAIL;

	if (eth_mac == NULL) return EM_FAIL;

	err = // TODO:
	EM_LOGD(TAG, "GSM IMEI: %d", gsm_imei);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to get GSM IMEI");
		err = EM_FAIL;
	}
	return err;
}
#endif

#if CONFIG_PLATFORM_SIMULATOR
#define SIMULATOR_ID_LEN	7
#define SIMULATOR_ID		{0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x00}

typedef struct {
	uint8_t wifi_mac[20];
	uint8_t bt_mac[20];
	uint8_t eth_mac[20];
	uint8_t imei[30];
} SimBoardIds;
static SimBoardIds m_sim_board_ids;
static volatile bool m_board_ids_set = false;

void board_ids_store(char *board_ids) {
	char *json_buff = board_ids;
	memset(&m_sim_board_ids, 0x00, sizeof(SimBoardIds));

	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "JSON Value type not matched");
		} else {
			rootObj = json_value_get_object(root_value);

			/* get wifi_mac data */
			ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(m_sim_board_ids.wifi_mac, "."), m_sim_board_ids.wifi_mac);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_sim_board_ids.wifi_mac, "."));
			}

			/* get bt_mac data */
			ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(m_sim_board_ids.bt_mac, "."), m_sim_board_ids.bt_mac);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_sim_board_ids.bt_mac, "."));
			}

			/* get eth_mac data */
			ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(m_sim_board_ids.eth_mac, "."), m_sim_board_ids.eth_mac);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_sim_board_ids.eth_mac, "."));
			}

			/* get imei data */
			ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(m_sim_board_ids.imei, "."), m_sim_board_ids.imei);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_sim_board_ids.imei, "."));
			}

			json_value_free(root_value); /* clear root_value */
			m_board_ids_set = true;
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
	}
}

em_err get_simulator_identifier(uint8_t *sim_id) {
	em_err err = EM_FAIL;

	if (sim_id == NULL)
		return EM_FAIL;

	m_board_ids_set = false;

	/* Send SIM_GET_BOARD_IDS command */
	char *json_buf;
	int json_len;
	simulator_make_request_json(&json_buf, &json_len, SIM_GET_BOARD_IDS, NULL);
	EM_LOGD(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	/* Wait till we get the board ids */
	while (m_board_ids_set == false) {
		TaskDelay(50);
	}

//	memcpy(sim_id, m_sim_board_ids.wifi_mac, sizeof(m_sim_board_ids.wifi_mac));
	str_to_hexbytearray(m_sim_board_ids.wifi_mac, sim_id, 6);
	err = EM_OK;
	EM_LOGI(TAG, "SIMULATOR ID: %02x:%02x:%02x:%02x:%02x:%02x", sim_id[0], sim_id[1], sim_id[2], sim_id[3], sim_id[4],
			sim_id[5]);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to get SIMULATOR ID");
		err = EM_FAIL;
	}
	return err;
}
#endif	/* CONFIG_PLATFORM_SIMULATOR */
