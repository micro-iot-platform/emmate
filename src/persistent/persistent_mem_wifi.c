/*
 * persistent_mem_wifi.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */
#include "persistent_mem_wifi.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT

#if CONFIG_USE_WIFI
/*
 * Read the wifi credentials data from persistent memory
 */
em_err read_wifi_credentials_from_persistent_mem(WiFiCredentials *wifi_cred) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_read_config_by_key(WIFI_CRED_NVS_KEY, (void*) wifi_cred, sizeof(WiFiCredentials))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Reading WiFiCredentials from persistent memory failed");
	}
	return ret;
}

/*
 * Write the wifi credentials data into persistent memory
 */
em_err write_wifi_credentials_to_persistent_mem(WiFiCredentials *wifi_cred) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_write_config_by_key(WIFI_CRED_NVS_KEY, (void*) wifi_cred, sizeof(WiFiCredentials))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Writing WiFiCredentials to persistent memory failed");
	}
	return ret;
}


/*
 * Read the wifi mac from persistent memory
 */
em_err read_wifi_mac_from_persistent_mem(uint8_t* mac) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_read_config_by_key(WIFI_MAC_ID, (void*) mac, WIFI_MAC_LEN)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Reading WiFi MAC from persistent memory failed");
	}
	return ret;
}

/*
 * Write the wifi mac into persistent memory
 */
em_err write_wifi_mac_to_persistent_mem(uint8_t* mac) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_write_config_by_key(WIFI_MAC_ID, (void*) mac, WIFI_MAC_LEN)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Writing WiFi MAC to persistent memory failed");
	}
	return ret;
}


#endif
