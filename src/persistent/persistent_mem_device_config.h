/*
 * persistent_mem_device_config.h
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_DEVICE_CONFIG_H_
#define PERSISTENT_MEM_DEVICE_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"
#if CONFIG_USE_DEVCONFIG
#include "device_config.h"
#endif

#if CONFIG_USE_DEVCONFIG
/**
 * @brief 	read_devconfig_from_persistent_mem reads only the 1st level of device configurations from the persistent memory
 * 			The 1st level of device configurations will include the device id, and configs required for the
 * 			selected network interface. These configurations will be used to do the 1st level OTA.
 *
 * @param[out]	devcfg	DeviceConfig data to get from persistent memory
 *
 * @return
 * 		- EM_OK				On success
 * 		- EM_FAIL				On failure
 *
 * */
em_err read_devconfig_from_persistent_mem(DeviceConfig *devcfg);

/**
 * @brief write_devconfig_to_persistent_mem writes only the 1st level of device configurations from the persistent memory
 *
 * @param[in]	devcfg	DeviceConfig data to set into persistent memory
 *
 * @return
 * 		- EM_OK				On success
 * 		- EM_FAIL				On failure
 *
 * */
em_err write_devconfig_to_persistent_mem(DeviceConfig *devcfg);

#endif /* CONFIG_USE_DEVCONFIG */

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_DEVICE_CONFIG_H_ */
