/*
 * core_nvs.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

//#include "core_config.h"
#include "core_nvs.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT_NVS

em_err nvs_initialize() {
	return init_nvs();
}

em_err nvs_deinitialize() {
	deinit_nvs();
	return EM_OK;
}

em_err nvs_erase() {
	return erase_nvs();
}

em_err nvs_read_by_key(char *key, void* value, size_t* bytes_read) {
	em_err ret = read_nvsdata_by_key(key, value, bytes_read);
	if (ret != EM_OK) {
		if ((ret == PLAT_NVS_ERR_NOT_INITIALIZED) || (ret == PLAT_NVS_ERR_NOT_FOUND)) {
			// do nothing, so these error codes will be returned
		} else {
			// for ny other error, return failure
			ret = EM_FAIL;
		}
	}
	return ret;
}

em_err nvs_write_by_key(char *key, void *value, size_t bytes_to_write) {
	return write_nvsdata_by_key(key, value, bytes_to_write);
}

em_err nsv_erase_by_key(char *key) {
	return erase_nvs_key(key);
}

