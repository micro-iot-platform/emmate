/*
 * persistent_mem_helper.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#if CONFIG_USE_NVS
#include "core_nvs.h"
#endif
#if CONFIG_USE_SDMMC_HOST
#include "sdmmc_host_core.h"
#endif

#define TAG LTAG_PERSISTENT

em_err persistent_read_config_by_key(char *key, void* buf, uint16_t buf_len) {
	em_err ret = EM_FAIL;
	size_t data_len = 0;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	memset(buf, 0x00, buf_len);

	ret = nvs_read_by_key(key, buf, &data_len);
	if (ret == EM_OK) {
		// TODO:
	} else {
		EM_LOGW(TAG, "persistent_read_config_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}

em_err persistent_write_config_by_key(char *key, void* buf, size_t buf_len) {
	em_err ret = EM_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nvs_write_by_key(key, buf, buf_len)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "persistent_write_config_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}

em_err persistent_erase_config_by_key(char *key) {
	em_err ret = EM_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nsv_erase_by_key(key)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "persistent_erase_config_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}
