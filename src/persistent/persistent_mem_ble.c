/*
 * persistent_mem_ble.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */
#include "persistent_mem_ble.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_BLE
#include "ble.h"
#endif
#define TAG LTAG_PERSISTENT

#if CONFIG_USE_BLE

/*
 * Read the ble mac from persistent memory
 */
em_err read_ble_mac_from_persistent_mem(uint8_t* mac) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_read_config_by_key(BLE_MAC_ID, (void*) mac, BLE_MAC_LEN)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Reading ble MAC from persistent memory failed");
	}
	return ret;
}

/*
 * Write the ble mac into persistent memory
 */
em_err write_ble_mac_to_persistent_mem(uint8_t* mac) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_write_config_by_key(BLE_MAC_ID, (void*) mac, BLE_MAC_LEN)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Writing ble MAC to persistent memory failed");
	}
	return ret;
}


#endif
