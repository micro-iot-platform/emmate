/*
 * persistent_mem_appconfig.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#include "core_config.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "persistent_mem_api.h"
#include "persistent_mem_helper.h"
#if CONFIG_USE_NVS
#include "core_nvs.h"
#endif
#if CONFIG_USE_SDMMC_HOST
#include "sdmmc_host_core.h"
#endif

#include <string.h>

#define TAG LTAG_PERSISTENT

em_err persistent_read_data_by_key(char *key, void* buf, size_t buf_len) {
	em_err ret = EM_FAIL;

	if (buf == NULL) {
		EM_LOGE(TAG, "Input buffer is NULL");
		return EM_FAIL;
	}
	if (buf_len == 0) {
		EM_LOGE(TAG, "Input buffer length is 0");
		return EM_FAIL;
	}

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	size_t data_len = buf_len;
//	memset(buf, 0x00, buf_len);
	if ((ret = nvs_read_by_key(key, buf, &data_len)) == EM_OK) {
		// TODO:
	} else {
		EM_LOGE(TAG, "Error: read_appconfig_from_persistent_mem_by_key failed for [key] = %s!", key);
		ret = EM_FAIL;
	}
#endif
	return ret;
}

em_err persistent_write_data_by_key(char *key, void* buf, size_t buf_len) {
	em_err ret = EM_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nvs_write_by_key(key, buf, buf_len)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Error: write_appconfig_from_persistent_mem_by_key failed for [key] = %s!", key);
	}
#endif

	return ret;
}
