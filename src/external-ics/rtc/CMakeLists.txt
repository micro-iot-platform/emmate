if(CONFIG_USE_DS1307)
	list(APPEND LIB_SRCS ds1307/ds1307.c)
	list(APPEND LIB_INC ${CMAKE_CURRENT_SOURCE_DIR}/ds1307)
endif()

set(srcs ${LIB_SRCS})

add_library(rtc STATIC ${srcs})

# Private linkages
list(APPEND EXTRA_LIBS 
			emmate_config
			${CMAKE_PROJECT_NAME}_inc
			)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

if(CONFIG_USE_THREADING)
	list(APPEND EXTRA_LIBS threading)
endif()

if(CONFIG_USE_I2C)
	list(APPEND EXTRA_LIBS i2c)
endif()

# Link the libraries
target_link_libraries(rtc PRIVATE ${EXTRA_LIBS})

target_include_directories(rtc INTERFACE ${LIB_INC})
