# External IC Module

## Overview

The External ICs module contains a database of libraries for commonly used sensors, rtc, memory chips, interface chips etc. The libraries are designed such that they can be directly used in an application without any hassle. The inclusion/exclusion of a library can be managed through the MenuConfig Application.

## Features

The below list specifies the libraries available under the External IC Module:

1. Actuator - 
2. EEPROM - 24Cxx, 24AA02UID
3. RTC - DS1307
4. Sensor
- Temperature - DHT22, LM35
- Humidity - DHT22

## How to use this module from an Application

Please check this module's examples to get a clear idea about its usage.

#### Module Specific Configurations

The following configurations are needed for the External IC module to work. Please enable the requried configuration by refering the below image.

![](img/external-ics-config.png)
