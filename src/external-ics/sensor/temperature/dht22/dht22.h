/*
 * File Name: dht22.h
 * File Path: /emmate/src/external-ics/sensor/temperature/dht22/dht22.h
 * Description:
 *
 *  Created on: 20-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef DHT22_H_
#define DHT22_H_

#include <stdbool.h>
#include "core_error.h"

#ifdef __cplusplus
extern "C" {
#endif


// == function prototypes =======================================

#define DHT22_HUMIDITY_ERR		-100		/**< DHT22 Humidity Error Value */
#define DHT22_TEMPERATURE_ERR	-100		/**< DHT22 Humidity Error Value */

/**
 * @brief	Configure DHT22 Data-out GPIO
 *
 * @param[in]	dout_gpio	DHT22 Data-out GPIO
 *
 * @return
 * 		- EM_OK on Success
 * 		- EM_ERR_INVALID_ARG DHT22 Data-out GPIO Parameter error
 */
em_err dht22_set_dout_gpio(int dout_gpio);

/**
 * @brief	Print DHT22 read error log
 *
 * @param	response	return value of DHT22 Data read process
 *
 */
void dht22_error_handler(em_err response);

/**
 * @brief	Execute DHT22 Data read process
 *
 * @note	DHT22 Sensor's Avg. Sensing Period time is 2s(as per datasheet).
 * 			So, Make sure the interval of whole process must be beyond 2 seconds.
 *
 * @return
 * 		- EM_OK on Success
 * 		- EM_ERR_TIMEOUT	Read Operation timed out
 * 		- EM_ERR_INVALID_CRC	CRC or checksum was invalid
 * 		- EM_ERR_INVALID_ARG DHT22 Data-out GPIO Parameter isn't configured
 */
em_err dht22_read();

/**
 * @brief	Get DHT22 Humidity value
 *
 * @return
 * 		- On Sensor Read Success, return current read Humidity Value
 * 		- On Sensor Read Failure, return 'DHT22_HUMIDITY_ERR' value
 */
float dht22_get_humidity();

/**
 * @brief	Get DHT22 Temperature value
 *
 * @return
 * 		- On Sensor Read Success, return current read Temperature Value
 * 		- On Sensor Read Failure, return 'DHT22_TEMPERATURE_ERR' value
 */
float dht22_get_temperature();

/**
 *
 */
int dht22_get_signal_level( int usTimeOut, bool state );


#ifdef __cplusplus
}
#endif


#endif /* DHT22_H_ */
