/*
 * File Name: mq5.h
 * File Path: /emmate/src/external-ics/sensor/gas-sensor/mq5/mq5.h
 * Description:
 *
 *  Created on: 29-Jan-2020
 *      Author: Noyel Seth
 */

#ifndef MQ5_H_
#define MQ5_H_

#include <stdbool.h>
#include "core_error.h"
#include "core_constant.h"
#include "core_config.h"


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief	Configure and Initialize MQ5 Sensor
 *
 * @param	mq5_an_gpio				MQ5 Analog Data-out GPIO
 *
 * @param	mq5_read_inteval_time	MQ5 Analog Data read interval time(milisec). (>= 1sec / 1000milisec)
 *
 * @param	adc_max_ref_volt		Max ADC voltage reference
 *
 * @param	adc_min_ref_volt		Min ADC voltage reference
 *
 * @return
 * 		- EM_OK on Success
 * 		- EM_ERR_INVALID_ARG MQ5 Analog Data-out GPIO Parameter error, Read interval Time error & Reference voltage error
 *		- EM_FAIL				if failed to start process
 *		- EM_ERR_NO_MEM		if failed to allocate memory for ADC configuration
 */
em_err mq5_init_sensor(int mq5_gpio, uint32_t mq5_read_inteval_time, float adc_max_ref_volt, float adc_min_ref_volt );




/**
 * @brief	Get Data(LPG Level [0-100%]) from MQ5 Sensor
 *
 * @param[out]	lpg_lvl		get MQ5 Sensors's read data from configured GPIO
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if MQ5 GPIO channel not valid
 */
em_err mq5_get_deteced_gas_level(float *lpg_lvl) ;



/**
 * @brief	De-Initialize the MQ5 sensor
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if MQ5 Sensor GPIO not valid or not Configures at Init.
 */
em_err mq5_deinit_sensor();


/**
 * @brief	Start the MQ5 sensor read processing
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if MQ5 Sensor GPIO not valid or not Configures at Init.
 */
em_err mq5_start_process() ;


/**
 * @brief	Stop the MQ5 sensor read processing
 *
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if MQ5 Sensor GPIO not valid or not Configures at Init.
 */
em_err mq5_stop_process();



#ifdef __cplusplus
}
#endif



#endif /* MQ5_H_ */
