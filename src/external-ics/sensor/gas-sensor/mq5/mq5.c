/*
 * File Name: mq5.c
 * File Path: /emmate/src/external-ics/sensor/gas-sensor/mq5/mq5.c
 * Description:
 *
 *  Created on: 29-Jan-2020
 *      Author: Noyel Seth
 */


#include "mq5.h"
#include "adc_core_channel.h"
#include "adc_core.h"
//#include "thing.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to defined 'CONFIG_USE_MQ5' before include the mq5.h
 */
#ifdef CONFIG_USE_MQ5

#define TAG LTAG_MQ5_LIB

int mq5_an_gpio = -1;
float mq5_adc_max_ref_volt=0;
float mq5_adc_min_ref_volt=0;
float mq5_adc_max_ref_mvolt=0;
float mq5_adc_min_ref_mvolt=0;

/*
 *
 */
em_err mq5_init_sensor(int mq5_gpio, uint32_t mq5_read_inteval_time, float adc_max_ref_volt, float adc_min_ref_volt )
{
	em_err ret = EM_FAIL;

	if(mq5_gpio < 0 ){
		return EM_ERR_INVALID_ARG;
	}

	if(mq5_an_gpio != -1){
		EM_LOGE(TAG, "Failed to Initialize, because MQ5 Sensor Already Initialize");
		return EM_PERIPHERAL_INITIALIZE;
	}

	if (adc_max_ref_volt < 0 || adc_min_ref_volt < 0) {
		EM_LOGE(TAG, "Failed to Initialize, because ADC Reference voltage are nor correct");
		return EM_ERR_INVALID_ARG;
	}else if(adc_max_ref_volt < adc_min_ref_volt){
		EM_LOGE(TAG, "Failed to Initialize, because Max ADC reference voltage less than Min ADC reference voltage are nor correct");
		return EM_ERR_INVALID_ARG;
	}

	if(mq5_read_inteval_time < DELAY_1_SEC){
		EM_LOGE(TAG, "Failed to Initialize, because MQ5 Sensor Read Interval less than 1sec or 1000milisec");
		return EM_ERR_INVALID_ARG;
	}


	mq5_an_gpio = mq5_gpio;

	mq5_adc_max_ref_volt = adc_max_ref_volt;
	mq5_adc_max_ref_mvolt = adc_max_ref_volt * 1000;

	mq5_adc_min_ref_volt = adc_min_ref_volt;
	mq5_adc_min_ref_mvolt = adc_min_ref_volt * 1000;

	// Initialize the MQ5 Gas Sensor's Analog Data Pin
	ret = init_adc_peripheral(mq5_an_gpio, mq5_read_inteval_time, ADC_CORE_WIDTH_10Bit, ADC_CORE_ATTEN_11db);

	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to Initialize the MQ5 LPG Gas Sensor");

		mq5_an_gpio = -1;
		mq5_adc_max_ref_volt = 0;
		mq5_adc_min_ref_volt = 0;
		mq5_adc_max_ref_mvolt = 0;
		mq5_adc_min_ref_mvolt = 0;

	}else{
		ret = mq5_stop_process();
	}

	return ret;

}

/*
 *
 */
em_err mq5_start_process() {
	em_err ret = EM_FAIL;
	ret = resume_adc_peripheral_read(mq5_an_gpio);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to start MQ5 Read Process");
		return ret;
	}
	return ret;
}

/*
 *
 */
em_err mq5_stop_process() {
	em_err ret = EM_FAIL;
	ret = pause_adc_peripheral_read(mq5_an_gpio);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to stop MQ5 Read Process");
		return ret;
	}
	return ret;
}



/**
 *
 */
em_err mq5_get_deteced_gas_level(float *lpg_lvl) {

	int adc_raw = 0;

	uint32_t adc_mV = 0;
	float lpg_level = 0;

	if (mq5_an_gpio < 0) {
		return EM_ERR_INVALID_ARG;
	}

	em_err res = get_adc_peripheral_data(mq5_an_gpio, &adc_raw, &adc_mV);
	if (res == EM_OK) {
		//EM_LOGI(TAG, "adc_mV = %d , adc_V = %.2f", adc_mV, (float )adc_mV / 1000);

		// calculate Gas detect Persentage
		lpg_level = ((adc_mV - mq5_adc_min_ref_mvolt)
				/ (mq5_adc_max_ref_mvolt - mq5_adc_min_ref_mvolt)) * 100;

		if (lpg_level < 0) {
			lpg_level = 0;
		} else if (lpg_level > 100) {
			lpg_level = 100;
		}

		EM_LOGD(TAG, "LPG Gas Detected %.2f%% ", lpg_level);
		*lpg_lvl = lpg_level;

	} else {
		EM_LOGE(TAG, "Failed to get Data From MQ5 LPG Gas Sensor");
	}

	return res;

}



/*
 *
 */
em_err mq5_deinit_sensor()
{
	em_err res = EM_FAIL;

	res = deinit_adc_peripheral(mq5_an_gpio);

	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to De-Initialize the MQ5 LPG Gas Sensor");

		mq5_an_gpio = -1;
		mq5_adc_max_ref_volt = 0;
		mq5_adc_min_ref_volt = 0;
		mq5_adc_max_ref_mvolt = 0;
		mq5_adc_min_ref_mvolt = 0;

	}

	return res;
}


#endif /* CONFIG_USE_MQ5 */


