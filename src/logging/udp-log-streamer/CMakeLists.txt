set(srcs
	udp_log_streamer_platform.c
	udp_log_streamer.c
	)

add_library(udp-log-streamer STATIC ${srcs})

target_include_directories(udp-log-streamer 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)