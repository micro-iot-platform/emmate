/*
 * persistent_mem_sys_hb.h
 *
 *  Created on: 06-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_SYS_HB_H_
#define PERSISTENT_MEM_SYS_HB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "core_common.h"

/**
 * @brief This function reads the System Heartbeat Frequency from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	p_hb_freq	Pointer to integer. Heartbeat frequency in seconds
 *
 * @return
 *		- EM_OK 		if successful
 *		- EM_FAIL		on failure
 *
 * */
em_err read_syshb_freq_from_persistent_mem(int *p_hb_freq);

/**
 * @brief This function writes the System Heartbeat Frequency to the selected persistent memory
 *
 * @param[in]	hb_freq		Integer value to be saved in the memory. Heartbeat frequency in seconds
 *
 * @return
 *		- EM_OK 		if successful
 *		- EM_FAIL		on failure
 *
 * */
em_err write_syshb_freq_to_persistent_mem(int hb_freq);

#ifdef __cplusplus
}
#endif

#endif /* PERSISTENT_MEM_SYS_HB_H_ */
