/*
 * persistent_mem_fota.c
 *
 *  Created on: 10-Oct-2019
 *      Author: Rohan Dey
 */

#include "persistent_mem_fota.h"
#include "persistent_mem_helper.h"
#include "migcloud_storage_util.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_FOTA
#include "fota_core.h"
#endif

#define TAG LTAG_PERSISTENT

#if CONFIG_USE_FOTA
/*
 * Read the fota id from persistent memory
 */
em_err read_fotaid_from_persistent_mem(char *fota_id) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_read_config_by_key(FOTA_ID_NVS_KEY, (void*) fota_id, FOTA_ID_LEN)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Reading FOTA ID from persistent memory failed");
	}
	return ret;
}

/*
 * Write the fota id into persistent memory
 */
em_err write_fotaid_to_persistent_mem(char *fota_id) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_write_config_by_key(FOTA_ID_NVS_KEY, (void*) fota_id, FOTA_ID_LEN)) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Writing FOTA ID to persistent memory failed");
	}
	return ret;
}
#endif
