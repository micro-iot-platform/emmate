/*
 * migcloud_storage_util.h
 *
 *  Created on: 27-Jan-2020
 *      Author: Rohan Dey
 */

#ifndef MIGCLOUD_STORAGE_UTIL_H_
#define MIGCLOUD_STORAGE_UTIL_H_

#ifdef __cplusplus
extern "C" {
#endif

// SOMTHING related NVS Key
#define SOMTHING_ID_NVS_KEY					"SOMTHING_ID"

// FOTA related NVS Key
#define FOTA_ID_NVS_KEY						"FOTA_ID"

// System Standby Mode related NVS Key
#define SYS_STANDBY_MODE_NVS_KEY			"STANDBY_MODE"
#define SYS_STANDBY_HBFREQ_NVS_KEY			"STANDBY_HBFREQ"

// System Heartbeat related NVS Key
#define SYS_HBFREQ_NVS_KEY					"SYSHBFREQ"


#ifdef __cplusplus
}
#endif

#endif /* MIGCLOUD_STORAGE_UTIL_H_ */
