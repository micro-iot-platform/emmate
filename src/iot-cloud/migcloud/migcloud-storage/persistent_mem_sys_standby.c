/*
 * persistent_mem_sys_standby.c
 *
 *  Created on: 05-Nov-2019
 *      Author: Rohan Dey
 */

#include "persistent_mem_sys_standby.h"
#include "persistent_mem_helper.h"
#include "migcloud_storage_util.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT

/*
 * Read the system standby mode from persistent memory
 */
em_err read_sys_standby_mode_from_persistent_mem(int *p_standby_mode) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_read_config_by_key(SYS_STANDBY_MODE_NVS_KEY, (void*) p_standby_mode, sizeof(int))) == EM_OK) {

	} else {
		EM_LOGW(TAG, "Reading Standby Mode from persistent memory failed");
	}
	return ret;
}

/*
 * Write the system standby mode into persistent memory
 */
em_err write_sys_standby_mode_to_persistent_mem(int standby_mode) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_write_config_by_key(SYS_STANDBY_MODE_NVS_KEY, (void*) &standby_mode, sizeof(int))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Writing Standby Mode to persistent memory failed");
	}
	return ret;
}

/*
 * Read the system standby heartbeat frequency from persistent memory
 */
em_err read_sys_standby_hbfreq_from_persistent_mem(int *p_standby_hb_freq) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_read_config_by_key(SYS_STANDBY_HBFREQ_NVS_KEY, (void*) p_standby_hb_freq, sizeof(int))) == EM_OK) {

	} else {
		EM_LOGW(TAG, "Reading Standby Heartbeat Frequency from persistent memory failed");
	}
	return ret;
}

/*
 * Write the system standby heartbeat frequency into persistent memory
 */
em_err write_sys_standby_hbfreq_to_persistent_mem(int standby_hb_freq) {
	em_err ret = EM_FAIL;

	if ((ret = persistent_write_config_by_key(SYS_STANDBY_HBFREQ_NVS_KEY, (void*) &standby_hb_freq, sizeof(int))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "Writing Standby Heartbeat Frequency to persistent memory failed");
	}
	return ret;
}
