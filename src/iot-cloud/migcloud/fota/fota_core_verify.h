/*
 * fota_core_verify.h
 *
 *  Created on: 09-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef FOTA_CORE_VERIFY_H_
#define FOTA_CORE_VERIFY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "fota_core.h"

/**
 * @brief 	Get the Current Running Image's state
 *
 *
 * @return
 * 		ota_img_states_t	See the enum documentation for details
*/
ota_img_states_t migcloud_get_running_fota_img_state();

/**
 * @brief OTA Image Verification Process
 *
 * This function is used for verifying the currently running image.
 * If any error occurs, then the system will rollback to the last running & valid image, else this image will be continued
 */
void migcloud_verify_running_firmware();


#ifdef __cplusplus
}
#endif

#endif /* FOTA_CORE_VERIFY_H_ */
