/*
 * fota_core_helper.c
 *
 *  Created on: 07-Aug-2019
 *      Author: Rohan Dey
 */

//#include "fota_core.h"
#include "fota_core_helper.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include <string.h>

#define TAG LTAG_OTA
#define FOTA_STATUS_RESPONSE_SIZE	(sizeof(int) + sizeof(EmError) + 1)
/*
 * {
 * 		"fota_id": "",
 * 		"fota_stat":123
 * 		"dl_try":123
 * 	}
 * */
typedef struct {
	char fota_id[FOTA_ID_LEN + 1];
	int fota_stat;
	int dl_try;
} FotaStatusRequest;

typedef struct {
	int status; /**< Sys Heartbeat Status. Success or Failure */
	EmError error; /**< Error object */
} FotaStatusResponse;

static em_err make_fota_status_request(char **ppbuf, int *plen, FotaStatusRequest *fotastat_req) {
//	em_err ret = EM_FAIL;
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	EM_LOGV(TAG, "Going to make Fota Status JSON with the following values:");

	/* Set JSON key value */
	EM_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(fotastat_req->fota_id, "->"), fotastat_req->fota_id);
	json_object_set_string(root_object, GET_VAR_NAME(fotastat_req->fota_id, "->"), fotastat_req->fota_id);

	EM_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(fotastat_req->fota_stat, "->"), fotastat_req->fota_stat);
	json_object_set_number(root_object, GET_VAR_NAME(fotastat_req->fota_stat, "->"), (int) fotastat_req->fota_stat);

	if (fotastat_req->fota_stat == FOTA_STATUS_DOWNLOADING) {
		EM_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(fotastat_req->dl_try, "->"), fotastat_req->dl_try);
		json_object_set_number(root_object, GET_VAR_NAME(fotastat_req->dl_try, "->"), (int) fotastat_req->dl_try);
	}

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	EM_LOGD(TAG, "SYS Heartbeat Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		EM_LOGE(TAG, "make_fota_status_request malloc failed!");
		return EM_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return EM_OK;
}

static em_err parse_fota_status_response(char *json_buff, FotaStatusResponse *fotastat_resp) {
	em_err ret = EM_FAIL;

	/* Parse the common info: stat and error */
	ret = inproc_parse_json_common_info(json_buff, &fotastat_resp->status, &fotastat_resp->error);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		ret = EM_FAIL;
	}

	return ret;
}
#if 0
em_err _send_fota_status_to_server(char *fota_id, FOTA_STATUSES fota_stat, int fota_try) {
	em_err ret = EM_FAIL;

	char *http_resp = NULL;
	size_t http_resp_len = 0;
	char *json_buf = NULL;
	int json_len = 0;

	FotaStatusRequest fotastat_req;
	memset(&fotastat_req, 0, sizeof(FotaStatusRequest));

	strcpy(fotastat_req.fota_id, fota_id);
	fotastat_req.fota_stat = fota_stat;
	fotastat_req.dl_try = fota_try;

	ret = make_fota_status_request(&json_buf, &json_len, &fotastat_req);
	EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(FOTA_STATUS_RESPONSE_SIZE, sizeof(char));
	if (http_resp == NULL) {
		EM_LOGE(TAG, "memory allocation for http response failed");
		goto free_memory;
	}
	memset(http_resp, 0, FOTA_STATUS_RESPONSE_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
	ret = do_http_operation(IQ_SYS_FOTASTATUS_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
	IQ_HOST, HTTP_USER_AGENT,
	CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len, FOTA_STATUS_RESPONSE_SIZE, &http_stat);

	if (ret == EM_OK) {
		EM_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);

		FotaStatusResponse fotastat_resp;
		memset(&fotastat_resp, 0, sizeof(FotaStatusResponse));

		ret = parse_fota_status_response(http_resp, &fotastat_resp);
		if (ret != EM_OK) {
			EM_LOGE(TAG, "Fota Status Response parsing failed, don't know what to do yet!!");
			// TODO: Handle error
		}
	} else {
		EM_LOGE(TAG, "HTTP failed with status code = [ %d ]!", http_stat);
		ret = EM_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	EM_LOGD(TAG, "Freeing allocated response memory");
	free(json_buf);
	free(http_resp);

	return ret;
}
#endif

em_err migcloud_save_fota_identifier(char *fota_id) {
	em_err ret = EM_FAIL;
#if CONFIG_USE_PERSISTENT

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)

	if (strlen(fota_id) > 0) {
		/* Save fota_id into Persistent Memory */
		if ((ret = write_fotaid_to_persistent_mem(fota_id)) == EM_OK) {
		} else {
			EM_LOGE(TAG, "write_fotaid_to_persistent_mem failed!");
		}
	}

#elif (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC)
	// TODO: Save data in file
#endif

#endif
	return ret;
}
