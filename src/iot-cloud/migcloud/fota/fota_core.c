/*
 * File Name: ota_core.c
 * File Path: /emmate/src/ota/ota_core.c
 * Description:
 *
 *  Created on: 25-Apr-2019
 *      Author: Rohan Dey
 */

#include "fota_core.h"
#include "fota_core_verify.h"
#include "fota_core_helper.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "migcloud_http_status_update.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include "threading.h"
#include "module_thread_priorities.h"
#include "event_group_core.h"
#include "conn.h"
#include "systime.h"

#if TESTING
#include "ota_testing_codes.h"
#endif

#include <string.h>
#include <stdlib.h>

#define TAG LTAG_OTA

typedef struct {
//	QueueHandle fota_sch_q;
//	QueueHandle fota_task_q;
	TaskHandle fota_thread;
//	bool fota_running;
//	bool fota_manager_init_stat;
} FotaHelperData;

static FotaHelperData m_fhd = { .fota_thread = NULL }; //, .fota_running = false, .fota_manager_init_stat = false };

static em_err download_new_firmware(HttpClientConfig *config) {
	em_err ret = EM_FAIL, ota_finish_ret = EM_FAIL;

	if (
#if CONFIG_USE_INTERNET_CHECKER
	(get_network_conn_status() & NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE) == NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE
#else
	((get_network_conn_status() & NETCONNSTAT_DISCONNECTED) == NETCONNSTAT_DISCONNECTED) || ((get_network_conn_status() & NETCONNSTAT_CONNECTING) == NETCONNSTAT_CONNECTING)
#endif
	) {
#if CONFIG_USE_INTERNET_CHECKER
		EM_LOGE(TAG, "IoT Cloud not present");
		return ret = EM_ERR_INVALID_STATE;
#else
		EM_LOGE(TAG, "Network not present");
		return ret=EM_ERR_INVALID_STATE;
#endif
	}

	ota_https_ota_config_t ota_config = { .http_config = config, };
	ota_https_ota_handle_t https_ota_handle = NULL;

	ret = ota_https_ota_begin(&ota_config, &https_ota_handle);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "HTTPS OTA Begin failed!");
		return EM_FAIL;
	}

#define IMAGE_SIZE_OFFSET_BYTES	(10*1024)
	uint32_t image_bytes = 0, prev_bytes = 0;

	while (1) {
		ret = ota_https_ota_perform(https_ota_handle);
		if (ret != OTA_ERR_HTTPS_OTA_IN_PROGRESS) {
			break;
		}
		// ota_https_ota_perform returns after every read operation which gives user the ability to
		// monitor the status of OTA upgrade by calling ota_https_ota_get_image_len_read, which gives length of image
		// data read so far.
//		EM_LOGW(TAG, "Image bytes read: %d", ota_https_ota_get_image_len_read(https_ota_handle));

		image_bytes = ota_https_ota_get_image_len_read(https_ota_handle);
		if (image_bytes > (prev_bytes + IMAGE_SIZE_OFFSET_BYTES)) {
			EM_LOGW(TAG, "Image bytes read: %0.2f kB", (float )(image_bytes / 1024));
			prev_bytes = image_bytes;
		}
	}

	ota_finish_ret = ota_https_ota_finish(https_ota_handle);
	if ((ret == EM_OK) && (ota_finish_ret == EM_OK)) {
		EM_LOGI(TAG, "HTTPS OTA download successful");
		ret = EM_OK;
	} else {
		EM_LOGE(TAG, "HTTPS OTA download failed!");
		ret = EM_FAIL;
	}
	return ret;
}

static TaskRet fota_task(void * pvParameter) {
	EM_LOGI(TAG, "Starting FOTA process...");

	em_err ret = EM_FAIL;

	FotaInfo *fota_data = (FotaInfo*) pvParameter;

	EM_LOGD(TAG, "Going to do FOTA with the following information:");
	EM_LOGD(TAG, "id = %s", fota_data->id ? fota_data->id : "no id");
//	EM_LOGD(TAG, "stat = %d", fota_data->stat);
	EM_LOGD(TAG, "url_len = %d", (int )fota_data->url_len);
	EM_LOGD(TAG, "url = %s", fota_data->url ? fota_data->url : "no url");
#if 0
	EM_LOGD(TAG, "cert_len = %d", (int )fota_data->cert_len);
	EM_LOGD(TAG, "cert : %s", fota_data->cert ? "cert is present" : "no cert");
#endif
	EM_LOGD(TAG, "ver = %s", fota_data->ver);
	EM_LOGD(TAG, "fname = %.*s", CONFIG_FIRMWARE_FILENAME_MAX_LEN, fota_data->fname);
	EM_LOGD(TAG, "fsize = %d", (int )fota_data->fsize);

	// TODO: Save the scheduled fota time in the NV memory so that we can check after reboot
	char strftime_buf[64];
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &fota_data->sch);
	EM_LOGD(TAG, "sch = %s", strftime_buf);

	MigcloudFotaStatus migcloud_stat;
	memset(&migcloud_stat, 0, sizeof(MigcloudFotaStatus));
	strcpy(migcloud_stat.fota_id, fota_data->id);

	int dnl_try_count = 1;
	do {
		do {
			/* Send DOWNLOADING status to server */
			/*_send_fota_status_to_server(fota_data->id, FOTA_STATUS_DOWNLOADING, dnl_try_count);*/
			migcloud_stat.dl_try = dnl_try_count;
			ret = migcloud_send_status_via_http(MIGCLOUD_TASK_FOTA, MIGCLOUD_STATUS_DOWNLOADING, &migcloud_stat);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not update MIGCLOUD_STATUS_DOWNLOADING status to server, retrying ...");
				TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
			}
		} while (ret != EM_OK);

		HttpClientConfig config = { .url = fota_data->url, .cert_pem = (char *) /*fota_data->cert*/IQ_HOST_ROOTCA,
		/*.event_handler = fota_http_event_handler,*/};
//		em_err ret = ota_perform_https_ota(&config);
		ret = download_new_firmware(&config);

		if (ret == EM_OK) {
			EM_LOGI(TAG, "FOTA is Successful, acknowledging the server");

			do {
				/* Send DOWNLOADED status to server */
				/*_send_fota_status_to_server(fota_data->id, FOTA_STATUS_DOWNLOADED, 0);*/
				migcloud_stat.dl_try = 0;
				ret = migcloud_send_status_via_http(MIGCLOUD_TASK_FOTA, MIGCLOUD_STATUS_DOWNLOADED, &migcloud_stat);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "Could not update MIGCLOUD_STATUS_DOWNLOADED status to server, retrying ...");
					TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
				}
			} while (ret != EM_OK);

			// TODO: If scheduled time not received from server, do fota
			EM_LOGI(TAG, "The next reboot will start the new firmware");
			EM_LOGI(TAG, "Scheduled time for starting the new firmware is: %s", strftime_buf);

			while (1) {
				double remaining_secs = diff_time_with_now(&fota_data->sch);
				if (remaining_secs == (-DBL_MAX)) {
					EM_LOGD(TAG, "diffence in time returned -DBL_MAX, hence system time is not set");
					remaining_secs = -1;
				}

				EM_LOGI(TAG, "Remaining time for booting the new firmware is: %lf secs", remaining_secs);
				if (remaining_secs <= 0) {
					/* call system's event dispatcher to notify other threads about a system reboot */
					/* TODO: To change the reboot event dispatcher to multiple modules */
					EM_LOGI(TAG, "Dispatching reboot event ...");
					notify_system_reboot_event();
//					EM_LOGI(TAG, "Waiting for all modules to complete their tasks");

					/* Save the fota id. Required for image verify after reboot */
					migcloud_save_fota_identifier(fota_data->id);

					do {
						/* Send UPDATING status to server */
						/*_send_fota_status_to_server(fota_data->id, FOTA_STATUS_UPDATING, 0);*/
						migcloud_stat.dl_try = 0;
						ret = migcloud_send_status_via_http(MIGCLOUD_TASK_FOTA, MIGCLOUD_STATUS_UPDATING,
								&migcloud_stat);
						if (ret != EM_OK) {
							EM_LOGE(TAG, "Could not update MIGCLOUD_STATUS_UPDATING status to server, retrying ...");
							TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
						}
					} while (ret != EM_OK);

					// REBOOTING ...
					core_system_restart();
				} else {
					TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
				}
			}
		} else {
			EM_LOGE(TAG, "Failed to download the new firmware. Retrying ...");
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
		}
	} while (++dnl_try_count < CONFIG_FOTA_MAX_RETRY_COUNT);

	EM_LOGE(TAG, "================================ FOTA FAILED ================================");

	do {
		/* Send DOWNLOAD_FAILED status to server */
		/*_send_fota_status_to_server(fota_data->id, FOTA_STATUS_DOWNLOAD_FAILED, 0);*/
		migcloud_stat.dl_try = 0;
		ret = migcloud_send_status_via_http(MIGCLOUD_TASK_FOTA, MIGCLOUD_STATUS_DOWNLOAD_FAILED, &migcloud_stat);
		if (ret != EM_OK) {
			EM_LOGE(TAG, "Could not update MIGCLOUD_STATUS_DOWNLOAD_FAILED status to server, retrying ...");
			TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
		}
	} while (ret != EM_OK);

//	EM_LOGI(TAG, "Stack remaining for task '%s' is %d bytes", TaskGetTaskName(NULL),
//			TaskPfGetStackHighWaterMark(NULL));
	m_fhd.fota_thread = NULL;

#if defined (CONFIG_PLATFORM_ESP_IDF)
	TaskDelete(NULL);
#endif
}

/*
 * Start A task for completing the OTA Process
 */
em_err migcloud_start_fota_task(FotaInfo *recv_fota_info) {

	if (m_fhd.fota_thread != NULL) {
		EM_LOGI(TAG, "A fota download is already running. Not starting another one.");
		return EM_FAIL;
	}

	static FotaInfo fota_info;
	memcpy(&fota_info, recv_fota_info, sizeof(FotaInfo));

	BaseType xReturned = TaskCreate(&fota_task, "fota-task", TASK_STACK_SIZE_8K, &fota_info, THREAD_PRIORITY_FOTA,
			&m_fhd.fota_thread);
	if (true == xReturned) {
		EM_LOGD(TAG, "Ota task started");
		return EM_OK;
	}
	return EM_FAIL;
}

#if 0
static void fota_manager(void * pvParameter) {
	EM_LOGI(TAG, "fota_manager task started");
	FotaInfo fota_info;
	TaskHandle fota_task_handle;

	while (1) {
		/* Check queue, if no items then continue loop */
		if (QueueMessagesWaiting(m_fhd.fota_sch_q) <= 0) {
			TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
			continue;
		}
		/* Get Fota Info from queue */
		QueueReceive(m_fhd.fota_sch_q, &fota_info, THREADING_MAX_DELAY);

		BaseType xReturned = TaskCreate(&fota_task, "fota-task", TASK_STACK_SIZE_8K, &fota_info, THREAD_PRIORITY_5,
				&m_fhd.fota_thread);
		if (thread_stat == false) {
			EM_LOGE(TAG, "Failed to create fota task thread");
//			return EM_FAIL;
		}

		/* Get Fota Task completion status from queue, wait indefinitely */
		QueueReceive(m_fhd.fota_task_q, &fota_task_handle, THREADING_MAX_DELAY);
		if (fota_task_handle == m_fhd.fota_thread) {
			TaskDelete(m_fhd.fota_thread);
		}
	}
}

em_err start_fota_manager() {
	if (m_fhd.fota_manager_init_stat) {
		return EM_OK;
	}

	/* Create a FOTA Scheduling Queue to receive new FOTA notification from sys-heartbeat */
	m_fhd.fota_sch_q = QueueCreate(MAX_FOTA_REQUEST_COUNT, sizeof(FotaInfo));
	if (m_fhd.fota_sch_q == 0) {
		EM_LOGE(TAG, "Failed to create FOTA Scheduling Queue");
		return EM_FAIL;
	}

	/* Create a queue to get the Thread ID of the FOTA task which has completed. This has to be deleted */
	m_fhd.fota_task_q = QueueCreate(1, sizeof(TaskHandle));
	if (m_fhd.fota_task_q == 0) {
		EM_LOGE(TAG, "Failed to create Thread ID queue");
		return EM_FAIL;
	}

	/* Create the FOTA manager thread */
	BaseType xReturned = TaskCreate(&fota_manager, "fota-manager", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_8, NULL);
	if (thread_stat == false) {
		EM_LOGE(TAG, "Failed to create fota manager thread");
		return EM_FAIL;
	}

	m_fhd.fota_manager_init_stat = true;

	return EM_OK;
}
#endif
