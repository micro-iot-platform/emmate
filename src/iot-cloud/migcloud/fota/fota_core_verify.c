/*
 * fota_core_verify.c
 *
 *  Created on: 09-Aug-2019
 *      Author: Rohan Dey
 */
#include "fota_core_verify.h"
#include "fota_core_helper.h"
#include "migcloud_http_status_update.h"
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include "system_utils.h"
#include "event_group_core.h"
#include "conn.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define TAG LTAG_OTA

/*
 * Get the Current Running Image's state.
 *
 * Note For ESP32:
 * 		Enable OTA Rollbackfrom make menuconfig->Bootloader Config->Enable app rollback support
 */
ota_img_states_t migcloud_get_running_fota_img_state() {
#if CONFIG_USE_FOTA
	ota_img_states_t state = OTA_IMG_INVALID;
	const ota_partition_t *current_partition = NULL;

	current_partition = ota_get_running_partition();

	/*Display the running partition*/
	EM_LOGI(TAG, "Running partition: %s", current_partition->label);

	em_err ret = ota_get_state_partition(current_partition, &state);
	if (ret == EM_OK) {
		EM_LOGI(TAG, "Current ota state 0x%x", state);
		if (OTA_IMG_PENDING_VERIFY == state || OTA_IMG_VALID == state) {
			return state;
		}
	} else if (EM_ERR_INVALID_ARG) {
		// TODO: error handle
	} else if (EM_ERR_NOT_SUPPORTED) {
		// TODO: error handle
	} else if (EM_ERR_NOT_FOUND) {
		// TODO: error handle
	}

	/* If return value of ota_get_state_partition() is not EM_OK, then handle the error. */
	//state = OTA_IMG_INVALID;
	return state;
#else
	return 0;
#endif
}

static em_err img_verify() {
	em_err ret = EM_FAIL;

	/*
	 * TODO: Verify the Downloaded & Running Image
	 *
	 */
	EM_LOGW(TAG, "Image Verify successful\r\n");
	ret = EM_OK;

	return ret;
}

/**
 * @brief OTA Image Verification Process
 *
 * This function user for verify the Current Running Image
 * to find out the Fault in it.
 * If Any error occur, Then the system will rollback to
 * last running & valid Image or else go on with the new Image..
 */
void migcloud_verify_running_firmware() {
#if CONFIG_USE_FOTA
	EM_LOGI(TAG, "Verifying the newly started firmware image");

	ota_img_states_t state = migcloud_get_running_fota_img_state();
	if (state == OTA_IMG_PENDING_VERIFY) {
		EM_LOGI(TAG, "Firmware image is pending to be verified");

		/*char fota_id[FOTA_ID_LEN+1];*/
		MigcloudFotaStatus migcloud_stat;
		memset(&migcloud_stat, 0, sizeof(MigcloudFotaStatus));

		em_err ret = read_fotaid_from_persistent_mem(migcloud_stat.fota_id);
		if (ret == EM_FAIL) {
			// TODO: Handle error
			EM_LOGE(TAG, "read_fotaid_from_persistent_mem failed. Don't know what to do yet!");
			return;
		}
//		/* Wait until we have a network IP */
//		event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);
//		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);

#if CONFIG_USE_INTERNET_CHECKER
	/* Wait until we make a stable connection with Cloud/Internet */
	event_group_wait_bits(get_system_evtgrp_hdl(), INTERNET_AVAILABLE_BIT, false, true, EventMaxDelay);
#else
	EM_LOGD(TAG, "Waiting for network IP ...");
	/* Wait until we have a network IP */
	event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);
#endif

		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);

#if CONFIG_USE_INTERNET_CHECKER
		while (get_network_conn_status() == NETCONNSTAT_INTERNET_NOT_AVAILABLE) {

		}
#else
		while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {

		}
#endif


		/* execute OTA Image Verification for validate that Its Ours release Binary Image. */
		ret = img_verify();
		if (EM_OK == ret) {
			EM_LOGI(TAG, "The image is verified and OK to run, informing the server");
			ota_mark_app_valid_cancel_rollback();

			do {
				/* Send FOTA_STATUS_UPDATED status to server */
				/*_send_fota_status_to_server(fota_id, FOTA_STATUS_UPDATED, 0);*/
				migcloud_stat.dl_try = 0;
				ret = migcloud_send_status_via_http(MIGCLOUD_TASK_FOTA, MIGCLOUD_STATUS_UPDATED, &migcloud_stat);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "Could not update MIGCLOUD_STATUS_UPDATED status to server, retrying ...");
					TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
				}
			} while (ret != EM_OK);

		} else {
			EM_LOGE(TAG, "The image could not be verified! Inform the server and rollback to previous version");
			ota_mark_app_invalid_rollback_and_reboot();

			do {
				/* Send FOTA_STATUS_UPDATE_FAILED status to server */
				/*_send_fota_status_to_server(fota_id, FOTA_STATUS_UPDATE_FAILED, 0);*/
				migcloud_stat.dl_try = 0;
				ret = migcloud_send_status_via_http(MIGCLOUD_TASK_FOTA, MIGCLOUD_STATUS_UPDATE_FAILED, &migcloud_stat);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "Could not update MIGCLOUD_STATUS_UPDATE_FAILED status to server, retrying ...");
					TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
				}
			} while (ret != EM_OK);
		}
	} else if (state == OTA_IMG_VALID) {
		EM_LOGI(TAG, "The running firmware is already verified, no need to verify it again...");
		/* Image is Valid, So no need to execute Self-test & Image Verification. Good to Continue */
	}
#endif /* CONFIG_USE_FOTA */
}
