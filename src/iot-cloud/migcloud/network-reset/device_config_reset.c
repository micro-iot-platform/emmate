/*
 * device_config_reset.c
 *
 *  Created on: 23-Oct-2019
 *      Author: Rohan Dey
 */

#include "device_config_reset.h"
#include "migcloud_http_status_update.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_HMI
#include "system_hmi_led_notification.h"
#endif
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include <string.h>

#define TAG LTAG_MIG_NETRESET

#if 0
#define DEVCFG_STATUS_RESPONSE_SIZE	(sizeof(int) + sizeof(EmError) + 1)

typedef struct {
	int devcfg_stat;
} DeviceConfigResetRequest;

typedef struct {
	int status; /**< Status. Success or Failure */
	EmError error; /**< Error object */
} DeviceConfigResetResponse;
static em_err make_devcfg_status_request(char **ppbuf, int *plen, DeviceConfigResetRequest *devcfgstat_req) {
//	em_err ret = EM_FAIL;
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	EM_LOGV(TAG, "Going to make Device Configuration Reset Status JSON with the following values:");

	/* Set JSON key value */
	EM_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(devcfgstat_req->devcfg_stat, "->"), devcfgstat_req->devcfg_stat);
	json_object_set_number(root_object, GET_VAR_NAME(devcfgstat_req->devcfg_stat, "->"),
			(int) devcfgstat_req->devcfg_stat);

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	EM_LOGD(TAG, "Device Configuration Reset Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		EM_LOGE(TAG, "make_devcfg_status_request malloc failed!");
		return EM_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return EM_OK;
}

static em_err parse_devcfg_status_response(char *json_buff, DeviceConfigResetResponse *devcfgstat_resp) {
	em_err ret = EM_FAIL;

	/* Parse the common info: stat and error */
	ret = inproc_parse_json_common_info(json_buff, &devcfgstat_resp->status, &devcfgstat_resp->error);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		ret = EM_FAIL;
	}

	return ret;
}

static em_err send_devcfg_stat_to_server(bool devcfg_stat) {
	em_err ret = EM_FAIL;

	char *http_resp = NULL;
	size_t http_resp_len = 0;
	char *json_buf = NULL;
	int json_len = 0;

	DeviceConfigResetRequest devcfgstat_req;
	memset(&devcfgstat_req, 0, sizeof(DeviceConfigResetRequest));

	devcfgstat_req.devcfg_stat = (int) devcfg_stat;

	ret = make_devcfg_status_request(&json_buf, &json_len, &devcfgstat_req);
	EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(DEVCFG_STATUS_RESPONSE_SIZE, sizeof(char));
	if (http_resp == NULL) {
		EM_LOGE(TAG, "memory allocation for http response failed");
		goto free_memory;
	}
	memset(http_resp, 0, DEVCFG_STATUS_RESPONSE_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
	ret = do_http_operation(IQ_SYS_DEVCFG_STATUS_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
	IQ_HOST, HTTP_USER_AGENT,
	CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len, DEVCFG_STATUS_RESPONSE_SIZE,
			&http_stat);

	if (ret == EM_OK) {
		EM_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);

		DeviceConfigResetResponse devcfgstat_resp;
		memset(&devcfgstat_resp, 0, sizeof(DeviceConfigResetResponse));

		ret = parse_devcfg_status_response(http_resp, &devcfgstat_resp);
		if (ret != EM_OK) {
			EM_LOGE(TAG, "Device Config Reset Status Response parsing failed, don't know what to do yet!!");
			// TODO: Handle error
		}
	} else {
		EM_LOGE(TAG, "HTTP failed with status code = [ %d ]!", http_stat);
		ret = EM_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	EM_LOGD(TAG, "Freeing allocated response memory");
	free(json_buf);
	free(http_resp);

	return ret;
}
#endif


static migcloud_netreset_function m_netreset_func = NULL;

em_err migcloud_register_netreset_event_function(migcloud_netreset_function deviceconfigreset_func) {
	if (deviceconfigreset_func != NULL) {
		m_netreset_func = deviceconfigreset_func;
		return EM_OK;
	} else {
		return EM_ERR_INVALID_ARG;
	}
}

static void notify_device_config_reset_event() {
	if (m_netreset_func != NULL)
		m_netreset_func();
}

void do_devcfg_reset_operation(bool devcfg_reset) {
	em_err ret = EM_FAIL;

	/* Notify the application about the device configuration reset event */
	notify_device_config_reset_event();

	show_system_resetting_notification();
	TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);

	/* Notify the application about a system reboot event */
	notify_system_reboot_event();

#if CONFIG_USE_PERSISTENT
	EM_LOGW(TAG, "Device Configuration Reset Request Received! Resetting the persistent memory ...");
//		em_err ret = erase_configs_from_persistent_mem();
	ret = reset_persistent_mem_for_reconfig();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to erase persistent memory, Device Configuration Reset is not successful");
		ret = EM_FAIL;
	}
#endif

	/* Inform the server */
	if (ret == EM_OK) {
		ret = migcloud_send_status_via_http(MIGCLOUD_TASK_DEVICE_CONFIG, MIGCLOUD_STATUS_PROCESSED, NULL);
	} else {
		ret = migcloud_send_status_via_http(MIGCLOUD_TASK_DEVICE_CONFIG, MIGCLOUD_STATUS_PROCESS_FAILED, NULL);
	}

	EM_LOGW(TAG, "Dispatching reboot event ...");
	core_system_restart();
}
