/*
 * sys_heartbeat_util.c
 *
 *  Created on: 12-Nov-2019
 *      Author: Rohan Dey
 */

#include "sys_heartbeat_util.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#include <string.h>

#define TAG	LTAG_MIG_HB

static SysHbSystemConfig syshb_sysconfig;

em_err migcloud_set_hb_interval(uint32_t interval_sec) {
	if (interval_sec < SYS_HB_MIN_INTERVAL)
		return EM_ERR_INVALID_ARG;

	syshb_sysconfig.hb_interval_sec = interval_sec;
	return EM_OK;
}

uint32_t migcloud_get_hb_interval() {
	return syshb_sysconfig.hb_interval_sec;
}

em_err migcloud_set_standby_hb_interval(uint32_t interval_sec) {
	if (interval_sec < SYS_HB_MIN_INTERVAL)
		return EM_ERR_INVALID_ARG;

	syshb_sysconfig.hb_standby_interval_sec = interval_sec;
	return EM_OK;
}

uint32_t migcloud_get_standby_hb_interval() {
	return syshb_sysconfig.hb_standby_interval_sec;
}

void migcloud_init_hb_default_val() {
	em_err ret = EM_FAIL;
#if CONFIG_USE_PERSISTENT
	ret = read_syshb_freq_from_persistent_mem((int*) &syshb_sysconfig.hb_interval_sec);
	if (ret != EM_OK) {
		EM_LOGW(TAG, "No Heartbeat Frequency is saved, setting the Heartbeat Frequency to %d", SYS_HB_INTERVAL_SEC);
		write_syshb_freq_to_persistent_mem(SYS_HB_INTERVAL_SEC);
		migcloud_set_hb_interval(SYS_HB_INTERVAL_SEC);
	}
	ret = read_sys_standby_hbfreq_from_persistent_mem((int*) &syshb_sysconfig.hb_standby_interval_sec);
	if (ret != EM_OK) {
		EM_LOGW(TAG, "No Standby Heartbeat Frequency is saved, setting the Standby Heartbeat Frequency to %d",
				SYS_HB_INTERVAL_SEC);
		write_sys_standby_hbfreq_to_persistent_mem(SYS_HB_INTERVAL_SEC);
		migcloud_set_standby_hb_interval(SYS_HB_INTERVAL_SEC);
	}
#else
	migcloud_set_standby_hb_interval(SYS_HB_INTERVAL_SEC);
	migcloud_set_hb_interval(SYS_HB_INTERVAL_SEC);
#endif
}
