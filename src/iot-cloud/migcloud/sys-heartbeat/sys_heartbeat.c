/*
 * File Name: sys_heartbeat.c
 * File Path: /emmate/src/system/sys-heartbeat/sys_heartbeat.c
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#include "sys_heartbeat.h"
#include "sys_heartbeat_parser.h"
#include "sys_heartbeat_util.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "threading.h"
#include "module_thread_priorities.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include "event_group_core.h"
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#if CONFIG_USE_CONN
#include "conn.h"
#endif
#include "app_version.h"
#include <string.h>


#define TAG	LTAG_MIG_HB

em_err get_sysconfig_from_server(char *req_data, int req_len);
em_err get_configurations_for_application(char *req_data, int req_len);
void do_devcfg_reset_operation(bool networkreset);
void do_system_standby_operation(SYSTEM_STANDBY_MODE standby_status, int hb_standby_interval_sec);
void do_release_somthing_operation();


typedef struct {
	TaskHandle hb_thread;
//	SYSHB_STATES hb_state;
//	uint32_t hb_interval_sec;
	SYSTEM_STANDBY_MODE standby;
//	int hb_standby_interval_sec;
} SysHeartbeatData;

static SysHeartbeatData syshb;

//{"status":true,"error":null,"sysconfig":{"freq":0,"standby":false,"standby_freq":0},"fota":{"id":"77","url":"https://13.234.183.14:8443/web-file-dir/secure/23ee34b7/test-app.bin","url_len":68,"ver":"0","fname":"test-app.bin","fsize":1441312,"sch":"1567613162555"},"conf":false}
#define SYS_HB_HTTP_RESPONSE_SIZE	(1*512)
#define SYSHB_MAX_RETRY				5

static em_err make_sys_heartbeat_data(SysHeartbeatRequest *hb_req, char **json_buf, int *json_len) {
	em_err ret = EM_FAIL;

	get_somthing_id(hb_req->somthing_id);
	strcpy(hb_req->core_version, CORE_DEV_VERSION_NUMBER);
#ifdef APP_DEV_VERSION_NUMBER
	strcpy(hb_req->app_version, APP_DEV_VERSION_NUMBER);
#else
	strcpy(hb_req->app_version, "NO APP VERSION FOUND");
#endif

//	get_netifinfo((void*)&hb_req->netifinfo);
//	get_netif_info(&hb_req->netifinfo);


	ret = migcloud_make_hb_request_json(json_buf, json_len, hb_req);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "make_sys_hb_request_json failed!");
	}
	return ret;
}

static em_err validate_sys_heartbeat_response(SysHeartbeatResponse *hb_resp) {
	em_err ret = EM_FAIL;

	if (hb_resp->status) {
		ret = EM_OK;
	}

	return ret;
}

#if CONFIG_USE_FOTA
static void notify_fota_module(SysHeartbeatResponse *hb_resp) {

	EM_LOGD(TAG, "Calling the FOTA module to begin the FOTA process");
	migcloud_start_fota_task(&hb_resp->fota);
}
#endif

static TaskRet perform_system_heartbeat(void * params) {
	em_err ret;
	int hb_retry = 0;
	bool retry_http_oper = false;
	char *http_response = NULL;
	size_t http_response_len = 0;

	EM_LOGD(TAG, "Waiting for START_SYSHEARTBEAT_BIT event ...");
	/* Wait until we get a confirmation to start the heartbeat */
	event_group_wait_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT, true, false, EventMaxDelay);

#if CONFIG_USE_INTERNET_CHECKER
	/* Wait until we make a stable connection with Cloud/Internet */
	event_group_wait_bits(get_system_evtgrp_hdl(), INTERNET_AVAILABLE_BIT, false, true, EventMaxDelay);
#else
	EM_LOGD(TAG, "Waiting for network IP ...");
	/* Wait until we have a network IP */
	event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);
#endif

	while (1) {
		/* Make heartbeat JSON */
		EM_LOGD(TAG, "Starting Sys Heartbeat ...");

		/* Make the SYS Heartbeat request data to be posted to server */
		char *json_buf;
		int json_len;
		SysHeartbeatRequest hb_req;
		memset(&hb_req, 0, sizeof(SysHeartbeatRequest));
		ret = make_sys_heartbeat_data(&hb_req, &json_buf, &json_len);
		EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

		/* Initialize a SYS Heartbeat response data to be populated after receiving the response from server */
		SysHeartbeatResponse hb_resp;
		memset(&hb_resp, 0, sizeof(SysHeartbeatResponse));

		/* Allocate memory for the http response */
		http_response = (char*) calloc(SYS_HB_HTTP_RESPONSE_SIZE, sizeof(char));
		if (http_response == NULL) {
			EM_LOGE(TAG, "memory allocation for http response failed");
			retry_http_oper = true;
			goto free_memory;
		}
		memset(http_response, 0, SYS_HB_HTTP_RESPONSE_SIZE);
		http_response_len = 0;

		/* Do http operation */
		uint16_t http_stat = 0;
		if (
	#if CONFIG_USE_INTERNET_CHECKER
				(get_network_conn_status() & NETCONNSTAT_IOT_CLOUD_AVAILABLE) == NETCONNSTAT_IOT_CLOUD_AVAILABLE
	#else

				(get_network_conn_status() & NETCONNSTAT_CONNECTED)	== NETCONNSTAT_CONNECTED
	#endif
		) {
#if (!MIGCLOUD_API_TEST)
			ret = do_http_operation(IQ_SYS_HEARTBEAT_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
			IQ_HOST, HTTP_USER_AGENT,
			CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_response, &http_response_len, SYS_HB_HTTP_RESPONSE_SIZE,
					&http_stat);
#else
			ret = do_http_operation("http://api.myjson.com/bins/uoty2", 8080, NULL, HTTP_CLIENT_METHOD_GET,
			"api.myjson.com", HTTP_USER_AGENT,
			CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_response, &http_response_len, SYS_HB_HTTP_RESPONSE_SIZE,
					&http_stat);
#endif

			if (ret == EM_OK) {
				EM_LOGI(TAG, "Response: %.*s", http_response_len, http_response);

				/* Parse the http response data */
				EM_LOGD(TAG, "Going to parse SYS Heartbeat response");
				ret = migcloud_parse_hb_response_json(http_response, &hb_resp);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "Sys Heartbeat response parsing failed! Free memory and try again!");
					retry_http_oper = true;
					goto free_memory;
				} else {
					/* Print the sys heartbeat response values received from the server */
					migcloud_print_hb_response(&hb_resp);

					/* Check and validate the response data */
					EM_LOGD(TAG, "Going to validate the parse sys heartbeat response");
					ret = validate_sys_heartbeat_response(&hb_resp);
					if (ret == EM_OK) {
#if 0
						if (hb_resp.sysconfig.freq != 0) {
#if CONFIG_USE_PERSISTENT
							write_syshb_freq_to_persistent_mem(hb_resp.sysconfig.freq);
#endif
							migcloud_set_hb_interval((uint32_t) hb_resp.sysconfig.freq);
						}
						if (hb_resp.sysconfig.standby_freq != 0) {
#if CONFIG_USE_PERSISTENT
							write_sys_standby_hbfreq_to_persistent_mem(hb_resp.sysconfig.standby_freq);
#endif
							migcloud_set_standby_hb_interval((uint32_t) hb_resp.sysconfig.standby_freq);
						}
						if (hb_resp.sysconfig.standby != -1) {
							SYSTEM_STANDBY_MODE prev_standby_state = migcloud_get_standby_hw_mode();
							if (hb_resp.sysconfig.standby != prev_standby_state) {
								syshb.standby = hb_resp.sysconfig.standby;
								EM_LOGD(TAG, "Standby Mode changed, doing standby operations ...");
								EM_LOGD(TAG, "STANDBY MODE: %s",
										(syshb.standby == STANDBY_MODE_ON) ? "STANDBY_MODE_ON" : "STANDBY_MODE_OFF");
								EM_LOGD(TAG, "SYSHB STANDBY FREQ: %d", migcloud_get_standby_hb_interval());
								/* Do the system standby operation and inform the server */
								do_system_standby_operation(syshb.standby, migcloud_get_standby_hb_interval());
							} else {
								EM_LOGD(TAG, "Requested Standby Mode same as Previous Mode, do nothing ...");
								EM_LOGD(TAG, "STANDBY MODE: %s",
										(syshb.standby == STANDBY_MODE_ON) ? "STANDBY_MODE_ON" : "STANDBY_MODE_OFF");
								EM_LOGD(TAG, "SYSHB STANDBY FREQ: %d", migcloud_get_standby_hb_interval());
							}
						}
#endif

						if (hb_resp.appconf == true) {
							/* Get application configurations from server and send them to the app */
							get_configurations_for_application(json_buf, json_len);
						}

						if (hb_resp.sysconf == true) {
							/* Get system configurations from server */
							get_sysconfig_from_server(json_buf, json_len);
						}

						/* Check the Device Config Reset Status */
						if (hb_resp.networkreset == true) {
							EM_LOGD(TAG, "Device Configuration Reset request received from migCloud server ...");
							/* Do a device configuration reset and inform the server */
							do_devcfg_reset_operation(hb_resp.networkreset);
							/* This function should not return. After device configuration reset, the system must reboot */
						}

	#if CONFIG_USE_RELEASE_SOMTHING
						/* Check the Release SoM Status */
						if (hb_resp.hw_rel == true) {
							EM_LOGD(TAG, "Release SoM request received from migCloud server ...");
							/* Release SoM request received from migCloud server. Do Release SoMThing tasks */
							do_release_somthing_operation();
							/* This function should not return. After Release SoMThing is done, the system must reboot */
						}
	#endif

	#if CONFIG_USE_FOTA
						/* Check if FOTA is required */
						if (strlen(hb_resp.fota.id) > 0) {
	//					if (hb_resp.fota.stat) {
							EM_LOGI(TAG, "FOTA is needed, processing further...");
							notify_fota_module(&hb_resp);
						}
	#endif
						hb_retry = 0;
					} else {
						retry_http_oper = true;
						goto free_memory;
					}
				}
			} else {
				retry_http_oper = true;
				EM_LOGE(TAG, "HTTP failed with status code = [ %d ]!", http_stat);
	//			if (hb_retry != SYSHB_MAX_RETRY) {
	//				EM_LOGI(TAG, "Retrying...");
	//				TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	//			} else
	//				EM_LOGE(TAG, "Retry max count reached!");
			}
		}else {

#if CONFIG_USE_INTERNET_CHECKER
			EM_LOGE(TAG, "Failed to complete System-HeartBeat, IoT Cloud not present");

#else

		EM_LOGE(TAG,"Failed to complete System-HeartBeat, Network not present");

#endif
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
		}

		free_memory:
		/* Free the allocated http respose memory */
		EM_LOGD(TAG, "Freeing allocated response memory");
		free(json_buf);
		free(http_response);

		if (retry_http_oper) {
			retry_http_oper = false;
			if (++hb_retry < SYSHB_MAX_RETRY) {
				EM_LOGW(TAG, "Retrying in %d secs... Retry Count = %d", (DELAY_5_SEC/1000), hb_retry);
				TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			} else
				EM_LOGE(TAG, "Retry max count reached!");
		}

		/* Go to sleep */
		if ((hb_retry == 0) || (hb_retry == SYSHB_MAX_RETRY)) {
			hb_retry = 0;
			if (syshb.standby) {
				EM_LOGD(TAG, "||IN STANDBY MODE|| sys-heartbeat task going to sleep for %d sec",
						migcloud_get_standby_hb_interval());
				TaskDelay((migcloud_get_standby_hb_interval() * 1000) / TICK_RATE_TO_MS);
			} else {
				EM_LOGD(TAG, "sys-heartbeat task going to sleep for %d sec", migcloud_get_hb_interval());
				TaskDelay((migcloud_get_hb_interval() * 1000) / TICK_RATE_TO_MS);
			}
		}
	}
}

static void init_system_heartbeat() {
//	em_err ret = EM_FAIL;

	syshb.standby = migcloud_get_standby_hw_mode();

//	syshb.hb_state = SYSHB_STATE_HAVE_SOMID;
	migcloud_init_hb_default_val();

#if 0
#if CONFIG_USE_PERSISTENT
	ret = read_syshb_freq_from_persistent_mem((int*) &syshb.hb_interval_sec);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "No Heartbeat Frequency is saved, setting the Heartbeat Frequency to %d", SYS_HB_INTERVAL_SEC);
		write_syshb_freq_to_persistent_mem(SYS_HB_INTERVAL_SEC);
		migcloud_set_hb_interval(SYS_HB_INTERVAL_SEC);
	}
	ret = read_sys_standby_hbfreq_from_persistent_mem((int*) &syshb.hb_standby_interval_sec);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "No Standby Heartbeat Frequency is saved, setting the Standby Heartbeat Frequency to %d",
				SYS_HB_INTERVAL_SEC);
		write_sys_standby_hbfreq_to_persistent_mem(SYS_HB_INTERVAL_SEC);
		migcloud_set_standby_hb_interval(SYS_HB_INTERVAL_SEC);
	}
#else
	migcloud_set_standby_hb_interval(SYS_HB_INTERVAL_SEC);
	migcloud_set_hb_interval(SYS_HB_INTERVAL_SEC);
#endif
#endif

	EM_LOGD(TAG, "STANDBY MODE: %s", (syshb.standby == STANDBY_MODE_ON) ? "STANDBY_MODE_ON" : "STANDBY_MODE_OFF");
	EM_LOGD(TAG, "SYSHB FREQ: %d", migcloud_get_hb_interval());
	EM_LOGD(TAG, "SYSHB STANDBY FREQ: %d", migcloud_get_standby_hb_interval());
}

em_err migcloud_start_heartbeat() {
	em_err ret = EM_FAIL;

//	/* Initialize sys heartbeat data to default/saved values */

	init_system_heartbeat();

//	syshb.hb_state = SYSHB_STATE_HAVE_SOMID;
//	migcloud_set_hb_interval(SYS_HB_INTERVAL_SEC);
//	migcloud_set_standby_hb_interval(SYS_HB_INTERVAL_SEC);
//	syshb.standby = STANDBY_MODE_OFF;

	/* Create a thread to perform system heartbeat */
	BaseType thread_stat;
	thread_stat = TaskCreate(perform_system_heartbeat, "sys-heartbeat", TASK_STACK_SIZE_8K, NULL, THREAD_PRIORITY_SYSHB,
			&syshb.hb_thread);
	if (thread_stat == false) {
		EM_LOGE(TAG, "Failed to create thread: %s, %d", (char*) __FILE__, __LINE__);
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}
	return ret;
}

