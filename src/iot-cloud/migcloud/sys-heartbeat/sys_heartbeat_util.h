/*
 * sys_heartbeat_util.h
 *
 *  Created on: 12-Nov-2019
 *      Author: Rohan Dey
 */

#ifndef SYS_HEARTBEAT_UTIL_H_
#define SYS_HEARTBEAT_UTIL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#define SYS_HB_INTERVAL_SEC		(CONFIG_SYSTEM_HEARTBEAT_INTERVAL)
#define SYS_HB_MIN_INTERVAL		(10)

typedef struct {
	uint32_t hb_interval_sec; 			/**< heartbeat frequency  (in secs) */
//	int standby; /**< put system in standby or running mode */
	uint32_t hb_standby_interval_sec; 	/**< heartbeat frequency when in standby  (in secs) */
} SysHbSystemConfig;

/**
 * @brief		This function sets the system's heartbeat interval. The input parameter must be in seconds and greater than 10
 *
 * @param[in]	interval_sec The interval in seconds. Valid values: interval_sec must be greater than 10
 *
 * @return
 * 			- EM_OK					If the value was set successfully
 * 			- EM_ERR_INVALID_ARG		If interval_sec is less than 10
 * */
em_err migcloud_set_hb_interval(uint32_t interval_sec);

/**
 * @brief		This function returns the system's current heartbeat interval in seconds
 *
 * @return
 * 			- uint32_t		system heartbeat interval in seconds
 * */
uint32_t migcloud_get_hb_interval();

/**
 * @brief		This function sets the system's heartbeat interval when in standby mode.
 * 				The input parameter must be in seconds and greater than 10
 *
 * @param[in]	interval_sec The interval in seconds. Valid values: interval_sec must be greater than 10
 *
 * @return
 * 			- EM_OK					If the value was set successfully
 * 			- EM_ERR_INVALID_ARG		If interval_sec is less than 10
 * */
em_err migcloud_set_standby_hb_interval(uint32_t interval_sec);

/**
 * @brief		This function returns the system's current heartbeat interval in standby mode in seconds
 *
 * @return
 * 			- uint32_t		system heartbeat interval in standby mode in seconds
 * */
uint32_t migcloud_get_standby_hb_interval();

/**
 * @brief		This function initializes all the system heartbeat's variables to default values
 *
 * */
void migcloud_init_hb_default_val();

#ifdef __cplusplus
}
#endif

#endif /* SYS_HEARTBEAT_UTIL_H_ */
