/*
 * sysconfig_change.c
 *
 *  Created on: 12-Nov-2019
 *      Author: Rohan Dey
 */

#include "sysconfig_change.h"
#include "migcloud_http_status_update.h"
#include "sys_heartbeat_util.h"
#include "http_client_api.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
#include "migcloud_urls.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_PERSISTENT
//#include "persistent_mem.h"
#include "migcloud_storage.h"
#endif
#if CONFIG_USE_CONN
#include "conn.h"
#endif

#include <string.h>

#define TAG	LTAG_MIG_HB

/* The value of CONFIG_SYSCONFIG_MAX_SIZE comes from menuconfig */
#define MAX_SYSCONFIG_SIZE		CONFIG_SYSCONFIG_MAX_SIZE
#define MAX_RESPONSE_BUFF_SIZE	(CONFIG_SYSCONFIG_MAX_SIZE + sizeof(int) + sizeof(EmError) + 1)

/**
 * @brief	Data structure to contain a SYS Configuration's response from the server
 *
 * @note	SYS Configuration response JSON structure:
 *
 *	{
 *		"status": true,
 * 		"error": {
 * 			"err_code": 123,
 * 			"err_msg": "This is an error!"
 * 		},
 * 		"config": {
 *			"freq": 123456,					// integer - heartbeat frequency (in secs)
 *			"standby_freq": 12345678,		// integer - heartbeat frequency when standby (in secs)
 * 		}
 * 	}
 * */
typedef struct {
	int freq; /**< heartbeat frequency  (in secs) */
//	int standby; /**< put system in standby or running mode */
	int standby_freq; /**< heartbeat frequency when in standby  (in secs) */
} SystemConfig;

typedef struct {
	int status; /**< Status. Success or Failure */
	EmError error; /**< Error object */
	SystemConfig config; /**< System configuration object  */
} SysConfigResponse;

static em_err parse_sysconfig_json(JSON_Object *rootObj, SystemConfig *config) {
	em_err ret = EM_FAIL;

	/* Get the config object */
	JSON_Value* scval = json_object_get_value(rootObj, GET_VAR_NAME(config, NULL));
	if (scval != NULL) {
		if (json_value_get_type(scval) != JSONObject) {
			EM_LOGE(TAG, "[ %s ] is not a valid JSON object!", GET_VAR_NAME(config, NULL));
			ret = EM_FAIL;
		} else {
			JSON_Object *scobj = json_value_get_object(scval);
			if (scobj != NULL) {
				/* Get the heartbeat frequency during normal operation */
				config->freq = json_object_get_number(scobj, GET_VAR_NAME(config->freq, "->"));
				/* Get the system standby status */
				/* config->standby = json_object_get_boolean(scobj, GET_VAR_NAME(config->standby, "->")); */
				/* Get the heartbeat frequency during standby operation */
				config->standby_freq = json_object_get_number(scobj, GET_VAR_NAME(config->standby_freq, "->"));

				ret = EM_OK;
			} else {
				ret = EM_FAIL;
			}
		}
	} else {
		EM_LOGE(TAG, "scval is null");
		ret = EM_FAIL;
	}

	return ret;
}

static em_err parse_sysconfig_response_json(char *json_buff, SysConfigResponse *syscfg_resp) {
	em_err ret = EM_FAIL;

	JSON_Value* root_value = NULL;
	JSON_Object * rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "JSON Value type not matched");
			ret = EM_FAIL;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Parse the common info: stat and error */
			ret = inproc_parse_json_common_info(json_buff, &syscfg_resp->status, &syscfg_resp->error);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
				ret = EM_FAIL;
				goto free_memory;
			}

			/* Check if response is successful */
			if (syscfg_resp->status) {
				ret = parse_sysconfig_json(rootObj, &syscfg_resp->config);
//				ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(syscfg_resp->config, "->"), syscfg_resp->config);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "Failed to parse config JSON response");
					ret = EM_FAIL;
				} else {
					ret = EM_OK;
				}
			}
		}

		free_memory: json_value_free(root_value); /* clear root_value */
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
		ret = EM_FAIL;
	}
	return ret;
}

static void print_sysconfig_response_json(SysConfigResponse *syscfg_resp) {
	EM_LOGD(TAG, "Parsing System Configuration response completed... The following data was received:");
	EM_LOGD(TAG, "%s : %d", GET_VAR_NAME(syscfg_resp->status, "->"), syscfg_resp->status);
	EM_LOGD(TAG, "%s : %d", GET_VAR_NAME(syscfg_resp->error.err_code, "->"), syscfg_resp->error.err_code);
	EM_LOGD(TAG, "%s : %d", GET_VAR_NAME(syscfg_resp->config.freq, "->"), syscfg_resp->config.freq);
	EM_LOGD(TAG, "%s : %d", GET_VAR_NAME(syscfg_resp->config.standby_freq, "->"),
			syscfg_resp->config.standby_freq);
}

em_err get_sysconfig_from_server(char *req_data, int req_len) {
	em_err ret = EM_FAIL;

	if (
#if CONFIG_USE_INTERNET_CHECKER
			(get_network_conn_status() & NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE) == NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE
#else

			((get_network_conn_status() & NETCONNSTAT_DISCONNECTED)	== NETCONNSTAT_DISCONNECTED) ||  ((get_network_conn_status() & NETCONNSTAT_CONNECTING)	== NETCONNSTAT_CONNECTING)
#endif
	) {
#if CONFIG_USE_INTERNET_CHECKER
		EM_LOGE(TAG, "Failed to complete SystemConfig Change, IoT Cloud not present");
		return ret=EM_ERR_INVALID_STATE;
#else

		EM_LOGE(TAG,"Failed to complete SystemConfig Change, Network not present");
		return ret=EM_ERR_INVALID_STATE;
#endif
	}

	EM_LOGI(TAG, "SYSCONFIG Request: %.*s", req_len, req_data);

	SysConfigResponse *syscfg_resp = NULL;
	char *http_resp = NULL;
	size_t http_resp_len = 0;

	/* Allocate memory for SysConfigResponse */
	syscfg_resp = (SysConfigResponse*) calloc(1, sizeof(SysConfigResponse));
	if (syscfg_resp == NULL) {
		EM_LOGE(TAG, "memory allocation for SysConfigResponse failed");
		ret = EM_FAIL;
		goto free_memory;
	}
	memset(syscfg_resp, 0, sizeof(SysConfigResponse));

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(MAX_RESPONSE_BUFF_SIZE, sizeof(char));
	if (http_resp == NULL) {
		EM_LOGE(TAG, "memory allocation for http response failed");
		ret = EM_FAIL;
		goto free_memory;
	}
	memset(http_resp, 0, MAX_RESPONSE_BUFF_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
#if (!MIGCLOUD_API_TEST)
	ret = do_http_operation(IQ_SYS_SYSCONFIG_POST_URL, IQ_HOST_PORT,
			IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
			IQ_HOST, HTTP_USER_AGENT,
			CONTENT_TYPE_APPLICATION_JSON, req_data, req_len, http_resp,
			&http_resp_len, MAX_RESPONSE_BUFF_SIZE, &http_stat);
#else
		ret = do_http_operation("http://api.myjson.com/bins/b1ize", 8080, NULL, HTTP_CLIENT_METHOD_GET, "api.myjson.com",
		HTTP_USER_AGENT,
		CONTENT_TYPE_APPLICATION_JSON, req_data, req_len, http_resp, &http_resp_len, MAX_RESPONSE_BUFF_SIZE, &http_stat);
#endif

	if (ret == EM_OK) {
		EM_LOGI(TAG, "SYSCONFIG Response: %.*s", http_resp_len, http_resp);

		/* Parse the received configurations */
		ret = parse_sysconfig_response_json(http_resp, syscfg_resp);
		if (ret == EM_OK) {
			print_sysconfig_response_json(syscfg_resp);

			/* Save the received configurations */
			if (syscfg_resp->config.freq != 0) {
#if CONFIG_USE_PERSISTENT
				ret = write_syshb_freq_to_persistent_mem(
						syscfg_resp->config.freq);
#endif
				migcloud_set_hb_interval((uint32_t) syscfg_resp->config.freq);
			}

			if (syscfg_resp->config.standby_freq != 0) {
#if CONFIG_USE_PERSISTENT
				ret = write_sys_standby_hbfreq_to_persistent_mem(
						syscfg_resp->config.standby_freq);
#endif
				migcloud_set_standby_hb_interval(
						(uint32_t) syscfg_resp->config.standby_freq);
			}

			/* Send the status to the server */
			if (ret == EM_OK) {
				ret = migcloud_send_status_via_http(MIGCLOUD_TASK_SYSTEM_CONFIG,
						MIGCLOUD_STATUS_UPDATED, NULL);
			} else {
				ret = migcloud_send_status_via_http(MIGCLOUD_TASK_SYSTEM_CONFIG,
						MIGCLOUD_STATUS_UPDATE_FAILED, NULL);
			}
		} else {
			EM_LOGE(TAG,
					"Failed to parse system configuration response json");
			ret = EM_FAIL;
		}
	} else {
		EM_LOGE(TAG, "HTTP failed with status code = [ %d ]", http_stat);
		ret = EM_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	EM_LOGD(TAG, "Freeing allocated memory");
	free(syscfg_resp);
	free(http_resp);

	return ret;
}
