/*
 * apppostdata_helper.c
 *
 *  Created on: 12-Aug-2019
 *      Author: Rohan Dey
 */

#include "apppostdata_helper.h"
#include "http_client_api.h"
//#include "http_client_core.h"
//#include "http_constant.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
//#include "system.h"
#include "migcloud_urls.h"
#include "system_utils.h"
#include "core_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#if CONFIG_USE_CONN
#include "conn.h"
#endif

#include <string.h>

#define TAG	LTAG_MIG_POSTDATA

// The value of CONFIG_APPPOSTDATA_MAX_SIZE comes from menuconfig
#define MAX_APPPOSTDATA_SIZE		CONFIG_APPPOSTDATA_MAX_SIZE
#define MAX_RESPONSE_BUFF_SIZE		(sizeof(int) + sizeof(EmError) + 1)

/*
 * {
 *		"somthing_id": "",
 *		"data" {
 *			// the data that application wants to send
 *		}
 * 	}
 * */
typedef struct {
	char somthing_id[SOMTHING_ID_LEN + 1];
	JSON_Value *data;
} AppPostDataHelperRequest;

/**
 * @brief	Data structure to contain a APP Post Data's response from the server
 *
 * @note	APP Configuration response JSON structure:
 *
 *	{
 *		"status": true,
 * 		"error": {
 * 			"err_code": 123,
 * 			"err_msg": "This is an error!"
 * 		}
 * */
typedef struct {
	int status; /**< Status. Success or Failure */
	EmError error; /**< Error object */
} AppPostDataHelperResponse;

static em_err _parse_apppostdata_response_json(char *json_buff, AppPostDataHelperResponse *apppost_resp) {
	/* Parse the common info: status and error */
	em_err ret = inproc_parse_json_common_info(json_buff, &apppost_resp->status, &apppost_resp->error);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		return EM_FAIL;
	}

	return EM_OK;
}

static void _print_apppostdata_response_json(AppPostDataHelperResponse *apppost_resp) {
	EM_LOGD(TAG, "Parsing App Post Data response completed... The following data was received:");
	EM_LOGD(TAG, "%s : %d", GET_VAR_NAME(apppost_resp->status, "->"), apppost_resp->status);
	EM_LOGD(TAG, "%s : %d", GET_VAR_NAME(apppost_resp->error.err_code, "->"), apppost_resp->error.err_code);
}

em_err _make_post_data_json(char *post_data, char **ppbuf, int *plen, AppPostDataHelperRequest *apppost_req) {
	em_err ret = EM_FAIL, data_ret = EM_FAIL;
	JSON_Status stat = JSONSuccess;

	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	/* Set somthing_id */
	json_object_set_string(root_object, GET_VAR_NAME(apppost_req->somthing_id, "->"), apppost_req->somthing_id);

	/* Set data */
	apppost_req->data = json_parse_string(post_data);
	if (apppost_req->data != NULL) {
		stat = json_object_set_value(root_object, GET_VAR_NAME(apppost_req->data, "->"), apppost_req->data);
		if (stat == JSONFailure) {
			EM_LOGE(TAG, "json_object_set_value failed");
			data_ret = EM_FAIL;
		} else {
			EM_LOGD(TAG, "json_object_set_value success");
			data_ret = EM_OK;
		}
	} else {
		EM_LOGE(TAG, "data is NULL");
		data_ret = EM_FAIL;
	}

	/* Check if data was set successfully */
	if (data_ret == EM_OK) {
		serialized_string = json_serialize_to_string(root_value);
		size_t len = json_serialization_size(root_value);
		len = len - 1;  // since json_serialization_size returns size + 1

		char *ptemp = (char*) malloc(len);
		if (ptemp == NULL) {
			EM_LOGE(TAG, "_make_post_data malloc failed!");
			return EM_FAIL;
		}
		memset(ptemp, 0x00, len);
		memcpy(ptemp, serialized_string, len);
		*plen = len;
		*ppbuf = ptemp;

		ret = EM_OK;
	} else {
		if (stat == JSONSuccess) {
			// If the cide reached here it means the JSON_Value data could not be allocated, hence it is safe to free it.
			EM_LOGD(TAG, "Freeing apppost_req->data");
			json_value_free(apppost_req->data);
		}
		ret = EM_FAIL;
	}

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return ret;
}

em_err _make_post_data(char *post_data, char **json_buf, int *json_len, AppPostDataHelperRequest *apppost_req) {
	em_err ret = EM_FAIL;

	get_somthing_id(apppost_req->somthing_id);
	apppost_req->data = NULL;

	ret = _make_post_data_json(post_data, json_buf, json_len, apppost_req);

	return ret;
}

em_err migcloud_post_app_json_http(char *post_data) {
	em_err ret = EM_FAIL;

	if (
#if CONFIG_USE_INTERNET_CHECKER
			(get_network_conn_status() & NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE) == NETCONNSTAT_IOT_CLOUD_NOT_AVAILABLE
#else

			((get_network_conn_status() & NETCONNSTAT_DISCONNECTED)	== NETCONNSTAT_DISCONNECTED) ||  ((get_network_conn_status() & NETCONNSTAT_CONNECTING)	== NETCONNSTAT_CONNECTING)
#endif
	) {
#if CONFIG_USE_INTERNET_CHECKER
		EM_LOGE(TAG, "Failed to complete AppPostData, IoT Cloud not present");
		return ret=EM_ERR_INVALID_STATE;
#else
		EM_LOGE(TAG, "Failed to complete AppPostData, Network not present");
		return ret=EM_ERR_INVALID_STATE;
#endif
	}

	/* First we need to check if we have a somthing_id or not. If we don't then we cannot proceed */
	char somthing_id[SOMTHING_ID_LEN + 1];
	if (get_somthing_id(somthing_id) == EM_ERR_NOT_FOUND) {
		EM_LOGE(TAG, "somthing_id not set, cannot do postdata!");
		return EM_ERR_INVALID_STATE;
	}

	if (post_data == NULL) {
		EM_LOGE(TAG, "post_data is NULL");
		return EM_ERR_INVALID_ARG;
	}
	size_t req_len = strlen(post_data);

	if (req_len > MAX_APPPOSTDATA_SIZE) {
		EM_LOGE(TAG,
				"The size of post_data is too long. Either pass the data size within CONFIG_APPPOSTDATA_MAX_SIZE, or increase the CONFIG_APPPOSTDATA_MAX_SIZE from menuconfig");
		return EM_ERR_NO_MEM;
	}

	char *http_resp = NULL;
	size_t http_resp_len = 0;
	char *json_buf = NULL;
	int json_len;

	/* Make the postdata json */
	AppPostDataHelperRequest apppost_req;
	ret = _make_post_data(post_data, &json_buf, &json_len, &apppost_req);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to create application's post data");
		goto free_memory;
	}
	EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	/* Allocate memory for AppPostDataHelperResponse */
	AppPostDataHelperResponse apppost_resp;
	memset(&apppost_resp, 0, sizeof(AppPostDataHelperResponse));

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(MAX_RESPONSE_BUFF_SIZE, sizeof(char));
	if (http_resp == NULL) {
		EM_LOGE(TAG, "memory allocation for http response failed");
		ret = EM_FAIL;
		goto free_memory;
	}
	memset(http_resp, 0, MAX_RESPONSE_BUFF_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
	ret = do_http_operation(
			IQ_SYS_APPPOSTDATA_POST_URL/*IQ_SYS_BIGDATA_POST_URL*/,
			IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
			IQ_HOST, HTTP_USER_AGENT,
			CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp,
			&http_resp_len, MAX_RESPONSE_BUFF_SIZE, &http_stat);

	if (ret == EM_OK) {
		if (http_resp_len <= 0) {
			EM_LOGW(TAG, "No Response!");
			ret = EM_FAIL;
		} else {
			EM_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);
			ret = _parse_apppostdata_response_json(http_resp, &apppost_resp);
			if (ret == EM_OK) {
				_print_apppostdata_response_json(&apppost_resp);
				ret = EM_OK;
			} else {
				EM_LOGE(TAG,
						"Failed to parse application http postdata response json");
				ret = EM_FAIL;
			}
		}
	} else {
		EM_LOGE(TAG, "HTTP failed with status code = [ %d ]", http_stat);
		ret = EM_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	EM_LOGD(TAG, "Freeing allocated memory");
	free(json_buf);
	free(http_resp);

	return ret;
}
