/*
 * File Name: som_registration_parser.h
 * File Path: /emmate/src/system/som-registration/som_registration_parser.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#ifndef SOM_REGISTRATION_PARSER_H_
#define SOM_REGISTRATION_PARSER_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "systime.h"
#include "som_registration.h"

/*
 * */
em_err make_som_regn_request_json(char **ppbuf, int *plen, SomRegistrationRequest *regn_req);

/*
 * */
em_err parse_som_registration_response_json(char *json_buff, SomRegistrationResponse *regn_resp);

/*
 * */
void print_som_registration_response(SomRegistrationResponse *regn_resp);

#endif /* SOM_REGISTRATION_PARSER_H_ */
