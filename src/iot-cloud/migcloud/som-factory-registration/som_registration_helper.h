/*
 * File Name: som_registration_helper.h
 * File Path: /emmate/src/system/som-registration/som_registration_helper.h
 * Description:
 *
 *  Created on: 27-May-2019
 *      Author: Rohan Dey
 */

#ifndef SOM_REGISTRATION_HELPER_H_
#define SOM_REGISTRATION_HELPER_H_

#include "core_config.h"
#include "core_error.h"
#include "core_common.h"
#include "systime.h"
//#include "som_registration.h"

/*
 * @brief Saves the SOM registration time into Non Volatile Storage (NVS)
 *
 * @param[in] reg_time 	A pointer to struct tm
 *
 * @return 	EM_OK 	if success
 * 			EM_FAIL	if failure
 * */
#if CONFIG_PLATFORM_ESP_IDF
em_err save_som_registration_time(struct tm* reg_time);
#endif

/*
 * @brief Saves the SOM registration location into Non Volatile Storage (NVS)
 *
 * @param[in] loc	NULL terminated location string
 *
 * @return 	EM_OK 	if success
 * 			EM_FAIL	if failure
 * */
em_err save_som_registration_location(char *loc);

/*
 * @brief Saves the Bluetooth Passkey into Non Volatile Storage (NVS)
 *
 * @param[in] bt_passkey	NULL terminated bt passkey string
 *
 * @return 	EM_OK 	if success
 * 			EM_FAIL	if failure
 * */
em_err save_som_registration_btpasskey(char *bt_passkey);


#endif /* SOM_REGISTRATION_HELPER_H_ */
