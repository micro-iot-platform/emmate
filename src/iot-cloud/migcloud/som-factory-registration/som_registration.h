/*
 * File Name: som_registration.h
 * File Path: /emmate/src/system/som-registration/som_registration.h
 * Description:
 *
 *  Created on: 24-May-2019
 *      Author: Rohan Dey
 */

#ifndef SOM_REGISTRATION_H_
#define SOM_REGISTRATION_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "systime.h"

#include "version.h"
#include "core_system_info.h"
#if CONFIG_USE_WIFI
#include "wifi_constants.h"
#endif
#if CONFIG_USE_ETH
#include "eth_constants.h"
#endif
#if CONFIG_USE_GSM
#include "gsm_constants.h"
#endif
#if CONFIG_USE_BLE
#include "ble_constant.h"
#endif
/*
 * @brief	Data structure to contain a SOM registration's POST request data to be sent to the server
 *
 * @note	SOM Registration request JSON structure:
 *
 * {
 * 	"som_id": "ffff2941002209b3",
 * 	"chip_info": {
 * 		"chip_name": "ESP32",
 * 		"chip_mfgr": "ESSPRESIF",
 * 		"has_wifi": true,
 * 		"has_bt": true,
 * 		"has_ble": true,
 * 		"has_gsm_2g": false,
 * 		"has_gsm_3g": false,
 * 		"has_gsm_4g": false,
 * 		"has_lora": false,
 * 		"has_nbiot": false,
 * 		"flash_size": 16777216,
 * 		"ram_size": 532480
 * 	},
 * 	"core_version": "0.1.0.0.415",
 * 	"app_version": "",
 * 	"wifi_mac": "24:0a:c4:8f:6c:0c",
 * 	"eth_mac": "",
 * 	"gsm_imei": "",
 * 	"bt_mac": ""
 * 	}
 *
 * */
typedef struct {
	char somthing_id[SOMTHING_ID_LEN+1];						/**< A System On Module (SOM) unique identifier. To be fetched from a hardwired silicon UID */
	HardwareInfo *chip_info;						/**< The hardware's complete information and features */
	char core_version[CORE_VERSION_NUMBER_LEN+1];	/**< Core Framework's version number */
	char app_version[APP_VERSION_NUMBER_LEN+1];		/**< Core Application's version number */

#if CONFIG_USE_WIFI
	uint8_t wifi_mac[WIFI_MAC_LEN+1];				/**< Wi-Fi module's MAC */
#endif

#if CONFIG_USE_ETH
	uint8_t eth_mac[ETH_MAC_LEN+1];					/**< Ethernet module's MAC */
#endif

#if CONFIG_USE_GSM
	uint8_t gsm_imei[GSM_IMEI_LEN+1];				/**< GSM module's IMEI number */
#endif

#if CONFIG_USE_BLE
	uint8_t bt_mac[BLE_MAC_LEN+1];					/**< BLE module's MAC */
#endif
} SomRegistrationRequest;

/*
 * @brief	Data structure to contain a SOM registration's response from the server
 *
 * @note	SOM Registration response JSON structure:
 *
 * {
 * 		"status": true,
 * 		"error": {
 * 			"err_code": 123,
 * 			"err_msg": "This is an error!"
 * 		},
 * 		"reg_time": 123456789,
 * 		"reg_location": "kolkata",
 * 		"bt_passkey": "scvsdcbhaskdvbsaydkvbyr78362g5ybq"
 * 	}
 *
 * */
typedef struct {
	int status;									/**< Registration Status. Success or Failure */
	EmError error;							/**< Error object if generated during the SOM registration process */
#if CONFIG_PLATFORM_ESP_IDF
	struct tm reg_time;							/**< Time when the registration took place */
#endif
	char reg_location[SOM_REG_LOCATION_LEN+1];	/**< Location from where the registration took place */
	char bt_passkey[SOM_BT_PASSKEY_LEN+1];		/**< The Bluetooth Passkey to be used in Device Configuration */
} SomRegistrationResponse;

/*
 * @brief	This function registers a SOM board with Iquester's Server
 * */
em_err register_som();

#endif /* SOM_REGISTRATION_H_ */
