/*
 * File Name: som_registration_helper.c
 * File Path: /emmate/src/system/som-registration/som_registration_helper.c
 * Description:
 *
 *  Created on: 27-May-2019
 *      Author: Rohan Dey
 */


#include "som_registration_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_NVS
#include "core_nvs.h"
#endif
#include "core_utils.h"

#include <string.h>

#define TAG	LTAG_SOM_REG

em_err save_som_registration_time(struct tm* reg_time) {
	em_err ret = EM_FAIL;
#if CONFIG_USE_PERSISTENT

	time_t reg_time_t = convert_tm_to_seconds(reg_time);
	char strbuf[20];
	sprintf(strbuf, "%lu", reg_time_t);

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nvs_write_by_key(GET_VAR_NAME(reg_time, NULL), strbuf, strlen(strbuf))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "save_som_registration_time failed!");
	}
#elif (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC)
	// TODO: Save data in file
#endif
#endif
	return ret;
}

em_err save_som_registration_location(char *reg_loc) {
	em_err ret = EM_FAIL;
#if CONFIG_USE_PERSISTENT
#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nvs_write_by_key(GET_VAR_NAME(reg_loc, NULL), reg_loc, strlen(reg_loc))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "save_som_registration_location failed!");
	}
#elif (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC)
	// TODO: Save data in file
#endif
#endif
	return ret;
}

em_err save_som_registration_btpasskey(char *bt_passkey) {
	em_err ret = EM_FAIL;
#if CONFIG_USE_PERSISTENT
#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = nvs_write_by_key(GET_VAR_NAME(bt_passkey, NULL), bt_passkey, strlen(bt_passkey))) == EM_OK) {

	} else {
		EM_LOGE(TAG, "save_som_registration_btpasskey failed!");
	}
#elif (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_SOMREGN_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC)
	// TODO: Save data in file
#endif
#endif
	return ret;
}
