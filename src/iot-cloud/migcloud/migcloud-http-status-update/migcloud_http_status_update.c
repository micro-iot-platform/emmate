/*
 * migcloud_http_status_update.c
 *
 *  Created on: 12-Nov-2019
 *      Author: Rohan Dey
 */

#include "migcloud_http_status_update.h"
#include "http_client_api.h"
#include "input_processor.h"
//#include "inpproc_utils.h"
#include "migcloud_urls.h"
#include "core_utils.h"
#include "board_ids.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"
#if CONFIG_USE_CONN
#include "conn.h"
#endif

#include <string.h>

#define TAG	LTAG_MIG_STAT
#define MIGCLOUD_HTTP_STATUS_RESPONSE_SIZE	(sizeof(int) + sizeof(EmError) + 1)
#define MIGCLOUD_STATUS_MAX_RETRY			5
/*
 * {
 * 		"type":	1
 * 		"somthing_id": "123",
 * 		"status": 20
 * 		"fota_id": "123"
 * 		"dl_try": 1
 * }
 * */
typedef struct {
	MIGCLOUD_TASK_TYPE type;
	char somthing_id[SOMTHING_ID_LEN + 1];
	int status;
	char fota_id[FOTA_ID_LEN + 1];
	int dl_try;
} MigCloudStatusUpdateRequest;

typedef struct {
	int status; /**< Sys Heartbeat Status. Success or Failure */
	EmError error; /**< Error object */
} MigCloudStatusUpdateResponse;

static em_err make_status_update_request(char **ppbuf, int *plen, MigCloudStatusUpdateRequest *stat_update_req) {
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	EM_LOGD(TAG, "Going to make Status Update JSON with the following values:");

	/* Set JSON key value */
	EM_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(stat_update_req->type, "->"), stat_update_req->type);
	json_object_set_number(root_object, GET_VAR_NAME(stat_update_req->type, "->"), (int) stat_update_req->type);

	EM_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(stat_update_req->somthing_id, "->"), stat_update_req->somthing_id);
	json_object_set_string(root_object, GET_VAR_NAME(stat_update_req->somthing_id, "->"), stat_update_req->somthing_id);

	EM_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(stat_update_req->status, "->"), stat_update_req->status);
	json_object_set_number(root_object, GET_VAR_NAME(stat_update_req->status, "->"), (int) stat_update_req->status);

	/* If we are updating the status for FOTA Task, then we need to send the fota id and download try count also */
	if (stat_update_req->type == MIGCLOUD_TASK_FOTA) {
		EM_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(stat_update_req->fota_id, "->"), stat_update_req->fota_id);
		json_object_set_string(root_object, GET_VAR_NAME(stat_update_req->fota_id, "->"), stat_update_req->fota_id);

		EM_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(stat_update_req->dl_try, "->"), stat_update_req->dl_try);
		json_object_set_number(root_object, GET_VAR_NAME(stat_update_req->dl_try, "->"), (int) stat_update_req->dl_try);
	}

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1; /* since json_serialization_size returns size + 1 */
	EM_LOGD(TAG, "Status Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		EM_LOGE(TAG, "make_status_update_request malloc failed!");
		return EM_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return EM_OK;
}

static em_err parse_status_update_response(char *json_buff, MigCloudStatusUpdateResponse *stat_update_resp) {
	em_err ret = EM_FAIL;

	/* Parse the common info: stat and error */
	ret = inproc_parse_json_common_info(json_buff, &stat_update_resp->status, &stat_update_resp->error);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		ret = EM_FAIL;
	}

	return ret;
}

em_err migcloud_send_status_via_http(MIGCLOUD_TASK_TYPE task_type, MIGCLOUD_TASK_STATUSES status,
		MigcloudFotaStatus *fota_stat_info) {
	em_err ret = EM_FAIL;

	bool retry_http_oper = false;
	int retry_count = 0;

	while (1) {
		char *http_resp = NULL;
		size_t http_resp_len = 0;
		char *json_buf = NULL;
		int json_len = 0;

		MigCloudStatusUpdateRequest stat_update_req;
		memset(&stat_update_req, 0, sizeof(MigCloudStatusUpdateRequest));

		/* Populate the Status Update Request Structure */
		stat_update_req.type = task_type;
		get_somthing_id(stat_update_req.somthing_id);
		stat_update_req.status = (int) status;
		if (fota_stat_info != NULL) {
			strcpy(stat_update_req.fota_id, fota_stat_info->fota_id);
			stat_update_req.dl_try = fota_stat_info->dl_try;
		}

		ret = make_status_update_request(&json_buf, &json_len, &stat_update_req);
		EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

		/* Allocate memory for the http response */
		http_resp = (char*) calloc(MIGCLOUD_HTTP_STATUS_RESPONSE_SIZE, sizeof(char));
		if (http_resp == NULL) {
			EM_LOGE(TAG, "memory allocation for http response failed");
			goto free_memory;
		}
		memset(http_resp, 0, MIGCLOUD_HTTP_STATUS_RESPONSE_SIZE);
		http_resp_len = 0;

		/* Do http operation */
		uint16_t http_stat = 0;
		if (
	#if CONFIG_USE_INTERNET_CHECKER
				(get_network_conn_status() & NETCONNSTAT_IOT_CLOUD_AVAILABLE) == NETCONNSTAT_IOT_CLOUD_AVAILABLE
	#else

				(get_network_conn_status() & NETCONNSTAT_CONNECTED)	== NETCONNSTAT_CONNECTED
	#endif
		){
#if (!MIGCLOUD_API_TEST)
			ret = do_http_operation(IQ_SYS_STATUS_UPDATED_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
			IQ_HOST, HTTP_USER_AGENT,
			CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len,
			MIGCLOUD_HTTP_STATUS_RESPONSE_SIZE, &http_stat);
#else
			ret = do_http_operation("http://api.myjson.com/bins/1coc4q", IQ_HOST_PORT, NULL, HTTP_CLIENT_METHOD_GET,
					"api.myjson.com", HTTP_USER_AGENT,
					CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len,
					MIGCLOUD_HTTP_STATUS_RESPONSE_SIZE, &http_stat);
#endif

			if (ret == EM_OK) {
				EM_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);

				MigCloudStatusUpdateResponse stat_update_resp;
				memset(&stat_update_resp, 0, sizeof(MigCloudStatusUpdateResponse));

				ret = parse_status_update_response(http_resp, &stat_update_resp);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "Status Update Response parsing failed, retrying ...");
					retry_http_oper = true;
					ret = EM_FAIL;
				}
			} else {
				EM_LOGE(TAG, "HTTP failed with status code = [ %d ]", http_stat);
				retry_http_oper = true;
				ret = EM_FAIL;
			}
		} else {

#if CONFIG_USE_INTERNET_CHECKER
			EM_LOGE(TAG,"Failed to complete MigCloud HTTP Status Update, IoT Cloud not present");
#else
			EM_LOGE(TAG,"Failed to complete MigCloud HTTP Status Update, Network not present");
#endif
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
		}


		free_memory:
		/* Free the allocated http respose memory */
		EM_LOGD(TAG, "Freeing allocated response memory");
		free(json_buf);
		free(http_resp);

		if (retry_http_oper) {
			retry_http_oper = false;
			if (++retry_count < MIGCLOUD_STATUS_MAX_RETRY) {
				EM_LOGW(TAG, "Retrying in %d secs... Retry Count = %d", (DELAY_5_SEC/1000), retry_count);
				TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			} else {
				EM_LOGE(TAG, "Retry max count reached. Status update failed!");
				ret = EM_FAIL;
				break;
			}
		} else {
			break;
		}
	}

	return ret;
}
