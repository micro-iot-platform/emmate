# System Module

The System Module is the most important module of the EmMate Framework. It provides the entry point of the system. After the system is booted this module does the following tasks sequentially:

1. Initializes the logger module
2. Acquires and prints the hardware and firmware info
3. Initializes the system's reset button (if enabled from the configuration file)
4. Initializes the HMI module in startup mode (if enabled from the configuration file) 
5. Initializes the Persistent Memory module
6. Checks and starts the device configuration process over a selected peripheral, for e.g. BLE, UART, USB etc.
7. Waits till the device configuration process is completed. The system module will not proceed until and unless the device configuration is completed.
8. Connects to the network through the selected network interface
9. Sets the System's Time via SNTP using the systime module (if enabled from the configuration file)
10. Does the SOM Registration (if enabled from the configuration file)
11. Starts the System's Heartbeat using sys-heartbeat module
12. Finally starts the application's entry point thread **emmate_main**

The System Module has the following sub modules:
* Device Configuration - For more details see [Details of Device Configuration Module](devconfig/README.md)
* System Information - For more details see [Details of System Information Module](sysinfo/README.md)
* System Time - For more details see [Details of System Time Module](systime/README.md)
* System Init - For more details see [Details of System Init Module](systime/README.md)
* SOM Registration - For more details see [Details of SOM Registration Module](som-registration/README.md)
* System Heartbeat - For more details see [Details of System Heartbeat Module](sys-heartbeat/README.md)
* Hardware Identify - For more details see [Details of Hardware Identify Module](hw-identify/README.md)
* App Config Helper - For more details see [Details of App Config Helper Module](appconfig-helper/README.md)
* App Postdata Helper - For more details see [Details of App Postdata Helper Module](apppostdata-helper/README.md)

## How to use the System Module from an application
The system module does not exposes all modules for the application to use. Only the following modules can be used by the application:
1. Device Configuration
2. System Time

Please see below how to use these modules from the application

### Device Configuration

*This module does not exposes any useable APIs to the application yet. This will be done in future release.*

### System Time

##### Overview
The System Time module does all the timekeeping work. Once the system's time is set either via SNTP or by API, the clock starts running internally by using the underlying hardware's RTC. 

##### Features
The System Time module can be used to get, set and check if the system time is set. Also, it can be used to do time conversions to and from many formats. Please see the below **sample APIs** and the **examples**

#### Header(s) to include

```
systime.h
```

##### Sample APIs

1. Setting the system time

```c
struct timeval now;
int rc;

now.tv_sec=866208142;
now.tv_usec=290944;

rc=core_settimeofday(&now, NULL);
if(rc==0) {
    EM_LOGI(TAG, "core_settimeofday() successful.");
}
else {
    EM_LOGE(TAG, "core_settimeofday() failed, errno = %d",errno);
    return EM_FAIL;
}
```

2. Getting the current system time

```c
struct timeval curr_time;
int ret = get_systime(&curr_time);
if (ret == 0)	EM_LOGI(TAG, "get_systime() successful.");
else			EM_LOGE(TAG, "get_systime() failed.");
```

3. To check if the system time is set or not use: `is_systime_set()`


4. To convert time related data many APIs are provided such as: `convert_tm_to_seconds`, `convert_seconds_to_tm`, `convert_millis_to_tm`, `convert_str_seconds_to_tm`, `convert_str_millis_to_tm`. Please check the **API documentation** and **examples** to get more info regarding these APIs


5. To evaluate the difference between to time represented as an object of `struct tm`

**Difference between a random time and the current system time**

```c
struct tm input_time; // Please populate the input_time with a real value
double diff_secs = diff_time_with_now(&input_time);

if (diff_secs == (-DBL_MAX)) {
	EM_LOGD(TAG, "diffence in time returned -DBL_MAX, hence system time is not set");
} else {
	EM_LOGI(TAG, "Diff = %lf secs", diff_secs);
}
```

**Difference between a 2 time objects**

```c
struct timeval time1; // Please populate the time1 with a real value
struct timeval time2; // Please populate the time2 with a real value

double diff_secs = diff_between_time(&time1, &time2);
EM_LOGI(TAG, "Diff = %lf secs", diff_secs);
```
