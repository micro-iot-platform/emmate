/*
 * parser_utils.c
 *
 *	File Path: /emmate/src/system/devconfig/parser_utils.c
 *
 *	Project Name: EmMate
 *	
 *  Created on: 17-Apr-2019
 *
 *      Author: Noyel Seth
 */

#include "parser_utils.h"
#include "core_utils.h"
#include <string.h>
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "input_processor.h"

#if CONFIG_BLE_CFG
#include "ble_config.h"
#define BLE_ADV_NAME "ESP32_Core"
#define BLE_AUT_ID		"esp32_core12345"
#endif

#define TAG LTAG_SYSTEM_DEVCFG

static bool data_parse_stat = false;

/**
 * @brief
 *
 * @return
 *
 **/
static em_err parse_ble_metadata(char * metadata, char* ble_authid, DeviceConfig *devcfg) {
	em_err err = EM_FAIL;
	EM_LOGI(TAG, "Received Metadata : %s", metadata);

	static uint8_t checklist_mask = 0x00;

	JSON_Value *root_value;
	JSON_Object *root_object;
	root_value = json_parse_string(metadata);
	if (root_value != NULL) {

		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "Config Respons JSON Value type not matched\r\n");
			json_value_free(root_value);
			EM_LOGE(TAG, "Failed to parse metadata.\n");
			return err;
		}

		root_object = json_value_get_object(root_value);

		if(json_object_get_string(root_object, DEVID_METADATA_KEY)!=NULL){
		EM_LOGI(TAG,"[%s]: %s", DEVID_METADATA_KEY, json_object_get_string(root_object, DEVID_METADATA_KEY));

			if (strcasecmp(json_object_get_string(root_object, DEVID_METADATA_KEY), ble_authid) == 0) {

				if(json_object_get_number(root_object, WIFI_SSID_METADATA_KEY) != 0){
					devcfg->wifi_cfg.wifi_ssid_len = json_object_get_number(root_object, WIFI_SSID_METADATA_KEY);
					EM_LOGI(TAG, "[%s]: %d", WIFI_SSID_METADATA_KEY, devcfg->wifi_cfg.wifi_ssid_len);
					checklist_mask = (checklist_mask << 1) | 0x01;
				}

				if(json_object_get_number(root_object, WIFI_PWD_METADATA_KEY) != 0){
					devcfg->wifi_cfg.wifi_pwd_len = json_object_get_number(root_object,WIFI_PWD_METADATA_KEY);
					EM_LOGI(TAG, "[%s]: %d", WIFI_PWD_METADATA_KEY, devcfg->wifi_cfg.wifi_pwd_len);
					checklist_mask = (checklist_mask << 1) | 0x01;
				}

#if 0
				if (cJSON_GetObjectItem(root, METADATA_CERT_KEY) != NULL) {
					EM_LOGI(TAG, "%s -> ", GET_VAR_NAME(METADATA_CERT_KEY, NULL));
					aws_cred_data.aws_cert_metadata =
					get_json_obj_data_from_root_json_obj(root,
							METADATA_CERT_KEY, &aws_cred_data.size_of_aws_cert,
							aws_cred_data.aws_cert,
							sizeof(aws_cred_data.aws_cert),
							&aws_cred_data.aws_cert_data_present);
					total_byte_stream_length += aws_cred_data.size_of_aws_cert;
				}

				if (cJSON_GetObjectItem(root, METADATA_KEY_KEY) != NULL) {
					EM_LOGI(TAG, "%s -> ", GET_VAR_NAME(METADATA_KEY_KEY, NULL));
					aws_cred_data.aws_key_metadata =
					get_json_obj_data_from_root_json_obj(root,
							METADATA_KEY_KEY, &aws_cred_data.size_of_aws_key,
							aws_cred_data.aws_key,
							sizeof(aws_cred_data.aws_key),
							&aws_cred_data.aws_key_data_present);
					total_byte_stream_length += aws_cred_data.size_of_aws_key;
				}

				if (cJSON_GetObjectItem(root, METADATA_ROOTCA_KEY) != NULL) {
					EM_LOGI(TAG, "%s -> ", GET_VAR_NAME(METADATA_ROOTCA_KEY, NULL));
					aws_cred_data.aws_rootca_metadata =
					get_json_obj_data_from_root_json_obj(root,
							METADATA_ROOTCA_KEY,
							&aws_cred_data.size_of_aws_rootca,
							aws_cred_data.aws_rootca,
							sizeof(aws_cred_data.aws_rootca),
							&aws_cred_data.aws_rootca_data_present);
					total_byte_stream_length +=
					aws_cred_data.size_of_aws_rootca;
				}

				if (cJSON_GetObjectItem(root, METADATA_URL_KEY) != NULL) {
					EM_LOGI(TAG, "%s -> ", GET_VAR_NAME(METADATA_URL_KEY, NULL));
					aws_cred_data.aws_url_metadata =
					get_json_obj_data_from_root_json_obj(root,
							METADATA_URL_KEY, &aws_cred_data.size_of_aws_url,
							aws_cred_data.aws_url,
							sizeof(aws_cred_data.aws_url),
							&aws_cred_data.aws_url_data_present);
					total_byte_stream_length += aws_cred_data.size_of_aws_url;
				}

				if (cJSON_GetObjectItem(root, METADATA_THING_KEY) != NULL) {
					EM_LOGI(TAG, "%s -> ", GET_VAR_NAME(METADATA_THING_KEY, NULL));
					aws_cred_data.aws_thing_name_metadata =
					get_json_obj_data_from_root_json_obj(root,
							METADATA_THING_KEY,
							&aws_cred_data.size_of_aws_thing_name,
							aws_cred_data.aws_thing_name,
							sizeof(aws_cred_data.aws_thing_name),
							&aws_cred_data.aws_thing_name_data_present);
					total_byte_stream_length +=
					aws_cred_data.size_of_aws_thing_name;
				}
#endif
				if (0x03 == checklist_mask) {
					err = EM_OK;
					checklist_mask = 0x00;
				} else {
					err = EM_ERR_INVALID_RESPONSE;
				}
			} else {
				err = EM_FAIL;
			}
		} else {
			err = EM_FAIL;
		}
	} else {
		err = EM_FAIL;
		EM_LOGE(TAG, "Failed to parse metadata\n");
	}

	if (root_value != NULL) {
		// Finally remember to free the memory!
		json_value_free(root_value);
	}
//	printf("err = %d, %s\r\n", err, (err == EM_OK) ? "EM_OK" : "!EM_OK");
	EM_LOGI(TAG,"");
	return err;
}

static int idx = 0;
static int copy_byte = 0;
static int mtu = 20;
/**
 * @brief
 *
 * @return
 *
 **/
static int copy_data(char * dest, char* src, int len_of_data) {
	int temp_copy_byte;
	if (len_of_data - copy_byte >= 20) {
		temp_copy_byte = mtu - idx;
		strncat(dest, src + idx, temp_copy_byte);
		//printf("..%s\n", dest);
		EM_LOGD(TAG, "..%s\n", dest);
		copy_byte += temp_copy_byte;
		idx = mtu;
	} else {
		if (idx != 0) {
			temp_copy_byte = 20 - idx;
			if (temp_copy_byte >= len_of_data) {
				temp_copy_byte = temp_copy_byte - (temp_copy_byte - len_of_data);
				//printf("temp_copy_byte = %d\r\n", temp_copy_byte);
				EM_LOGD(TAG, "temp_copy_byte = %d\r\n", temp_copy_byte);
			} else {
				//printf("temp_copy_byte = %d\r\n", temp_copy_byte);
				EM_LOGD(TAG, "temp_copy_byte = %d\r\n", temp_copy_byte);
			}
		} else {
			temp_copy_byte = len_of_data - copy_byte;
		}
		//printf("%d\n", temp_copy_byte);
		strncat(dest, src + idx, temp_copy_byte);
		//printf(".%s\n", dest);
		idx += temp_copy_byte;
		copy_byte += temp_copy_byte;
	}
	if (idx == mtu) {
		//printf("idx = 0 \n");
		idx = 0;
	}
	if (copy_byte == len_of_data) {
		//printf("copy_byte.. %d\n", copy_byte);
		EM_LOGD(TAG, "copy_byte.. %d\n", copy_byte);
		//printf(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n");
		EM_LOGD(TAG, ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n");
		copy_byte = 0;
		return 1;
	} else {
		//printf("copy_byte %d\n", copy_byte);
		EM_LOGD(TAG, "copy_byte %d\n", copy_byte);
		return 0;
	}
}

/**
 * @brief
 *
 * @return
 *
 **/
static em_err parse_and_store_byte_stream(char* ble_read_buff, DeviceConfig *devcfg) {

	em_err err = EM_FAIL;
	static BYTE_STREAM_DATA byte_stream_data = SSID_BYTE_STREAM;

	switch (byte_stream_data) {
	case SSID_BYTE_STREAM: {
		if (copy_data(devcfg->wifi_cfg.wifi_ssid, ble_read_buff, devcfg->wifi_cfg.wifi_ssid_len) == 1) {
			EM_LOGI(TAG, "[wifi_ssid]: %s", devcfg->wifi_cfg.wifi_ssid);
			byte_stream_data = PWD_BYTE_STREAM;
//			if (idx == 0) {
//				err = EM_ON_PROCESS;
//				break;
//			}
		} else {
			err = EM_ON_PROCESS;
			break;
		}
	}

	case PWD_BYTE_STREAM: {
		if (copy_data(devcfg->wifi_cfg.wifi_pwd, ble_read_buff, devcfg->wifi_cfg.wifi_pwd_len) == 1) {
			EM_LOGI(TAG, "[wifi_pwd]: %s", devcfg->wifi_cfg.wifi_pwd);
			byte_stream_data = CERT_BYTE_STREAM;
//			if (idx == 0) {
//				err = EM_ON_PROCESS;
//				break;
//			}
		} else {
			err = EM_ON_PROCESS;
			break;
		}
	}
#if 0
		case CERT_BYTE_STREAM: {
			if (aws_cred_data.aws_cert_metadata == METADATA_ELEMENT_RECEIVED) {
				if (copy_data(aws_cred_data.aws_cert, ble_read_buff,
								aws_cred_data.size_of_aws_cert) == 1) {
					EM_LOGI(TAG, "aws_cert %s\n", aws_cred_data.aws_cert);
					//aws_cred_data.aws_cert_recv_stat = DATA_RECEIVED;
					aws_cred_data.aws_cert_data_present = true;
					byte_stream_data = KEY_BYTE_STREAM;
					aws_cred_data.aws_cert_metadata = NOT_RECEIVED;
					if (idx == 0) {
						break;
					}
				} else {
					break;
				}
			}
		}
		case KEY_BYTE_STREAM: {
			if (aws_cred_data.aws_key_metadata == METADATA_ELEMENT_RECEIVED) {
				if (copy_data(aws_cred_data.aws_key, ble_read_buff,
								aws_cred_data.size_of_aws_key) == 1) {
					EM_LOGI(TAG, "aws_key %s\n", aws_cred_data.aws_key);
					//aws_cred_data.aws_key_recv_stat = DATA_RECEIVED;
					aws_cred_data.aws_key_data_present = true;
					byte_stream_data = ROOTCA_BYTE_STREAM;
					aws_cred_data.aws_key_metadata = NOT_RECEIVED;
					if (idx == 0) {
						break;
					}
				} else {
					break;
				}
			}
		}
		case ROOTCA_BYTE_STREAM: {
			if (aws_cred_data.aws_rootca_metadata == METADATA_ELEMENT_RECEIVED) {
				if (copy_data(aws_cred_data.aws_rootca, ble_read_buff,
								aws_cred_data.size_of_aws_rootca) == 1) {
					EM_LOGI(TAG, "aws_rootca %s\n", aws_cred_data.aws_rootca);
					//aws_cred_data.aws_rootca_recv_stat = DATA_RECEIVED;
					aws_cred_data.aws_rootca_data_present = true;
					byte_stream_data = URL_BYTE_STREAM;
					aws_cred_data.aws_rootca_metadata = NOT_RECEIVED;
					if (idx == 0) {
						break;
					}
				} else {
					break;
				}
			}
		}
		case URL_BYTE_STREAM: {
			if (aws_cred_data.aws_url_metadata == METADATA_ELEMENT_RECEIVED) {
				if (copy_data(aws_cred_data.aws_url, ble_read_buff,
								aws_cred_data.size_of_aws_url) == 1) {
					EM_LOGI(TAG, "aws_url %s\n", aws_cred_data.aws_url);
					//aws_cred_data.aws_url_recv_stat = DATA_RECEIVED;
					aws_cred_data.aws_url_data_present = true;
					byte_stream_data = THING_BYTE_STREAM;
					aws_cred_data.aws_url_metadata = NOT_RECEIVED;
					if (idx == 0) {
						break;
					}
				} else {
					break;
				}
			}
		}
		case THING_BYTE_STREAM: {
			if (aws_cred_data.aws_thing_name_metadata
					== METADATA_ELEMENT_RECEIVED) {
				if (copy_data(aws_cred_data.aws_thing_name, ble_read_buff,
								aws_cred_data.size_of_aws_thing_name) == 1) {
					EM_LOGI(TAG, "aws_thing_name %s\n",
							aws_cred_data.aws_thing_name);
					//aws_cred_data.aws_url_recv_stat = DATA_RECEIVED;
					aws_cred_data.aws_thing_name_data_present = true;
					byte_stream_data = SSID_BYTE_STREAM;
					aws_cred_data.aws_thing_name_metadata = NOT_RECEIVED;
					if (idx == 0) {
						break;
					}
				} else {
					break;
				}
			}

		}
#endif
	default: {
		byte_stream_data = SSID_BYTE_STREAM;
		idx = 0;
		copy_byte = 0;
		mtu = 20;
		err = EM_OK;
		break;
	}
	}
	return err;
}

/*******************************************************************************************************/

em_err parser_cnf_data(QueueHandle *recv_queue, DeviceConfig *devcfg, char* ble_buff, uint16_t sizeof_ble_buff,
		char* ble_authId) {
	em_err err = EM_FAIL;

#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_BLE_CFG)
	if (recv_queue != NULL) {
		err = EM_ON_PROCESS;
//		int i = 0;
		while (1) {
			BLE_DATA ble_recv_data = { .data = { 0 }, .length = 0 };
			if (QueueReceive(recv_queue, &ble_recv_data, (TickType ) 10)) {
//				printf("ble_recv_data = %s\r\n", ble_recv_data.data);
//				printf("ble_recv_data count = %d\r\n", i++);
//				printf("ble_recv_data length = %d\r\n", ble_recv_data.length);
//				for (int j = 0; j < ble_recv_data.length; j++) {
//					printf("%c, %02x\r\n", ble_recv_data.data[j], ble_recv_data.data[j]);
//				}

				strncat(ble_buff, ble_recv_data.data, ble_recv_data.length);
				static int val = 0;
				val += ble_recv_data.length;
//				printf("total recv_data length = %d\r\n", val);

//				for (int j = 0; j < strlen(ble_buff); j++) {
//					printf("%c, %02x\r\n", ble_buff[j], ble_buff[j]);
//				}

				if (strstr(ble_buff, BLE_CMD_POSTFIX) != NULL && get_ble_auth_stat() == BLE_AUTH_NOT_DONE) {
					err = parse_ble_metadata(ble_buff, ble_authId, devcfg);
					memset(devcfg->wifi_cfg.wifi_ssid, 0x00, sizeof(devcfg->wifi_cfg.wifi_ssid));
					memset(devcfg->wifi_cfg.wifi_pwd, 0x00, sizeof(devcfg->wifi_cfg.wifi_pwd));
					memset(ble_buff, 0x00, sizeof_ble_buff);
					data_parse_stat = false;
//					printf("err.. = %d, %s\r\n", err, (err == EM_OK) ? "EM_OK" : "!EM_OK");
					val = 0;
					break;
				} else if (val >= ((float) sizeof_ble_buff * (float) 0.95)) {
					err = EM_FAIL;
					memset(ble_buff, 0x00, sizeof_ble_buff);
					val = 0;
					break;
				} else {
					if (get_ble_auth_stat() == BLE_AUTHENTICATED) {
						if (true != data_parse_stat) {
							err = parse_and_store_byte_stream(ble_buff, devcfg);
							memset(ble_buff, 0x00, sizeof_ble_buff);
							if (err == EM_OK) {
//								printf("data_parse_true\r\n");
								data_parse_stat = true;
								if (QueueMessagesWaiting(recv_queue) > 0) {
									continue;
								}
							}
						} else {
//							printf("invalid response\r\n");
							memset(ble_buff, 0x00, sizeof_ble_buff);
							err = EM_ERR_INVALID_RESPONSE;
						}
					}
				}
				set_ble_ongoing_process_stat(BLE_CONFIGURATION_IN_PROCESS);
				//goto CONTINUE;
			} else {
				break;
			}
		}
	}
#endif
//	printf("err.... = %d, %s\r\n", err,
//			(err == EM_OK) ? "EM_OK" : "!EM_OK");
	return err;
}
