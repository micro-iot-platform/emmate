/*
 * File Name: device_config.c
 * File Path: /emmate/src/system/device_config.c
 * Description:
 *
 *  Created on: 13-Apr-2019
 *      Author: Rohan Dey
 */

#include "device_config.h"
#include "parser_utils.h"
#include "persistent_mem.h"
#include "conn.h"

#include <string.h>

#include "core_logger.h"
#include "threading.h"
#include "module_thread_priorities.h"
#include "system_utils.h"
#include "system_hmi_led_notification.h"

#define TAG "device_config"

#if CONFIG_BLE_CFG

#include "ble_config.h"
#define BLE_ADV_NAME "EmMate-BLE"

#define BLE_AUT_ID		"esp32_core12345"

#endif

static QueueHandle recv_queue = NULL;

// static fun
static em_err init_config_peripheral() {
	em_err ret = EM_FAIL;

#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_BLE_CFG)
	recv_queue = QueueCreate(BLE_DATA_QUEUE_MAX_SIZE, sizeof(BLE_DATA)); // size of spp_mtu_size
	// TODO: init and enable ble
	ret = init_ble_cfg(&recv_queue, BLE_ADV_NAME, BLE_AUT_ID);
#endif

#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_USB_CFG)

#endif

#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_UART_CFG)

#endif

	return ret;
}

static em_err start_device_cfg(DeviceConfig *devcfg) {
	em_err ret = EM_FAIL;
#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_BLE_CFG)
	ret = start_ble_cfg();
#endif

#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_USB_CFG)

#endif

#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_UART_CFG)

#endif

	return ret;
}

#define BLE_RECV_DATA_LEN	300

static TaskRet device_cfg_pasrser_task(void * pvParameters) {
	em_err err = EM_FAIL;
	DeviceConfig *devcfg = ((DeviceConfig *) pvParameters);
	char *ble_metabuff = NULL;
	ble_metabuff = (char*) calloc(sizeof(char), BLE_RECV_DATA_LEN);
	if (ble_metabuff == NULL) {
		EM_LOGE(TAG, "Failed to allocate memory for BLE Metadata");
		// TODO: Handle Error
	}

	while (1) {
#if (CONFIG_SELECTED_SYSTEM_CONFIG_PERIPHERAL == CONFIG_BLE_CFG)
		if (isbleconnected() == BLE_CONNECTED) {
			// De-queue the ble recv data and parse it
			err = parser_cnf_data(recv_queue, devcfg, ble_metabuff, BLE_RECV_DATA_LEN, BLE_AUT_ID);
			// return EM_OK on parse successfull
			if (err == EM_OK) {
//				printf("err.._____ = %d, %s\r\n", err, (err == EM_OK) ? "EM_OK" : "!EM_OK");
				/* On the status of Ble authentication not done
				 * & the parse is successfull
				 * then the BLE authentication status set as  'BLE_AUTH_SUCCESS"
				 */
				if (get_ble_auth_stat() == BLE_AUTH_NOT_DONE) {
//					printf("BLE_AUTH_SUCCESS\r\n");
					set_ble_auth_stat(BLE_AUTH_SUCCESS);
				}
				/*
				 * BLE received data successfully parse after the BLE authentication successfully done,
				 * then the device config data saved into the NVS and set the BLE received status as BLE_BYTEDATA_RECV_DONE
				 */
				else if (get_ble_auth_stat() == BLE_AUTHENTICATED) {
					write_devconfig_to_persistent_mem(devcfg);

					set_ble_data_process_stat(BLE_BYTEDATA_RECV_DONE);
					event_group_set_bits(get_system_evtgrp_hdl(), DEVCFG_DATA_VALIDATION_BIT);

//					show_network_connecting_notification();

					/* Wait until we get an IP */
					if (get_system_evtgrp_hdl() != NULL) {
						EventBits evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(),
								(CONN_GOT_IP_BIT | CONN_GOT_IP_FAILED), false, false, EventMaxDelay);

						if ((evt_bits & (CONN_GOT_IP_BIT | CONN_GOT_IP_FAILED)) == CONN_GOT_IP_BIT) {
							EM_LOGI(TAG, "CONN_WIFI_GOT_IP_BIT is set\r\n");
							set_ble_data_process_stat(BLE_SEND_RES_DEVCFG_DONE);
							event_group_set_bits(get_system_evtgrp_hdl(), DEVCFG_COMPLETE_BIT);
							TaskDelay(DELAY_30_SEC/TICK_RATE_TO_MS);
							goto KILL_ITSELF;
						} else if ((evt_bits & (CONN_GOT_IP_BIT | CONN_GOT_IP_FAILED)) == CONN_GOT_IP_FAILED) {
							/* Set status for device config failed */
							set_ble_data_process_stat(BLE_SEND_RES_DEVCFG_FAILED);

							/* Wait for the BLE Configurator to send failed message */
							TaskDelay(DELAY_5_SEC/TICK_RATE_TO_MS);
							/*Set event group bit to indicate that device config is completed */
							event_group_set_bits(get_system_evtgrp_hdl(), DEVCFG_COMPLETE_BIT);
							TaskDelay(DELAY_5_SEC/TICK_RATE_TO_MS);
							goto KILL_ITSELF;
						}
					} else {
						EM_LOGE(TAG, "System event handler is NULL: %s, %d\r\n", (char*)__FILE__, __LINE__);
					}

				}
			} else if (err == EM_ON_PROCESS) {

			}
			//return EM_ERR_INVALID_RESPONSE on Byte-stream > required data length
			else if (err == EM_ERR_INVALID_RESPONSE) {
				set_ble_data_process_stat(BLE_BYTEDATA_RECV_FAILED);

			}
			// return EM_FAIL on parse failed or wrong data input
			else if (err == EM_FAIL) {
				set_ble_auth_stat(BLE_AUTH_FAILED);
			}
			TaskDelay(DELAY_5_MSEC/TICK_RATE_TO_MS);
		} else {
			memset(ble_metabuff, 0x00, BLE_RECV_DATA_LEN);
		}
#endif
		TaskDelay(DELAY_100_MSEC/TICK_RATE_TO_MS);
	}

	/*
	 * TODO: process for kill the device_cfg_pasrser_task */
	KILL_ITSELF: {
		free(ble_metabuff);
		ble_metabuff = NULL;
#if defined (CONFIG_PLATFORM_ESP_IDF)
		TaskDelete(NULL);
#endif
	}

}

/***************************************************************************************************/

void set_devcfg_to_default(DeviceConfig *devcfg) {
	memset(devcfg, 0, sizeof(DeviceConfig));
	devcfg->checklist_mask = 0x01;
	devcfg->devid_len = DEV_ID_LEN;

#if CONFIG_USE_BLE
#endif
#if CONFIG_USE_WIFI
	devcfg->wifi_cfg.wifi_ssid_len = WIFI_SSID_LEN;
	devcfg->wifi_cfg.wifi_pwd_len = WIFI_PWD_LEN;
#endif

#if CONFIG_USE_ETH
#endif

#if CONFIG_USE_NBIOT
#endif

#if CONFIG_USE_GSM
#endif

	devcfg->opermode_len = OPERMODE_LEN;
}

em_err init_and_start_device_cfg(DeviceConfig *devcfg) {
	em_err err = EM_FAIL;

	err = init_config_peripheral();
	if (err == EM_FAIL) {
		EM_LOGE(TAG, "init_config_peripheral failed!");
		return err;
	}

	err = start_device_cfg(devcfg);
	if (err == EM_FAIL) {
		EM_LOGE(TAG, "failed to start device configuration!");
		return err;
	}

	BaseType xReturned = TaskCreate(device_cfg_pasrser_task, "device_cfg_parser", TASK_STACK_SIZE_4K, devcfg,
			THREAD_PRIORITY_DEVICE_CONFIG, NULL);
	if (true != xReturned) {
		EM_LOGE(TAG, "failed to create device_cfg_parser!");
		/* TODO: If code reaches here, the device config peripheral thread must be stopped and resources must be cleaned
		 before returning; */
		err = EM_FAIL;
	} else {
		err = EM_OK;
	}

	return err;
}
