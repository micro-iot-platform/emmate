/*
 * system_init.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */
#include "core_config.h"
//#include "system.h"
#include "system_init.h"
#include "system_constants.h"

#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#if CONFIG_USE_MIGCLOUD
#include "migcloud_storage.h"
#endif
#endif
#include "gpio_helper_api.h"
#if CONFIG_USE_ADC
#include "adc_core.h"
#endif
#if CONFIG_USE_HMI
#include "hmi.h"
#include "system_hmi_led_notification.h"
#endif
#if CONFIG_USE_CONN
#include "conn.h"
#endif
#include "system_utils.h"
#if CONFIG_USE_SYSTIME
#include "systime.h"
#endif
#include "tcpip_adapter_core.h"
#if CONFIG_USE_SOM_REGISTRATION
#include "som_registration.h"
#endif
#if CONFIG_USE_SYS_HEARTBEAT
#include "sys_heartbeat.h"
#endif
#if CONFIG_USE_SYS_STANDBY
#include "sys_standby.h"
#endif
#if CONFIG_USE_HW_IDENTIFY
#include "hw_identify.h"
#endif
#if CONFIG_USE_FOTA
#include "fota_core.h"
#include "fota_core_verify.h"
#endif
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include "threading.h"
#include <string.h>

#define TAG	LTAG_SYSTEM_SYSINIT

/* Static Function Declarations */
static void get_opermode_name_by_value(uint16_t opermode, char *opername) {
	switch (opermode) {
	case STARTUP:
		strcpy(opername, MODE_NAME_STARTUP);
		break;
	case SYSTEM_CONFIG_MODE_FACTORY:
		strcpy(opername, MODE_NAME_SYSTEM_CONFIG_MODE_FACTORY);
		break;
	case NETWORK_CONNECTION_MODE:
		strcpy(opername, MODE_NAME_NETWORK_CONNECTION_MODE);
		break;
	case SETUP_SYSTIME_SNTP:
		strcpy(opername, MODE_NAME_SETUP_SYSTIME_SNTP);
		break;
	case SOM_REGISTRATION_MODE:
		strcpy(opername, MODE_NAME_SOM_REGISTRATION_MODE);
		break;
	case SYSTEM_HEARTBEAT:
		strcpy(opername, MODE_NAME_SYSTEM_HEARTBEAT);
		break;
	case START_APPLICATION:
		strcpy(opername, MODE_NAME_START_APPLICATION);
		break;
	case MAINTENANCE_MODE:
		strcpy(opername, MODE_NAME_MAINTENANCE_MODE);
		break;
	case OTA_UPDATE_MODE:
		strcpy(opername, MODE_NAME_OTA_UPDATE_MODE);
		break;
	case SYS_DO_NOTHING:
		strcpy(opername, MODE_NAME_SYS_DO_NOTHING);
		break;
	}
}

/*
 * Check if Startup Mode requires HMI. If yes then initialize HMI's LED module
 * Initialize persistent memory
 * Read saved operating mode from persistent memory if any, else start with device config mode
 * Select Operation Mode depending upon the configurations read
 * If Operation Mode = Device Configuration Mode, then select & initialize the required configuration peripheral (BLE/USB/UART)
 * If Operation Mode = OTA Update, then select & initialize network interface and required peripherals
 *
 * */
em_err init_system(SystemData *sys_data) {
	em_err ret = EM_FAIL;
	EventBits evt_bits;

#if CONFIG_SHOW_THREAD_DEBUG
	// start task List print task
	start_sysTaskList_task();
#endif

/* Initialize Peripherals & HMI Modules */
	gpio_api_initialize_gpio();
#if CONFIG_USE_ADC
	adc_core_initialize_adc();
#endif
#if CONFIG_USE_LED
	init_leds(LED_HELPER_MODE_SYSTEM);
#endif
#if CONFIG_USE_BUTTON
	init_buttons();
#endif

/* Check if the system requires HMI */
#if CONFIG_HAVE_SYSTEM_HMI
	init_startup_hmi_module();
#endif	/* CONFIG_HAVE_SYSTEM_HMI */

	/* Initialize all available persistent memory for configuration and data storage */
#if CONFIG_USE_PERSISTENT
	ret = init_persistent_mem_for_config();
	EM_LOGD(TAG, "init_persistent_mem_for_config");

	ret = init_persistent_mem_for_data_storage();

	ret = read_opermode_from_persistent_mem();
	if (ret == EM_FAIL) {
		// TODO: Handle error
		EM_LOGE(TAG, "Error: read_opermode_from_persistent_mem failed. %s, %d", (char*) __FILE__, __LINE__);
		return ret;
	} else {
		sys_data->oper_mode = ret;
		char oper_name[64] = { 0 };
		get_opermode_name_by_value(sys_data->oper_mode, oper_name);
		EM_LOGD(TAG, "Found Operation Mode = (%s) from persistent memory", oper_name);
	}
#else
	sys_data->oper_mode = SYSTEM_CONFIG_MODE_FACTORY;
#endif


#if CONFIG_USE_CONN
	/* Initialize Connection interfaces prerequisite setup */
	conn_prerequisite_setup();
#endif

#if CONFIG_USE_MIGCLOUD
	/* Read and save the SOM ID */
	read_save_somthing_id();
//#endif

//#if CONFIG_USE_SYS_STANDBY
	/* Set last state of system standby values */
	SYSTEM_STANDBY_MODE standby_mode = STANDBY_MODE_OFF;
//#if CONFIG_USE_PERSISTENT
//#if CONFIG_USE_MIGCLOUD
	ret = read_sys_standby_mode_from_persistent_mem((int*) &standby_mode);
	if (ret != EM_OK) {
		EM_LOGW(TAG, "No Standby Mode is saved, setting the standby mode to STANDBY_MODE_OFF");
		standby_mode = STANDBY_MODE_OFF;
		write_sys_standby_mode_to_persistent_mem(standby_mode);
	}
//#endif
//#endif

	migcloud_set_standby_hw_mode(standby_mode);
	/* Set last state of system heartbeat values */
#endif

	bool is_sys_init_done = false;
	bool is_device_config_reqd = false;

	do {
		/* Check the extracted operation mode and proceed accordingly */
		switch (sys_data->oper_mode) {
		case SYSTEM_CONFIG_MODE_FACTORY: {
#if CONFIG_USE_DEVCONFIG
			EM_LOGI(TAG, "============================ SYSTEM_CONFIG_MODE_FACTORY ============================\r\n");

			EM_LOGI(TAG, "Starting the Device Configuration Process ...");

			/* Initialize and start the device configuration via the selected device config peripheral */
			ret = init_and_start_device_cfg(&sys_data->dev_cfg);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Error: init_and_start_device_cfg failed %s, %d", (char*) __FILE__, __LINE__);
				return ret;
			}

			/* Here we wait for the device configuration data to be received and validated */
			evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(), DEVCFG_DATA_VALIDATION_BIT, true, false,
					EventMaxDelay);
			if ((evt_bits & DEVCFG_DATA_VALIDATION_BIT) == DEVCFG_DATA_VALIDATION_BIT) {
				EM_LOGI(TAG, "System configuration data is received successfully");
				/* SYSTEM_CONFIG_MODE_FACTORY is done, now we move to NETWORK_CONFIG_MODE */
				sys_data->oper_mode = NETWORK_CONNECTION_MODE;
				is_device_config_reqd = true;
			}
#else
			sys_data->oper_mode = NETWORK_CONNECTION_MODE;
#endif	/* CONFIG_USE_DEVCONFIG */
			break;
		}
		case NETWORK_CONNECTION_MODE: {
#if (CONFIG_USE_CONN)
			EM_LOGI(TAG, "============================== NETWORK_CONNECTION_MODE ==============================\r\n");

#if (CONFIG_USE_DEVCONFIG & CONFIG_USE_PERSISTENT)
			/* Read the saved config from persistent memory */
			ret = read_devconfig_from_persistent_mem(&sys_data->dev_cfg);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Error: read_devconfig_from_persistent_mem failed %s, %d", (char*) __FILE__, __LINE__);
				return ret;
			}
#endif	/* (CONFIG_USE_DEVCONFIG & CONFIG_USE_PERSISTENT) */

#if CONFIG_USE_WIFI
			/////////////////////////////////////////////////////////////////////
			WiFiCredentials *cred = get_wifi_credentials();
			if (cred != NULL) {
				EM_LOGI(TAG, "Printing stored WiFi Credentials");
				EM_LOGI(TAG, "NUM CFG = %d", cred->config_count);
				for (int i = 0; i < cred->config_count; i++) {
					EM_LOGI(TAG, "[%d] SSID = %s", i, cred->cfg[i].wifi_ssid);
					EM_LOGI(TAG, "[%d] PASS = %s", i, cred->cfg[i].wifi_pwd);
				}
			} else {
				EM_LOGI(TAG, "No saved WiFi Credentials");
			}
			/////////////////////////////////////////////////////////////////////
#endif /* CONFIG_USE_WIFI */

			/* Validate that all configurations required in Level-1 are set */
			bool is_config_ok = false;
#if CONFIG_PLATFORM_SIMULATOR
			is_config_ok = true;
#else
			VALIDATE_DEVICE_CONFIG(sys_data->dev_cfg.dev_conf_checklist, DEVCFG_VALIDATION_MASK, is_config_ok);
#endif	/* CONFIG_PLATFORM_SIMULATOR */

			/* Check if configurations are valid */
			if (is_config_ok) {
				/* Start network connection thread */
				ret = start_network_connection();

				/* We wait only if device config has been run before coming here */
				if (is_device_config_reqd) {
					is_device_config_reqd = false;
					/* Network connection is successful. Acknowledge the user via device config peripheral */
					/* 4. Acknowledge the user via device config peripheral */

					/* Wait here until the Device Configuration is completed */
					evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(), DEVCFG_COMPLETE_BIT, true, false,
							EventMaxDelay);

					if ((evt_bits & DEVCFG_COMPLETE_BIT) == DEVCFG_COMPLETE_BIT) {
						/* If event_group_wait_bits () provides the same bit, which we are looking for.
						 * then continue with remaining bit dependent process or else execute other functionality. */
						EM_LOGI(TAG, "Device configuration is completed successfully\r\n");
					} else {
						/* If event_group_wait_bits () provides the same bit, which we are looking for.
						 * then continue with remaining bit dependent process or else execute other functionality. */
					}
				}

#if CONFIG_USE_PERSISTENT
				/* NETWORK_CONNECTION_MODE is done */
				/* And save NETWORK_CONNECTION_MODE to persistent memory so that the device starts running network connection after rebooting */
				ret = write_opermode_to_persistent_mem(sys_data->oper_mode);
				if (ret != EM_OK) {
					EM_LOGE(TAG, "write_opermode_to_persistent_mem failed");
					return ret;
				}
#endif /* CONFIG_USE_PERSISTENT */

				sys_data->oper_mode = VERIFY_RUNNING_FIRMWARE;
			} else {
				EM_LOGI(TAG,
						"Device configurations are invalid! Changing operation mode to SYSTEM_CONFIG_MODE_FACTORY");
				sys_data->oper_mode = SYSTEM_CONFIG_MODE_FACTORY;
			}
#else	/* #if (CONFIG_USE_CONN & CONFIG_USE_WIFI) */
			sys_data->oper_mode = VERIFY_RUNNING_FIRMWARE;
#endif
			break;
		}

		case VERIFY_RUNNING_FIRMWARE: {
#if CONFIG_USE_FOTA
			EM_LOGI(TAG, "============================== VERIFY_RUNNING_FIRMWARE ==============================\r\n");
			/* OTA Image Verification Process */
			/*
			 * TODO: Please Enable app rollback support from make menuconfig
			 * for execute Application Rollback Functionality (for ESP32)
			 */
			migcloud_verify_running_firmware();
#endif

#if CONFIG_USE_SYSTIME
				/* Now we move to SETUP_SYSTIME_SNTP */
				sys_data->oper_mode = SETUP_SYSTIME_SNTP;
#elif CONFIG_USE_SOM_REGISTRATION
				sys_data->oper_mode = SOM_REGISTRATION_MODE;
#elif (CONFIG_USE_HW_IDENTIFY) || (CONFIG_USE_SYS_HEARTBEAT)
				if (!is_hw_somthing_id_present()) {
					sys_data->oper_mode = HARDWARE_IDENTIFICATION;
				} else {
					sys_data->oper_mode = SYSTEM_HEARTBEAT;
				}
#else
				sys_data->oper_mode = START_APPLICATION;
#endif /* CONFIG_USE_SYSTIME */
			break;
		}

#if CONFIG_USE_SYSTIME
		case SETUP_SYSTIME_SNTP: {
			EM_LOGI(TAG, "============================== SETUP_SYSTIME_SNTP ==============================\r\n");
#if CONFIG_HAVE_SNTP_SYSTIME
			start_system_time_module();
#endif

#if CONFIG_USE_SOM_REGISTRATION
			/* now we move to SOM_REGISTRATION_MODE */
			sys_data->oper_mode = SOM_REGISTRATION_MODE;
#elif (CONFIG_USE_HW_IDENTIFY) || (CONFIG_USE_SYS_HEARTBEAT)
			if (!is_hw_somthing_id_present()) {
				EM_LOGD(TAG, "No Hard-Wired SOMTHING ID present, starting Hardware Identification module");
				sys_data->oper_mode = HARDWARE_IDENTIFICATION;
			} else {
				EM_LOGD(TAG, " Hard-Wired SOMTHING ID present, starting System Heartbeat module");
				sys_data->oper_mode = SYSTEM_HEARTBEAT;
			}
#else
			sys_data->oper_mode = START_APPLICATION;
//			TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);
#endif /* CONFIG_USE_SOM_REGISTRATION */
			break;
		}
#endif /* CONFIG_USE_SYSTIME */

#if CONFIG_USE_SOM_REGISTRATION
			case SOM_REGISTRATION_MODE: {
				EM_LOGI(TAG, "============================== SOM_REGISTRATION_MODE ==============================\r\n");
				/* Check for Internet availability */
				/* wait for network to connect */
				evt_bits = event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

				static uint8_t som_regn_retry = 1;
				/* Do the SOM registration */
				ret = register_som();

				if (ret == EM_OK) {
					/* SOM Registration is done, now start the application
					 * When an application is using the SOM Registration module, it should not need Sys Heartbeat and OTA module
					 * */
					sys_data->oper_mode = START_APPLICATION;
				} else {
					// TODO: implement retry logic
					if (++som_regn_retry > 5) {
						EM_LOGI(TAG, "Retrying SOM registration for %d time ...", som_regn_retry);
						/* SOM Registration failed */
						sys_data->oper_mode = SYS_DO_NOTHING;
						TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
					}
				}
				break;
			}
#endif

		case HARDWARE_IDENTIFICATION: {
			EM_LOGI(TAG, "============================== HARDWARE_IDENTIFICATION ==============================\r\n");
#if CONFIG_USE_HW_IDENTIFY
			migcloud_start_hardware_identification();
#else
			/* The sys heartbeat module should be waiting on this bit. It is actually set inside the hardware identification
			 * thread. Since the hw identification functionality is not enabled, we set the START_SYSHEARTBEAT_BIT bit here */
			event_group_set_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT);
#endif
			sys_data->oper_mode = SYSTEM_HEARTBEAT;
			break;
		}

		case SYSTEM_HEARTBEAT: {
			EM_LOGI(TAG, "============================== SYSTEM_HEARTBEAT ==============================\r\n");
			if (is_hw_somthing_id_present()) {
				/* The sys heartbeat module should be waiting on this bit. It is actually set inside the hardware identification
				 * thread. Since the hw identification functionality is not enabled, we set the START_SYSHEARTBEAT_BIT bit here */
				event_group_set_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT);
			}
#if CONFIG_USE_SYS_HEARTBEAT
			/* Start the System Heartbeat Process */
			migcloud_start_heartbeat();
#endif
			/* Initialization complete, now start the application */
			sys_data->oper_mode = START_APPLICATION;
			break;
		}

		case START_APPLICATION: {
			EM_LOGI(TAG, "============================== START_APPLICATION ==============================\r\n");
			/* If control reaches here, it means everything is OK, set set the return value to EM_OK */
			ret = EM_OK;
			/* Start the application */
			is_sys_init_done = true;
			sys_data->oper_mode = SYS_DO_NOTHING;
			break;
		}
		case MAINTENANCE_MODE: {
			EM_LOGI(TAG, "============================== MAINTENANCE_MODE ==============================\r\n");
			break;
		}
		case SYS_DO_NOTHING: {
			/* The control should never come here unless an fatal error occurs */
			EM_LOGE(TAG, "\n\n**************************** SYS_DO_NOTHING ****************************\n\n");
			TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			break;
		}
		default: {
			break;
		}
		} // switch (sys_data->oper_mode)
	} while (!is_sys_init_done); // Loop until the system is not completely initialized

	return ret;
}

/*
 * Perform a Power On Self Test
 *
 *
 */

em_err perform_post() {

	// TODO: write POST fucntionality

	return EM_OK;
}
