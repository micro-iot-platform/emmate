/*
 * File Name: sysconfig_helper.c
 * File Path: /emmate/src/system/sysconfig/sysconfig_helper.c
 * Description:
 *
 *  Created on: 12-May-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include <stdarg.h>

#include "sysconfig_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "gll.h"

#define TAG LTAG_SYSTEM_SYSCFG

static gll_t *dclist = NULL;
static SYSCONFIG_STATUS sysconfig_stat = SYSCONFIG_FREE;

SYSCONFIG_STATUS get_sysconfig_stat() {
	return sysconfig_stat;
}

em_err init_sysconfig(int key_count, ...) {

	em_err ret = EM_FAIL;

	if (sysconfig_stat == SYSCONFIG_BUSY) {
		EM_LOGE(TAG, "Could not initialize the system configuration module because it is already in use!");
		return EM_FAIL;
	}

	sysconfig_stat = SYSCONFIG_BUSY;

	/* create linked list node */
	if (dclist == NULL) {
		dclist = gll_init();
		if (dclist == NULL) {
			EM_LOGE(TAG, "Could not allocate memory for System Configurator\n");
			return EM_ERR_NO_MEM;
		}
	}

	va_list valist;

	/* initialize valist for number of arguments */
	va_start(valist, key_count);

	for (int i = 0; i < key_count; i++) {
		/* Get the next key from the list */
		char *key = va_arg(valist, char*);
		int key_len = strlen(key);

		EM_LOGI(TAG, "%d) key = %s, len = %d\n", i, key, key_len);

		/* allocate memory for key */
		char *ptr = (char*) calloc((key_len + 1), sizeof(char));
		if (ptr == NULL) {
			EM_LOGE(TAG, "Could not allocate memory for key = %s\n", key);
			ret = EM_ERR_NO_MEM;
			break;
		} else {
			strcpy(ptr, key);
			SysconfigData *scdata = (SysconfigData*) calloc(1, sizeof(SysconfigData));
			if (scdata != NULL) {
				scdata->key = ptr;
				EM_LOGD(TAG, "%d) adding list node at position %d\n", i, dclist->size);
				EM_LOGD(TAG, "%d) node: key = %s %d\n", i, scdata->key, scdata->value);
				gll_add(dclist, scdata, dclist->size);
				ret = EM_OK;
			} else {
				EM_LOGE(TAG, "Could not allocate memory for SysconfigData!");
				ret = EM_ERR_NO_MEM;
				break;
			}
		}
	}

//cleanup:
	if (ret == EM_ERR_NO_MEM) {
		EM_LOGE(TAG, "Error occured while initializing System Config, cleaning up memory");
		deinit_sysconfig();
	}
	/* clean memory reserved for valist */
	va_end(valist);

	return ret;
}

em_err update_sysconfig_metakey(char *oldkey, char *newkey) {
	em_err ret = EM_FAIL;
	SysconfigData *scdata = NULL;

	for (int i=0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);

		if(strcmp(scdata->key, oldkey) == 0) {
			EM_LOGD(TAG, "found key = %s, having old value = %d\n", scdata->key, scdata->value);
			EM_LOGD(TAG, "updating new key = %s\n", newkey);
			scdata->key = newkey;
			return EM_OK;
		}
	}
	EM_LOGI(TAG, "could not find key = %s\n", oldkey);

	return ret;
}

em_err update_sysconfig_metavalue(char *oldkey, int newvalue) {
	em_err ret = EM_FAIL;
	SysconfigData *scdata = NULL;

	for (int i=0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);

		if(strcmp(scdata->key, oldkey) == 0) {
			EM_LOGD(TAG, "found key = %s, having old value = %d\n", scdata->key, scdata->value);
			EM_LOGD(TAG, "updating new value = %d\n", newvalue);
			scdata->value = newvalue;
			return EM_OK;
		}
	}
	EM_LOGI(TAG, "could not find key = %s\n", oldkey);

	return ret;
}

void print_sysconfig_list() {
	SysconfigData *scdata = NULL;
	EM_LOGI(TAG, "\n============== PRINTING METADATA LIST ==============\n");
	EM_LOGI(TAG, "meta list size is = %d\n", dclist->size);
	for (int i=0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);
		if (scdata != NULL) {
			EM_LOGI(TAG, "%d) key = %s,\tvalue = %d\n", i, scdata->key, scdata->value);
		} else {
			EM_LOGE(TAG, "%d) meta is NULL\n", i);
		}
	}
	EM_LOGI(TAG, "\n\n");
}

void deinit_sysconfig() {
	SysconfigData *scdata = NULL;

	EM_LOGD(TAG, "meta list size is = %d\n", dclist->size);
	for (int i = 0; i < dclist->size; i++) {
		scdata = gll_get(dclist, i);
		if (scdata != NULL) {
			free(scdata->key);
			free(scdata);
		} else {
			EM_LOGE(TAG, "%d) meta is NULL\n", i);
		}
	}
	gll_destroy(dclist);
	sysconfig_stat = SYSCONFIG_FREE;
}
