/*
 * File Name: sysconfig.c
 * File Path: /emmate/src/system/sysconfig/sysconfig.c
 * Description:
 *
 *  Created on: 12-May-2019
 *      Author: Rohan Dey
 */

#include "sysconfig.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "core_utils.h"
#include "threading.h"

#define TAG	LTAG_SYSTEM_SYSCFG

SystemConfig syscfg;

void do_system_configuration(void *params) {
	em_err ret = EM_FAIL;
	while (1) {
		if (get_sysconfig_stat() == SYSCONFIG_FREE) {

			/* Initialize the System Configuration by passing all the keys */
			ret = init_sysconfig(3, GET_VAR_NAME(syscfg.devid, "."), GET_VAR_NAME(syscfg.wifi_cfg.wifi_ssid, "."),
					GET_VAR_NAME(syscfg.wifi_cfg.wifi_pwd, "."));
			if (ret != EM_OK) {
				EM_LOGE(TAG, "System config initialization failed");
				// TODO: Handle error
			}

			/* Get the Metadata from the peripheral */

			/* Parse it and populate the length fields in the SystemConfig data structure */

			/* Now update each key's length value */
			update_sysconfig_metavalue(GET_VAR_NAME(syscfg.devid, "."), syscfg.devid_len);
			update_sysconfig_metavalue(GET_VAR_NAME(syscfg.wifi_cfg.wifi_ssid, "."), syscfg.wifi_cfg.wifi_ssid_len);
			update_sysconfig_metavalue(GET_VAR_NAME(syscfg.wifi_cfg.wifi_pwd, "."), syscfg.wifi_cfg.wifi_pwd_len);

			/* Now */
		} else {
			TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
		}
	}
}
