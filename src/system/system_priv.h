/*
 * system_priv.h
 *
 *  Created on: 06-Apr-2020
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_PRIV_H_
#define SYSTEM_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#if CONFIG_USE_EVENT_GROUP
#include "event_group_core.h"
#endif
#if CONFIG_USE_DEVCONFIG
#include "device_config.h"
#endif


typedef enum {
	STARTUP = 200,					/*!< System StartUp Operation mode */
	SYSTEM_CONFIG_MODE_FACTORY,			/*!< System Lv1 Configuration Operation mode */
	NETWORK_CONNECTION_MODE,		/*!< System Network Connection Operation mode */
	VERIFY_RUNNING_FIRMWARE,
	SETUP_SYSTIME_SNTP,				/*!< Mode to setup system time via SNTP */
	SOM_REGISTRATION_MODE,			/*!< SOM Registration Mode */
	HARDWARE_IDENTIFICATION,		/*!< Hardware Identification Mode */
	SYSTEM_HEARTBEAT,				/*!< Mode to start the System Heartbeat process */
	START_APPLICATION,					/*!< System Working Operation mode */
	MAINTENANCE_MODE,				/*!< System Maintenance Operation mode */
	OTA_UPDATE_MODE,				/*!< System OTA Update Operation mode */
	SYS_DO_NOTHING					/*!< Mode where all error handling has been tried and nothing more can be done */
} SYSTEM_OPERATION_MODES;

typedef struct {
	SYSTEM_OPERATION_MODES oper_mode;			/*!< System Data variable for Operation Control */
#if CONFIG_USE_DEVCONFIG
	DeviceConfig dev_cfg;						/*!< System Data variable for Device Config */
#endif
#if CONFIG_USE_EVENT_GROUP
	EventGrpHandle sys_evt_hdl;					/*!< System Data variable for Group Event Control */
#endif
} SystemData;


#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_PRIV_H_ */
