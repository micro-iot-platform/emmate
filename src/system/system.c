/*
 * system.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include "system.h"
#include "system_priv.h"
#include "core_utils.h"
#if CONFIG_USE_FOTA
#include "fota_core.h"
#endif
#include "core_system_info.h"
#include "system_init.h"
#include "system_utils.h"
#include "system_platform.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include "module_thread_priorities.h"
#include "threading.h"
#if defined (CONFIG_PLATFORM_SIMULATOR)
#include "simulator_comm.h"
#endif

#define TAG LTAG_SYSTEM

/* Globals */
SystemData m_sys_data;

static void set_sysdata_to_default(SystemData *sysdata) {
	memset(sysdata, 0, sizeof(SystemData));
	sysdata->oper_mode = STARTUP;

	/* Set device configuration default values */
#if CONFIG_USE_DEVCONFIG
	set_devcfg_to_default(&sysdata->dev_cfg);
#endif
}

#if defined (CONFIG_PLATFORM_SIMULATOR)
void start_simulator(int argc, char *argv[]) {
	if (argc != 4) {
		fprintf(stderr, "usage: %s <ip address> <talker port number> <listener port number> \n", argv[0]);
		exit(1);
	}
	simulator_comm_main(argc, argv);

	/* Wait for the simulator to start */
	TaskDelay(2000);
}
#endif

void start_system() {
	em_err ret;

	/* Initialize the systems logger */
	init_core_logger((CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE | CORE_LOG_OUTPUT_NETWORK), EM_LOG_INFO);

	/* Set system log levels */
	set_all_log_levels_at_init();

	/* Initialize the system's platform layer */
	init_system_platform();

	/* Read and save the hardware specific information */
	read_save_system_info();

	/* Print the acquired system info */
	print_system_info();

	/* Initialize the underlying hardware */
	set_sysdata_to_default(&m_sys_data);

	/* Initialize the system level event group */
	m_sys_data.sys_evt_hdl = event_group_create();
#if CONFIG_PLATFORM_ESP_IDF
	if (m_sys_data.sys_evt_hdl == NULL) {
#elif CONFIG_PLATFORM_SIMULATOR
		if (m_sys_data.sys_evt_hdl == 0) {
#endif
		EM_LOGE(TAG, "Error: System Initialization failed! Rebooting!");
		/* Reboot the system and try again */
		core_system_restart();
	}

	/* Initialize the system */
	ret = init_system(&m_sys_data);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Error: System Initialization failed! Rebooting!");
		/* Reboot the system and try again */
		core_system_restart();
	}

	/* Register the factory reset button */
#if (CONFIG_HAVE_SYSTEM_HMI & CONFIG_ENABLE_SYSTEM_BUTTON_HMI)
	register_factory_reset_button();
#endif

	/* Perform a Power On Self Test */
	perform_post();
}

/**
 * @brief Start the User Application
 *
 * Create a thread to start the User Application.
 *
 */
void start_application() {
	/* Create a thread to perform the notifications */
	BaseType thread_stat;
	thread_stat = TaskCreate(emmate_main, "emmate-app", TASK_STACK_SIZE_8K, NULL, THREAD_PRIORITY_EMMATE_APP, NULL);
	if (thread_stat == false) {
		EM_LOGE(TAG, "Failed to <EmMate App> create thread!");
		printf("Rebooting\r\n");
		/* Reboot the system and try again */
		core_system_restart();
	}

	/* System Started Successfully, show LED notification */
//	show_system_running_notification();
}
