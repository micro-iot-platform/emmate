/*
 * File Name: threading_platform.c
 * File Path: /emmate/src/threading/esp-idf/threading_platform.c
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */
#define _GNU_SOURCE
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <time.h>
#include <string.h>

#include "threading_platform.h"
#include "gll.h"
#include "core_logger.h"

#define TAG "threading"
#define NAMELEN 16
static char task_name[NAMELEN] = {0x00};

BaseType_pf TaskPfCreate(TaskFuncPf func,
						const char * const name,
						const uint32_t stack_depth,
						void * const params,
						UBaseType_pf priority,
						TaskHandle_pf * task_id)
{

	pthread_attr_t tattr;
	int ret;
	struct sched_param param;
	pthread_t tid;

	/* initialized with default attributes */
	ret = pthread_attr_init (&tattr);

	/* safe to get existing scheduling param */
	ret = pthread_attr_getschedparam (&tattr, &param);

	/* set the priority; others are unchanged */
	param.sched_priority = (int) priority;

	/* setting the new scheduling param */
	ret = pthread_attr_setschedparam (&tattr, &param);

	/* with new priority specified */
	ret = pthread_create (&tid, &tattr, func, params);

	if (ret == 0) {
		pthread_setname_np(tid, name);
		if (task_id != NULL)
			*task_id = tid;
		return true;
	} else {
		return ret;
	}
}

BaseType_pf TaskPfCreatePinnedToCore(TaskFuncPf func,
						const char * const name,
						const uint32_t stack_depth,
						void * const params,
						UBaseType_pf priority,
						TaskHandle_pf * task_id,
                        const BaseType_pf core_id )
{
	return 0;
}

THREAD_STATE TaskPfGetState( TaskHandle_pf xTask )
{
	return TH_UNKNOWN;
}

void TaskPfDelete( TaskHandle_pf task_id )
{
	if (task_id != (TaskHandle_pf)NULL) {
		pthread_cancel(task_id);
	}
}

TickType_pf TaskPfGetTickCount( void )
{
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    return (uint64_t)(ts.tv_nsec / 1000000) + ((uint64_t)ts.tv_sec * 1000ull);
}

void TaskPfDelay(long msec_to_delay)
{
    struct timespec ts;
    int res;

    if (msec_to_delay < 0)
    {
        errno = EINVAL;
        return;
    }

    ts.tv_sec = msec_to_delay / 1000;
    ts.tv_nsec = (msec_to_delay % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);
}

char *TaskPfGetTaskName(TaskHandle_pf task_id)
{
	memset(task_name, 0x00, NAMELEN);
	pthread_getname_np(task_id, task_name, NAMELEN);
	return task_name;
}

UBaseType_pf TaskPfGetStackHighWaterMark( TaskHandle_pf xTask )
{
	return 0;
}

void TaskPfSuspend( TaskHandle_pf xTaskToSuspend )
{
	// nothing happens
}

void TaskPfResume( TaskHandle_pf xTaskToResume )
{
	// nothing happens
}

////////////////////////////

typedef struct {
	QueueHandle_pf q;
	UBaseType_pf max_len;
	UBaseType_pf item_size;
	pthread_cond_t qcond;
	pthread_mutex_t qmutex;
	int cond_check;
} QueueInfo;

static gll_t *qinfo_ll = NULL;
static pthread_mutex_t m_qmutex;

QueueHandle_pf QueuePfCreate(const UBaseType_pf q_length, const UBaseType_pf item_size) {

	if (qinfo_ll == NULL) {
		qinfo_ll = gll_init();
		pthread_mutex_init(&m_qmutex, NULL);
	}

	QueueHandle_pf q_to_return = NULL;

	/* Allocate memory for a qinfo_ll object. This will make a node */
	QueueInfo *qinfo = (QueueInfo*) calloc(1, sizeof(QueueInfo));
	if (qinfo == NULL) {
		EM_LOGE(TAG, "calloc failed %s, %d", __FUNCTION__, __LINE__);
		q_to_return = NULL;
		goto end_of_func;
	}
	qinfo->q = gll_init();
	qinfo->max_len = q_length;
	qinfo->item_size = item_size;
	pthread_cond_init(&qinfo->qcond, NULL);
	pthread_mutex_init(&qinfo->qmutex, NULL);
	qinfo->cond_check = 0;

	/* Queue handle to return to caller */
	q_to_return = qinfo->q;

	/* Add a qinfo_ll node */
	int ret = -1;

	pthread_mutex_lock(&m_qmutex);
	ret = gll_pushBack(qinfo_ll, qinfo);
	pthread_mutex_unlock(&m_qmutex);

	if (ret == -1) {
		EM_LOGE(TAG, "gll_pushBack failed %s, %d", __FUNCTION__, __LINE__);
		free(qinfo);
		q_to_return = NULL;
		goto end_of_func;
	}

	end_of_func: return q_to_return;
}

static int search_qinfo(QueueHandle_pf queue, QueueInfo **p_info, int *pos, gll_t *ll) {
	if (queue == NULL) {
		EM_LOGE(TAG, "Invalid Queue!");
		return -1;
	}
	int ll_size = ll->size;
	int ll_pos = 0;
	bool found = false;
	QueueInfo *qinfo = NULL;

	pthread_mutex_lock(&m_qmutex);
	for (ll_pos = 0; ll_pos < ll_size; ll_pos++) {
		qinfo = (QueueInfo*) gll_get(ll, ll_pos);
		if (qinfo != NULL) {
			if (qinfo->q == queue) {
//				EM_LOGD(TAG, "QueueInfo matched");
				found = true;
				break;
			}
		}
	}
	pthread_mutex_unlock(&m_qmutex);

	if (found) {
		*p_info = qinfo;
		*pos = ll_pos;
		return 0;
	} else {
		*p_info = NULL;
		return -1;
	}
}

BaseType_pf QueuePfSend(QueueHandle_pf queue, const void *const item_to_queue, TickType_pf ticks_to_wait) {
	if (queue == NULL) {
		EM_LOGE(TAG, "Invalid Queue!");
		return false;
	}
	if (qinfo_ll == NULL) {
		EM_LOGE(TAG, "Queue not initialized");
		return false;
	}

	/* Search for the QueueInfo from the qinfo_ll list */
	QueueInfo *qinfo = NULL;
	int pos = -1;
	int search_result = search_qinfo(queue, &qinfo, &pos, qinfo_ll);

	/* If the QueueInfo was found then enqueue the data by copy */
	if (search_result == 0) {
		/* Check if max queue length has reached */
		if (qinfo->q->size == qinfo->max_len) {
			EM_LOGD(TAG, "cannot enqueue, max queue length reached %s, %d", __FUNCTION__, __LINE__);
			return false;
		}

		/* Allocate memory and copy data */
		void *data = calloc(1, qinfo->item_size);
		if (data == NULL) {
			EM_LOGE(TAG, "calloc failed %s, %d", __FUNCTION__, __LINE__);
			return false;
		}
		memcpy(data, item_to_queue, qinfo->item_size);

		/* Enqueue/Push the data into the queue */
		pthread_mutex_lock(&qinfo->qmutex);
		int ret = gll_pushBack(qinfo->q, data);
		qinfo->cond_check = 0;
		if (ret != 0) {
			EM_LOGE(TAG, "gll_push failed %s, %d", __FUNCTION__, __LINE__);
			free(data);
			pthread_cond_broadcast(&qinfo->qcond);
			pthread_mutex_unlock(&qinfo->qmutex);
			return false;
		}
		pthread_cond_broadcast(&qinfo->qcond);
		pthread_mutex_unlock(&qinfo->qmutex);
	} else {
		EM_LOGE(TAG, "Invalid QueueHandle passed %s, %d", __FUNCTION__, __LINE__);
		return false;
	}

	return true;
}

BaseType_pf QueuePfReceive(QueueHandle_pf queue, void *const buffer, TickType_pf ms_to_wait) {
	if (queue == NULL) {
		EM_LOGE(TAG, "Invalid Queue!");
		return false;
	}
	if (qinfo_ll == NULL) {
		EM_LOGE(TAG, "Queue not initialized");
		return false;
	}

	/* Search for the QueueInfo from the qinfo_ll list */
	QueueInfo *qinfo = NULL;
	int pos = -1;
	int search_result = search_qinfo(queue, &qinfo, &pos, qinfo_ll);

	/* If the QueueInfo was found then dequeue the data by copy */
	if (search_result == 0) {
		/* Dequeue/Pop a data from the queue. Wait for ms_to_wait if queue is empty */
		void *data = gll_pop(qinfo->q);
		if (data == NULL) {
			pthread_mutex_lock(&qinfo->qmutex);
			qinfo->cond_check = 1;
			if (ms_to_wait == THREADING_MAX_DELAY_PF) {
				while (qinfo->cond_check != 0) {
					pthread_cond_wait(&qinfo->qcond, &qinfo->qmutex);
				}
			} else {
				if (qinfo->cond_check != 0) {
					struct timespec wait_time = { 0, 0 };
					clock_gettime(CLOCK_REALTIME, &wait_time);
					wait_time.tv_sec += (ms_to_wait / 1000);
					wait_time.tv_nsec += (ms_to_wait % 1000) * 1000000;

					pthread_cond_timedwait(&qinfo->qcond, &qinfo->qmutex, &wait_time);
					qinfo->cond_check = 0;
//					data = gll_pop(qinfo->q);
//					if (data == NULL) {
//						EM_LOGD(TAG, "Cannot dequeue, the queue is empty %s, %d", __FUNCTION__, __LINE__);
//						pthread_mutex_unlock(&qinfo->qmutex);
//						return -1;
//					}
				}
			}
			data = gll_pop(qinfo->q);
			pthread_mutex_unlock(&qinfo->qmutex);
		}
		/* Copy the data into caller's buffer */
		if (data != NULL) {
			memcpy(buffer, data, qinfo->item_size);
			/* The data is copied to the caller's buffer, now free it from our buffer */
			free(data);
		} else {
			EM_LOGD(TAG, "Cannot dequeue, the queue is empty %s, %d", __FUNCTION__, __LINE__);
			return false;
		}
	} else {
		EM_LOGE(TAG, "Invalid QueueHandle passed %s, %d", __FUNCTION__, __LINE__);
		return false;
	}

	return true;
}

BaseType_pf QueuePfSendFromISR( QueueHandle_pf xQueue, const void * const pvItemToQueue, BaseType_pf * const pxHigherPriorityTaskWoken )
{
	return 0;
}

BaseType_pf QueuePfReset( QueueHandle_pf xQueue )
{
	return 0;
}

BaseType_pf QueuePfMessagesWaiting(const QueueHandle_pf queue) {
	if (queue == NULL) {
		EM_LOGE(TAG, "Invalid Queue!");
		return -1;
	}
	if (qinfo_ll == NULL) {
		EM_LOGE(TAG, "Queue not initialized");
		return -1;
	}

	/* Search for the QueueInfo from the qinfo_ll list */
	QueueInfo *qinfo = NULL;
	int pos = -1;
	int search_result = search_qinfo(queue, &qinfo, &pos, qinfo_ll);

	/* If the QueueInfo was found then return its size */
	if (search_result == 0) {
		return qinfo->q->size;
	} else {
		EM_LOGE(TAG, "Invalid QueueHandle passed %s, %d", __FUNCTION__, __LINE__);
		return -1;
	}
}

void QueuePfDelete(QueueHandle_pf queue) {
	if (queue == NULL) {
		EM_LOGE(TAG, "Invalid Queue!");
		return;
	}
	if (qinfo_ll == NULL) {
		EM_LOGE(TAG, "Queue not initialized");
		return;
	}

	/* Search for the QueueInfo from the qinfo_ll list */
	QueueInfo *qinfo = NULL;
	int pos = -1;
	int search_result = search_qinfo(queue, &qinfo, &pos, qinfo_ll);

	/* If the QueueInfo was found then return its size */
	if (search_result == 0) {
		void *data = NULL;

		/* Pop each node and free its memory */
		pthread_mutex_lock(&qinfo->qmutex);
		EM_LOGD(TAG, "delete: freeing data");
		qinfo->cond_check = 0;
		pthread_cond_broadcast(&qinfo->qcond);
		while ((data = gll_pop(qinfo->q)) != NULL) {
			free(data);
		}
		pthread_mutex_unlock(&qinfo->qmutex);

		/* Destroy the mutex and cond */
		pthread_cond_destroy(&qinfo->qcond);
		pthread_mutex_destroy(&qinfo->qmutex);

		/* Destroy the queue */
		gll_destroy(qinfo->q);

		/* Remove the QueueInfo from the main list */
		pthread_mutex_lock(&m_qmutex);
		QueueInfo *qinfo_data = (QueueInfo*) gll_remove(qinfo_ll, pos);
		pthread_mutex_unlock(&m_qmutex);

		/* Free the QueueInfo object */
		free(qinfo_data);

		/* If there are no more nodes in the main list, then destroy the list */
		if (qinfo_ll->size <= 0) {
			pthread_mutex_lock(&m_qmutex);
			gll_destroy(qinfo_ll);
			qinfo_ll = NULL;
			pthread_mutex_unlock(&m_qmutex);

			/* Destroy the main mutex */
			pthread_mutex_destroy(&m_qmutex);
		}

	} else {
		EM_LOGE(TAG, "Invalid QueueHandle passed %s, %d", __FUNCTION__, __LINE__);
		return;
	}
}
//////////////////////

typedef struct {
	pthread_mutex_t mut;
} SemMutexInfo;

static gll_t *m_sminfo_ll = NULL;
static pthread_mutex_t m_smmutex;

SemaphoreMutexHandle_pf SemaphorePfCreateMutex( )
{
	if (m_sminfo_ll == NULL) {
		m_sminfo_ll = gll_init();
		pthread_mutex_init(&m_smmutex, NULL);
	}

	SemaphoreMutexHandle_pf smh_to_return = NULL;

	/* Allocate memory for a m_sminfo_ll object. This will make a node */
	SemMutexInfo *sminfo = (SemMutexInfo*) calloc(1, sizeof(SemMutexInfo));
	if (sminfo == NULL) {
		EM_LOGE(TAG, "calloc failed %s, %d", __FUNCTION__, __LINE__);
		smh_to_return = NULL;
		goto end_of_func;
	}
	int rv = pthread_mutex_init(&sminfo->mut, NULL);
	if (rv != 0) {
		EM_LOGE(TAG, "pthread_mutex_init failed %s, %d", __FUNCTION__, __LINE__);
		smh_to_return = NULL;
		goto end_of_func;
	}
	/* SemaphoreMutexHandle_pf to return to caller */
	smh_to_return = &sminfo->mut;

	/* Add a node to the linked list */
	pthread_mutex_lock(&m_smmutex);
	rv = gll_pushBack(m_sminfo_ll, sminfo);
	pthread_mutex_unlock(&m_smmutex);

	if (rv == -1) {
		EM_LOGE(TAG, "gll_pushBack failed %s, %d", __FUNCTION__, __LINE__);
		free(sminfo);
		smh_to_return = NULL;
		goto end_of_func;
	}

	end_of_func: return smh_to_return;
}

static int search_sminfo(SemaphoreMutexHandle_pf mutex, SemMutexInfo **p_sminfo, int *pos, gll_t *ll) {
	if (mutex == NULL) {
		EM_LOGE(TAG, "Invalid Mutex!");
		return -1;
	}

	int ll_size = ll->size;
	int ll_pos = 0;
	bool found = false;
	SemMutexInfo *sminfo = NULL;

	pthread_mutex_lock(&m_smmutex);
	for (ll_pos = 0; ll_pos < ll_size; ll_pos++) {
		sminfo = (SemMutexInfo*) gll_get(ll, ll_pos);
		if (sminfo != NULL) {
			if ((&sminfo->mut) == mutex) {
//				EM_LOGD(TAG, "Mutex matched");
				found = true;
				break;
			}
		}
	}
	pthread_mutex_unlock(&m_smmutex);

	if (found) {
		*p_sminfo = sminfo;
		*pos = ll_pos;
		return 0;
	} else {
		*p_sminfo = NULL;
		return -1;
	}
}

void SemaphorePfDeleteMutex( SemaphoreMutexHandle_pf mutex )
{
	if (mutex == NULL) {
		EM_LOGE(TAG, "Invalid Mutex!");
		return;
	}
	if (m_sminfo_ll == NULL) {
		EM_LOGE(TAG, "Mutex not created");
		return;
	}

	/* Search for the SemMutexInfo from the m_sminfo_ll list */
	SemMutexInfo *sminfo = NULL;
	int pos = -1;
	int search_result = search_sminfo(mutex, &sminfo, &pos, m_sminfo_ll);

	if (search_result == 0) {
		/* Destroy the mutex */
		pthread_mutex_destroy(&sminfo->mut);

		/* Remove the SemMutexInfo from the m_sminfo_ll list */
		pthread_mutex_lock(&m_smmutex);
		SemMutexInfo *sminfo_data = (SemMutexInfo*) gll_remove(m_sminfo_ll, pos);
		pthread_mutex_unlock(&m_smmutex);

		/* Free the SemMutexInfo object */
		free(sminfo_data);

		/* If there are no more nodes in the m_sminfo_ll list, then destroy the list */
		if (m_sminfo_ll->size <= 0) {
			pthread_mutex_lock(&m_smmutex);
			gll_destroy(m_sminfo_ll);
			m_sminfo_ll = NULL;
			pthread_mutex_unlock(&m_smmutex);

			/* Destroy the main mutex */
			pthread_mutex_destroy(&m_smmutex);
		}
	} else {
		EM_LOGE(TAG, "Invalid SemaphoreMutexHandle passed %s, %d", __FUNCTION__, __LINE__);
		return;
	}
}

BaseType_pf SemaphorePfGiveMutex( SemaphoreMutexHandle_pf mutex)
{
	if (mutex == NULL) {
		EM_LOGE(TAG, "Invalid Mutex!");
		return false;
	}
	if (m_sminfo_ll == NULL) {
		EM_LOGE(TAG, "Mutex not created");
		return false;
	}

	/* Search for the SemMutexInfo from the m_sminfo_ll list */
	SemMutexInfo *sminfo = NULL;
	int pos = -1;
	int search_result = search_sminfo(mutex, &sminfo, &pos, m_sminfo_ll);

	int rv = -1;
	if (search_result == 0) {
		rv = pthread_mutex_unlock(&sminfo->mut);
	} else {
		EM_LOGE(TAG, "Invalid SemaphoreMutexHandle passed %s, %d", __FUNCTION__, __LINE__);
		rv = -1;
	}

	if (rv == 0) {
		return true;
	} else {
		return false;
	}
}

BaseType_pf SemaphorePfTakeMutex( SemaphoreMutexHandle_pf mutex, TickType_pf ticks_to_wait)
{
	if (mutex == NULL) {
		EM_LOGE(TAG, "Invalid Mutex!");
		return false;
	}
	if (m_sminfo_ll == NULL) {
		EM_LOGE(TAG, "Mutex not created");
		return false;
	}

	/* Search for the SemMutexInfo from the m_sminfo_ll list */
	SemMutexInfo *sminfo = NULL;
	int pos = -1;
	int search_result = search_sminfo(mutex, &sminfo, &pos, m_sminfo_ll);

	int rv = -1;
	if (search_result == 0) {
		rv = pthread_mutex_lock(&sminfo->mut);
	} else {
		EM_LOGE(TAG, "Invalid SemaphoreMutexHandle passed %s, %d", __FUNCTION__, __LINE__);
		rv = -1;
	}

	if (rv == 0) {
		return true;
	} else {
		return false;
	}
}

/////////////////
