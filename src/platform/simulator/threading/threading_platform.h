/*
 * File Name: threading_platform.h
 * File Path: /emmate/src/threading/esp-idf/threading_platform.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef THREADING_PLATFORM_H_
#define THREADING_PLATFORM_H_

#include <pthread.h>
#include "core_common.h"
#include "gll.h"

/* Thread/Task related datatypes, enums and macros */
#define TICK_RATE_TO_MS			1
#define THREADING_MAX_DELAY_PF		0xffffffffUL

#define portCHAR		int8_t
#define portFLOAT		float
#define portDOUBLE		double
#define portLONG		int32_t
#define portSHORT		int16_t
#define portSTACK_TYPE	uint8_t

typedef portSTACK_TYPE StackType_t;

typedef void* TaskRetPf;
typedef TaskRetPf (*TaskFuncPf) (void *);
typedef pthread_t			TaskHandle_pf;
typedef uint64_t 			TickType_pf;
typedef int 				BaseType_pf;
typedef unsigned int 		UBaseType_pf;

/** Thread states returned by GET_THREAD_STATE. */
typedef enum {
	TH_RUNNING = 0,
	TH_READY,
	TH_BLOCKED,
	TH_SUSPENDED,
	TH_DELETED,
	TH_UNKNOWN
} THREAD_STATE;

////////////////////////////////

BaseType_pf TaskPfCreate(TaskFuncPf pxTaskCode, const char * const pcName, const uint32_t usStackDepth,
		void * const pvParameters, UBaseType_pf uxPriority, TaskHandle_pf * const pxCreatedTask);

BaseType_pf TaskPfCreatePinnedToCore(TaskFuncPf pxTaskCode, const char * const pcName, const uint32_t usStackDepth,
		void * const pvParameters, UBaseType_pf uxPriority, TaskHandle_pf * const pxCreatedTask, const BaseType_pf xCoreID);

THREAD_STATE TaskPfGetState(TaskHandle_pf xTask);
void TaskPfDelete(TaskHandle_pf xTaskToDelete);
TickType_pf TaskPfGetTickCount(void);

void TaskPfDelay(long msec_to_delay);

char *TaskPfGetTaskName(TaskHandle_pf xTaskToQuery);
UBaseType_pf TaskPfGetStackHighWaterMark(TaskHandle_pf xTask);
void TaskPfSuspend(TaskHandle_pf xTaskToSuspend);
void TaskPfResume(TaskHandle_pf xTaskToResume);

////////////////////////////
typedef gll_t *QueueHandle_pf;
QueueHandle_pf QueuePfCreate(const UBaseType_pf q_length, const UBaseType_pf item_size);
BaseType_pf QueuePfSend(QueueHandle_pf queue, const void *const item_to_queue, TickType_pf ticks_to_wait);
BaseType_pf QueuePfReceive(QueueHandle_pf queue, void *const buffer, TickType_pf ms_to_wait);
BaseType_pf QueuePfSendFromISR(QueueHandle_pf xQueue, const void *const pvItemToQueue, BaseType_pf *const pxHigherPriorityTaskWoken);
BaseType_pf QueuePfReset(QueueHandle_pf queue);
void QueuePfDelete(QueueHandle_pf queue);
BaseType_pf QueuePfMessagesWaiting(const QueueHandle_pf queue);

//////////////////////
typedef pthread_mutex_t *	SemaphoreMutexHandle_pf;
SemaphoreMutexHandle_pf SemaphorePfCreateMutex();
void SemaphorePfDeleteMutex(SemaphoreMutexHandle_pf mutex);
BaseType_pf SemaphorePfGiveMutex(SemaphoreMutexHandle_pf mutex);
BaseType_pf SemaphorePfTakeMutex(SemaphoreMutexHandle_pf mutex, TickType_pf ticks_to_wait);

#endif /* THREADING_PLATFORM_H_ */
