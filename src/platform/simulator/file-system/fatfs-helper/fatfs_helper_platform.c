/*
 * fatfs_helper_platform.c
 *
 *  Created on: 03-Jul-2019
 *      Author: Rohan Dey
 */



#include "fatfs_helper_platform.h"


em_err fat_sdmmc_mount_pf(const char* base_path,
                                  const stub_sdmmc_host_t* host_config,
                                  const void* slot_config,
                                  const stub_vfs_fat_mount_config_t* mount_config,
                                  sdmmc_card_t** out_card)
{
	return 0;
}

em_err fat_sdmmc_unmount_pf(void)
{
	return 0;
}
