/*
 * fatfs_helper_platform.h
 *
 *  Created on: 03-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef FATFS_HELPER_PLATFORM_H_
#define FATFS_HELPER_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "esp_vfs_fat.h"

typedef struct {
    bool format_if_mount_failed;
    int max_files;                  ///< Max number of open files
    size_t allocation_unit_size;
} stub_vfs_fat_mount_config_t;

typedef stub_vfs_fat_mount_config_t	fat_sdmmc_mount_config_pf_t;
typedef int sdmmc_card_t;

typedef struct {
    uint32_t flags;             /*!< flags defining host properties */
    int slot;                   /*!< slot number, to be passed to host functions */
    int max_freq_khz;           /*!< max frequency supported by the host */
#define SDMMC_FREQ_DEFAULT      20000       /*!< SD/MMC Default speed (limited by clock divider) */
#define SDMMC_FREQ_HIGHSPEED    40000       /*!< SD High speed (limited by clock divider) */
#define SDMMC_FREQ_PROBING      400         /*!< SD/MMC probing speed */
#define SDMMC_FREQ_52M          52000       /*!< MMC 52MHz speed */
#define SDMMC_FREQ_26M          26000       /*!< MMC 26MHz speed */
    float io_voltage;           /*!< I/O voltage used by the controller (voltage switching is not supported) */
    int (*init)(void);    /*!< Host function to initialize the driver */
    int (*set_bus_width)(int slot, size_t width);    /*!< host function to set bus width */
    size_t (*get_bus_width)(int slot); /*!< host function to get bus width */
    int (*set_bus_ddr_mode)(int slot, bool ddr_enable); /*!< host function to set DDR mode */
    int (*set_card_clk)(int slot, uint32_t freq_khz); /*!< host function to set card clock frequency */
//    int (*do_transaction)(int slot, sdmmc_command_t* cmdinfo);    /*!< host function to do a transaction */
    union {
        int (*deinit)(void);  /*!< host function to deinitialize the driver */
        int (*deinit_p)(int slot);  /*!< host function to deinitialize the driver, called with the `slot` */
    };
    int (*io_int_enable)(int slot); /*!< Host function to enable SDIO interrupt line */
//    int (*io_int_wait)(int slot, TickType_t timeout_ticks); /*!< Host function to wait for SDIO interrupt line to be active */
    int command_timeout_ms;     /*!< timeout, in milliseconds, of a single command. Set to 0 to use the default value. */
} stub_sdmmc_host_t;


em_err fat_sdmmc_mount_pf(const char* base_path,
                                  const stub_sdmmc_host_t* host_config,
                                  const void* slot_config,
                                  const stub_vfs_fat_mount_config_t* mount_config,
                                  sdmmc_card_t** out_card);

em_err fat_sdmmc_unmount_pf(void);


//
//#define fat_sdmmc_mount_pf(base_path, host_config, slot_config, mount_config, out_card) \
//										esp_vfs_fat_sdmmc_mount( \
//										/*const char* */base_path, \
//										/*const stub_sdmmc_host_t* */host_config, \
//										/*const void* */slot_config, \
//										/*const stub_vfs_fat_mount_config_t* */mount_config, \
//										/*sdmmc_card_t** */out_card)

/**
 * @brief Unmount FAT filesystem and release resources acquired using fat_sdmmc_mount_pf
 *
 * @return
 *      - EM_OK on success
 *      - EM_ERR_INVALID_STATE if fat_sdmmc_mount_pf hasn't been called
 */
//#define fat_sdmmc_unmount_pf()	esp_vfs_fat_sdmmc_unmount()

#ifdef __cplusplus
}
#endif

#endif /* FATFS_HELPER_PLATFORM_H_ */
