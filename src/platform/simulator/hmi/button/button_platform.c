/*
 * File Name: button_platform.c
 * Description:
 *
 *  Created on: 10-May-2020
 *      Author: Rohan Dey
 */

#include "button_platform.h"
#include "thing.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "gpio_platform.h"

#if CONFIG_USE_BUTTON

#define TAG		LTAG_HMI_BUTTON

#if BUTTONS_NUMBER > 0
static const uint8_t m_board_btn_list[BUTTONS_NUMBER] = BUTTONS_LIST;
#else
#error "BUTTONS_LIST is not defined. Please set this MACRO in thing.h"
#endif

#if BUTTONS_NUMBER  > 0

bool get_bsp_board_btn_state(uint32_t button_idx)
{
	uint8_t pin_set = gpio_pf_io_get_level(m_board_btn_list[button_idx]);

//	EM_LOGW(TAG, "button %d val = %d", m_board_btn_list[button_idx], pin_set);

	if (pin_set == 1) {
		return true;
	} else {
		return false;
	}

//	bool pin_set = ( gpio_pf_io_get_level(m_board_btn_list[button_idx]) ==  BUTTONS_ACTIVE_STATE ) ? true : false;
//	return pin_set; //(pin_set == (BUTTONS_ACTIVE_STATE ? true : false));
}


static void init_bsp_board_buttons(void)
{
	uint32_t i;
	for (i = 0; i < BUTTONS_NUMBER; ++i)
	{
		gpio_pf_io_config_t gpio_config;
		gpio_config.pin_bit_mask = m_board_btn_list[i];
		gpio_config.mode = GPIO_PF_IO_MODE_INPUT;
		gpio_pf_io_config(&gpio_config);
#if 0
		gpio_set_direction(m_board_btn_list[i], GPIO_MODE_INPUT);
#endif
	}
}

uint32_t pin_to_btn_idx_bsp_board(uint32_t pin_number)
{
	uint32_t i;
	uint32_t ret = 0xFFFFFFFF;
	for (i = 0; i < BUTTONS_NUMBER; ++i)
	{
		if (m_board_btn_list[i] == pin_number)
		{
			ret = i;
			break;
		}
	}
	return ret;
}

#endif	/* BUTTONS_NUMBER */

void init_platform_buttons(uint32_t init_flags) {

#if BUTTONS_NUMBER > 0
	if (init_flags & BSP_INIT_BUTTONS)
	{
		init_bsp_board_buttons();
	}
#endif /* BUTTONS_NUMBER */
}

#endif	/* CONFIG_USE_BUTTON */
