/*
 * conn_platform.c
 *
 *  Created on: 05-Jun-2020
 *      Author: Rohan Dey
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <curl/curl.h>

#include "core_config.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "conn_platform.h"

#define TAG	LTAG_CONN

#define LOCAL_IP_GET_URL	"http://example.com"
#define PUBLIC_IP_GET_URL	"http://example.com"

em_err conn_global_init() {
	CURLcode res;

	/* In windows, this will init the winsock stuff */
	res = curl_global_init(CURL_GLOBAL_ALL);

	if (res != 0) {
		return EM_FAIL;
	} else {
		return EM_OK;
	}
}

static size_t temp_func(void *buffer, size_t size, size_t nmemb, void *userp) {
//	printf("temp_func:\n");
//	printf("size = %ld\n", size);
//	printf("nmemb = %ld\n", nmemb);
//	printf("%s\n", (char*) buffer);
//	printf("\n");

	return size * nmemb;
}

em_err conn_get_local_ip(char *ip) {
	CURL *curl;
	CURLcode res;

	/* get a curl handle */
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, LOCAL_IP_GET_URL);

		/* Write callback functions */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, temp_func);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res == CURLE_OK) {
			char *temp_ip;
			curl_easy_getinfo(curl, CURLINFO_LOCAL_IP, &temp_ip);
			EM_LOGD(TAG, "Local IP: %s\n", temp_ip);
			strcpy(ip, temp_ip);
		}

		/* always cleanup */
		curl_easy_cleanup(curl);

		if (res == CURLE_OK) {
			return EM_OK;
		} else {
			return EM_FAIL;
		}
	} else {
		return EM_FAIL;
	}
}

em_err conn_get_public_ip(char *ip) {
	CURL *curl;
	CURLcode res;

	/* get a curl handle */
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, PUBLIC_IP_GET_URL);

		/* Write callback functions */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, temp_func);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res == CURLE_OK) {
			char *temp_ip;
			curl_easy_getinfo(curl, CURLINFO_PRIMARY_IP, &temp_ip);
			EM_LOGD(TAG, "Public IP: %s\n", temp_ip);
			strcpy(ip, temp_ip);
		}

		/* always cleanup */
		curl_easy_cleanup(curl);

		if (res == CURLE_OK) {
			return EM_OK;
		} else {
			return EM_FAIL;
		}
	} else {
		return EM_FAIL;
	}
}
