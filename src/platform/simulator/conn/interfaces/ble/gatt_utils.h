/*
 * gatt_utils.h
 *
 *	File Path: /emmate/src/conn/ble/esp-idf/gatt_utils.h
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

#ifndef GATT_UTILS_H_
#define GATT_UTILS_H_

#include "core_config.h"
#include "threading.h"
#include "core_error.h"
#include "ble_platform_constant.h"

#define SPP_PROFILE_NUM             1
#define SPP_PROFILE_APP_IDX         0
#define ESP_SPP_APP_ID              0x56
#define SPP_SVC_INST_ID	            0

/// SPP Service
#define SPP_SERVICE_UUID  0xABF0//0xABF0
/// Characteristic UUID
#define ESP_GATT_UUID_SPP_DATA_RECEIVE      0xABF1
#define ESP_GATT_UUID_SPP_DATA_NOTIFY       0xABF2
#define ESP_GATT_UUID_SPP_COMMAND_RECEIVE   0xABF3
#define ESP_GATT_UUID_SPP_COMMAND_NOTIFY    0xABF4

#define BLE_ADV_PAYLOAD_SIZE 			31
#define BLE_NAME_MAX_LENGTH				(BLE_ADV_PAYLOAD_SIZE-9)
#define BLE_ADV_DATA_DEFAULT_LENGTH		((BLE_ADV_PAYLOAD_SIZE - BLE_NAME_MAX_LENGTH)-2) // 2 byte (length + BLE AD type CMD (0x09))

#define BLE_DATA_QUEUE_MAX_SIZE  50

#define BLE_DATA_RECV_LENGHT	50

typedef struct{
	char data[BLE_DATA_RECV_LENGHT];
	uint8_t length;
}BLE_RECV_DATA;


/**
 * @brief
 *
 * @return
 *
 **/
em_err gatt_utils_set_ble_adv_data(char* ble_name);


/**
 * @brief
 *
 * @return
 *
 **/
char* get_ble_adv_name();


/**
 * @brief
 *
 * @return
 *
 **/
uint8_t* get_spp_adv_data();


/**
 * @brief
 *
 * @return
 *
 **/
em_err ble_send_response(char* res);


#endif /* GATT_UTILS_H_ */
