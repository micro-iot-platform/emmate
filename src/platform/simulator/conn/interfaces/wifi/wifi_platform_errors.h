/*
 * File Name: wifi_platform_errors.h
 * File Path: /emmate/src/platform/esp/conn/wifi/wifi_platform_errors.h
 * Description:
 *
 *  Created on: 05-May-2019
 *      Author: Rohan Dey
 */

#ifndef WIFI_PLATFORM_ERRORS_H_
#define WIFI_PLATFORM_ERRORS_H_

#include "platform_error_base.h"
#include "platform_error.h"
//#include "esp_wifi.h"

#define PLAT_ERR_WIFI_NOT_INIT			10 //ESP_ERR_WIFI_NOT_INIT      /*!< WiFi driver was not installed by esp_wifi_init */
#define PLAT_ERR_WIFI_NOT_STARTED		11 //ESP_ERR_WIFI_NOT_STARTED   /*!< WiFi driver was not started by esp_wifi_start */
#define PLAT_ERR_WIFI_NOT_STOPPED		12 //ESP_ERR_WIFI_NOT_STOPPED   /*!< WiFi driver was not stopped by esp_wifi_stop */
#define PLAT_ERR_WIFI_IF				13 //ESP_ERR_WIFI_IF            /*!< WiFi interface error */
#define PLAT_ERR_WIFI_MODE				14 //ESP_ERR_WIFI_MODE          /*!< WiFi mode error */
#define PLAT_ERR_WIFI_STATE				15 //ESP_ERR_WIFI_STATE         /*!< WiFi internal state error */
#define PLAT_ERR_WIFI_CONN				16 //ESP_ERR_WIFI_CONN          /*!< WiFi internal control block of station or soft-AP error */
#define PLAT_ERR_WIFI_NVS				17 //ESP_ERR_WIFI_NVS           /*!< WiFi internal NVS module error */
#define PLAT_ERR_WIFI_MAC				18 //ESP_ERR_WIFI_MAC           /*!< MAC address is invalid */
#define PLAT_ERR_WIFI_SSID				19 //ESP_ERR_WIFI_SSID          /*!< SSID is invalid */
#define PLAT_ERR_WIFI_PASSWORD			20 //ESP_ERR_WIFI_PASSWORD      /*!< Password is invalid */
#define PLAT_ERR_WIFI_TIMEOUT			21 //ESP_ERR_WIFI_TIMEOUT       /*!< Timeout error */
#define PLAT_ERR_WIFI_WAKE_FAIL			22 //ESP_ERR_WIFI_WAKE_FAIL     /*!< WiFi is in sleep state(RF closed) and wakeup fail */
#define PLAT_ERR_WIFI_WOULD_BLOCK		23 //ESP_ERR_WIFI_WOULD_BLOCK   /*!< The caller would block */
#define PLAT_ERR_WIFI_NOT_CONNECT		24 //ESP_ERR_WIFI_NOT_CONNECT   /*!< Station still in disconnect status */


#endif /* WIFI_PLATFORM_ERRORS_H_ */
