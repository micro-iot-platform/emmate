/*
 * File Name: http_platform.h
 * Description:
 *
 *  Created on: 24-Apr-2020
 *      Author: Rohan Dey
 */

#ifndef HTTP_CLIENT_PLATFORM_H_
#define HTTP_CLIENT_PLATFORM_H_

#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include <curl/curl.h>

#define HTTP_CLIENT_PF_ERR_HTTP_BASE				300 /*!< Starting number of HTTP error codes */
#define HTTP_CLIENT_PF_ERR_HTTP_MAX_REDIRECT		301 /*!< The error exceeds the number of HTTP redirects */
#define HTTP_CLIENT_PF_ERR_HTTP_CONNECT				302 /*!< Error open the HTTP connection */
#define HTTP_CLIENT_PF_ERR_HTTP_WRITE_DATA			303 /*!< Error write HTTP data */
#define HTTP_CLIENT_PF_ERR_HTTP_FETCH_HEADER		304 /*!< Error read HTTP header from server */
#define HTTP_CLIENT_PF_ERR_HTTP_INVALID_TRANSPORT	305 /*!< There are no transport support for the input scheme */
#define HTTP_CLIENT_PF_ERR_HTTP_CONNECTING			306 /*!< HTTP connection hasn't been established yet */
#define HTTP_CLIENT_PF_ERR_HTTP_EAGAIN				307 /*!< Mapping of errno EAGAIN to esp_err_t */

typedef enum {
	HTTP_CLIENT_PF_EVENT_ERROR = 		30, /*!< This event occurs when there are any errors during execution */
	HTTP_CLIENT_PF_EVENT_ON_CONNECTED = 31, /*!< Once the HTTP has been connected to the server, no data exchange has been performed */
	HTTP_CLIENT_PF_EVENT_HEADER_SENT = 	32, /*!< After sending all the headers to the server */
	HTTP_CLIENT_PF_EVENT_ON_HEADER = 	33, /*!< Occurs when receiving each header sent from the server */
	HTTP_CLIENT_PF_EVENT_ON_DATA = 		34, /*!< Occurs when receiving data from the server, possibly multiple portions of the packet */
	HTTP_CLIENT_PF_EVENT_ON_FINISH = 	35, /*!< Occurs when finish a HTTP session */
	HTTP_CLIENT_PF_EVENT_DISCONNECTED = 36  /*!< The connection has been disconnected */
} HTTP_CLIENT_PF_EVENT_ID;

typedef enum {
	HTTP_CLIENT_PF_METHOD_GET = CURLOPT_HTTPGET, 	/*!< HTTP GET Method */
	HTTP_CLIENT_PF_METHOD_POST = CURLOPT_HTTPPOST, 	/*!< HTTP POST Method */
	HTTP_CLIENT_PF_METHOD_PUT = -1,
	HTTP_CLIENT_PF_METHOD_PATCH = -1,
	HTTP_CLIENT_PF_METHOD_DELETE = -1,
	HTTP_CLIENT_PF_METHOD_HEAD = -1,
	HTTP_CLIENT_PF_METHOD_NOTIFY = -1,
	HTTP_CLIENT_PF_METHOD_SUBSCRIBE = -1,
	HTTP_CLIENT_PF_METHOD_UNSUBSCRIBE = -1,
	HTTP_CLIENT_PF_METHOD_OPTIONS = -1,
	HTTP_CLIENT_PF_METHOD_MAX = -1
} HTTP_CLIENT_PF_METHOD;

typedef HTTP_CLIENT_PF_METHOD HttpClientPfMethod;

typedef enum {
    HTTP_AUTH_TYPE_NONE = 0,    /*!< No authentication */
    HTTP_AUTH_TYPE_BASIC,       /*!< HTTP Basic authentication */
    HTTP_AUTH_TYPE_DIGEST,      /*!< HTTP Digest authentication */
} sim_http_client_auth_type_t;

typedef enum {
    HTTP_TRANSPORT_UNKNOWN = 0x0,   /*!< Unknown */
    HTTP_TRANSPORT_OVER_TCP,        /*!< Transport over tcp */
    HTTP_TRANSPORT_OVER_SSL,        /*!< Transport over ssl */
} sim_http_client_transport_t;

typedef struct CURL *HttpClientPfHandle;

typedef struct {
	HTTP_CLIENT_PF_EVENT_ID event_id;	/*!< event_id, to know the cause of the event */
	HttpClientPfHandle client;			/*!< CURL * context */
    void *data;							/*!< data of the event */
    int data_len;						/*!< data length of data */
    void *user_data;					/*!< user_data context, from HttpClientPfConfig user_data */
    char *header_key;					/*!< For HTTP_CLIENT_PF_EVENT_ON_HEADER event_id, it's store current http header key */
    char *header_value;					/*!< For HTTP_CLIENT_PF_EVENT_ON_HEADER event_id, it's store current http header value */
} HttpClientPfEvent;

typedef em_err (*sim_http_event_handle_cb)(HttpClientPfEvent *evt);

typedef struct {
    const char                  *url;                /*!< HTTP URL, the information on the URL is most important, it overrides the other fields below, if any */
    const char                  *host;               /*!< Domain or IP as string */
    int                         port;                /*!< Port to connect, default depend on esp_http_client_transport_t (80 or 443) */
    const char                  *username;           /*!< Using for Http authentication */
    const char                  *password;           /*!< Using for Http authentication */
    sim_http_client_auth_type_t auth_type;           /*!< Http authentication type, see `esp_http_client_auth_type_t` */
    const char                  *path;               /*!< HTTP Path, if not set, default is `/` */
    const char                  *query;              /*!< HTTP query */
    const char                  *cert_pem;           /*!< SSL server certification, PEM format as string, if the client requires to verify server */
    const char                  *client_cert_pem;    /*!< SSL client certification, PEM format as string, if the server requires to verify client */
    const char                  *client_key_pem;     /*!< SSL client key, PEM format as string, if the server requires to verify client */
    HTTP_CLIENT_PF_METHOD    	method;                   /*!< HTTP Method */
    int                         timeout_ms;               /*!< Network timeout in milliseconds */
    bool                        disable_auto_redirect;    /*!< Disable HTTP automatic redirects */
    int                         max_redirection_count;    /*!< Max redirection number, using default value if zero*/
    sim_http_event_handle_cb    event_handler;             /*!< HTTP Event Handle */
    sim_http_client_transport_t transport_type;           /*!< HTTP transport type, see `esp_http_client_transport_t` */
    int                         buffer_size;              /*!< HTTP receive buffer size */
    int                         buffer_size_tx;           /*!< HTTP transmit buffer size */
    void                        *user_data;               /*!< HTTP user_data context */
    bool                        is_async;                 /*!< Set asynchronous mode, only supported with HTTPS for now */
    bool                        use_global_ca_store;      /*!< Use a global ca_store for all the connections in which this bool is set. */
    bool                        skip_cert_common_name_check;    /*!< Skip any validation of server certificate CN field */
} HttpClientPfConfig;

HttpClientPfHandle http_client_pf_init(const HttpClientPfConfig *config);
em_err http_client_pf_set_url(HttpClientPfHandle client, const char *url);
em_err http_client_pf_set_method(HttpClientPfHandle client, HttpClientPfMethod method);
em_err http_client_pf_set_header(HttpClientPfHandle client, const char *key, const char *value);
em_err http_client_pf_set_post_field(HttpClientPfHandle client, const char *data, int len);
em_err http_client_pf_perform(HttpClientPfHandle client);
int http_client_pf_get_status_code(HttpClientPfHandle client);
int http_client_pf_get_content_length(HttpClientPfHandle client);
int http_client_pf_read(HttpClientPfHandle client, char *buffer, int len);
em_err http_client_pf_cleanup(HttpClientPfHandle client);

#endif /* HTTP_CLIENT_PLATFORM_H_ */
