/*
 * File Name: http_platform.c
 * File Path: /emmate/src/platform/esp/conn-proto/http-client/http_client_platform.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include <stdlib.h>

#include "http_client_platform.h"
#include "threading.h"
#include "core_constant.h"
#include "core_logger.h"

#define TAG "http_client_platform"

static sim_http_event_handle_cb http_event_cb = NULL;

size_t response_data(void *buffer, size_t size, size_t nmemb, void *userp) {
	HttpClientPfEvent evt;

	if (nmemb <= 0) {
		/* No data received */
		return size * nmemb;
	}

	memset(&evt, 0x00, sizeof(evt));

	evt.client = userp;
	evt.user_data = userp;
	evt.event_id = HTTP_CLIENT_PF_EVENT_ON_DATA;
	evt.data = buffer;
	evt.data_len = (size * nmemb);

	/* Call the emmate http core callback function */
	(*http_event_cb)(&evt);

	return size * nmemb;
}

HttpClientPfHandle http_client_pf_init(const HttpClientPfConfig *config) {
	HttpClientPfHandle client = curl_easy_init();
	if (client) {
		/* Write callback functions */
		curl_easy_setopt(client, CURLOPT_WRITEDATA, (void*) client);
		curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, response_data);

//		curl_easy_setopt(client, CURLOPT_HEADERDATA, &headerdata);
//		curl_easy_setopt(client, CURLOPT_HEADERFUNCTION, write_header);

		/* Set the CA Cert Info */
		curl_easy_setopt(client, CURLOPT_SSL_VERIFYPEER, 0L);
		//curl_easy_setopt(client, CURLOPT_CAINFO, CAINFO);

		http_event_cb = config->event_handler;
		return client;
	} else {
		return NULL;
	}
}

em_err http_client_pf_set_url(HttpClientPfHandle client, const char *url) {
	if (client) {
		CURLcode res = curl_easy_setopt(client, CURLOPT_URL, url);
		if (res != CURLE_OK) {
			return EM_FAIL;
		}
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}

em_err http_client_pf_set_method(HttpClientPfHandle client, HttpClientPfMethod method) {
	return EM_OK;
}

em_err http_client_pf_set_header(HttpClientPfHandle client, const char *key, const char *value) {
	if (client) {
		struct curl_slist *headers = NULL;
		headers = curl_slist_append(headers, value);

		/* pass our list of custom made headers */
		curl_easy_setopt(client, CURLOPT_HTTPHEADER, headers);
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}

em_err http_client_pf_set_post_field(HttpClientPfHandle client, const char *data, int len) {
	if (client) {
		/* post binary data */
		curl_easy_setopt(client, CURLOPT_POSTFIELDS, data);

		/* set the size of the postfields data */
		curl_easy_setopt(client, CURLOPT_POSTFIELDSIZE, len);
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}

em_err http_client_pf_perform(HttpClientPfHandle client) {
	if (client) {
		/* Perform the request, res will get the return code */
		CURLcode res = curl_easy_perform(client);
		/* Check for errors */
		if (res != CURLE_OK) {
			EM_LOGE(TAG, "curl_easy_perform() failed: %s", curl_easy_strerror(res));
			return EM_FAIL;
		} else {
			EM_LOGD(TAG, "curl_easy_perform() success");

			HttpClientPfEvent evt;
			memset(&evt, 0x00, sizeof(evt));
			evt.client = client;
			evt.user_data = client;
			evt.event_id = HTTP_CLIENT_PF_EVENT_ON_FINISH;

			/* Call the emmate http core callback function */
			(*http_event_cb)(&evt);
			return EM_OK;
		}
	} else {
		return EM_FAIL;
	}
}

int http_client_pf_get_status_code(HttpClientPfHandle client) {
	if (client) {
		long resp_code = 0;
		curl_easy_getinfo(client, CURLINFO_RESPONSE_CODE, &resp_code);
		EM_LOGD(TAG, "CURLINFO_RESPONSE_CODE = %ld\n", resp_code);
		return (int) resp_code;
	} else {
		return EM_FAIL;
	}
}

int http_client_pf_get_content_length(HttpClientPfHandle client) {
	return 0;
}

int http_client_pf_read(HttpClientPfHandle client, char *buffer, int len) {
	return 0;
}

em_err http_client_pf_cleanup(HttpClientPfHandle client) {
	if (client) {
		curl_easy_cleanup(client);
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}
