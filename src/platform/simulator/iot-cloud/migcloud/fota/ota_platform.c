/*
 * File Name: ota_platform.c
 * File Path: /emmate/src/platform/simulator/ota_platform/ota_platform.c
 * Description:
 *
 *  Created on: 09-May-2020
 *      Author: Rohan Dey
 */

#include "ota_platform.h"


const ota_pf_partition_t* ota_pf_get_running_partition(void)
{
	ota_pf_partition_t temp;
	return temp;
}

int ota_pf_get_state_partition(const ota_pf_partition_t *partition, ota_pf_img_states_t *ota_state)
{
	return 0;
}

int ota_pf_mark_app_valid_cancel_rollback(void)
{
    return 0;
}

int ota_pf_mark_app_invalid_rollback_and_reboot(void)
{
    return 0;
}

int ota_pf_https_ota(const ota_pf_https_ota_config_t *config)
{
	return 0;
}

int ota_pf_https_ota_begin(ota_pf_https_ota_config_t *ota_config, ota_pf_https_ota_handle_t *handle)
{
	return 0;
}

int ota_pf_https_ota_perform(ota_pf_https_ota_handle_t https_ota_handle)
{
	return 0;
}

int ota_pf_https_ota_finish(ota_pf_https_ota_handle_t https_ota_handle)
{
	return 0;
}

int ota_pf_https_ota_get_image_len_read(ota_pf_https_ota_handle_t https_ota_handle)
{
	return 0;
}
