/*
 * File Name: ota_platform.h
 * File Path: /emmate/src/platform/simulator/ota_platform/ota_platform.h
 * Description:
 *
 *  Created on: 09-May-2020
 *      Author: Rohan Dey
 */

#ifndef OTA_PLATFORM_H_
#define OTA_PLATFORM_H_

#include "http_client_platform.h"

/* Wrapper typedefs for abstracting the platform specific names */
#define OTA_PF_ERR_HTTPS_OTA_IN_PROGRESS	0x9001
#define OTA_PF_ERR_OTA_VALIDATE_FAILED		0x9002
#define OTA_PF_ERR_FLASH_OP_TIMEOUT			0x9003
#define OTA_PF_ERR_FLASH_OP_FAIL			0x9004

typedef int ota_pf_partition_t;
typedef void *	ota_pf_https_ota_handle_t;
typedef int	ota_pf_https_ota_config_t;

/* Wrapper typedefs for abstracting the platform specific names */
/* OTA_DATA states for checking operability of the app */
typedef enum {
	OTA_PF_IMG_NEW = 0x01,
	OTA_PF_IMG_PENDING_VERIFY = 0x02,
	OTA_PF_IMG_VALID = 0x03,
	OTA_PF_IMG_INVALID = 0x04,
	OTA_PF_IMG_ABORTED = 0x05,
	OTA_PF_IMG_UNDEFINED = 0xFF
} ota_pf_img_states_t;

const ota_pf_partition_t* ota_pf_get_running_partition(void);
int ota_pf_get_state_partition(const ota_pf_partition_t *partition, ota_pf_img_states_t *ota_state);
int ota_pf_mark_app_valid_cancel_rollback(void);
int ota_pf_mark_app_invalid_rollback_and_reboot(void);
int ota_pf_https_ota(const ota_pf_https_ota_config_t *config);
int ota_pf_https_ota_begin(ota_pf_https_ota_config_t *ota_config, ota_pf_https_ota_handle_t *handle);
int ota_pf_https_ota_perform(ota_pf_https_ota_handle_t https_ota_handle);
int ota_pf_https_ota_finish(ota_pf_https_ota_handle_t https_ota_handle);
int ota_pf_https_ota_get_image_len_read(ota_pf_https_ota_handle_t https_ota_handle);

#endif /* OTA_PLATFORM_H_ */
