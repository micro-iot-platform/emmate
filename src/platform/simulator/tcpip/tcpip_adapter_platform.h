/*
 * File Name: tcpip_adapter_platform.h
 * File Path: /emmate/src/platform/esp/tcpip/tcpip_adapter_platform.h
 * Description:
 *
 *  Created on: 06-May-2019
 *      Author: Rohan Dey
 */

#ifndef TCPIP_ADAPTER_PLATFORM_H_
#define TCPIP_ADAPTER_PLATFORM_H_

#include <stdbool.h>
#include "core_config.h"
#include "core_error.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

//#include "tcpip_adapter.h"

struct ip4_addr {
  uint32_t addr;
};

typedef struct ip4_addr ip4_addr_t;

typedef struct {
    ip4_addr_t ip;      /**< Interface IPV4 address */
    ip4_addr_t netmask; /**< Interface IPV4 netmask */
    ip4_addr_t gw;      /**< Interface IPV4 gateway address */
} tcpip_adapter_ip_info_t;

typedef tcpip_adapter_ip_info_t TcpipAdapterIpInfo_Pf;

/* @brief On-chip network interfaces */
typedef enum {
	PF_TCPIP_ADAPTER_IF_STA		= 0, //TCPIP_ADAPTER_IF_STA,		/**< Wi-Fi STA (station) interface */
	PF_TCPIP_ADAPTER_IF_AP		= 1, //TCPIP_ADAPTER_IF_AP,		/**< Wi-Fi soft-AP interface */
	PF_TCPIP_ADAPTER_IF_ETH		= 2, //TCPIP_ADAPTER_IF_ETH,		/**< Ethernet interface */
	PF_TCPIP_ADAPTER_IF_MAX		= 3 //TCPIP_ADAPTER_IF_MAX
} TCPIP_ADAPTER_IF_PF;

/**
 * @brief  Get interface's IP address information
 *
 * If the interface is up, IP information is read directly from the TCP/IP stack.
 *
 * If the interface is down, IP information is read from a copy kept in the TCP/IP adapter
 * library itself.
 *
 * @param[in]   tcpip_if Interface to get IP information
 * @param[out]  ip_info If successful, IP information will be returned in this argument.
 *
 * @return
 *         - EM_OK
 *         - PF_TCPIP_ERR_INVALID_PARAMS
 */
em_err get_tcpip_pf_ip_info(TCPIP_ADAPTER_IF_PF tcpip_if, TcpipAdapterIpInfo_Pf *ip_info);
bool tcpip_adapter_is_netif_up(TCPIP_ADAPTER_IF_PF tcpip_if);

/**
 * @brief  Test if supplied interface is up or down
 *
 * @param[in]   tcpip_if Interface to test up/down status
 *
 * @return
 *         - true - Interface is up
 *         - false - Interface is down
 */
//#define	is_tcpip_pf_netif_up(tcpip_if) /*bool*/ tcpip_adapter_is_netif_up(/*TCPIP_ADAPTER_IF_PF*/ tcpip_if)


#endif /* TCPIP_ADAPTER_PLATFORM_H_ */
