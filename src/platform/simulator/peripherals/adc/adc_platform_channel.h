/*
 * File Name: adc_platform_channel.h
 * Description:
 *
 *  Created on: 28-Aug-2020
 *      Author: Rohan Dey
 */

#ifndef ADC_PLATFORM_CHANNEL_H_
#define ADC_PLATFORM_CHANNEL_H_

#include "som.h"
#include "adc_platform.h"

/**************************** ADC Channel 1 ******************************************/
#define ADC_1_PF_CHANNEL_0_GPIO 		SOM_PIN_8
#define ADC_1_PF_CHANNEL_1_GPIO 		SOM_PIN_6
#define ADC_1_PF_CHANNEL_2_GPIO
#define ADC_1_PF_CHANNEL_3_GPIO 		SOM_PIN_2
#define ADC_1_PF_CHANNEL_4_GPIO 		SOM_PIN_88
#define ADC_1_PF_CHANNEL_5_GPIO 		SOM_PIN_86
#define ADC_1_PF_CHANNEL_6_GPIO 		SOM_PIN_36
#define ADC_1_PF_CHANNEL_7_GPIO 		SOM_PIN_90

/**************************** ADC Channel 2 ******************************************/
#define ADC_2_PF_CHANNEL_0_GPIO 		SOM_PIN_49
#define ADC_2_PF_CHANNEL_1_GPIO 		SOM_PIN_38
#define ADC_2_PF_CHANNEL_2_GPIO 		SOM_PIN_192
#define ADC_2_PF_CHANNEL_3_GPIO 		SOM_PIN_33
#define ADC_2_PF_CHANNEL_4_GPIO 		SOM_PIN_53
#define ADC_2_PF_CHANNEL_5_GPIO 		SOM_PIN_51
#define ADC_2_PF_CHANNEL_6_GPIO 		SOM_PIN_47
#define ADC_2_PF_CHANNEL_7_GPIO
#define ADC_2_PF_CHANNEL_8_GPIO
#define ADC_2_PF_CHANNEL_9_GPIO

#endif /* ADC_PLATFORM_CHANNEL_H_ */
