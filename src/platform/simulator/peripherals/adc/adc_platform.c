/*
 * File Name: adc_platform.c
 * Description:
 *
 *  Created on: 28-Aug-2020
 *      Author: Rohan Dey
 */

#include <stdbool.h>
#include <string.h>

#include "adc_platform.h"
#include "adc_platform_channel.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "input_processor.h"
#include "core_utils.h"
#include "threading_platform.h"

#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"

#define TAG	"adc_plat"

typedef struct {
	adc_pf_channel_t adc_ch;
	int adc_val;
} ADCGetVal;

static ADCGetVal m_adc_val;
static SemaphoreMutexHandle_pf m_adc_mutex;
static volatile bool m_cmd_complete = false;

static em_err make_gpio_request_subjson(adc1_pf_channel_t channel, char *buf, int *buf_len) {
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	/* Set JSON key value */
	json_object_set_number(root_object, "adc_ch", (int)channel);

	serialized_string = json_serialize_to_string(root_value);
	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1

	memcpy(buf, serialized_string, len);
	*buf_len = len;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);

	return EM_OK;
}

void adc_platform_get_val(char *adc_val) {
	char *json_buff = adc_val;
	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);
	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "Invalid json object");
			ret = EM_FAIL;
		} else {
			rootObj = json_value_get_object(root_value);

			m_adc_val.adc_ch = json_object_get_number(rootObj, "adc_ch");
			m_adc_val.adc_val = json_object_get_number(rootObj, "adc_val");

			json_value_free(root_value); /* clear root_value */
			m_cmd_complete = true;
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
		ret = EM_FAIL;
	}
}

int adc_pf_adc1_get_raw(adc1_pf_channel_t channel) {
	SemaphorePfTakeMutex(m_adc_mutex, THREADING_MAX_DELAY_PF);
	m_cmd_complete = false;

	char data[128] = {0x00};
	int data_len = 0;
	make_gpio_request_subjson(channel, data, &data_len);

	/* Send SIM_GET_GPIO_STATE command */
	char *json_buf;
	int json_len;

	simulator_make_request_json(&json_buf, &json_len, SIM_GET_ADC_VAL, data);
	EM_LOGD(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	int time = 0;
	while (!m_cmd_complete) {
		if (++time > 500) {
			m_adc_val.adc_val = 0;
			break;
		}
		TaskPfDelay(10);
	}

	SemaphorePfGiveMutex(m_adc_mutex);

	return m_adc_val.adc_val;
}

em_err adc_pf_adc2_get_raw(adc2_pf_channel_t channel, adc_pf_bits_width_t width_bit, int *raw_out) {
	SemaphorePfTakeMutex(m_adc_mutex, THREADING_MAX_DELAY_PF);
	m_cmd_complete = false;

	char data[128] = {0x00};
	int data_len = 0;
	make_gpio_request_subjson(channel, data, &data_len);

	/* Send SIM_GET_GPIO_STATE command */
	char *json_buf;
	int json_len;

	simulator_make_request_json(&json_buf, &json_len, SIM_GET_ADC_VAL, data);
	EM_LOGD(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	while (!m_cmd_complete);

	*raw_out = m_adc_val.adc_val;
	SemaphorePfGiveMutex(m_adc_mutex);

	return EM_OK;
}

em_err adc_pf_adc1_config_width(adc_pf_bits_width_t width_bit) {
	return EM_OK;
}

em_err adc_pf_adc1_config_channel_atten(adc1_pf_channel_t channel, adc_pf_atten_t atten) {
	return EM_OK;
}

em_err adc_pf_adc2_config_channel_atten(adc2_pf_channel_t channel, adc_pf_atten_t atten) {
	return EM_OK;
}

bool select_adc_channel(uint8_t adc_channel_gpio, adc_pf_unit_t *unit_t, adc_pf_channel_t* channel_t){

	bool ret=true;
	adc_pf_channel_t channel = -1;
	adc_pf_unit_t unit=-1;
	switch (adc_channel_gpio) {

/**************************************************** ADC 1 Channel ***********************************/
#if ADC_1_PF_CHANNEL_0_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_0_GPIO: {
			channel = ADC1_PF_CHANNEL_0;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_1_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_1_GPIO: {
			channel = ADC1_PF_CHANNEL_1;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

//
//		case ADC_1_PF_CHANNEL_2_GPIO: {
//			channel = ADC1_PF_CHANNEL_2;
//			unit = ADC_PF_UNIT_1;
//			break;
//		}

#if ADC_1_PF_CHANNEL_3_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_3_GPIO: {
			channel = ADC1_PF_CHANNEL_3;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_4_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_4_GPIO: {
			channel = ADC1_PF_CHANNEL_4;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_5_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_5_GPIO: {
			channel = ADC1_PF_CHANNEL_5;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_6_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_6_GPIO: {
			channel = ADC1_PF_CHANNEL_6;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

#if ADC_1_PF_CHANNEL_7_GPIO != UNASSIGN_PIN
		case ADC_1_PF_CHANNEL_7_GPIO: {
			channel = ADC1_PF_CHANNEL_7;
			unit = ADC_PF_UNIT_1;
			break;
		}
#endif

/****************************************************  ADC 2 Channel ***********************************/
#if ADC_2_PF_CHANNEL_0_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_0_GPIO: {
			channel = ADC2_PF_CHANNEL_0;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_1_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_1_GPIO: {
			channel = ADC2_PF_CHANNEL_1;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_2_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_2_GPIO: {
			channel = ADC2_PF_CHANNEL_2;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_3_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_3_GPIO: {
			channel = ADC2_PF_CHANNEL_3;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_4_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_4_GPIO: {
			channel = ADC2_PF_CHANNEL_4;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_5_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_5_GPIO: {
			channel = ADC2_PF_CHANNEL_5;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

#if ADC_2_PF_CHANNEL_6_GPIO != UNASSIGN_PIN
		case ADC_2_PF_CHANNEL_6_GPIO: {
			channel = ADC2_PF_CHANNEL_6;
			unit = ADC_PF_UNIT_2;
			break;
		}
#endif

//		case ADC_2_PF_CHANNEL_7_GPIO: {
//			channel = ADC2_PF_CHANNEL_7;
//			unit = ADC_PF_UNIT_2;
//			break;
//		}
//
//		case ADC_2_PF_CHANNEL_8_GPIO: {
//			channel = ADC2_PF_CHANNEL_8;
//			unit = ADC_PF_UNIT_2;
//			break;
//		}
//
//		case ADC_2_PF_CHANNEL_9_GPIO: {
//			channel = ADC2_PF_CHANNEL_9;
//			unit = ADC_PF_UNIT_2;
//			break;
//		}

		default:{
			ret = false;
			break;
		}
	}

	if(channel_t != NULL){
		*channel_t = channel;
	}
	if(unit_t != NULL){
		*unit_t = unit;
	}

	return ret;
}

bool validate_adc_channel(uint8_t adc_channel_gpio){
	bool ret = select_adc_channel(adc_channel_gpio, NULL, NULL);
	return ret;
}

em_err adc_platform_initialize_adc() {
	/* Initialize mutex for thread safety */
	m_adc_mutex = SemaphorePfCreateMutex();
	if (m_adc_mutex != NULL) {
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}

