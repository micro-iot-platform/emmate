/*
 * File Name: adc_platform_cal.c
 * Description:
 *
 *  Created on: 28-Aug-2020
 *      Author: Rohan Dey
 */


#include "adc_platform_cal.h"

#define SYSTEM_MILI_VOLTS		3300
#define ADC_MAX_RESOLUTION		1023

em_err pf_adc_cal_check_efuse(pf_adc_cal_value_t value_type) {
	return EM_OK;
}

uint32_t pf_adc_cal_raw_to_voltage(uint32_t adc_reading, const adc_cal_pf_characteristics_t *chars) {

	/*
	 * ADC Reading to Voltage Conversion Formulae:
	 *
	 * Resolution of ADC					ADC Reading
	 * ---------------------	=	--------------------------
	 * 	 System Voltage				 Analog Voltage Measured
	 *
	 * Analog Voltage Measured = (ADC Reading * System Voltage) / Resolution of ADC
	 *
	 * In our case:
	 * Resolution of ADC = 1023
	 * System Voltage = 3.3V or 3300mV
	 * */

	/* We are going to calculate the milivolts value from adc_reading */
	uint32_t mvolt = (adc_reading * SYSTEM_MILI_VOLTS) / ADC_MAX_RESOLUTION;

	return mvolt;
}

pf_adc_cal_value_t pf_adc_cal_characterize(adc_pf_unit_t adc_num, adc_pf_atten_t atten, adc_pf_bits_width_t bit_width,
		uint32_t default_vref, adc_cal_pf_characteristics_t *chars) {
	return PF_ADC_CAL_VAL_EFUSE_VREF;
}
