/*
 * File Name: gpio_platform.c
 * File Path: /emmate/src/platform/esp/gpio/gpio_platform.c
 * Description:
 *
 *  Created on: 10-May-2019
 *      Author: Noyel Seth
 */


#include "gpio_platform.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "input_processor.h"
#include "core_utils.h"
#include "threading_platform.h"
#include "gll.h"

#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"

#define TAG  LTAG_GPIO

static SemaphoreMutexHandle_pf m_gpio_mutex;
static volatile bool m_cmd_complete = false;
static gll_t *gpio_ll = NULL;

static em_err make_gpio_request_subjson(SIMUMATOR_COMMANDS cmd, gpio_pf_io_num gpio_num, uint8_t level, char *buf, int *buf_len) {
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	/* Set JSON key value */
	json_object_set_number(root_object, "som_pin", (int)gpio_num);
	if (cmd == SIM_SET_GPIO_STATE) {
		json_object_set_boolean(root_object, "som_pin_val", (level ? true : false));
	}

	serialized_string = json_serialize_to_string(root_value);
	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1

	memcpy(buf, serialized_string, len);
	*buf_len = len;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);

	return EM_OK;
}

static em_err parse_gpio_revc_json(char *in_json, int *gpio_num, int* gpio_val) {
	char *json_buff = in_json;
	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);
	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "Invalid json object");
			ret = EM_FAIL;
		} else {
			rootObj = json_value_get_object(root_value);

			*gpio_num = json_object_get_number(rootObj, "som_pin");
			*gpio_val = json_object_get_boolean(rootObj, "som_pin_val");

			json_value_free(root_value); /* clear root_value */
			ret = EM_OK;
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
		ret = EM_FAIL;
	}
	return ret;
}

static em_err gpio_make_request_data(char **ppbuf, int *plen, int pin_count, int *gpio_num) {
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);

	JSON_Value* gpio_arr_val = json_value_init_array();
	JSON_Array* gpio_arr_arr = json_value_get_array(gpio_arr_val);
	char *serialized_string = NULL;

	/* Make JSON Array */
	for (int i=0; i<pin_count; i++) {
		JSON_Value *gpio_value = json_value_init_object();
		JSON_Object *gpio_object = json_value_get_object(gpio_value);
		json_object_set_number(gpio_object, "som_pin", gpio_num[i]);
		json_array_append_value(gpio_arr_arr, gpio_value);
	}

	/* Set JSON key value */
	json_object_set_number(root_object, "cmd", (int)SIM_GET_GPIO_STATE);
	json_object_set_value(root_object, "data", gpio_arr_val);

	serialized_string = json_serialize_to_string(root_value);
	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	EM_LOGD(TAG, "Simulator Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		EM_LOGE(TAG, "simulator_make_request_json malloc failed!");
		return EM_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return EM_OK;
}

typedef struct {
	int gpio_num;
	int gpio_val;
} GPIOGetStates;
static GPIOGetStates m_gpio_get_states;
//static int m_gpio_get_count = 0;

void gpio_platform_get_state(char *gpio_states) {
	em_err ret = EM_FAIL;

	int gpio_num = -1;
	int gpio_val = -1;

	ret = parse_gpio_revc_json(gpio_states, &gpio_num, &gpio_val);
	if (ret != EM_OK)	return;

	m_gpio_get_states.gpio_num = gpio_num;
	m_gpio_get_states.gpio_val = gpio_val;

	m_cmd_complete = true;

#if 0
	char *json_buff = gpio_states;
	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);
	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONArray) {
			EM_LOGE(TAG, "JSON Value type not matched");
		} else {
			JSON_Array *data_arr = json_value_get_array(root_value);
			int gpio_count = json_array_get_count(data_arr);

			m_gpio_get_states = (GPIOGetStates*) calloc((gpio_count+1), sizeof(GPIOGetStates));
			GPIOGetStates *gpio = m_gpio_get_states;
			for (int i=0; i<gpio_count; i++) {
				gpio += i;
				JSON_Object *arr_obj = json_array_get_object(data_arr, i);
				gpio->gpio_num = json_object_get_number(arr_obj, "som_pin");
				gpio->gpio_val = json_object_get_boolean(arr_obj, "som_pin_val");
			}
			m_gpio_get_count = gpio_count;
			json_value_free(root_value); /* clear root_value */
			m_cmd_complete = true;
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
	}
#endif
}

uint8_t gpio_pf_io_get_level(gpio_pf_io_num gpio_num) {

	SemaphorePfTakeMutex(m_gpio_mutex, THREADING_MAX_DELAY_PF);

	m_cmd_complete = false;
	memset(&m_gpio_get_states, 0x00, sizeof(m_gpio_get_states));

	char data[128] = {0x00};
	int data_len = 0;
	make_gpio_request_subjson(SIM_GET_GPIO_STATE, gpio_num, 0, data, &data_len);

	/* Send SIM_GET_GPIO_STATE command */
	char *json_buf;
	int json_len;

	simulator_make_request_json(&json_buf, &json_len, SIM_GET_GPIO_STATE, data);
	EM_LOGV(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	uint8_t level = 0;

	int time = 0;
	while (!m_cmd_complete) {
		if (++time > 500) {
			level = 0;
			break;
		}
		TaskPfDelay(10);
	}

	if (gpio_num == m_gpio_get_states.gpio_num) {
		level = m_gpio_get_states.gpio_val;
	} else {
		EM_LOGE(TAG, "gpio num not matched!");
	}

#if 0
	GPIOGetStates *gpio = m_gpio_get_states;
	uint8_t level;
	for (int i=0; i<1; i++) {
		gpio += i;
		level = gpio->gpio_val;
	}

	free(m_gpio_get_states);
#endif

	SemaphorePfGiveMutex(m_gpio_mutex);

	return level;
}

em_err gpio_pf_io_set_level(gpio_pf_io_num gpio_num, uint8_t level) {
	char data[128] = {0x00};
	int data_len = 0;
	make_gpio_request_subjson(SIM_SET_GPIO_STATE, gpio_num, level, data, &data_len);

	/* Send SIM_SET_GPIO_STATE command */
	char *json_buf;
	int json_len;
	simulator_make_request_json(&json_buf, &json_len, SIM_SET_GPIO_STATE, data);
	EM_LOGV(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	return EM_OK;
}

static int search_gpio_node(int gpio_num, gpio_pf_io_config_t **p_gpio_node, int *pos, gll_t *ll) {
	int ll_size = ll->size;
	int ll_pos = 0;
	bool found = false;
	gpio_pf_io_config_t *gpio_node = NULL;

	for (ll_pos = 0; ll_pos < ll_size; ll_pos++) {
		gpio_node = (gpio_pf_io_config_t*) gll_get(ll, ll_pos);
		if (gpio_node != NULL) {
			if (gpio_node->pin_bit_mask == gpio_num) {
//				EM_LOGI(TAG, "gpio_pf_io_config_t matched");
				found = true;
				break;
			}
		}
	}

	if (found) {
		*p_gpio_node = gpio_node;
		*pos = ll_pos;
		return 0;
	} else {
		*p_gpio_node = NULL;
		return -1;
	}
}

em_err gpio_pf_io_set_intr_type(gpio_pf_io_num gpio_num, gpio_pf_interrupt_type intr_type) {
	/* Match the received data with the linked list */
	gpio_pf_io_config_t *gpio_node = NULL;
	int pos = -1;
	int search_result = search_gpio_node(gpio_num, &gpio_node, &pos, gpio_ll);
	if (search_result == 0) {
		gpio_node->intr_type = intr_type;
		return EM_OK;
	} else {
		EM_LOGW(TAG, "search_gpio_node failed");
		return EM_FAIL;
	}
}

em_err gpio_pf_io_isr_handler_add(gpio_pf_io_num gpio_num, gpio_pf_io_isr_t isr_handler, void *args) {
	/* Match the received data with the linked list */
	gpio_pf_io_config_t *gpio_node = NULL;
	int pos = -1;
	int search_result = search_gpio_node(gpio_num, &gpio_node, &pos, gpio_ll);
	if (search_result == 0) {
		gpio_node->isr = isr_handler;
		gpio_node->arg = args;
		return EM_OK;
	} else {
		EM_LOGW(TAG, "search_gpio_node failed");
		return EM_FAIL;
	}
}

void gpio_platform_gpio_event(char *gpio_event_data) {
	em_err ret = EM_FAIL;

	int gpio_num = -1;
	int gpio_val = -1;

	ret = parse_gpio_revc_json(gpio_event_data, &gpio_num, &gpio_val);
	if (ret != EM_OK)	return;

	/* Match the received data with the linked list */
	gpio_pf_io_config_t *gpio_node = NULL;
	int pos = -1;
	int search_result = search_gpio_node(gpio_num, &gpio_node, &pos, gpio_ll);
	if (search_result == 0) {
		if (gpio_node->isr != NULL) {
			/* Call the interrupt service routine and pass the arg */
			(*gpio_node->isr)(gpio_node->arg);
		}
	} else {
		EM_LOGW(TAG, "search_gpio_node failed");
	}
}

em_err gpio_platform_initialize_gpio() {
	/* Initialize GLL */
	if (gpio_ll == NULL) {
		gpio_ll = gll_init();
	}

	/* Initialize mutex for thread safety */
	m_gpio_mutex = SemaphorePfCreateMutex();
	if (m_gpio_mutex != NULL) {
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}

em_err gpio_pf_io_config(const gpio_pf_io_config_t *pGPIOConfig) {
	em_err ret = EM_FAIL;

	/* Validate pin number */
	uint32_t pin_num = pGPIOConfig->pin_bit_mask;
	if ((pin_num < GPIO_PF_GPIO_MIN) || (pin_num > GPIO_PF_GPIO_MAX)) {
		EM_LOGE(TAG, "Invalid GPIO Number");
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}

	/* Create Node */
	gpio_pf_io_config_t *gpio_node = (gpio_pf_io_config_t*) calloc(1, sizeof(gpio_pf_io_config_t));
	if (gpio_node == NULL) {
		EM_LOGE(TAG, "calloc failed");
		return EM_FAIL;
	}
	gpio_node->pin_bit_mask = pGPIOConfig->pin_bit_mask;
	gpio_node->mode = pGPIOConfig->mode;
	gpio_node->pull_down_en = pGPIOConfig->pull_down_en;
	gpio_node->pull_up_en = pGPIOConfig->pull_up_en;
	gpio_node->intr_type = GPIO_PF_INTERRUPT_DISABLE;
	gpio_node->isr = NULL;
	gpio_node->arg = NULL;

	/* Add a gpio_ll node */
	int err = -1;
	err = gll_pushBack(gpio_ll, gpio_node);
	if (err == -1) {
		EM_LOGE(TAG, "gll_pushBack failed");
		free(gpio_node);
		return EM_FAIL;
	}

	return ret;
}

em_err gpio_pf_io_reset_pin(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_intr_enable(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_intr_disable(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_set_direction(gpio_pf_io_num gpio_num, gpio_pf_io_mode mode)
{
	return 0;
}

em_err gpio_pf_io_set_pull_mode(gpio_pf_io_num gpio_num, gpio_pf_io_mode pull)
{
	return 0;
}

em_err gpio_pf_io_wakeup_enable(gpio_pf_io_num gpio_num, gpio_pf_interrupt_type intr_type)
{
	return 0;
}

em_err gpio_pf_io_wakeup_disable(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_isr_register(void (*fn)(void *), void *arg, int intr_alloc_flags, gpio_pf_io_isr_handle_t *handle)
{
	return 0;
}

em_err gpio_pf_io_pullup_en(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_pullup_dis(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_pulldown_en(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_pulldown_dis(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_install_isr_service(int intr_alloc_flags)
{
	return 0;
}

void gpio_pf_io_uninstall_isr_service(void)
{

}

em_err gpio_pf_io_isr_handler_remove(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_set_drive_capability(gpio_pf_io_num gpio_num, gpio_pf_io_drive_cap strength)
{
	return 0;
}

em_err gpio_pf_io_get_drive_capability(gpio_pf_io_num gpio_num, gpio_pf_io_drive_cap strength)
{
	return 0;
}

em_err gpio_hold_en(gpio_pf_io_num gpio_num)
{
	return 0;
}

em_err gpio_pf_io_hold_dis(gpio_pf_io_num gpio_num)
{
	return 0;
}

void gpio_pf_io_deep_sleep_hold_en(void)
{

}

void gpio_pf_io_deep_sleep_hold_dis(void)
{

}

void gpio_pf_io_iomux_in(uint32_t gpio, uint32_t signal_idx)
{

}

void gpio_pf_io_iomux_out(uint8_t gpio_num, int func, bool oen_inv)
{

}

void gpio_pf_io_pad_select_gpio(uint8_t gpio_num)
{

}











