/*
 * File Name: gpio_platform.h
 * File Path: /emmate/src/platform/simulator/gpio/gpio_platform.h
 * Description:
 *
 *  Created on: 10-May-2019
 *      Author: Noyel Seth
 */

#ifndef GPIO_PLATFORM_H_
#define GPIO_PLATFORM_H_

#include "core_config.h"
#include "core_error.h"
#include "pf_peripheral_common.h"
#include <stdint.h>
#include <stdbool.h>

#define GPIO_PF_GPIO_MIN	1
#define GPIO_PF_GPIO_MAX	200

/* Wrapper typedefs for abstracting the platform specific names */

//Keep the LEVELx values as they are here; they match up with (1<<level)
#define GPIO_PF_IO_INTR_FLAG_LEVEL1 		PF_INTR_FLAG_LEVEL1	///< Accept a Level 1 interrupt vector (lowest priority)
#define GPIO_PF_IO_INTR_FLAG_LEVEL2 		PF_INTR_FLAG_LEVEL2	///< Accept a Level 2 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL3 		PF_INTR_FLAG_LEVEL3	///< Accept a Level 3 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL4 		PF_INTR_FLAG_LEVEL4	///< Accept a Level 4 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL5 		PF_INTR_FLAG_LEVEL5	///< Accept a Level 5 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_LEVEL6 		PF_INTR_FLAG_LEVEL6	///< Accept a Level 6 interrupt vector
#define GPIO_PF_IO_INTR_FLAG_NMI 			PF_INTR_FLAG_NMI			///< Accept a Level 7 interrupt vector (highest priority)
#define GPIO_PF_IO_INTR_FLAG_SHARED 		PF_INTR_FLAG_SHARED	///< Interrupt can be shared between ISRs
#define GPIO_PF_IO_INTR_FLAG_EDGE 			PF_INTR_FLAG_EDGE		///< Edge-triggered interrupt
#define GPIO_PF_IO_INTR_FLAG_IRAM 			PF_INTR_FLAG_IRAM			///< ISR can be called if cache is disabled
#define GPIO_PF_IO_INTR_FLAG_INTRDISABLED 	PF_INTR_FLAG_INTRDISABLED		///< Return with this interrupt disabled

#define GPIO_PF_IO_INTR_FLAG_LOWMED 		PF_INTR_FLAG_LOWMED ///< Low and medium prio interrupts. These can be handled in C.
#define GPIO_PF_IO_INTR_FLAG_HIGH 			PF_INTR_FLAG_HIGH ///< High level interrupts. Need to be handled in assembly.

#define GPIO_PF_IO_INTR_FLAG_LEVELMASK 		PF_INTR_FLAG_LEVELMASK	///< Mask for all level flags

#define	GPIO_PF_IO_INTERRUPT		//IRAM_ATTR			// interrupt handler's declared type

#define GPIO_PLATFORM_IS_VALID_IO(gpio_num) 			1 //GPIO_IS_VALID_GPIO(gpio_num) /*!< Check whether it is a valid GPIO number */
#define GPIO_PLATFORM_IS_VALID_OUTPUT_IO(gpio_num)   	1 //GPIO_IS_VALID_OUTPUT_GPIO(gpio_num)    /*!< Check whether it can be a valid GPIO number of output mode */

typedef enum {
	GPIO_PF_IO_NUM_NC = -1, /*!< Use to signal not connected to S/W */
	GPIO_PF_IO_NUM_0 = 0, /*!< GPIO0, input and output */
	GPIO_PF_IO_NUM_1 = 1, /*!< GPIO1, input and output */
	GPIO_PF_IO_NUM_2 = 2, /*!< GPIO2, input and output
	 @note There are more enumerations like that
	 up to GPIO39, excluding GPIO20, GPIO24 and GPIO28..31.
	 They are not shown here to reduce redundant information.
	 @note GPIO34..39 are input mode only. */
	/** @cond */
	GPIO_PF_IO_NUM_3 = 3, /*!< GPIO3, input and output */
	GPIO_PF_IO_NUM_4 = 4, /*!< GPIO4, input and output */
	GPIO_PF_IO_NUM_5 = 5, /*!< GPIO5, input and output */
	GPIO_PF_IO_NUM_6 = 6, /*!< GPIO6, input and output */
	GPIO_PF_IO_NUM_7 = 7, /*!< GPIO7, input and output */
	GPIO_PF_IO_NUM_8 = 8, /*!< GPIO8, input and output */
	GPIO_PF_IO_NUM_9 = 9, /*!< GPIO9, input and output */
	GPIO_PF_IO_NUM_10 = 10, /*!< GPIO10, input and output */
	GPIO_PF_IO_NUM_11 = 11, /*!< GPIO11, input and output */
	GPIO_PF_IO_NUM_12 = 12, /*!< GPIO12, input and output */
	GPIO_PF_IO_NUM_13 = 13, /*!< GPIO13, input and output */
	GPIO_PF_IO_NUM_14 = 14, /*!< GPIO14, input and output */
	GPIO_PF_IO_NUM_15 = 15, /*!< GPIO15, input and output */
	GPIO_PF_IO_NUM_16 = 16, /*!< GPIO16, input and output */
	GPIO_PF_IO_NUM_17 = 17, /*!< GPIO17, input and output */
	GPIO_PF_IO_NUM_18 = 18, /*!< GPIO18, input and output */
	GPIO_PF_IO_NUM_19 = 19, /*!< GPIO19, input and output */
	GPIO_PF_IO_NUM_20 = 20,   /*!< GPIO20, input and output */
	GPIO_PF_IO_NUM_21 = 21, /*!< GPIO21, input and output */
	GPIO_PF_IO_NUM_22 = 22, /*!< GPIO22, input and output */
	GPIO_PF_IO_NUM_23 = 23, /*!< GPIO23, input and output */

	GPIO_PF_IO_NUM_25 = 25, /*!< GPIO25, input and output */
	GPIO_PF_IO_NUM_26 = 26, /*!< GPIO26, input and output */
	GPIO_PF_IO_NUM_27 = 27, /*!< GPIO27, input and output */

	GPIO_PF_IO_NUM_32 = 32, /*!< GPIO32, input and output */
	GPIO_PF_IO_NUM_33 = 33, /*!< GPIO33, input and output */
	GPIO_PF_IO_NUM_34 = 34, /*!< GPIO34, input mode only */
	GPIO_PF_IO_NUM_35 = 35, /*!< GPIO35, input mode only */
	GPIO_PF_IO_NUM_36 = 36, /*!< GPIO36, input mode only */
	GPIO_PF_IO_NUM_37 = 37, /*!< GPIO37, input mode only */
	GPIO_PF_IO_NUM_38 = 38, /*!< GPIO38, input mode only */
	GPIO_PF_IO_NUM_39 = 39, /*!< GPIO39, input mode only */
	GPIO_PF_IO_NUM_MAX,
/** @endcond */
} gpio_pf_io_num;

typedef enum {
	GPIO_PF_INTERRUPT_DISABLE = 0, /*!< Disable GPIO interrupt                             */
	GPIO_PF_INTERRUPT_POSEDGE = 1, /*!< GPIO interrupt type : rising edge                  */
	GPIO_PF_INTERRUPT_NEGEDGE = 2, /*!< GPIO interrupt type : falling edge                 */
	GPIO_PF_INTERRUPT_ANYEDGE = 3, /*!< GPIO interrupt type : both rising and falling edge */
	GPIO_PF_INTERRUPT_LOW_LEVEL = 4, /*!< GPIO interrupt type : input low level trigger      */
	GPIO_PF_INTERRUPT_HIGH_LEVEL = 5, /*!< GPIO interrupt type : input high level trigger     */
	GPIO_PF_INTERRUPT_MAX,
} gpio_pf_interrupt_type;

typedef enum {
	GPIO_PF_IO_MODE_DISABLE = 0, /*!< GPIO mode : disable input and output             */
	GPIO_PF_IO_MODE_INPUT = 1, /*!< GPIO mode : input only                           */
	GPIO_PF_IO_MODE_OUTPUT = 2, /*!< GPIO mode : output only mode                     */
	GPIO_PF_IO_MODE_OUTPUT_OD = 3, /*!< GPIO mode : output only with open-drain mode     */
	GPIO_PF_IO_MODE_INPUT_OUTPUT_OD = 4, /*!< GPIO mode : output and input with open-drain mode*/
	GPIO_PF_IO_MODE_INPUT_OUTPUT = 5, /*!< GPIO mode : output and input mode                */
} gpio_pf_io_mode;

typedef enum {
	GPIO_PF_IO_PULLUP_DISABLE = 0, /*!< Disable GPIO pull-up resistor */
	GPIO_PF_IO_PULLUP_ENABLE = 1, /*!< Enable GPIO pull-up resistor */
} gpio_pf_io_pullup;

typedef enum {
	GPIO_PF_IO_PULLDOWN_DISABLE = 0, /*!< Disable GPIO pull-down resistor */
	GPIO_PF_IO_PULLDOWN_ENABLE = 1, /*!< Enable GPIO pull-down resistor  */
} gpio_pf_io_pulldown;

typedef enum {
	GPIO_PF_IO_PULLUP_ONLY = 0, /*!< Pad pull up            */
	GPIO_PF_IO_PULLDOWN_ONLY = 1, /*!< Pad pull down          */
	GPIO_PF_IO_PULLUP_PULLDOWN = 2, /*!< Pad pull up + pull down*/
	GPIO_PF_IO_FLOATING = 3, /*!< Pad floating           */
} gpio_pf_io_pull_mode;

typedef enum {
	GPIO_PF_IO_DRIVE_CAP_0 = 0, /*!< Pad drive capability: weak          */
	GPIO_PF_IO_DRIVE_CAP_1 = 1, /*!< Pad drive capability: stronger      */
	GPIO_PF_IO_DRIVE_CAP_2 = 2, /*!< Pad drive capability: default value */
	GPIO_PF_IO_DRIVE_CAP_DEFAULT = 2, /*!< Pad drive capability: default value */
	GPIO_PF_IO_DRIVE_CAP_3 = 3, /*!< Pad drive capability: strongest     */
	GPIO_PF_IO_DRIVE_CAP_MAX,
} gpio_pf_io_drive_cap;

typedef void (*gpio_isr_t)(void *);
typedef void* gpio_isr_handle_t;
/* Wrapper typedefs for abstracting the platform specific names */
/**
 * @brief Configuration parameters of GPIO pad for gpio_config function
 */
typedef gpio_isr_t gpio_pf_io_isr_t;
typedef gpio_isr_handle_t gpio_pf_io_isr_handle_t;

typedef struct {
    uint64_t pin_bit_mask;          /*!< GPIO pin: set with bit mask, each bit maps to a GPIO */
    gpio_pf_io_mode mode;               /*!< GPIO mode: set input/output mode                     */
    gpio_pf_io_pullup pull_up_en;       /*!< GPIO pull-up                                         */
    gpio_pf_io_pulldown pull_down_en;   /*!< GPIO pull-down                                       */
    gpio_pf_interrupt_type intr_type;      /*!< GPIO interrupt type                                  */
    gpio_isr_t isr;
    void *arg;
} sim_gpio_config_t;
typedef sim_gpio_config_t gpio_pf_io_config_t;

/* Callback Functions */
void gpio_platform_get_state(char *gpio_states);
void gpio_platform_gpio_event(char *gpio_event_data);

/* Other Functions */
em_err gpio_platform_initialize_gpio();
em_err gpio_pf_io_config(const gpio_pf_io_config_t *pGPIOConfig);
em_err gpio_pf_io_reset_pin(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_set_intr_type(gpio_pf_io_num gpio_num, gpio_pf_interrupt_type intr_type);
em_err gpio_pf_io_intr_enable(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_intr_disable(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_set_level(gpio_pf_io_num gpio_num, uint8_t level);
uint8_t gpio_pf_io_get_level(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_set_direction(gpio_pf_io_num gpio_num, gpio_pf_io_mode mode);
em_err gpio_pf_io_set_pull_mode(gpio_pf_io_num gpio_num, gpio_pf_io_mode pull);
em_err gpio_pf_io_wakeup_enable(gpio_pf_io_num gpio_num, gpio_pf_interrupt_type intr_type);
em_err gpio_pf_io_wakeup_disable(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_isr_register(void (*fn)(void *), void *arg, int intr_alloc_flags, gpio_pf_io_isr_handle_t *handle);
em_err gpio_pf_io_pullup_en(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_pullup_dis(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_pulldown_en(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_pulldown_dis(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_install_isr_service(int intr_alloc_flags);
void gpio_pf_io_uninstall_isr_service(void);
em_err gpio_pf_io_isr_handler_add(gpio_pf_io_num gpio_num, gpio_pf_io_isr_t isr_handler, void *args);
em_err gpio_pf_io_isr_handler_remove(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_set_drive_capability(gpio_pf_io_num gpio_num, gpio_pf_io_drive_cap strength);
em_err gpio_pf_io_get_drive_capability(gpio_pf_io_num gpio_num, gpio_pf_io_drive_cap strength);
em_err gpio_hold_en(gpio_pf_io_num gpio_num);
em_err gpio_pf_io_hold_dis(gpio_pf_io_num gpio_num);
void gpio_pf_io_deep_sleep_hold_en(void);
void gpio_pf_io_deep_sleep_hold_dis(void);
void gpio_pf_io_iomux_in(uint32_t gpio, uint32_t signal_idx);
void gpio_pf_io_iomux_out(uint8_t gpio_num, int func, bool oen_inv);
void gpio_pf_io_pad_select_gpio(uint8_t gpio_num);


#endif /* GPIO_PLATFORM_H_ */
