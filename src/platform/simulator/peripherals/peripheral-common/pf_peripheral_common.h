/*
 * File Name: esp_pf_intr_alloc.h
 * File Path: /emmate/src/platform/esp32/peripherals/pf_peripheral_common.h.h
 * Description:
 *
 *  Created on: 30-May-2019
 *      Author: Noyel Seth
 */

#ifndef PF_PERIPHERAL_COMMON_H_
#define PF_PERIPHERAL_COMMON_H_

//Keep the LEVELx values as they are here; they match up with (1<<level)
#define PF_INTR_FLAG_LEVEL1 		(1<<1)
#define PF_INTR_FLAG_LEVEL2 		(1<<2)
#define PF_INTR_FLAG_LEVEL3 		(1<<3)
#define PF_INTR_FLAG_LEVEL4 		(1<<4)
#define PF_INTR_FLAG_LEVEL5 		(1<<5)
#define PF_INTR_FLAG_LEVEL6 		(1<<6)
#define PF_INTR_FLAG_NMI 			(1<<7)
#define PF_INTR_FLAG_SHARED 		(1<<8)
#define PF_INTR_FLAG_EDGE 			(1<<9)
#define PF_INTR_FLAG_IRAM 			(1<<10)
#define PF_INTR_FLAG_INTRDISABLED 	(1<<11)

#define PF_INTR_FLAG_LOWMED 		(PF_INTR_FLAG_LEVEL1|PF_INTR_FLAG_LEVEL2|PF_INTR_FLAG_LEVEL3) /*< Low and medium prio interrupts. These can be handled in C. */
#define PF_INTR_FLAG_HIGH 			(PF_INTR_FLAG_LEVEL4|PF_INTR_FLAG_LEVEL5|PF_INTR_FLAG_LEVEL6|PF_INTR_FLAG_NMI) /*< High level interrupts. Need to be handled in assembly. */

#define PF_INTR_FLAG_LEVELMASK 		(PF_INTR_FLAG_LEVEL1|PF_INTR_FLAG_LEVEL2|PF_INTR_FLAG_LEVEL3| \
								 PF_INTR_FLAG_LEVEL4|PF_INTR_FLAG_LEVEL5|PF_INTR_FLAG_LEVEL6| \
								 PF_INTR_FLAG_NMI)	/*< Mask for all level flags */

#endif /* PF_PERIPHERAL_COMMON_H_ */
