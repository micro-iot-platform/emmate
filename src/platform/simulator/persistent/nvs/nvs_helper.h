/*
 * nvs_helper.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef NVS_HELPER_H_
#define NVS_HELPER_H_

#include <stdio.h>
#include "core_error.h"

/**
 * @brief initialize the Non Volatile Storage/Memory
 *
 * @return
 * 		- EM_OK		On success
 * 		- EM_FAIL		On failure
 *
 * */
em_err init_nvs();

/**
 * @brief De-initializes the Non Volatile Storage/Memory (NVS)
 *
 **/
void deinit_nvs();

/**
 * @brief Erase key-value pair with given key name. init_nvs() must be called first
 *
 * @param[in]	key unique name for key
 *
 * @return
 * 		- EM_OK							Successfully erased the key from NVS
 * 		- EM_FAIL							Failed to erase the key from NVS
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		if the storage driver is not initialized
 * */
em_err erase_nvs_key(char *key);

/**
 * @brief Completely erase the NVS memory
 *
 * @return
 * 		- EM_OK							On success
 * 		- EM_FAIL							On failure
 *		- PLAT_NVS_ERR_NOT_INITIALIZED		if the storage driver is not initialized
 * */
em_err erase_nvs();

/**
 * @brief read data from NVS
 *
 * @param[in]	key 		unique name for key
 * @param[out]	value		data read from NVS
 * @param[out]	val_size	length of data read
 *
 * @return
 * 		- EM_OK							On success
 * 		- EM_FAIL							On failure
 * 		- PLAT_NVS_ERR_NOT_FOUND 			if key not present
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		if the storage driver is not initialized
 * */
em_err read_nvsdata_by_key(char *key, void* value, size_t * val_size);

/**
 * @brief write data to NVS
 *
 * @param[in]	key 		unique name for key
 * @param[in]	value		input data to write into NVS
 * @param[in]	val_size	input data length to write into NVS
 *
 * @return
 * 		- EM_OK							On success
 * 		- EM_FAIL							On failure
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		if the storage driver is not initialized
 * */
em_err write_nvsdata_by_key(char *key, void *set_data, size_t len);

#endif /* NVS_HELPER_H_ */
