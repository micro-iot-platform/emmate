/*
 * File Name: nvs_helper_errors.h
 * Description:
 *
 *  Created on: 15-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef NVS_HELPER_ERRORS_H_
#define NVS_HELPER_ERRORS_H_

#include "platform_error_base.h"

#define PLAT_NVS_ERR_NVS_BASE                    0x1100
#define PLAT_NVS_ERR_NOT_INITIALIZED		(PLAT_NVS_ERR_NVS_BASE + 0x01)/*!< The storage driver is not initialized */
#define PLAT_NVS_ERR_NOT_FOUND				(PLAT_NVS_ERR_NVS_BASE + 0x02)/*!< Id namespace doesn’t exist yet and mode is NVS_READONLY */
#define PLAT_NVS_ERR_TYPE_MISMATCH			(PLAT_NVS_ERR_NVS_BASE + 0x03)/*!< The type of set or get operation doesn't match the type of value stored in NVS */
#define PLAT_NVS_ERR_READ_ONLY				(PLAT_NVS_ERR_NVS_BASE + 0x04)/*!< Storage handle was opened as read only */
#define PLAT_NVS_ERR_NOT_ENOUGH_SPACE		(PLAT_NVS_ERR_NVS_BASE + 0x05)/*!< There is not enough space in the underlying storage to save the value */
#define PLAT_NVS_ERR_INVALID_NAME			(PLAT_NVS_ERR_NVS_BASE + 0x06)/*!< Namespace name doesn’t satisfy constraints */
#define PLAT_NVS_ERR_INVALID_HANDLE			(PLAT_NVS_ERR_NVS_BASE + 0x07)/*!< Handle has been closed or is NULL */
#define PLAT_NVS_ERR_REMOVE_FAILED			(PLAT_NVS_ERR_NVS_BASE + 0x08)/*!< The value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again. */
#define PLAT_NVS_ERR_KEY_TOO_LONG			(PLAT_NVS_ERR_NVS_BASE + 0x09)/*!< Key name is too long */
#define PLAT_NVS_ERR_PAGE_FULL				(PLAT_NVS_ERR_NVS_BASE + 0x0a)/*!< Internal error; never returned by nvs API functions */
#define PLAT_NVS_ERR_INVALID_STATE			(PLAT_NVS_ERR_NVS_BASE + 0x0b)/*!< NVS is in an inconsistent state due to a previous error. Call nvs_flash_init and nvs_open again, then retry. */
#define PLAT_NVS_ERR_INVALID_LENGTH			(PLAT_NVS_ERR_NVS_BASE + 0x0c)/*!< String or blob length is not sufficient to store data */
#define PLAT_NVS_ERR_NO_FREE_PAGES			(PLAT_NVS_ERR_NVS_BASE + 0x0d)/*!< NVS partition doesn't contain any empty pages. This may happen if NVS partition was truncated. Erase the whole partition and call nvs_flash_init again. */
#define PLAT_NVS_ERR_VALUE_TOO_LONG			(PLAT_NVS_ERR_NVS_BASE + 0x0e)/*!< String or blob length is longer than supported by the implementation */
#define PLAT_NVS_ERR_PART_NOT_FOUND			(PLAT_NVS_ERR_NVS_BASE + 0x0f)/*!< Partition with specified name is not found in the partition table */

#define PLAT_NVS_ERR_NEW_VERSION_FOUND		(PLAT_NVS_ERR_NVS_BASE + 0x10)/*!< NVS partition contains data in new format and cannot be recognized by this version of code */
#define PLAT_NVS_ERR_XTS_ENCR_FAILED		(PLAT_NVS_ERR_NVS_BASE + 0x11)/*!< XTS encryption failed while writing NVS entry */
#define PLAT_NVS_ERR_XTS_DECR_FAILED		(PLAT_NVS_ERR_NVS_BASE + 0x12)/*!< XTS decryption failed while reading NVS entry */
#define PLAT_NVS_ERR_XTS_CFG_FAILED			(PLAT_NVS_ERR_NVS_BASE + 0x13)/*!< XTS configuration setting failed */
#define PLAT_NVS_ERR_XTS_CFG_NOT_FOUND		(PLAT_NVS_ERR_NVS_BASE + 0x14)/*!< XTS configuration not found */
#define PLAT_NVS_ERR_ENCR_NOT_SUPPORTED		(PLAT_NVS_ERR_NVS_BASE + 0x15)/*!< NVS encryption is not supported in this version */
#define PLAT_NVS_ERR_KEYS_NOT_INITIALIZED	(PLAT_NVS_ERR_NVS_BASE + 0x16)	/*!< NVS key partition is uninitialized */
#define PLAT_NVS_ERR_CORRUPT_KEY_PART		(PLAT_NVS_ERR_NVS_BASE + 0x17)/*!< NVS key partition is corrupt */

#define PLAT_NVS_ERR_CONTENT_DIFFERS		(PLAT_NVS_ERR_NVS_BASE + 0x18)/*!< Internal error; never returned by nvs API functions.  NVS key is different in comparison */


#endif /* NVS_HELPER_ERRORS_H_ */
