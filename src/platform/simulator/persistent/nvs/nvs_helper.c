/*
 * nvs_helper.c
 *
 *  Created on: 16-Jun-2020
 *      Author: Rohan Dey
 */

#define _XOPEN_SOURCE 500
#include <ftw.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>

#include "core_config.h"
#include "core_error.h"
#include "core_constant.h"
#include "nvs_helper.h"
#include "nvs_helper_errors.h"
#include "threading.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG LTAG_PERSISTENT_NVS

#define NVS_LOCATION			"EMMATE_NVS"
#define NVS_INITIALIZED			1
#define NVS_NOT_INITIALIZED		0

static int m_nvs_init_stat = NVS_NOT_INITIALIZED;
static pthread_mutex_t m_fmutex;

/* Static Functions */
static em_err open_nvs_handle();
static void close_nvs_handle();

static em_err open_nvs_handle() {
}

static void close_nvs_handle() {
}

static void make_path(const char* filename, char* outpath) {
	if(outpath == NULL) {
		return;
	}

	strcat(outpath, NVS_LOCATION);
	strcat(outpath, "/");
	strcat(outpath, filename);

	EM_LOGD(TAG, "outpath = %s", outpath);
}

em_err init_nvs() {

	if (m_nvs_init_stat == NVS_INITIALIZED) {
		EM_LOGE(TAG, "NVS already initialized before");
		return EM_FAIL;
	}

	pthread_mutex_init(&m_fmutex, NULL);

	int rv;
	char *dirname = NVS_LOCATION;

	rv = mkdir(dirname, 0777);
	EM_LOGD(TAG, "mkdir returned %d, errno = %d", rv, errno);

	/* check if directory is created or not */
	if (!rv) {
		EM_LOGD(TAG, "Directory created");
		m_nvs_init_stat = NVS_INITIALIZED;
		return EM_OK;
	}
	else {
		if (errno == EEXIST) {
			EM_LOGD(TAG, "NVS Directory already exists");
			m_nvs_init_stat = NVS_INITIALIZED;
			return EM_OK;
		}
		EM_LOGE(TAG, "Unable to create NVS directory");
		pthread_mutex_destroy(&m_fmutex);
		m_nvs_init_stat = NVS_NOT_INITIALIZED;
		return EM_FAIL;
	}
}

void deinit_nvs() {
	if (m_nvs_init_stat == NVS_NOT_INITIALIZED) {
		return;
	}
	pthread_mutex_destroy(&m_fmutex);
	m_nvs_init_stat = NVS_NOT_INITIALIZED;
}

static int remove_files(const char* pathname, const struct stat *sbuf, int type, struct FTW *ftwb) {
	if (remove(pathname) < 0) {
		EM_LOGE(TAG, "ERROR: remove");
		return -1;
	}
	return 0;
}

em_err erase_nvs() {
	if (m_nvs_init_stat == NVS_NOT_INITIALIZED) {
		EM_LOGE(TAG, "NVS not initialized, %s", __FUNCTION__);
		return PLAT_NVS_ERR_NOT_INITIALIZED;
	}

	em_err ret = EM_FAIL;

	pthread_mutex_lock(&m_fmutex);
	int rv = nftw(NVS_LOCATION, remove_files, 10, FTW_DEPTH|FTW_MOUNT|FTW_PHYS);
	if (rv < 0) {
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}
	pthread_mutex_unlock(&m_fmutex);

	if (ret == EM_OK) {
		deinit_nvs();
		init_nvs();
	}

	return ret;
}

em_err erase_nvs_key(char *key) {
	if (m_nvs_init_stat == NVS_NOT_INITIALIZED) {
		EM_LOGE(TAG, "NVS not initialized, %s", __FUNCTION__);
		return PLAT_NVS_ERR_NOT_INITIALIZED;
	}

	if (key == NULL) {
		EM_LOGE(TAG, "Invalid key, %s", __FUNCTION__);
		return EM_FAIL;
	}
	em_err ret = EM_FAIL;

	/* Length of path = "NVS_LOCATION" + "/" + "<key>" + "\0" */
	int pathlen = strlen(NVS_LOCATION) + strlen(key) + 2;
	char *filepath = (char*) calloc(1, pathlen);

	make_path(key, filepath);

	pthread_mutex_lock(&m_fmutex);

	int rv = remove(filepath);
	if (rv == 0) {
		ret = EM_OK;
	} else {
		ret = EM_FAIL;
	}

	pthread_mutex_unlock(&m_fmutex);
	free(filepath);
	return ret;
}

em_err read_nvsdata_by_key(char *key, void* value, size_t * val_size) {
	if (m_nvs_init_stat == NVS_NOT_INITIALIZED) {
		EM_LOGE(TAG, "NVS not initialized, %s", __FUNCTION__);
		return PLAT_NVS_ERR_NOT_INITIALIZED;
	}

	if (key == NULL) {
		EM_LOGE(TAG, "Invalid key, %s", __FUNCTION__);
		return EM_FAIL;
	}

	if (value == NULL) {
		EM_LOGE(TAG, "Invalid data, %s", __FUNCTION__);
		return EM_FAIL;
	}

	if (val_size == NULL) {
		EM_LOGE(TAG, "Invalid length variable, %s", __FUNCTION__);
		return EM_FAIL;
	}

	em_err ret = EM_FAIL;

	/* Length of path = "NVS_LOCATION" + "/" + "<key>" + "\0" */
	int pathlen = strlen(NVS_LOCATION) + strlen(key) + 2;
	char *filepath = (char*) calloc(1, pathlen);

	make_path(key, filepath);

	pthread_mutex_lock(&m_fmutex);

	FILE *fp = fopen(filepath, "r");
	if (fp == NULL) {
		/* This means the file has not been created */
		EM_LOGW(TAG, "The key could not be found");
		ret = PLAT_NVS_ERR_NOT_FOUND;
		goto end_read;
	}

	fseek(fp, 0L, SEEK_END);
	long int file_size = ftell(fp);

	fseek(fp, 0L, SEEK_SET);
	size_t count = 1;

	size_t rv = fread(value, file_size, count, fp);
	if (rv != count) {
		EM_LOGE(TAG, "fread failed, %lu, %s", rv, __FUNCTION__);
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}

	*val_size = file_size;
	fclose(fp);

end_read:
	pthread_mutex_unlock(&m_fmutex);
	free(filepath);

	return ret;
}

em_err write_nvsdata_by_key(char *key, void *set_data, size_t len) {
	if (m_nvs_init_stat == NVS_NOT_INITIALIZED) {
		EM_LOGE(TAG, "NVS not initialized, %s", __FUNCTION__);
		return PLAT_NVS_ERR_NOT_INITIALIZED;
	}

	if (key == NULL) {
		EM_LOGE(TAG, "Invalid key, %s", __FUNCTION__);
		return EM_FAIL;
	}

	if (set_data == NULL) {
		EM_LOGE(TAG, "Invalid data, %s", __FUNCTION__);
		return EM_FAIL;
	}

	if (len <= 0) {
		EM_LOGE(TAG, "Invalid length, %s", __FUNCTION__);
		return EM_FAIL;
	}

	em_err ret = EM_FAIL;

	/* Length of path = "NVS_LOCATION" + "/" + "<key>" + "\0" */
	int pathlen = strlen(NVS_LOCATION) + strlen(key) + 2;
	char *filepath = (char*) calloc(1, pathlen);

	make_path(key, filepath);

	pthread_mutex_lock(&m_fmutex);

	FILE *fp = fopen(filepath, "w");
	if (fp == NULL) {
		ret = EM_FAIL;
		goto end_write;
	}

	size_t rv = fwrite(set_data, 1, len, fp);
	if (rv != len) {
		EM_LOGE(TAG, "fwrite failed, %lu, %s", rv, __FUNCTION__);
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}

	fclose(fp);

end_write:
	pthread_mutex_unlock(&m_fmutex);
	free(filepath);

	return ret;
}
