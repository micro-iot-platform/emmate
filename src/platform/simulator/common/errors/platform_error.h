/*
 * platform_error.h
 *
 *  Created on: 11-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef PLATFORM_ERROR_H_
#define PLATFORM_ERROR_H_

#include "platform_error_base.h"
//#include "esp_err.h"

/* Definitions for error constants. */
#define PLATFORM_OK          0 //ESP_OK
#define PLATFORM_FAIL        1 //ESP_FAIL

#define PLATFORM_ERR_NO_MEM              100 //ESP_ERR_NO_MEM   			/*!< Out of memory */
#define PLATFORM_ERR_INVALID_ARG         101 //ESP_ERR_INVALID_ARG   		/*!< Invalid argument */
#define PLATFORM_ERR_INVALID_STATE       102 //ESP_ERR_INVALID_STATE   	/*!< Invalid state */
#define PLATFORM_ERR_INVALID_SIZE        103 //ESP_ERR_INVALID_SIZE   	/*!< Invalid size */
#define PLATFORM_ERR_NOT_FOUND           104 //ESP_ERR_NOT_FOUND   		/*!< Requested resource not found */
#define PLATFORM_ERR_NOT_SUPPORTED       105 //ESP_ERR_NOT_SUPPORTED   	/*!< Operation or feature not supported */
#define PLATFORM_ERR_TIMEOUT             106 //ESP_ERR_TIMEOUT   			/*!< Operation timed out */
#define PLATFORM_ERR_INVALID_RESPONSE    107 //ESP_ERR_INVALID_RESPONSE   /*!< Received response was invalid */
#define PLATFORM_ERR_INVALID_CRC         108 //ESP_ERR_INVALID_CRC   		/*!< CRC or checksum was invalid */
#define PLATFORM_ERR_INVALID_VERSION     109 //ESP_ERR_INVALID_VERSION   	/*!< Version was invalid */
#define PLATFORM_ERR_INVALID_MAC         110 //ESP_ERR_INVALID_MAC   		/*!< MAC address was invalid */


#endif /* PLATFORM_ERROR_H_ */
