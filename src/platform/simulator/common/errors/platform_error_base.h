/*
 * File Name: platform_error_base.h
 * Description:
 *
 *  Created on: 05-May-2019
 *      Author: Rohan Dey
 */

#ifndef PLATFORM_ERROR_BASE_H_
#define PLATFORM_ERROR_BASE_H_

#define PLAT_ERR_WIFI_BASE			1 //ESP_ERR_WIFI_BASE
#define PLAT_ERR_MESH_BASE			2 //ESP_ERR_MESH_BASE
#define PLAT_ERR_NVS_BASE			3 //ESP_ERR_NVS_BASE
#define PLAT_TCPIP_ADAPTER_BASE		4 //ESP_ERR_TCPIP_ADAPTER_BASE

#endif /* PLATFORM_ERROR_BASE_H_ */
