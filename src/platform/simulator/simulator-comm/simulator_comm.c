/*
 * main.c
 *
 *  Created on: 04-May-2020
 *      Author: Rohan
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <pthread.h>
#include <sys/time.h>

#include "simulator_comm.h"
#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"
#include "core_logger.h"

#define TAG	"sim_comm"

typedef struct {
	pthread_t id_talker;
	pthread_t id_listener;
	SocketInfo sockinfo_t;
	SocketInfo sockinfo_l;
} ApplicationData;

static ApplicationData app_data = {0};

static void simulator_init() {
	simulator_adaptor_init();
}

static void start_app_threads() {
	pthread_attr_t attr;
	int status;

	pthread_attr_init(&attr);

	/* Create Socket comm talker thread */
	status = pthread_create(&app_data.id_talker, &attr, (void*) &sim_socket_talker, &app_data.sockinfo_t);
	if (status != 0) {
		EM_LOGE(TAG, "Failed to create thread with status = %d\n", status);
		return;
	}

	/* Create Socket comm listener thread */
	status = pthread_create(&app_data.id_listener, &attr, (void*) &sim_socket_listener, &app_data.sockinfo_l);
	if (status != 0) {
		EM_LOGE(TAG, "Failed to create thread with status = %d\n", status);
		return;
	}
}

static void start_app_state_machine() {
//	sim_adaptor_state_mc(app_data.id_app_activity, app_data.id_talker, app_data.id_listener);
}

int simulator_comm_main(int argc, char *argv[]) {
	if (argc != 4) {
//		EM_LOGE(TAG, "usage: %s <ip address> <talker port number> <listener port number> \n", argv[0]);
		exit(1);
	}
	app_data.sockinfo_t.server_ip = argv[1];
	app_data.sockinfo_t.port = argv[2];
	app_data.sockinfo_l.server_ip = argv[1];
	app_data.sockinfo_l.port = argv[3];

	/* Initialize default states */
	simulator_init();

	/* Start 3 threads:
	 * 1. User app activity queue manager
	 * 2. Socket comm talker
	 * 3. Socket comm listener
	 * */
	start_app_threads();

	/* Start a state machine to the following:
	 * 1. Manage the 3 threads
	 * 2. Manage state transitions
	 * 3. Exception Handling
	 * */
	start_app_state_machine();

	return 0;
}
