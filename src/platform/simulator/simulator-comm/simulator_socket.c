/*
 * simulator_socket.c
 *
 *  Created on: 04-May-2020
 *      Author: Rohan
 */
#include "simulator_socket.h"
#include "threading_platform.h"
#include "simulator_config.h"
#include "core_logger.h"

#define TAG	"sim_socket"

static QueueHandle_pf talker_q = NULL;
static QueueHandle_pf listener_q = NULL;

typedef struct {
	int packet_sequence;				// Packet sequence number
	SimCommData comm_data;
} SimCommPacket;

bool sim_socket_queue_push(SimCommData *pdata) {
	if (talker_q == NULL) {
		EM_LOGE(TAG, "sim_talker: Queue not initialized");
		return false;
	}
	if(QueuePfSend(talker_q, pdata, 1) == true) {
//		EM_LOGD(TAG, "sim_talker: Queue push success");
		return true;
	} else {
		EM_LOGE(TAG, "sim_talker: Queue push failed");
		return false;
	}
}

void* sim_socket_talker(void * arg) {
	EM_LOGD(TAG, "sim_talker thread created successfully");

	/* Create the talker queue */
	talker_q = QueuePfCreate(SIMCONFIG_TALKER_Q_MAXLEN, sizeof(SimCommData));
	if(talker_q == NULL) {
		EM_LOGE(TAG, "sim_talker: Failed to create queue");
		return NULL;
	}

	/* Create the talker socket */
	SocketInfo *sockinfo = (SocketInfo *) arg;

	EM_LOGI(TAG, "sim_talker: Opening Socket with IP: %s, Port: %s", sockinfo->server_ip, sockinfo->port);

	struct addrinfo hints;
	int rv, sockfd;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	struct addrinfo *servinfo, *p;

	if ((rv = getaddrinfo(sockinfo->server_ip, sockinfo->port, &hints, &servinfo)) != 0) {
		EM_LOGE(TAG, "sim_talker: getaddrinfo: %s", gai_strerror(rv));
		return NULL;
	}
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("sim_talker: socket");
			continue;
		}
		break;
	}
	if (p == NULL) {
		EM_LOGE(TAG, "talker: failed to create socket");
		return NULL;
	}

	/* Start communication with the simulator application */
	static int packet_seq = 0;
	SimCommPacket comm_packet;
	memset(&comm_packet, 0x00, sizeof(SimCommPacket));
	char serialised_buffer[MAX_UDP_PAYLOAD_LEN] = {'\0'};
	int buffer_pointer = 0, serialised_data_len = 0;
	int num_bytes = 0;
	while (1) {
#if 0	// Dummy
		SimCommData dat = {.datalen = 10, .data = "abcdefghij"};
		sim_socket_queue_push(&dat);
		sleep(3);
#endif

		/* Receive data from the talker queue */
		bool ret = QueuePfReceive(talker_q, &comm_packet.comm_data, THREADING_MAX_DELAY_PF);
		if (ret == false) {
			EM_LOGE(TAG, "QueuePfReceive failed!");
			TaskPfDelay(50);
			continue;
		}

		/* Increment the packet sequence */
		comm_packet.packet_sequence = ++packet_seq;
#if 0
		/* Print the acquired data */
		EM_LOGI(TAG, "sim_talker: Sending the following packet:");
		EM_LOGI(TAG, "Packet Sequence = %d", comm_packet.packet_sequence);
		EM_LOGI(TAG, "Data Length = %d", comm_packet.comm_data.datalen);
		EM_LOGI(TAG, "Data = %.*s", comm_packet.comm_data.datalen, comm_packet.comm_data.data);
		EM_LOGI(TAG, "=====================================================\n");
#endif
		/* Make the serialized data packet */
		memset(serialised_buffer, 0x00, sizeof(serialised_buffer));
		buffer_pointer = 0;
		serialised_data_len = 0;

//		memcpy(serialised_buffer, &(comm_packet.packet_sequence), sizeof(comm_packet.packet_sequence));
//		buffer_pointer += sizeof(comm_packet.packet_sequence);
//
//		memcpy(serialised_buffer + buffer_pointer, &(comm_packet.comm_data.datalen), sizeof(comm_packet.comm_data.datalen));
//		buffer_pointer += sizeof(comm_packet.comm_data.datalen);

		memcpy(serialised_buffer + buffer_pointer, &(comm_packet.comm_data.data), comm_packet.comm_data.datalen);
		buffer_pointer += comm_packet.comm_data.datalen;
		serialised_data_len = buffer_pointer;

#if 0
		/* Print the serialized data */
		EM_LOGI(TAG, "sim_talker: Printing Serialized data:");
		EM_LOGI(TAG, "Serialized Data Length = %d", serialised_data_len);
		EM_LOGI(TAG, "Serialized Data:%.*s\n", serialised_data_len, serialised_buffer+8);
#endif
		/* Send the packet via socket */
		num_bytes = sendto(sockfd, serialised_buffer, serialised_data_len, 0, (struct sockaddr*)p->ai_addr, p->ai_addrlen);
		if (num_bytes == -1) {
			EM_LOGE(TAG, "sim_talker: sendto failed");
		}
	}

	return 0;
}

bool sim_socket_queue_pop(SimCommData *pdata) {
	if (listener_q == NULL) {
		EM_LOGE(TAG, "sim_listener: Queue not initialized");
		return false;
	}
	/* Receive data from the talker queue */
	if (QueuePfReceive(listener_q, pdata, THREADING_MAX_DELAY_PF) == true) {
		return true;
	} else {
		return false;
	}
}

void* sim_socket_listener(void *arg) {
	EM_LOGD(TAG, "sim_listener: thread created successfully");

	/* Create the listener queue */
	listener_q = QueuePfCreate(SIMCONFIG_LISTENER_Q_MAXLEN, sizeof(SimCommData));
	if (listener_q == NULL) {
		EM_LOGE(TAG, "sim_listener: Failed to create queue");
		return NULL;
	}

	/* Create the listener socket */
	SocketInfo *sockinfo = (SocketInfo*) arg;

	EM_LOGI(TAG, "sim_listener: Opening Socket with IP: %s, Port: %s", sockinfo->server_ip, sockinfo->port);

	struct addrinfo hints;
	int rv, sockfd;
	struct addrinfo *servinfo, *p;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; /*set to AF_INET to force IPv4*/
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; /*use my IP*/

	if ((rv = getaddrinfo(sockinfo->server_ip, sockinfo->port, &hints, &servinfo)) != 0) {
		EM_LOGE(TAG, "sim_listener: getaddrinfo: %s", gai_strerror(rv));
		return NULL;
	}

	/*loop through all the results and bind to the first we can*/
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("sim_listener: socket");
			continue;
		}
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("sim_listener: bind");
			continue;
		}
		break;
	}
	if (p == NULL) {
		EM_LOGE(TAG, "sim_listener: failed to bind socket");
		return NULL;
	}

	EM_LOGD(TAG, "sim_listener: Waiting for data ...");
	char receive_buffer[MAX_UDP_PAYLOAD_LEN] = { '\0' };
	int recvbytes = -1;
	struct sockaddr_in in_addr;
	unsigned int in_addr_len = sizeof(in_addr);
	SimCommPacket recvpacket;
	int buffer_pointer = 0;
	while (1) {
		memset(receive_buffer, 0x00, sizeof(receive_buffer));
		buffer_pointer = 0;

		if ((recvbytes = recvfrom(sockfd, receive_buffer, MAX_UDP_PAYLOAD_LEN, 0, (struct sockaddr*) &in_addr,
				&in_addr_len)) == -1) {
			perror("sim_listener: recvfrom");
		} else {
			receive_buffer[recvbytes] = '\0';
#if 0
			EM_LOGI(TAG, "Received %d bytes", recvbytes);
			EM_LOGI(TAG, "Data = %s\n", receive_buffer);
#endif
			memset(&recvpacket, 0x00, sizeof(SimCommPacket));
#if 0
			memcpy(&recvpacket.packet_sequence, receive_buffer, sizeof(recvpacket.packet_sequence));
			buffer_pointer += sizeof(recvpacket.packet_sequence);

			memcpy(&recvpacket.comm_data.datalen, (receive_buffer + buffer_pointer), sizeof(recvpacket.comm_data.datalen));
			buffer_pointer += sizeof(recvpacket.comm_data.datalen);

			memcpy(&recvpacket.comm_data.data, (receive_buffer + buffer_pointer), recvpacket.comm_data.datalen);
			buffer_pointer += recvpacket.comm_data.datalen;
#else
			memcpy(recvpacket.comm_data.data, receive_buffer, (recvbytes));
#endif
#if 1
			EM_LOGD(TAG, "sim_listener: received data:");
#if 0
//			EM_LOGD(TAG, "packet_sequence = %d", recvpacket.packet_sequence);
//			EM_LOGD(TAG, "datalen = %d", recvpacket.comm_data.datalen);
//			EM_LOGD(TAG, "data = %.*s", recvpacket.comm_data.datalen, recvpacket.comm_data.data);
#else
			EM_LOGD(TAG, "data = %.*s", (recvbytes), recvpacket.comm_data.data);
#endif
			EM_LOGD(TAG, "=====================================================\n");
#endif
			if(QueuePfSend(listener_q, &recvpacket.comm_data, 1) == true) {
			}
		}
	}

	return NULL;
}
