/*
 * simulator_comm.h
 *
 *  Created on: 21-Aug-2020
 *      Author: Rohan Dey
 */

#ifndef SIMULATOR_COMM_H_
#define SIMULATOR_COMM_H_

#ifdef __cplusplus
extern "C" {
#endif

int simulator_comm_main(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif /* SIMULATOR_COMM_H_ */
