/*
 * simulator_adaptor_app.c
 *
 *  Created on: 04-May-2020
 *      Author: Rohan
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>

#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"
#include "gll.h"
#include "core_logger.h"
#include "input_processor.h"
#include "core_utils.h"

#define TAG	"sim_adaptor"

static pthread_t id_receive_parser;
static gll_t *emmod_ll = NULL;

em_err simulator_make_request_json(char **ppbuf, int *plen, int cmd, char *data) {
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	/* Set JSON key value */
	json_object_set_number(root_object, "cmd", (int)cmd);
	if (data != NULL) {
		JSON_Value *data_val = json_parse_string(data);
		if (data_val != NULL) {
			if (json_value_get_type(root_value) != JSONObject) {
				EM_LOGE(TAG, "Invalid json object");
			} else {
				json_object_set_value(root_object, "data", data_val);
			}
		} else {
			EM_LOGE(TAG, "Could not parse json string");
		}
	}

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	EM_LOGD(TAG, "Simulator Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		EM_LOGE(TAG, "simulator_make_request_json malloc failed!");
		return EM_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return EM_OK;
}

static em_err parse_simulator_recv_json(char *json_buff, SimCmdResponse *simcmd_resp) {
	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "JSON Value type not matched");
			ret = EM_FAIL;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Parse the common info: stat and error */
			ret = inproc_parse_json_common_info(json_buff, &simcmd_resp->status, &simcmd_resp->error);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
				ret = EM_FAIL;
				goto free_memory;
			}

			/* Check if response is successful */
			if (simcmd_resp->status) {
				/* get simulator command */
				simcmd_resp->cmd = json_object_get_number(rootObj, GET_VAR_NAME(simcmd_resp->cmd, "->"));
				if (simcmd_resp->cmd == 0) {
					EM_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(simcmd_resp->cmd, "->"));
					ret = EM_FAIL;
					goto free_memory;
				}

				/* get simulator data */
				JSON_Value* data_value = json_object_get_value(rootObj, GET_VAR_NAME(simcmd_resp->data, "->"));
				if (data_value != NULL) {
					if (json_value_get_type(data_value) != JSONObject) {
						EM_LOGE(TAG, "The key error_value is not a JSON Object!");
						ret = EM_FAIL;
						goto free_memory;
					} else {
						int stat = json_serialize_to_buffer(data_value, simcmd_resp->data, sizeof(simcmd_resp->data));
						if (stat != JSONSuccess) {
							ret = EM_FAIL;
							goto free_memory;
						}
					}
				}

				ret = EM_OK;
			}

			free_memory: json_value_free(root_value); /* clear root_value */
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
		ret = EM_FAIL;
	}
	return ret;
}

em_err simulator_register_emmate_module(SIMUMATOR_COMMANDS simcmd, sim_write_data write_data) {
	if ((simcmd <= 0) || (write_data == NULL)) {
		EM_LOGE(TAG, "Invalid parameters passed to simulator_register_emmate_module");
		return EM_FAIL;
	}

	/* Allocate memory for a qinfo_ll object. This will make a node */
	EmmateModuleNode *emnode = (EmmateModuleNode*) calloc(1, sizeof(EmmateModuleNode));
	if (emnode == NULL) {
		EM_LOGE(TAG, "calloc failed");
		return EM_FAIL;
	}
	emnode->cmd = simcmd;
	emnode->write_data = write_data;

	/* Add a emmod_ll node */
	int ret = -1;
	ret = gll_pushBack(emmod_ll, emnode);
	if (ret == -1) {
		EM_LOGE(TAG, "gll_pushBack failed");
		free(emnode);
		return EM_FAIL;
	}

	return EM_OK;
}

static int search_emmate_module(SIMUMATOR_COMMANDS simcmd, EmmateModuleNode **p_emnode, int *pos, gll_t *ll) {
	int ll_size = ll->size;
	int ll_pos = 0;
	bool found = false;
	EmmateModuleNode *emnode = NULL;

	for (ll_pos = 0; ll_pos < ll_size; ll_pos++) {
		emnode = (EmmateModuleNode*) gll_get(ll, ll_pos);
		if (emnode != NULL) {
			if (emnode->cmd == simcmd) {
//				EM_LOGI(TAG, "EmmateModuleNode matched");
				found = true;
				break;
			}
		}
	}

	if (found) {
		*p_emnode = emnode;
		*pos = ll_pos;
		return 0;
	} else {
		*p_emnode = NULL;
		return -1;
	}
}

static void* receive_parser(void *arg) {
	em_err ret = EM_FAIL;

	/* Initialize GLL */
	if (emmod_ll == NULL) {
		emmod_ll = gll_init();
	}

	/* Wait until the emmate modules are registered */
	while (emmod_ll->size <= 0) {
		sleep(1);
	}

	while (1) {
		/* Receive data from the queue */
		SimCommData sim_data;
		memset(&sim_data, 0x00, sizeof(SimCommData));
		if (sim_socket_queue_pop(&sim_data) != true) {
			EM_LOGE(TAG, "Failed to pop queue");
			continue;
		}

		/* Parse the received data and populate __________*/
		SimCmdResponse simcmd_resp;
		ret = parse_simulator_recv_json(sim_data.data, &simcmd_resp);
		if (ret != EM_OK) {
			continue;
		}
		/* Match the received data with the linked list */
		EmmateModuleNode *emnode = NULL;
		int pos = -1;
		int search_result = search_emmate_module(simcmd_resp.cmd, &emnode, &pos, emmod_ll);
		if (search_result == 0) {
			/* Pass the data to the identified emmate module by call the registered callback function */
			(*emnode->write_data)(simcmd_resp.data);
		} else {
			EM_LOGW(TAG, "search_emmate_module failed");
		}
	}

	return NULL;
}

void simulator_adaptor_init() {
	/* Initialize Receive Parser thread */
	pthread_attr_t attr;
	int status;

	pthread_attr_init(&attr);

	status = pthread_create(&id_receive_parser, &attr, (void*) &receive_parser, NULL);
	if (status != 0) {
		EM_LOGE(TAG, "Failed to create thread with status = %d\n", status);
		return;
	}

}

//void sim_adaptor_state_mc(pthread_t id_app_activity, pthread_t id_talker, pthread_t id_listener) {
//	sim_data.id_app_activity = id_app_activity;
//	sim_data.id_talker = id_talker;
//	sim_data.id_listener = id_listener;
//
//	while (1) {
//		switch (sim_data.state) {
//		case INITIALIZE:
//			break;
//		}
//
//		sleep(1);
//	}
//}
