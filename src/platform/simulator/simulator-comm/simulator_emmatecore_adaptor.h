/*
 * simulator_emmatecore_adaptor.h
 *
 *  Created on: 04-May-2020
 *      Author: Rohan
 */

#ifndef SIMULATOR_EMMATECORE_ADAPTOR_H_
#define SIMULATOR_EMMATECORE_ADAPTOR_H_

#include "simulator_config.h"
#include "core_error.h"

#define SIM_DATA_MAXLEN		SIMCONFIG_SIM_DATA_MAXLEN

typedef enum {
	SIM_GET_SYSTEM_INFO = 10,
	SIM_GET_BOARD_IDS = 11,
	SIM_GET_GPIO_STATE = 20,
	SIM_GET_ADC_VAL = 21,
	SIM_EVENT_GPIO_STATE = 22,
	SIM_GET_SENSOR_VAL_DHT22 = 30,
	SIM_GET_SENSOR_VAL_LDR = 31,
	SIM_GET_SENSOR_VAL_MQ5 = 32,
	SIM_GET_SENSOR_VAL_MQ7 = 33,
	SIM_SET_GPIO_STATE = 50,
	SIM_REBOOT_DEVICE = 60
} SIMUMATOR_COMMANDS;

typedef struct {
	SIMUMATOR_COMMANDS cmd;
	char data[SIM_DATA_MAXLEN];
} SimCmdRequest;

typedef int (*sim_write_data)(char * data);

typedef struct {
	int status;
	EmError error;
	SIMUMATOR_COMMANDS cmd;
	char data[SIM_DATA_MAXLEN];
} SimCmdResponse;

typedef struct {
	SIMUMATOR_COMMANDS cmd;
	sim_write_data write_data;
} EmmateModuleNode;

em_err simulator_make_request_json(char **ppbuf, int *plen, int cmd, char *data);
void simulator_adaptor_init();
int simulator_register_emmate_module(SIMUMATOR_COMMANDS simcmd, sim_write_data write_data);

#endif /* SIMULATOR_EMMATECORE_ADAPTOR_H_ */
