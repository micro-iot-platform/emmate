/*
 * simulator_socket.h
 *
 *  Created on: 04-May-2020
 *      Author: Rohan
 */

#ifndef SOCKET_H_
#define SOCKET_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <pthread.h>
#include <signal.h>
#include <sys/time.h>

#define MAX_UDP_PAYLOAD_LEN 	65507
#define MAX_TALKER_HEADER_LEN	8
#define MAX_SIMULATOR_PAYLOAD_LEN	(MAX_UDP_PAYLOAD_LEN) - (MAX_TALKER_HEADER_LEN)

typedef struct {
	int datalen;						// Data buffer length
	char data[MAX_SIMULATOR_PAYLOAD_LEN];	// Data buffer pointer
} SimCommData;

typedef struct {
	char *server_ip;	// Cmd Line User input
	char *port;			// Cmd Line User input
} SocketInfo;

bool sim_socket_queue_push(SimCommData *pdata);
void* sim_socket_talker(void * arg);
bool sim_socket_queue_pop(SimCommData *pdata);
void* sim_socket_listener(void * arg);

#endif /* SOCKET_H_ */
