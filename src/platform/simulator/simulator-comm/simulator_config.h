/*
 * simulator_config.h
 *
 *  Created on: 04-May-2020
 *      Author: Rohan
 */

#ifndef SIMULATOR_CONFIG_H_
#define SIMULATOR_CONFIG_H_

//#define SOCKET_TALKER			1
#define SIMCONFIG_TALKER_Q_MAXLEN			100
#define SIMCONFIG_LISTENER_Q_MAXLEN			100
#define SIMCONFIG_SIM_DATA_MAXLEN			4096

#endif /* SIMULATOR_CONFIG_H_ */
