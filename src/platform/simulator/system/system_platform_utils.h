/*
 * File Name: system_platform_utils.h
 * Description:
 *
 *  Created on: 31-Aug-2020
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_PLATFORM_UTILS_H_
#define SYSTEM_PLATFORM_UTILS_H_

/**
 * @brief Creating a Microseconds dalay
 *
 */
//#define core_platform_us_delay(delay) \
//	do { \
//	} while ((delay--)*1000 > 0);

void core_platform_us_delay(uint32_t us_delay);

#endif /* SYSTEM_PLATFORM_UTILS_H_ */
