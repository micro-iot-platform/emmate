/*
 * platform_info.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */
#include <string.h>
#include "system_info.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "input_processor.h"
#include "core_utils.h"
#include "threading_platform.h"

#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"

#define TAG	LTAG_SYSTEM_SYSINFO

static HardwareInfo m_hw_info;
static volatile bool m_hw_info_set = false;

static int get_ram_in_kb(void) {
	FILE *meminfo = fopen("/proc/meminfo", "r");
	if (meminfo == NULL) {
		return -1;
	}

	char line[256];
	while (fgets(line, sizeof(line), meminfo)) {
		int ram;
		if (sscanf(line, "MemTotal: %d kB", &ram) == 1) {
			fclose(meminfo);
			return ram;
		}
	}

	// If we got here, then we couldn't find the proper line in the meminfo file:
	// do something appropriate like return an error code, throw an exception, etc.
	fclose(meminfo);
	return -1;
}

static void read_save_hardware_info(HardwareInfo *hw_info) {
	strcpy(hw_info->chip_name, m_hw_info.chip_name);
	strcpy(hw_info->chip_mfgr, m_hw_info.chip_mfgr);
	hw_info->has_wifi = m_hw_info.has_wifi;
	hw_info->has_bt = m_hw_info.has_bt;
	hw_info->has_ble = m_hw_info.has_ble;
	hw_info->has_gsm_2g = m_hw_info.has_gsm_2g;
	hw_info->has_gsm_3g = m_hw_info.has_gsm_3g;
	hw_info->has_gsm_4g = m_hw_info.has_gsm_4g;
	hw_info->has_lora = m_hw_info.has_lora;
	hw_info->has_nbiot = m_hw_info.has_nbiot;
	hw_info->flash_size = m_hw_info.flash_size;
//	hw_info->ram_size = get_ram_in_kb();
	hw_info->ram_size = m_hw_info.ram_size;
}

static void read_save_sdk_info(SDKInfo *sdk_info) {
	strcpy(sdk_info->sdk_name, "EmMate Simulator");
	strcpy(sdk_info->sdk_version, "0.1.0.0");
}

void read_save_platform_info(PlatformInfo *pl_info) {
	m_hw_info_set = false;

	/* Send SIM_GET_SYSTEM_INFO command */
	char *json_buf;
	int json_len;
	simulator_make_request_json(&json_buf, &json_len, SIM_GET_SYSTEM_INFO, NULL);
	EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	while (!m_hw_info_set) {
		TaskPfDelay(50);
	}

	read_save_hardware_info(&pl_info->hw_info);
	read_save_sdk_info(&pl_info->sdk_info);
	m_hw_info_set = false;
}

void print_system_specific_info(PlatformInfo *pl_info) {
	EM_LOGW(TAG, "-----------------------------");
	EM_LOGW(TAG, "Hardware Information");
	EM_LOGW(TAG, "-----------------------------");
	EM_LOGW(TAG, "Chip Name: %s", pl_info->hw_info.chip_name);
	EM_LOGW(TAG, "Chip Manufacturer: %s", pl_info->hw_info.chip_mfgr);
	EM_LOGW(TAG, "Features:");
	EM_LOGW(TAG, "\tWIFI : %s", pl_info->hw_info.has_wifi ? "YES" : "NO");
	EM_LOGW(TAG, "\tBT : %s", pl_info->hw_info.has_bt ? "YES" : "NO");
	EM_LOGW(TAG, "\tBLE : %s", pl_info->hw_info.has_ble ? "YES" : "NO");
	EM_LOGW(TAG, "\tGSM 2G : %s", pl_info->hw_info.has_gsm_2g ? "YES" : "NO");
	EM_LOGW(TAG, "\tGSM 3G : %s", pl_info->hw_info.has_gsm_3g ? "YES" : "NO");
	EM_LOGW(TAG, "\tGSM 4G : %s", pl_info->hw_info.has_gsm_4g ? "YES" : "NO");
	EM_LOGW(TAG, "\tLORA : %s", pl_info->hw_info.has_lora ? "YES" : "NO");
	EM_LOGW(TAG, "\tNBIOT : %s", pl_info->hw_info.has_nbiot ? "YES" : "NO");
	EM_LOGW(TAG, "FLASH SIZE : %d GB", pl_info->hw_info.flash_size);
	EM_LOGW(TAG, "RAM SIZE : %d MB", (pl_info->hw_info.ram_size) / 1024);
	EM_LOGW(TAG, "-----------------------------");
	EM_LOGW(TAG, "SDK Information");
	EM_LOGW(TAG, "-----------------------------");
	EM_LOGW(TAG, "Running SDK : %s", pl_info->sdk_info.sdk_name);
	EM_LOGW(TAG, "SDk Version : %s", pl_info->sdk_info.sdk_version);
}

void system_info_store_hw_info(char *hardware_info) {
	char *json_buff = hardware_info;
	memset(&m_hw_info, 0x00, sizeof(HardwareInfo));

	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "JSON Value type not matched");
		} else {
			rootObj = json_value_get_object(root_value);

			/* get chip_name data */
			ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(m_hw_info.chip_name, "."), m_hw_info.chip_name);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.chip_name, "."));
			}

			/* get chip_mfgr data */
			ret = cpy_json_str_obj(rootObj, GET_VAR_NAME(m_hw_info.chip_mfgr, "."), m_hw_info.chip_mfgr);
			if (ret != EM_OK) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.chip_mfgr, "."));
			}

			/* get has_wifi command */
			m_hw_info.has_wifi = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_wifi, "."));
			if (m_hw_info.has_wifi == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_wifi, "."));
			}

			/* get has_bt command */
			m_hw_info.has_bt = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_bt, "."));
			if (m_hw_info.has_bt == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_bt, "."));
			}

			/* get has_ble command */
			m_hw_info.has_ble = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_ble, "."));
			if (m_hw_info.has_ble == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_ble, "."));
			}

			/* get has_gsm_2g command */
			m_hw_info.has_gsm_2g = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_gsm_2g, "."));
			if (m_hw_info.has_gsm_2g == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_gsm_2g, "."));
			}

			/* get has_gsm_3g command */
			m_hw_info.has_gsm_3g = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_gsm_3g, "."));
			if (m_hw_info.has_gsm_3g == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_gsm_3g, "."));
			}

			/* get has_gsm_4g command */
			m_hw_info.has_gsm_4g = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_gsm_4g, "."));
			if (m_hw_info.has_gsm_4g == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_gsm_4g, "."));
			}

			/* get has_lora command */
			m_hw_info.has_lora = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_lora, "."));
			if (m_hw_info.has_lora == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_lora, "."));
			}

			/* get has_nbiot command */
			m_hw_info.has_nbiot = json_object_get_boolean(rootObj, GET_VAR_NAME(m_hw_info.has_nbiot, "."));
			if (m_hw_info.has_nbiot == -1) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.has_nbiot, "."));
			}

			/* get flash_size command */
			m_hw_info.flash_size = json_object_get_number(rootObj, GET_VAR_NAME(m_hw_info.flash_size, "."));
			if (m_hw_info.flash_size == 0) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.flash_size, "."));
			}

			/* get ram_size command */
			m_hw_info.ram_size = json_object_get_number(rootObj, GET_VAR_NAME(m_hw_info.ram_size, "."));
			if (m_hw_info.ram_size == 0) {
				EM_LOGE(TAG, "Could not parse JSON key %s, ignoring!", GET_VAR_NAME(m_hw_info.ram_size, "."));
			}

			json_value_free(root_value); /* clear root_value */
			m_hw_info_set = true;
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
	}
}
