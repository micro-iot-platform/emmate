/*
 * File Name: system_platform.c
 * Description:
 *
 *  Created on: 04-May-2019
 *      Author: Rohan Dey
 */

#include "system_platform.h"
#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"
#include "core_error.h"
#include "core_logger.h"

#include "system_info.h"
#include "board_ids.h"
#include "gpio_platform.h"
#if CONFIG_USE_ADC
#include "adc_platform.h"
#endif
#if CONFIG_USE_DHT22
#include "dht22_sim.h"
#endif

#define TAG	"sys_plat"

/* Callback function definitions */
static int write_system_info(char *data) {
	system_info_store_hw_info(data);
	return 0;
}

static int write_board_ids(char *data) {
	board_ids_store(data);
	return 0;
}

static int write_gpio_state(char *data) {
	gpio_platform_get_state(data);
	return 0;
}

static int write_adc_val(char *data) {
#if CONFIG_USE_ADC
	adc_platform_get_val(data);
#endif
	return 0;
}

static int write_gpio_event(char *data) {
	gpio_platform_gpio_event(data);
	return 0;
}

static int write_sensor_val_dht22(char *data) {
#if CONFIG_USE_DHT22
	dht22_sim_get_val(data);
#endif
	return 0;
}

static int write_sensor_val_ldr(char *data) {
	return 0;
}

static int write_sensor_val_mq5(char *data) {
	return 0;
}

static int write_sensor_val_mq7(char *data) {
	return 0;
}

static void register_simcomm_callbacks() {
	simulator_register_emmate_module(SIM_GET_SYSTEM_INFO, write_system_info);
	simulator_register_emmate_module(SIM_GET_BOARD_IDS, write_board_ids);
	simulator_register_emmate_module(SIM_GET_GPIO_STATE, write_gpio_state);
	simulator_register_emmate_module(SIM_GET_ADC_VAL, write_adc_val);
	simulator_register_emmate_module(SIM_EVENT_GPIO_STATE, write_gpio_event);
	simulator_register_emmate_module(SIM_GET_SENSOR_VAL_DHT22, write_sensor_val_dht22);
	simulator_register_emmate_module(SIM_GET_SENSOR_VAL_LDR, write_sensor_val_ldr);
	simulator_register_emmate_module(SIM_GET_SENSOR_VAL_MQ5, write_sensor_val_mq5);
	simulator_register_emmate_module(SIM_GET_SENSOR_VAL_MQ7, write_sensor_val_mq7);
}

void init_system_platform() {
	em_err ret = EM_FAIL;

	/* Register all callbacks with the Simulator Communications Module */
	register_simcomm_callbacks();
}

void core_platform_restart() {
	/* Send SIM_REBOOT_DEVICE command */
	char *json_buf;
	int json_len;
	simulator_make_request_json(&json_buf, &json_len, SIM_REBOOT_DEVICE, NULL);
	EM_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);
}

void core_platform_us_delay(uint32_t us_delay) {
	/* Dummy delay routine */
	do {
	} while ((us_delay--) * 1000 > 0);
}
