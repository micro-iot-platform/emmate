/*
 * dht22_sim.h
 *
 *  Created on: 31-Aug-2020
 *      Author: Rohan Dey
 */

#ifndef DHT22_SIM_H_
#define DHT22_SIM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"

em_err dht22_sim_set_gpio(int dout_gpio);
void dht22_sim_get_val(char *dht22_val);
int dht22_sim_read(float *p_hum, float *p_temp);

#ifdef __cplusplus
}
#endif

#endif /* DHT22_SIM_H_ */
