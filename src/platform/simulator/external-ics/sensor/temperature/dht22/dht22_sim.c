/*
 * dht22_sim.c
 *
 *  Created on: 31-Aug-2020
 *      Author: Rohan Dey
 */

#include "dht22_sim.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include "input_processor.h"
#include "core_utils.h"
#include "threading_platform.h"

#include "simulator_emmatecore_adaptor.h"
#include "simulator_socket.h"

#define TAG	"dht22_sim"

static bool m_state = false;
static float m_humidity = 0;
static float m_temperature = 0;
static volatile bool m_cmd_complete = false;

em_err dht22_sim_set_gpio(int dout_gpio) {
	return EM_OK;
}

void dht22_sim_get_val(char *dht22_val) {
	char *json_buff = dht22_val;
	em_err ret = EM_FAIL;

	JSON_Value *root_value = NULL;
	JSON_Object *rootObj = NULL;

	root_value = json_parse_string(json_buff);
	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "Invalid json object");
			ret = EM_FAIL;
		} else {
			rootObj = json_value_get_object(root_value);
			m_state = json_object_get_boolean(rootObj, "state");
			if (m_state) {
				m_temperature = json_object_get_number(rootObj, "temp");
				m_humidity = json_object_get_number(rootObj, "humid");
			} else {
				m_temperature = 0;
				m_humidity = 0;
			}

			json_value_free(root_value); /* clear root_value */
			m_cmd_complete = true;
		}
	} else {
		EM_LOGE(TAG, "Could not create JSON root object");
		ret = EM_FAIL;
	}
}

int dht22_sim_read(float *p_hum, float *p_temp) {
	m_cmd_complete = false;
	m_state = false;
	m_humidity = 0;
	m_temperature = 0;

	/* Send SIM_GET_SENSOR_VAL_DHT22 command */
	char *json_buf;
	int json_len;
	simulator_make_request_json(&json_buf, &json_len, SIM_GET_SENSOR_VAL_DHT22, NULL);
	EM_LOGD(TAG, "Request: %.*s", json_len, json_buf);

	SimCommData sysinfo_cmd;
	memset(&sysinfo_cmd, 0x00, sizeof(SimCommData));
	sysinfo_cmd.datalen = json_len;
	memcpy(sysinfo_cmd.data, json_buf, json_len);
	free(json_buf);
	sim_socket_queue_push(&sysinfo_cmd);

	int time = 0;
	while (!m_cmd_complete) {
		if (++time > 500) {
			m_state = false;
			break;
		}
		TaskPfDelay(10);
	}

	*p_hum = m_humidity;
	*p_temp = m_temperature;

	if (m_state) {
		return EM_OK;
	} else {
		return EM_FAIL;
	}
}
