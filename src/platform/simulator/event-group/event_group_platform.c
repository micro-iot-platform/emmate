/*
 * File Name: event_group_platform.c
 * File Path: /emmate/src/platform/esp/event-group/event_group_platform.c
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>

#include "gll.h"
#include "event_group_platform.h"
#include "core_logger.h"

#define TAG "threading"

typedef struct {
	EventGroupPfHandle hdl;
	EventPfBits flags;
	pthread_cond_t cond;
	pthread_mutex_t mutex;
} EventData;

static gll_t *m_evt_ll = NULL;
static pthread_mutex_t m_emutex;

EventGroupPfHandle event_group_ptf_create() {
	if (m_evt_ll == NULL) {
		m_evt_ll = gll_init();
		pthread_mutex_init(&m_emutex, NULL);
	}

	EventData *evt = (EventData*) calloc(1, sizeof(EventData));
	if (evt == NULL) {
		EM_LOGE(TAG, "calloc failed %s, %d", __FUNCTION__, __LINE__);
		return 0;
	}

	memset(evt, 0x00, sizeof(EventData));

	/* Handle increases by 1 every time */
	evt->hdl += 1;
	evt->flags = 0;
	pthread_cond_init(&evt->cond, NULL);
	pthread_mutex_init(&evt->mutex, NULL);

	/* Add a m_evt_ll node */
	int ret = -1;

	pthread_mutex_lock(&m_emutex);
	ret = gll_pushBack(m_evt_ll, evt);
	pthread_mutex_unlock(&m_emutex);

	if (ret == -1) {
		EM_LOGE(TAG, "gll_pushBack failed %s, %d", __FUNCTION__, __LINE__);
		free(evt);
		return 0;
	}

	return evt->hdl;
}

static int search_event_grp(EventGroupPfHandle event_grp_hdl, EventData **pedata, int *pos, gll_t *ll) {
	int ll_size = ll->size;
	int ll_pos = 0;
	bool found = false;
	EventData *edata = NULL;

	pthread_mutex_lock(&m_emutex);
	for (ll_pos = 0; ll_pos < ll_size; ll_pos++) {
		edata = (EventData*) gll_get(ll, ll_pos);
		if (edata != NULL) {
			if (edata->hdl == event_grp_hdl) {
//				EM_LOGD(TAG, "EventData matched");
				found = true;
				break;
			}
		}
	}
	pthread_mutex_unlock(&m_emutex);

	if (found) {
		*pedata = edata;
		*pos = ll_pos;
		return 0;
	} else {
		*pedata = NULL;
		return -1;
	}
}

EventPfBits event_group_ptf_wait_bits(EventGroupPfHandle eventgroup, const EventPfBits bitstowaitfor,
		const int clearonexit, const int waitforallbits, TickType_pf tickstowait) {
	if (m_evt_ll == NULL) {
		EM_LOGE(TAG, "Event list not initialized");
		return 0;
	}

	/* Search and get the event from the list */
	EventData *evt = NULL;
	int pos = -1;
	int search_result = search_event_grp(eventgroup, &evt, &pos, m_evt_ll);

	/* If the EventData was found then process */
	if (search_result == 0) {
		EventPfBits flags = 0;
		int timedout = 0;

		struct timespec wait_time = { 0, 0 };
		if (tickstowait != EventPfMaxDelay) {
			/* Set the absolute time for timedwait */
			clock_gettime(CLOCK_REALTIME, &wait_time);
			wait_time.tv_sec += (tickstowait / 1000);
			wait_time.tv_nsec += (tickstowait % 1000) * 1000000;
		}

		/* Check the flags once before we go into a wait */
		pthread_mutex_lock(&evt->mutex);
		flags = evt->flags;
		pthread_mutex_unlock(&evt->mutex);
		if (waitforallbits) {
			/* Check if all the bits are set or not */
			if ((flags & bitstowaitfor) == bitstowaitfor) {
				goto exit_wait;
			}
		} else {
			/* Check if any bit is set or not */
			if ((flags & bitstowaitfor) != 0) {
				goto exit_wait;
			}
		}

		/* Go into a wait state and check the flags */
		while (1) {
			/* Take a lock */
			pthread_mutex_lock(&evt->mutex);

			/* Wait on condition */
			if (tickstowait == EventPfMaxDelay) {
				pthread_cond_wait(&evt->cond, &evt->mutex);
			} else {
				int ret = pthread_cond_timedwait(&evt->cond, &evt->mutex, &wait_time);
				if (ret == ETIMEDOUT) {
					timedout = 1;
				}
			}

			/* Copy the flags to a local variable */
			flags = evt->flags;

			/* Unlock the mutex as we are going to check for the flag statuses */
			pthread_mutex_unlock(&evt->mutex);

			if (!timedout) {
				if (waitforallbits) {
					/* Check if all the bits are set or not */
					if ((flags & bitstowaitfor) == bitstowaitfor) {
						break;
					}
				} else {
					/* Check if any bit is set or not */
					if ((flags & bitstowaitfor) != 0) {
						break;
					}
				}
			} else {
				/* The time has expired, so we break from the loop */
				break;
			}
		}

exit_wait:
		if (!timedout && clearonexit) {
			/* clear the bits that were set */
			EventPfBits bits_to_clear = (~bitstowaitfor);
			pthread_mutex_lock(&evt->mutex);
			evt->flags &= bits_to_clear;
			pthread_mutex_unlock(&evt->mutex);
//		EM_LOGD(TAG, "clear on exit: %d", evt->flags);
		}
		return flags;
	} else {
		EM_LOGE(TAG, "Invalid Event Handle passed %s, %d", __FUNCTION__, __LINE__);
		return 0;
	}
}

EventPfBits event_group_ptf_set_bits(EventGroupPfHandle eventgroup, const EventPfBits bitstoset) {
	if (m_evt_ll == NULL) {
		EM_LOGE(TAG, "Event list not initialized");
		return 0;
	}

	/* Search and get the event from the list */
	EventData *evt = NULL;
	int pos = -1;
	int search_result = search_event_grp(eventgroup, &evt, &pos, m_evt_ll);

	/* If the EventData was found then process */
	if (search_result == 0) {
		EventPfBits flags = 0;

		/* Set the flags and Signal the condition variable */
		pthread_mutex_lock(&evt->mutex);
		evt->flags |= bitstoset;
		flags = evt->flags;
		pthread_cond_broadcast(&evt->cond);
		pthread_mutex_unlock(&evt->mutex);

		return flags;
	} else {
		EM_LOGE(TAG, "Invalid Event Handle passed %s, %d", __FUNCTION__, __LINE__);
		return 0;
	}
}

EventPfBits event_group_ptf_clear_bits(EventGroupPfHandle eventgroup, const EventPfBits bitstoclear) {
	if (m_evt_ll == NULL) {
		EM_LOGE(TAG, "Event list not initialized");
		return 0;
	}

	/* Search and get the event from the list */
	EventData *evt = NULL;
	int pos = -1;
	int search_result = search_event_grp(eventgroup, &evt, &pos, m_evt_ll);

	/* If the EventData was found then process */
	if (search_result == 0) {

		EventPfBits clear = (~bitstoclear);
		EventPfBits flags;

		pthread_mutex_lock(&evt->mutex);
		flags = evt->flags;
		evt->flags &= clear;
		pthread_mutex_unlock(&evt->mutex);

		return flags;
	} else {
		EM_LOGE(TAG, "Invalid Event Handle passed %s, %d", __FUNCTION__, __LINE__);
		return 0;
	}
}

EventPfBits event_group_ptf_get_bits(EventGroupPfHandle eventgroup) {
	if (m_evt_ll == NULL) {
		EM_LOGE(TAG, "Event list not initialized");
		return 0;
	}

	/* Search and get the event from the list */
	EventData *evt = NULL;
	int pos = -1;
	int search_result = search_event_grp(eventgroup, &evt, &pos, m_evt_ll);

	/* If the EventData was found then process */
	if (search_result == 0) {
		EventPfBits flags;

		pthread_mutex_lock(&evt->mutex);
		flags = evt->flags;
		pthread_mutex_unlock(&evt->mutex);

		return flags;
	} else {
		EM_LOGE(TAG, "Invalid Event Handle passed %s, %d", __FUNCTION__, __LINE__);
		return 0;
	}
}

void event_group_ptf_delete(EventGroupPfHandle eventgroup) {
	if (m_evt_ll == NULL) {
		EM_LOGE(TAG, "Event list not initialized");
		return;
	}

	/* Search and get the event from the list */
	EventData *evt = NULL;
	int pos = -1;
	int search_result = search_event_grp(eventgroup, &evt, &pos, m_evt_ll);

	/* If the EventData was found then process */
	if (search_result == 0) {

		/* Set all flags and broadcast the condition variable to unblock and waiting threads */
		pthread_mutex_lock(&evt->mutex);
		evt->flags = 0xFFFFFFFF;
		pthread_cond_broadcast(&evt->cond);
		pthread_mutex_unlock(&evt->mutex);

		/* Wait for some time  for the unblocking to happen */
		sleep(1);

		/* Destroy the condition and mutex variables */
		pthread_cond_destroy(&evt->cond);
		pthread_mutex_destroy(&evt->mutex);

		/* Remove the EventData from the main list */
		pthread_mutex_lock(&m_emutex);
		EventData *evt_data = (EventData*) gll_remove(m_evt_ll, pos);
		pthread_mutex_unlock(&m_emutex);

		/* Free the EventData object */
		free(evt_data);

		/* If there are no more nodes in the main list, then destroy the list */
		if (m_evt_ll->size <= 0) {
			pthread_mutex_lock(&m_emutex);
			gll_destroy(m_evt_ll);
			m_evt_ll = NULL;
			pthread_mutex_unlock(&m_emutex);

			/* Destroy the main mutex */
			pthread_mutex_destroy(&m_emutex);
		}
	} else {
		EM_LOGE(TAG, "Invalid Event Handle passed %s, %d", __FUNCTION__, __LINE__);
		return;
	}
}

/* STUBS */

EventPfBits event_group_ptf_sync(EventGroupPfHandle eventgroup, const EventPfBits bitstoset,
		const EventPfBits bitstowaitfor, TickType_pf tickstowait) {
	EventPfBits flags = 0;
	return flags;
}

int event_group_ptf_set_bits_from_isr(EventGroupPfHandle eventgroup, EventPfBits bitstoset, int higherprioritytaskwoken) {
	return 0;
}

EventPfBits event_group_ptf_clear_bits_from_isr(EventGroupPfHandle eventgroup, const EventPfBits bitstoclear) {
	EventPfBits flags = 0;
	return flags;
}

EventPfBits event_group_ptf_get_bits_from_isr(EventGroupPfHandle eventgroup) {
	EventPfBits flags = 0;
	return flags;
}

