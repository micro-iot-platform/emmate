/*
 * File Name: event_group_platform.h
 * File Path: /emmate/src/platform/simulator/event-group/event_group_platform.h
 * Description:
 *
 *  Created on: 30-May-2020
 *      Author: Rohan Dey
 */

#ifndef EVENT_GROUP_PLATFORM_H_
#define EVENT_GROUP_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#define EventPfMaxDelay		0xffffffffUL
typedef uint64_t TickType_pf;
typedef unsigned int EventPfBits;
typedef unsigned int EventGroupPfHandle;

#define EVT_PTF_BIT31 	0x80000000
#define EVT_PTF_BIT30	0x40000000
#define EVT_PTF_BIT29	0x20000000
#define EVT_PTF_BIT28	0x10000000
#define EVT_PTF_BIT27	0x08000000
#define EVT_PTF_BIT26	0x04000000
#define EVT_PTF_BIT25	0x02000000
#define EVT_PTF_BIT24	0x01000000
#define EVT_PTF_BIT23	0x00800000
#define EVT_PTF_BIT22	0x00400000
#define EVT_PTF_BIT21	0x00200000
#define EVT_PTF_BIT20	0x00100000
#define EVT_PTF_BIT19	0x00080000
#define EVT_PTF_BIT18	0x00040000
#define EVT_PTF_BIT17	0x00020000
#define EVT_PTF_BIT16	0x00010000
#define EVT_PTF_BIT15	0x00008000
#define EVT_PTF_BIT14	0x00004000
#define EVT_PTF_BIT13	0x00002000
#define EVT_PTF_BIT12	0x00001000
#define EVT_PTF_BIT11	0x00000800
#define EVT_PTF_BIT10	0x00000400
#define EVT_PTF_BIT9 	0x00000200
#define EVT_PTF_BIT8	0x00000100
#define EVT_PTF_BIT7	0x00000080
#define EVT_PTF_BIT6	0x00000040
#define EVT_PTF_BIT5	0x00000020
#define EVT_PTF_BIT4	0x00000010
#define EVT_PTF_BIT3 	0x00000008
#define EVT_PTF_BIT2 	0x00000004
#define EVT_PTF_BIT1 	0x00000002
#define EVT_PTF_BIT0 	0x00000001

EventGroupPfHandle event_group_ptf_create();

EventPfBits event_group_ptf_wait_bits(EventGroupPfHandle eventgroup, const EventPfBits bitstowaitfor,
		const int clearonexit, const int waitforallbits, TickType_pf tickstowait);

EventPfBits event_group_ptf_set_bits(EventGroupPfHandle eventgroup, const EventPfBits bitstoset);

EventPfBits event_group_ptf_clear_bits(EventGroupPfHandle eventgroup, const EventPfBits bitstoclear);

EventPfBits event_group_ptf_get_bits(EventGroupPfHandle eventgroup);

void event_group_ptf_delete(EventGroupPfHandle eventgroup);


/* STUBS */

EventPfBits event_group_ptf_sync(EventGroupPfHandle eventgroup, const EventPfBits bitstoset,
		const EventPfBits bitstowaitfor, TickType_pf tickstowait);

int event_group_ptf_set_bits_from_isr(EventGroupPfHandle eventgroup, EventPfBits bitstoset, int higherprioritytaskwoken);

EventPfBits event_group_ptf_clear_bits_from_isr(EventGroupPfHandle eventgroup, const EventPfBits bitstoclear);

EventPfBits event_group_ptf_get_bits_from_isr(EventGroupPfHandle eventgroup);

#ifdef __cplusplus
}
#endif

#endif /* EVENT_GROUP_PLATFORM_H_ */
