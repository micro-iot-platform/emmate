/*
 * logging.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef PLATFORM_LOG_H_
#define PLATFORM_LOG_H_

#include <stdint.h>
#include <stdarg.h>
#include "core_config.h"

#define LOG_USE_COLOR 1
typedef int (*vprintf_like_t)(const char *, va_list);

/* LOG LEVELS */
#define PF_LOG_LOCAL_LEVEL		LOG_LOCAL_LEVEL

#define PF_LOG_VERBOSE	0
#define PF_LOG_DEBUG	1
#define PF_LOG_INFO		2
#define PF_LOG_WARN		3
#define PF_LOG_ERROR	4
#define PF_LOG_FATAL	5
#define PF_LOG_NONE		6

void pf_log_level_set(const char *tag, int level);
vprintf_like_t pf_log_redirect(vprintf_like_t func);
uint32_t pf_log_timestamp(void);
uint32_t pf_log_early_timestamp(void);
void pf_log_init();

#define PF_LOGV( tag, format, ... )	log_log(tag, PF_LOG_VERBOSE, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define PF_LOGD( tag, format, ... )	log_log(tag, PF_LOG_DEBUG, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define PF_LOGI( tag, format, ... )	log_log(tag, PF_LOG_INFO, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define PF_LOGW( tag, format, ... )	log_log(tag, PF_LOG_WARN, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define PF_LOGE( tag, format, ... )	log_log(tag, PF_LOG_ERROR, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define PF_LOGF( tag, format, ... )	log_log(tag, PF_LOG_FATAL, __FILE__, __LINE__, format, ##__VA_ARGS__)

void log_log(const char* tag, int level, const char *file, int line, const char *fmt, ...);


/* Functions below this line are stubs */
#define PF_LOG_BUFFER_HEX_LEVEL(tag, buffer, buff_len, level) \
	    do {\
	    } while(0)

#define PF_LOG_BUFFER_CHAR_LEVEL(tag, buffer, buff_len, level)	\
	    do {\
	    } while(0)

#define PF_LOG_BUFFER_HEXDUMP(tag, buffer, buff_len, level)	\
	    do {\
	    } while(0)

#define PF_LOG_BUFFER_HEX(tag, buffer, buff_len)	\
	    do {\
	    } while(0)

#define PF_LOG_BUFFER_CHAR(tag, buffer, buff_len)	\
	    do {\
	    } while(0)

/* macro to output logs in startup code, before heap allocator and syscalls have been initialized */
#define PF_EARLY_LOGE(tag, format, ...)	\
	    do {\
	    } while(0)

#define PF_EARLY_LOGW(tag, format, ...)	\
	    do {\
	    } while(0)

#define PF_EARLY_LOGI(tag, format, ...)	\
	    do {\
	    } while(0)

#define PF_EARLY_LOGD(tag, format, ...)	\
	    do {\
	    } while(0)

#define PF_EARLY_LOGV(tag, format, ...)	\
	    do {\
	    } while(0)

#define PF_LOG_LEVEL(level, tag, format, ...)	\
	    do {\
	    } while(0)

/* runtime macro to output logs at a specified level. */
#define PF_LOG_LEVEL_LOCAL(level, tag, format, ...)	\
	    do {\
	    } while(0)

#endif /* PLATFORM_LOG_H_ */
