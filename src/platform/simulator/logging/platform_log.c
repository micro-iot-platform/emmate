/*
 * logging.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include "platform_log.h"

typedef void (*log_LockFn)(void *udata, int lock);

static struct {
	void *udata;
	log_LockFn lock;
	FILE *fp;
	int level;
	int quiet;
	pthread_mutex_t log_mutex;
} log_data;

static uint32_t id = 0;

static const char *level_names[] = { "V", "D", "I", "W", "E", "F" };

#ifdef LOG_USE_COLOR
static const char *level_colors[] = { "\x1b[94m", "\x1b[36m", "\x1b[32m", "\x1b[33m", "\x1b[31m", "\x1b[35m" };
#endif

static void lock(void) {
//	if (log_data.lock) {
//		log_data.lock(log_data.udata, 1);
//	}
	pthread_mutex_lock(&log_data.log_mutex);
}

static void unlock(void) {
//	if (log_data.lock) {
//		log_data.lock(log_data.udata, 0);
//	}
	pthread_mutex_unlock(&log_data.log_mutex);
}

static void log_set_quiet(int enable) {
	log_data.quiet = enable ? 1 : 0;
}

void pf_log_level_set(const char *tag, int level) {
	if (level == PF_LOG_NONE) {
		log_set_quiet(1);
		return;
	} else {
		log_set_quiet(0);
	}
	log_data.level = level;
}

void log_log(const char *tag, int level, const char *file, int line, const char *fmt, ...) {
	if (level < log_data.level) {
		return;
	}

	/* Acquire lock */
	lock();

	/* Get current time */
#if TIME_IN_LOG
	time_t t = time(NULL);
	struct tm *lt = localtime(&t);
#endif
	/* Log to stdout */
	if (!log_data.quiet) {
		va_list args;
#if TIME_IN_LOG
		char buf[16];
		buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
#endif

#ifdef LOG_USE_COLOR
		fprintf(stdout, "%s%s (%d) %s: ", level_colors[level], level_names[level], ++id, tag);
#else
		fprintf(stdout, "%s (%d) %s: ", level_names[level], ++id, tag);
#endif
		va_start(args, fmt);
		vfprintf(stdout, fmt, args);
		va_end(args);
#ifdef LOG_USE_COLOR
		fprintf(stdout, "\x1b[0m\n");
#else
		fprintf(stdout, "\n");
#endif
		fflush(stdout);
	}
#if 0
	/* Log to file */
	if (log_data.fp) {
		va_list args;
#if TIME_IN_LOG
		char buf[32];
		buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", lt)] = '\0';
#endif
		fprintf(log_data.fp, "%s (%d) %s: ", level_names[level], ++id, tag);

		va_start(args, fmt);
		vfprintf(log_data.fp, fmt, args);
		va_end(args);
		fprintf(log_data.fp, "\n");
		fflush(log_data.fp);
	}
#endif
	/* Release lock */
	unlock();
}

vprintf_like_t pf_log_redirect(vprintf_like_t func) {
	/* Not implemented */
	return NULL;
}

uint32_t pf_log_timestamp(void) {
	/* Not implemented */
	return 0;
}

uint32_t pf_log_early_timestamp(void) {
	/* Not implemented */
	return 0;
}

void pf_log_init() {
	pthread_mutex_init(&log_data.log_mutex, NULL);
}
