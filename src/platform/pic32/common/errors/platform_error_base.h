/*
 * File Name: platform_error_base.h
 * File Path: /emmate/src/platform/pic32/common/errors/platform_error_base.h
 * Description:
 *
 *  Created on: 05-May-2019
 *      Author: Rohan Dey
 */

#ifndef PLATFORM_ERROR_BASE_H_
#define PLATFORM_ERROR_BASE_H_

#include "esp_err.h"

#define PLAT_ERR_WIFI_BASE			ESP_ERR_WIFI_BASE
#define PLAT_ERR_MESH_BASE			ESP_ERR_MESH_BASE
#define PLAT_ERR_NVS_BASE			ESP_ERR_NVS_BASE
#define PLAT_TCPIP_ADAPTER_BASE		ESP_ERR_TCPIP_ADAPTER_BASE

#endif /* PLATFORM_ERROR_BASE_H_ */
