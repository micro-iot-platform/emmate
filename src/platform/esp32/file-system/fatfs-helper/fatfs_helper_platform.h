/*
 * fatfs_helper_platform.h
 *
 *  Created on: 03-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef FATFS_HELPER_PLATFORM_H_
#define FATFS_HELPER_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "esp_vfs_fat.h"

typedef esp_vfs_fat_sdmmc_mount_config_t	fat_sdmmc_mount_config_pf_t;

/**
 * @brief Convenience function to get FAT filesystem on SD card registered in VFS
 *
 * This is an all-in-one function which does the following:
 * - initializes SDMMC driver or SPI driver with configuration in host_config
 * - initializes SD card with configuration in slot_config
 * - mounts FAT partition on SD card using FATFS library, with configuration in mount_config
 * - registers FATFS library with VFS, with prefix given by base_prefix variable
 *
 * This function is intended to make example code more compact.
 * For real world applications, developers should implement the logic of
 * probing SD card, locating and mounting partition, and registering FATFS in VFS,
 * with proper error checking and handling of exceptional conditions.
 *
 * @param base_path     path where partition should be registered (e.g. "/sdcard")
 * @param host_config   Pointer to structure describing SDMMC host. When using
 *                      SDMMC peripheral, this structure can be initialized using
 *                      SDMMC_HOST_DEFAULT() macro. When using SPI peripheral,
 *                      this structure can be initialized using SDSPI_HOST_DEFAULT()
 *                      macro.
 * @param slot_config   Pointer to structure with slot configuration.
 *                      For SDMMC peripheral, pass a pointer to sdmmc_slot_config_t
 *                      structure initialized using SDMMC_SLOT_CONFIG_DEFAULT.
 *                      For SPI peripheral, pass a pointer to sdspi_slot_config_t
 *                      structure initialized using SDSPI_SLOT_CONFIG_DEFAULT.
 * @param mount_config  pointer to structure with extra parameters for mounting FATFS
 * @param[out] out_card  if not NULL, pointer to the card information structure will be returned via this argument
 * @return
 *      - EM_OK on success
 *      - EM_ERR_INVALID_STATE if esp_vfs_fat_sdmmc_mount was already called
 *      - EM_ERR_NO_MEM if memory can not be allocated
 *      - EM_FAIL if partition can not be mounted
 *      - other error codes from SDMMC or SPI drivers, SDMMC protocol, or FATFS drivers
 */
#define fat_sdmmc_mount_pf(base_path, host_config, slot_config, mount_config, out_card) \
										esp_vfs_fat_sdmmc_mount( \
										/*const char* */base_path, \
										/*const sdmmc_host_t* */host_config, \
										/*const void* */slot_config, \
										/*const esp_vfs_fat_mount_config_t* */mount_config, \
										/*sdmmc_card_t** */out_card)

/**
 * @brief Unmount FAT filesystem and release resources acquired using fat_sdmmc_mount_pf
 *
 * @return
 *      - EM_OK on success
 *      - EM_ERR_INVALID_STATE if fat_sdmmc_mount_pf hasn't been called
 */
#define fat_sdmmc_unmount_pf()	esp_vfs_fat_sdmmc_unmount()

#ifdef __cplusplus
}
#endif

#endif /* FATFS_HELPER_PLATFORM_H_ */
