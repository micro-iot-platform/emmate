/*
 * conn_platform.c
 *
 *  Created on: 05-Jun-2020
 *      Author: Rohan Dey
 */

#include "conn_platform.h"

em_err conn_global_init() {
	return EM_OK;
}

em_err conn_get_local_ip(char *ip) {
	return EM_OK;
}

em_err conn_get_public_ip(char *ip) {
	return EM_OK;
}
