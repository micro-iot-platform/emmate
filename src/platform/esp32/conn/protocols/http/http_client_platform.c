/*
 * File Name: http_platform.c
 * File Path: /emmate/src/platform/esp/conn-proto/http-client/http_client_platform.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include "http_client_platform.h"
#include "threading.h"
#include "core_constant.h"

#include <string.h>
#include <stdlib.h>
#include "core_logger.h"

#define TAG "http_client_platform"

