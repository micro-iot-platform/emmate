/*
 * conn_platform.h
 *
 *  Created on: 05-Jun-2020
 *      Author: Rohan Dey
 */

#ifndef CONN_PLATFORM_H_
#define CONN_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_error.h"

em_err conn_global_init();

em_err conn_get_local_ip(char *ip);

em_err conn_get_public_ip(char *ip);

#ifdef __cplusplus
}
#endif

#endif /* CONN_PLATFORM_H_ */
