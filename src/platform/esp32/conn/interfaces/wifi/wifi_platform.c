/*
 * File Name: wifi_platform.c
 * File Path: /emmate/src/platform/esp/conn/wifi/wifi_platform.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */
#include <string.h>

#include "wifi_platform.h"
#include "wifi_platform_errors.h"
#include "wifi_constants.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG	LTAG_CONN_WIFIPLAT

static WifiPlatformHelper wifi_ph;
static char wifi_sta_ip[WIFI_STA_IP_LEN+1]={0};
static uint8_t wifi_curr_evnt = WIFI_PLAT_EVENT_STA_DISCONNECTED;

/* Static function declarations */
static em_err init_wifi_platform_sta_mode();
static em_err init_wifi_platform_event_loop(WiFiEvents handler);

static em_err init_wifi_platform_sta_mode() {

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	if (esp_wifi_init(&cfg) != EM_OK) {
		EM_LOGE(TAG, "error: esp_wifi_init failed! %s, %d", (char*) __FILE__, __LINE__);
		return EM_FAIL;
	}

	if (esp_wifi_set_mode(WIFI_MODE_STA) != EM_OK) {
		return EM_FAIL;
	}

	return EM_OK;
}

static em_err init_wifi_platform_event_loop(WiFiEvents handler) {
	return esp_event_loop_init(handler, NULL);
}

static em_err wifi_event_handler(void *ctx, system_event_t *event) {
	wifi_curr_evnt=event->event_id;
	switch (event->event_id) {
	case WIFI_PLAT_EVENT_STA_START:
		bzero(wifi_sta_ip, sizeof(wifi_sta_ip));
//		printf("WIFI_PLAT_EVENT_STA_START\r\n");
//		connect_wifi_platform();
		break;
	case WIFI_PLAT_EVENT_STA_GOT_IP:
		bzero(wifi_sta_ip, sizeof(wifi_sta_ip));
		sprintf(wifi_sta_ip, "%s",  ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
		EM_LOGI(TAG, "got ip:%s", wifi_sta_ip);
//		s_retry_num = 0;
		break;
	case WIFI_PLAT_EVENT_STA_DISCONNECTED: {
		bzero(wifi_sta_ip, sizeof(wifi_sta_ip));
//		printf("WIFI_PLAT_EVENT_STA_DISCONNECTED\r\n");
//		connect_wifi_platform();
//		if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
//			esp_wifi_connect();
//			xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
//			s_retry_num++;
//			EM_LOGI(TAG, "retry to connect to the AP");
//		}
//		EM_LOGI(TAG, "connect to the AP fail\n");
		break;
	}
	default:
		break;
	}

	/* Call the upper layer's callback to do system specific tasks */
	wifi_ph.event_handler(event->event_id);

	return EM_OK;
}

em_err set_wifi_platform_configs(char* ssid, char* password, ESP_BOARD_INTERFACE esp_wifi_if_type) {

	wifi_config_t wifi_config = {
			.sta = {
					.ssid = "",
					.password = "",
					.scan_method = WIFI_FAST_SCAN,
					.sort_method = WIFI_CONNECT_AP_BY_SIGNAL,
			}, };

	memcpy((char *) wifi_config.sta.ssid, ssid, strnlen(ssid, sizeof(wifi_config.sta.ssid)));
	memcpy((char *) wifi_config.sta.password, password, strnlen(password, sizeof(wifi_config.sta.password)));

	/* Configure WiFi station with host credentials */
	esp_err_t err = esp_wifi_set_config(esp_wifi_if_type, &wifi_config);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to set WiFi configuration (%s)", esp_err_to_name(err));
		return EM_FAIL;
	}

	return EM_OK;
}

void connect_wifi_platform() {
	em_err ret = esp_wifi_connect();
	switch (ret) {
	case PLAT_ERR_WIFI_NOT_INIT:
		EM_LOGE(TAG, "Attempting to connect, but wi-fi is not initialized");
		break;
	case PLAT_ERR_WIFI_NOT_STARTED:
		EM_LOGE(TAG, "Attempting to connect, but wi-fi is not started yet");
		break;
	case PLAT_ERR_WIFI_CONN:
		EM_LOGE(TAG, "WiFi internal error, station or soft-AP control block wrong");
		break;
	case PLAT_ERR_WIFI_SSID:
		EM_LOGE(TAG, "SSID of AP which station connects is invalid");
		break;
	}
}

em_err start_wifi_platform() {
	em_err ret = esp_wifi_start();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "error: esp_wifi_start. err = %d", ret);
		return ret;
	}
	return EM_OK;
}

em_err init_wifi_platform(WifiPlatformModes wifi_mode, WifiEventHandler handler, char* ssid, char* pwd) {

	em_err ret = EM_FAIL;

	wifi_ph.wifi_mode = wifi_mode;
	wifi_ph.event_handler = handler;

	/* Initialize the main event loop callback */
	ret = init_wifi_platform_event_loop(wifi_event_handler);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "init_wifi_platform_event_loop failed. err = %d", ret);
		return ret;
	}

	/* Initialize the TCPIP stack */
	tcpip_adapter_init();

	/* Initialize the underlying wifi module based on the selected wifi mode */
	if (wifi_ph.wifi_mode == WIFI_PLATFORM_MODE_STA) {
		ret = init_wifi_platform_sta_mode();
		if (ret != EM_OK) {
			EM_LOGE(TAG, "init_wifi_sta_mode failed. err = %d", ret);
			return ret;
		}

		ret = set_wifi_platform_configs(ssid, pwd, ESP_BOARD_IF_WIFI_STA);
		if (ret != EM_OK) {
			EM_LOGE(TAG, "set_wifi_platform_configs failed. err = %d", ret);
			return ret;
		}

	} else if (wifi_ph.wifi_mode == WIFI_PLATFORM_MODE_AP) {

	} else if (wifi_ph.wifi_mode == WIFI_PLATFORM_MODE_APSTA) {

	}

	return ret;
}

em_err deinit_wifi_platform() {
	em_err ret = esp_wifi_deinit();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "deinit_wifi_platform failed: err = %d", ret);
	}
	return ret;
}

em_err get_wifi_platform_mode(WifiPlatformModes *mode) {
	em_err ret = esp_wifi_get_mode((wifi_mode_t *)mode);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "get_wifi_platform_mode failed: err = %d", ret);
	}
	return ret;
}

em_err stop_wifi_platform(void) {
	em_err ret = esp_wifi_stop();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "stop_wifi_platform failed: err = %d", ret);
	}
	return ret;
}

em_err disconnect_wifi_platform(void) {
	em_err ret = esp_wifi_disconnect();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "disconnect_wifi_platform failed: err = %d", ret);
	}
	return ret;
}

em_err get_wifi_pf_sta_mac(uint8_t *mac) {
	em_err err = esp_wifi_get_mac(ESP_BOARD_IF_WIFI_STA, mac);
	EM_LOGD(TAG, "Wi-Fi station mac[%p]: %x:%x:%x:%x:%x:%x", mac, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to get WIFI Station MAC");
		return err;
	}
	return err;
}


/**
 *
 */
em_err get_wifi_pf_sta_ip(char *ip) {
	if (wifi_curr_evnt == WIFI_PLAT_EVENT_STA_GOT_IP) {
		sprintf(ip, "%s", wifi_sta_ip);
		return EM_OK;
	}
	return EM_FAIL;
}

