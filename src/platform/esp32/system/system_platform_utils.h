/*
 * File Name: system_platform_utils.h
 * File Path: /temp-humidity-monitor/platform/esp/system/system_platform_utils.h
 * Description:
 *
 *  Created on: 07-Aug-2019
 *      Author: Noyel Seth
 */

#ifndef SYSTEM_PLATFORM_UTILS_H_
#define SYSTEM_PLATFORM_UTILS_H_

#include "esp_system.h"

/**
 * @brief Creating a Microseconds dalay
 *
 */
#define core_platform_us_delay(delay)			ets_delay_us(/*uint32_t*/ delay)



#endif /* PLATFORM_ESP_SYSTEM_SYSTEM_PLATFORM_UTILS_H_ */
