/**
 * Company:
 *  Iquester Solutions LLP
 *
 * Project:
 *   EmMate
 *
 * File Name:
 *   nvs_helper.c
 *
 * Created On:
 *   29-Mar-2019
 *
 * Author:
 *   Noyel Seth
 *
 * Summary:
 *
 *
 * Description:
 *
 *
 **/

#include <stdio.h>
#include <string.h>

#include "core_config.h"
//#include "core_error.h"
#include "core_constant.h"
#include "nvs_helper.h"
#include "nvs_helper_errors.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

#define TAG LTAG_PERSISTENT_NVS

/* Globals */
static nvs_handle nvs_hndl;

/* Static Functions */
static em_err open_nvs_handle();
static void close_nvs_handle();

static em_err open_nvs_handle() {
	em_err err = nvs_open("storage", NVS_READWRITE, &nvs_hndl);
	if (err != EM_OK) {
		EM_LOGE(TAG, "Error (%s) opening NVS handle!", esp_err_to_name(err));
	} else {
		EM_LOGD(TAG, "(%s) opening NVS handle!", esp_err_to_name(err));
	}
	return err;
}

static void close_nvs_handle() {
	// Close
	if (nvs_hndl != 0) {
		nvs_close(nvs_hndl);
	}
}

/* Initialize the default ESP32 NVS partition */
em_err init_nvs() {
	em_err err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		// NVS partition was truncated and needs to be erased
		// Retry nvs_flash_init
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}
	err = open_nvs_handle();
	if (err != EM_OK) {
		EM_LOGE(TAG, "Error (%s) opening NVS handle!", esp_err_to_name(err));
	}

	return err;
}

em_err deinit_nvs() {
	close_nvs_handle();
	return nvs_flash_deinit();
}

em_err erase_nvs() {
	em_err err = deinit_nvs();
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to de-initialize NVS: %s", esp_err_to_name(err));
		return err;

	}

	err = nvs_flash_erase();
	if (err != EM_OK) {
		EM_LOGE(TAG, "Failed to Erase NVS: %s", esp_err_to_name(err));
	}

	// Re-initialize the NVS again
	if (init_nvs() != EM_OK) {
		EM_LOGE(TAG, "Failed to Re-initialize the NVS");
	}

	return err;
}

em_err erase_nvs_key(char *key) {
	em_err err = nvs_erase_key(nvs_hndl, key);
	if (err == EM_OK) {
		EM_LOGI(TAG, "Erasing key from NVS: %s", key);
		err = nvs_commit(nvs_hndl);
		if (err == EM_OK) {
			EM_LOGI(TAG, "Successfully erased key: %s", key);
		} else {
			EM_LOGE(TAG, "Failed! to erase key: %s", key);
			err = EM_FAIL;
		}
	} else {
		err = EM_FAIL;
	}
	return err;
}

em_err read_nvsdata_by_key(char *key, void* value, size_t * val_size) {
	em_err err;

	EM_LOGD(TAG, "Reading data from NVS ... ");

	TaskDelay(DELAY_250_MSEC / TICK_RATE_TO_MS);

	size_t size = 0;
	err = nvs_get_blob(nvs_hndl, (const char*) key, NULL, &size);

	switch (err) {
	case EM_OK:
		EM_LOGD(TAG, "Size = %d", size);
		break;
	case ESP_ERR_NVS_NOT_FOUND:
		EM_LOGE(TAG, "The value is not initialized yet!");
		break;
	default:
		EM_LOGE(TAG, "Error (%s) reading!", esp_err_to_name(err));
		break;
	}

	if (size != 0) {
		*val_size = size;
		err = nvs_get_blob(nvs_hndl, key, value, val_size);
		switch (err) {
		case EM_OK:
			EM_LOGD(TAG, "Done");
			break;
		case ESP_ERR_NVS_NOT_FOUND:
			EM_LOGE(TAG, "The key %s is not initialized yet!", key);
			break;
		default:
			EM_LOGE(TAG, "Error (%s) reading!", esp_err_to_name(err));
		}
	}
	return err;
}

em_err write_nvsdata_by_key(char *key, void *set_data, size_t len) {
	em_err err;

	err = nvs_erase_key(nvs_hndl, key);
	err = nvs_set_blob(nvs_hndl, key, (const void*) set_data, len);

	if (err == EM_OK) {
		EM_LOGI(TAG, "Committing %s to NVS ... ", key);

		err = nvs_commit(nvs_hndl);
		if (err != EM_OK) {
			EM_LOGE(TAG, "Failed! to set key: %s", key);
		} else {
			EM_LOGI(TAG, "Successfully commited %s", key);
		}
	} else {
		EM_LOGE(TAG, "Failed! to set key:%s", key);
	}
	return err;
}
