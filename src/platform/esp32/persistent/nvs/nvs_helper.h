/**
 * Company:
 *  Iquester Solutions LLP
 *
 * Project:
 *   EmMate
 *
 * File Name:
 *   nvs_helper.h
 *
 * Created On:
 *   29-Mar-2019
 *
 * Author:
 *   Noyel Seth
 *
 * Summary:
 *
 *
 * Description:
 *
 *
 **/

#ifndef NVS_HELPER_H_
#define NVS_HELPER_H_

#include <stdio.h>
#include "core_error.h"

/**
 * @brief initialize the ESP32 NVS
 *
 * @return
 * 		- EM_OK		On success
 * 		- EM_FAIL		On failure
 *
 * */
em_err init_nvs();

/**
 * @brief This function de-initializes the underlying Non Volatile Storage/Memory (Default NVS)
 *
 * @return
 * 		- EM_OK		On success
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		On failure if initialize the ESP32 NVS not done yet
 *
 **/
em_err deinit_nvs();

/**
 * @brief Erase key-value pair with given key name. init_nvs() must be called first
 *
 * @param[in]	key unique Tag for collect data from NVS
 *
 * @return
 * 		- EM_OK		Successfully erased the key from NVS
 * 		- EM_FAIL		Failed to erase the key from NVS
 * */
em_err erase_nvs_key(char *key);

/**
 * @brief Erase the ESP32 NVS memory
 *
 * @return
 * 		- EM_OK		On success
 * 		- EM_FAIL		On failure
 * 		- PLAT_NVS_ERR_NOT_FOUND 			if no NVS partition labeled “nvs” in the partition table
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		On failure if initialize the ESP32 NVS not done yet
 *
 * */
em_err erase_nvs();

/**
 * @brief read data from ESP32 NVS
 *
 * @param[in]	key unique Tag for collect data from NVS
 *
 * @param[out]	value	out data read from NVS
 *
 * @param[out]	val_size	out data-length of the read value
 *
 * @return
 * 		- EM_OK							On success
 * 		- EM_FAIL							On failure
 * 		- PLAT_NVS_ERR_NOT_FOUND 			if data not present
 * 		- PLAT_NVS_ERR_NOT_INITIALIZED		if the storage driver is not initialized
 *
 * */
em_err read_nvsdata_by_key(char *key, void* value, size_t * val_size);

/**
 * @brief write data to ESP32 NVS
 *
 * @param[in]	key unique Tag for store data from NVS
 *
 * @param[in]	value	input data to write into NVS
 *
 * @param[in]	val_size	input data length to write into NVS
 *
 * @return
 * 		- EM_OK		On success
 * 		- EM_FAIL		On failure
 *
 * */
em_err write_nvsdata_by_key(char *key, void *set_data, size_t len);


#endif /* NVS_HELPER_H_ */
