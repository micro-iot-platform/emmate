add_library(som INTERFACE)

list(APPEND EXTRA_LIBS
					emmate_config
					)
target_link_libraries(som INTERFACE ${EXTRA_LIBS})

target_include_directories(som 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)