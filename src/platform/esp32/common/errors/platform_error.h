/*
 * platform_error.h
 *
 *  Created on: 11-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef PLATFORM_ERROR_H_
#define PLATFORM_ERROR_H_

#include "platform_error_base.h"

/* Definitions for error constants. */
#define PLATFORM_OK          ESP_OK
#define PLATFORM_FAIL        ESP_FAIL

#define PLATFORM_ERR_NO_MEM              ESP_ERR_NO_MEM   			/*!< Out of memory */
#define PLATFORM_ERR_INVALID_ARG         ESP_ERR_INVALID_ARG   		/*!< Invalid argument */
#define PLATFORM_ERR_INVALID_STATE       ESP_ERR_INVALID_STATE   	/*!< Invalid state */
#define PLATFORM_ERR_INVALID_SIZE        ESP_ERR_INVALID_SIZE   	/*!< Invalid size */
#define PLATFORM_ERR_NOT_FOUND           ESP_ERR_NOT_FOUND   		/*!< Requested resource not found */
#define PLATFORM_ERR_NOT_SUPPORTED       ESP_ERR_NOT_SUPPORTED   	/*!< Operation or feature not supported */
#define PLATFORM_ERR_TIMEOUT             ESP_ERR_TIMEOUT   			/*!< Operation timed out */
#define PLATFORM_ERR_INVALID_RESPONSE    ESP_ERR_INVALID_RESPONSE   /*!< Received response was invalid */
#define PLATFORM_ERR_INVALID_CRC         ESP_ERR_INVALID_CRC   		/*!< CRC or checksum was invalid */
#define PLATFORM_ERR_INVALID_VERSION     ESP_ERR_INVALID_VERSION   	/*!< Version was invalid */
#define PLATFORM_ERR_INVALID_MAC         ESP_ERR_INVALID_MAC   		/*!< MAC address was invalid */


#endif /* PLATFORM_ERROR_H_ */
