/*
 * File Name: led_pwm_driver_platform.c
 *
 * Description:
 *
 *  Created on: 23-Sep-2019
 *      Author: Noyel Seth
 */

#include "led_pwm_platform_driver.h"

typedef struct{
	led_pwm_pf_timer_bit_t duty_res;
	uint32_t max_duty_cycle;
}led_pwm_pf_timer_mapping_data;

typedef struct{
	int gpio_num;
	led_pwm_pf_timer_t timer;
}led_pwm_pf_timer_channel_mapping_data;

static led_pwm_pf_timer_mapping_data timer_map_data[LED_PWM_PF_TIMER_MAX];
static led_pwm_pf_timer_channel_mapping_data channel_map_data[LED_PWM_PF_CHANNEL_MAX];


/**
 *
 */
em_err led_pwm_pf_driver_configure_timer(led_pwm_pf_timer_bit_t duty_res, uint32_t freq_hz, led_pwm_pf_timer_t timer) {

	em_err res = EM_FAIL;

	/*
	 * Prepare and set configuration of timers
	 * that will be used by PWM Controller
	 */
	ledc_timer_config_t led_pwm_timer = { 0 };

	led_pwm_timer.duty_resolution = duty_res; // resolution of PWM duty
	led_pwm_timer.freq_hz = freq_hz;                      // frequency of PWM signal
	led_pwm_timer.speed_mode = LED_PWM_PF_HIGH_SPEED_MODE;           // timer mode
	led_pwm_timer.timer_num = timer;            // timer index
	led_pwm_timer.clk_cfg = LED_PWM_PF_AUTO_CLK;              // Auto select the source clock

	// Set configuration for timer
	res = led_pwm_pf_timer_config(&led_pwm_timer);
	if(res == EM_OK){
		timer_map_data[timer].duty_res = duty_res;
		timer_map_data[timer].max_duty_cycle = 1 << duty_res;
	}

	return res;
}

/**
 *
 */
uint32_t led_pwm_pf_driver_get_max_duty_by_timer(led_pwm_pf_timer_t timer){
	return timer_map_data[timer].max_duty_cycle;
}

/**
 *
 */
uint32_t led_pwm_pf_driver_get_max_duty_by_channel(led_pwm_pf_channel_t channel){
	led_pwm_pf_timer_t timer = channel_map_data[channel].timer;
	return led_pwm_pf_driver_get_max_duty_by_timer(timer);
}


/**
 *
 */
em_err led_pwm_pf_driver_configure_channel(led_pwm_pf_channel_t channel, uint32_t duty, int gpio_num,
		led_pwm_pf_timer_t timer) {

	em_err res = EM_FAIL;

	/*
	 * Prepare individual configuration
	 * for each channel of LED Controller
	 * by selecting:
	 * - controller's channel number
	 * - output duty cycle, set initially to 0
	 * - GPIO number where LED is connected to
	 * - speed mode, either high or low
	 * - timer servicing selected channel
	 *   Note: if different channels use one timer,
	 *         then frequency and bit_num of these channels
	 *         will be the same
	 */
	ledc_channel_config_t led_pwm_channel = { 0 };

	led_pwm_channel.channel = channel;
	led_pwm_channel.duty = duty;
	led_pwm_channel.gpio_num = gpio_num;
	led_pwm_channel.speed_mode = LED_PWM_PF_HIGH_SPEED_MODE;
	led_pwm_channel.hpoint = 0;
	led_pwm_channel.timer_sel = timer;

	res = led_pwm_pf_channel_config(&led_pwm_channel);

	if(res == EM_OK){
		channel_map_data[channel].gpio_num = gpio_num;
		channel_map_data[channel].timer = timer;
	}

	return res;
}

/**
 *
 */
em_err led_pwm_pf_driver_stop(led_pwm_pf_channel_t channel) {
	em_err res = EM_FAIL;
	led_pwm_pf_mode_t speed_mode = LED_PWM_PF_HIGH_SPEED_MODE;
	res = led_pwm_pf_stop(speed_mode, channel, 0);
	return res;
}

/**
 *
 */
em_err led_pwm_pf_driver_set_duty_cycle(led_pwm_pf_channel_t channel, uint32_t duty) {
	em_err res = EM_FAIL;
	led_pwm_pf_mode_t speed_mode = LED_PWM_PF_HIGH_SPEED_MODE;
	res = led_pwm_pf_set_duty_and_update(speed_mode, channel, duty, 0);
	return res;
}

/**
 *
 */
em_err led_pwm_pf_driver_set_fade_step_and_start(led_pwm_pf_channel_t channel, uint32_t target_duty, uint32_t scale,
		uint32_t cycle_num) {
	em_err res = EM_FAIL;
	led_pwm_pf_mode_t speed_mode = LED_PWM_PF_HIGH_SPEED_MODE;
	res = led_pwm_pf_set_fade_step_and_start(speed_mode, channel, target_duty, scale, cycle_num, LED_PWM_PF_FADE_NO_WAIT);
	return res;
}

/**
 *
 */
em_err led_pwm_pf_driver_set_fade_time_and_start(led_pwm_pf_channel_t channel, uint32_t target_duty,
		uint32_t max_fade_time_ms) {
	em_err res = EM_FAIL;
	led_pwm_pf_mode_t speed_mode = LED_PWM_PF_HIGH_SPEED_MODE;
	res = led_pwm_pf_set_fade_time_and_start(speed_mode, channel, target_duty, max_fade_time_ms, LED_PWM_PF_FADE_NO_WAIT);
	return res;
}

/**
 *
 */
em_err led_pwm_pf_driver_fade_func_install(){
	return led_pwm_pf_fade_func_install(0);
}

/**
 *
 */
em_err led_pwm_pf_driver_fade_func_uninstall(){
	led_pwm_pf_fade_func_uninstall();
	return EM_OK;
}

