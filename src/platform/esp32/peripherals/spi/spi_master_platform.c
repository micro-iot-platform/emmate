/*
 * File Name: spi_master_platform.c
 *
 * Description:
 *
 *  Created on: 28-Sep-2019
 *      Author: Noyel Seth
 */

#include "spi_core_common.h"
#include "spi_master_platform.h"
#include "core_error.h"
#include <string.h>

/**
 *
 */
em_err spi_master_pf_bus_config(spi_bus_config * config_data) {
	em_err ret = EM_FAIL;

	if (config_data != NULL) {
		spi_pf_bus_config_t buscfg;
		buscfg.miso_io_num = config_data->miso_io_num;
		buscfg.mosi_io_num = config_data->mosi_io_num;
		buscfg.sclk_io_num = config_data->sclk_io_num;
		buscfg.quadwp_io_num = -1;
		buscfg.quadhd_io_num = -1;
		buscfg.max_transfer_sz = config_data->max_transfer_sz;
		//		buscfg.flags=SPICOMMON_BUSFLAG_MASTER;
		//		buscfg.intr_flags=0;

		//Initialize the SPI bus
		ret = spi_pf_bus_initialize(config_data->spi_host, (spi_bus_config_t *)&buscfg, 1);
	}
	return ret;
}

/**
 *
 */
em_err spi_master_pf_device_interface_config(uint8_t host, int clock_speed_hz, int spics_io_num, int queue_size,
		spi_pf_device_handle_t *spi_drv_handle) {
	em_err ret = EM_FAIL;

	spi_pf_device_interface_config_t devcfg;
	devcfg.command_bits = 0;
	devcfg.address_bits = 0;
	devcfg.dummy_bits = 0;
	devcfg.clock_speed_hz = clock_speed_hz;
	devcfg.duty_cycle_pos = 0;       //50% duty cycle
	devcfg.mode = 0;
	devcfg.spics_io_num = spics_io_num;
	devcfg.queue_size = queue_size;
	devcfg.cs_ena_posttrans=0;
	devcfg.cs_ena_pretrans=0;
	//devcfg.flags=0;
	//devcfg.input_delay_ns=0;
	devcfg.post_cb=NULL;
	devcfg.pre_cb=NULL;

	ret = spi_pf_bus_add_device(host, (spi_device_interface_config_t *)&devcfg, spi_drv_handle);

	return ret;
}

/**
 *
 */
em_err spi_master_pf_device_transmit(spi_pf_device_handle_t spi_drv_handle, void * tx_data, void* rx_data,
		size_t tx_length_in_byte) {
	em_err ret = EM_FAIL;
	spi_pf_transaction_t tran_data;
	memset(&tran_data, 0, sizeof(tran_data));

	//tran_data.flags = 0;
	//tran_data.addr=0;
	//tran_data.cmd=0;

	tran_data.length = tx_length_in_byte;

	//	printf("tran_data.length = %d\r\n", tran_data.length);
	tran_data.rx_buffer = rx_data;
	//tran_data.rx_data=0;
	//	tran_data.rxlength=0;

	tran_data.tx_buffer = tx_data;
	//tran_data.tx_data=0;
	//tran_data.user=0;

	ret = spi_pf_device_transmit(spi_drv_handle, (spi_transaction_t *)&tran_data);

	return ret;
}


/**
 *
 */
em_err spi_master_pf_remove_device_from_bus(spi_pf_device_handle_t spi_drv_handle) {
	em_err ret = EM_FAIL;
	ret = spi_pf_bus_remove_device(spi_drv_handle);
	return ret;
}


/**
 *
 */
em_err spi_master_pf_bus_free(uint8_t host) {
	em_err ret = EM_FAIL;
	ret = spi_pf_bus_free(host);
	return ret;
}


