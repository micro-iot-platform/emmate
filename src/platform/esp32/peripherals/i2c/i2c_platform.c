/*
 * File Name: i2c_platform.c
 * File Path: /emmate/src/platform/esp32/peripherals/i2c/i2c_platform.c
 * Description:
 *
 *  Created on: 30-May-2019
 *      Author: Noyel Seth
 */

#include "i2c_platform.h"
//#include "esp_err.h"
//#include "esp_log.h"
#include "core_config.h"
#include "threading.h"
#include "core_constant.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include "gpio_platform.h"
//#include "threading_platform.h"

#include <string.h>

#define TAG LTAG_I2C

#define DATA_LENGTH 512                  /*!< Data buffer length of test buffer */

#define I2C_MASTER_RX_BUF_DISABLE		0
#define I2C_MASTER_TX_BUF_DISABLE		0

#define I2C_SLAVE_RX_BUF_LEN		(2*DATA_LENGTH)
#define I2C_SLAVE_TX_BUF_LEN		(2*DATA_LENGTH)

#define WRITE_BIT 		(0xFE | I2C_PF_MASTER_WRITE)              /*!< I2C master write */
#define READ_BIT 		(0x01 | I2C_PF_MASTER_READ)                /*!< I2C master read */

#define ACK_CHECK_EN 	0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 	0x0                       /*!< I2C master will not check ack from slave */

#define ACK_VAL 		0x0                             /*!< I2C ack value */
#define NACK_VAL 		0x1                            /*!< I2C nack value */

#define WITH_CMD		true					/*!< I2C Master write to Slave with CMD value */
#define WITHOUT_CMD		false					/*!< I2C Master write to Slave without CMD value */

static em_err master_write_slave_priv_func(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t cmd_addr,
		uint8_t *data_wr, size_t size, uint8_t mode_bit, bool is_write_cmd) {

	/*
	 * I2C Write Buffer
	 * S=Start
	 * A=Acknowledge (from slave)
	 * W=write bit(0)
	 * P=stop
	 *
	 * i2c Write with data
	 * |S|slave-addr+W|A|cmd-addr|A|data+n|A|data+n|A|...|data+x|A|P|
	 *
	 * i2c Write without data
	 * |S|slave-addr+W|A|cmd-addr|A|P|
	 *
	 * i2c Write before i2c read start
	 * |S|slave-addr+W|A|cmd-addr|A|
	 *
	 *
	 */

	em_err ret = EM_FAIL;
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	if (cmd != NULL) {
		ret = i2c_master_start(cmd);		// I2C prcoess "|S|"
		if (ret == EM_OK) {
			// I2C prcoess "|slave-addr+W|A|"
			if (i2c_master_write_byte(cmd, (slave_addr) & WRITE_BIT, ACK_CHECK_EN) == EM_OK) {
				//printf("slave_addr %x,  %x\r\n", slave_addr, (slave_addr) & WRITE_BIT);

				if (WITH_CMD == is_write_cmd) {
					// I2C prcoess "|cmd-addr|A|"
					if (i2c_master_write_byte(cmd, cmd_addr, ACK_CHECK_EN) != EM_OK) {

					}
				} else {

				}
				if (size > 0) {
					//i2c Write with data
					//I2C prcoess "|data+n|A|data+n|A|...|data+x|A|P|"
					if (i2c_master_write(cmd, data_wr, size, ACK_CHECK_EN) == EM_OK) {
						//I2C prcoess "|P|"
						if (i2c_master_stop(cmd) == EM_OK) {
							ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
						}
					}
				} else {
					if (mode_bit == READ_BIT) {
						//i2c Write before i2c read start
						ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);

					} else if (mode_bit == WRITE_BIT) {
						//i2c Write without data
						//I2C prcoess "|P|"
						if (i2c_master_stop(cmd) == EM_OK) {
							ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
						}
					}
				}

			}
		}
		i2c_cmd_link_delete(cmd);
	}
	return ret;
}

/***************************************************************************************************************************/

em_err init_platform_i2c(I2C_CONN_DATA *i2c_conn_data) {
	em_err ret = EM_FAIL;

	if (i2c_conn_data != NULL) {
		if (I2C_PF_MODE_MASTER == i2c_conn_data->i2c_mode) {
			int i2c_master_port = i2c_conn_data->i2c_port;
			i2c_config_t conf;

			conf.mode = i2c_conn_data->i2c_mode;

			conf.sda_io_num = i2c_conn_data->sda_io_num;
			conf.scl_io_num = i2c_conn_data->scl_io_num;

			if (i2c_conn_data->is_internal_pullup_enable == true) {
				conf.sda_pullup_en = GPIO_PF_IO_PULLUP_ENABLE;
				conf.scl_pullup_en = GPIO_PF_IO_PULLUP_ENABLE;
			} else {
				conf.sda_pullup_en = GPIO_PF_IO_PULLUP_DISABLE;
				conf.scl_pullup_en = GPIO_PF_IO_PULLUP_DISABLE;
			}

			conf.master.clk_speed = i2c_conn_data->master.clk_speed;

			ret = i2c_param_config(i2c_master_port, &conf);
			if (ret == EM_OK) {
				return i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE,
				I2C_MASTER_TX_BUF_DISABLE, 0);
			} else {
				EM_LOGE(TAG, "i2c_param_config failed (%s)", esp_err_to_name(ret));
			}

		} else if (I2C_PF_MODE_SLAVE == i2c_conn_data->i2c_mode) {

			int i2c_slave_port = i2c_conn_data->i2c_port;
			i2c_config_t conf;

			conf.mode = i2c_conn_data->i2c_mode;

			conf.sda_io_num = i2c_conn_data->sda_io_num;
			conf.scl_io_num = i2c_conn_data->scl_io_num;

			if (i2c_conn_data->is_internal_pullup_enable == true) {
				conf.sda_pullup_en = GPIO_PF_IO_PULLUP_ENABLE;
				conf.scl_pullup_en = GPIO_PF_IO_PULLUP_ENABLE;
			} else {
				conf.sda_pullup_en = GPIO_PF_IO_PULLUP_DISABLE;
				conf.scl_pullup_en = GPIO_PF_IO_PULLUP_DISABLE;
			}

			conf.slave.addr_10bit_en = i2c_conn_data->slave.addr_10bit_en;
			conf.slave.slave_addr = i2c_conn_data->slave.slave_addr >> 1;	// right shift 1 bit.

			ret = i2c_param_config(i2c_slave_port, &conf);
			if (ret == EM_OK) {
				return i2c_driver_install(i2c_slave_port, conf.mode, I2C_SLAVE_RX_BUF_LEN,
				I2C_SLAVE_TX_BUF_LEN, 0);
			} else {
				EM_LOGE(TAG, "i2c_param_config failed (%s)", esp_err_to_name(ret));
			}

		}
	} else {
		return EM_ERR_INVALID_ARG;
	}

	return ret;

}

em_err i2c_platform_master_write_slave(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t cmd_addr, uint8_t *data_wr,
		size_t size) {
	/*
	 em_err ret = EM_FAIL;
	 i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	 if (cmd != NULL) {
	 ret = i2c_master_start(cmd);
	 if (ret == EM_OK) {
	 if (i2c_master_write_byte(cmd, (slave_addr << 1) | WRITE_BIT, ACK_CHECK_EN) == EM_OK) {
	 printf("slave_addr %x,  %x\r\n", slave_addr, (slave_addr << 1) | WRITE_BIT);
	 if (i2c_master_write_byte(cmd, cmd_addr, ACK_CHECK_EN) == EM_OK) {
	 if (size > 0) {
	 if (i2c_master_write(cmd, data_wr, size, ACK_CHECK_EN) == EM_OK) {
	 if (i2c_master_stop(cmd) == EM_OK) {
	 ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
	 }
	 }
	 } else {
	 ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
	 }
	 }

	 }
	 }
	 i2c_cmd_link_delete(cmd);
	 }*/

	em_err ret = EM_FAIL;
	ret = master_write_slave_priv_func(i2c_port_num, slave_addr, cmd_addr, data_wr, size, WRITE_BIT, WITH_CMD);
	return ret;
}

/**
 *
 */
em_err i2c_platform_master_write_slave_without_cmd(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t *data_wr,
		size_t size) {
	/*
	 em_err ret = EM_FAIL;
	 i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	 if (cmd != NULL) {
	 ret = i2c_master_start(cmd);
	 if (ret == EM_OK) {
	 if (i2c_master_write_byte(cmd, (slave_addr << 1) | WRITE_BIT, ACK_CHECK_EN) == EM_OK) {
	 printf("slave_addr %x,  %x\r\n", slave_addr, (slave_addr << 1) | WRITE_BIT);
	 if (i2c_master_write_byte(cmd, cmd_addr, ACK_CHECK_EN) == EM_OK) {
	 if (size > 0) {
	 if (i2c_master_write(cmd, data_wr, size, ACK_CHECK_EN) == EM_OK) {
	 if (i2c_master_stop(cmd) == EM_OK) {
	 ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
	 }
	 }
	 } else {
	 ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
	 }
	 }

	 }
	 }
	 i2c_cmd_link_delete(cmd);
	 }*/

	em_err ret = EM_FAIL;
	ret = master_write_slave_priv_func(i2c_port_num, slave_addr, 0x00, data_wr, size, WRITE_BIT, WITHOUT_CMD);
	return ret;
}

em_err i2c_platform_master_read_slave(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t cmd_addr, uint8_t *data_rd,
		size_t size, uint32_t delay_before_restart) {

	/*
	 * I2C Read Buffer
	 *
	 * S=Start
	 * Sr=Restart
	 * A=Acknowledge (from slave)
	 * !A=No Acknowledge
	 * W=write bit(0)
	 * R=read bit(1)
	 * P=stop
	 *
	 * i2c Read data
	 * |S|slave-addr+W|A|cmd-addr|A|----
	 *									|Sr|slave-addr+R|A|data+n|A|data+n|A|...|data+x|!A|P|
	 *
	 */

	em_err ret = EM_FAIL;
	if (size == 0) {
		return EM_OK;
	}
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	if (cmd != NULL) {
		// I2C process "|S|A|slave-addr+W|A|cmd-addr|A|"
		if (master_write_slave_priv_func(i2c_port_num, slave_addr, cmd_addr, NULL, 0, READ_BIT, WITH_CMD) == EM_OK) {
			if (delay_before_restart > 0) {
				TaskPfDelay(delay_before_restart / portTICK_RATE_MS);
			}
			ret = i2c_master_start(cmd);	// I2C process "|Sr|"
			if (ret == EM_OK) {

				// I2C process "|slave-addr+R|A|"
				if (i2c_master_write_byte(cmd, (slave_addr) | READ_BIT, ACK_CHECK_EN) == EM_OK) {
					//printf("i2c_platform_master_read_slave slave_addr %x,  %x\r\n", slave_addr, (slave_addr) | READ_BIT);

					if (size > 1) {
						// I2C process "|data+n|A|data+n|A|"
						i2c_master_read(cmd, data_rd, size - 1, ACK_VAL);
					}

					// I2C process "|data+x|!A|"
					i2c_master_read_byte(cmd, data_rd + size - 1, NACK_VAL);

					//I2C prcoess "|P|"
					if (i2c_master_stop(cmd) == EM_OK) {
						ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
					}
				}
			}
		}
		i2c_cmd_link_delete(cmd);
	}

	return ret;
}

em_err i2c_platform_master_read_slave_without_cmd(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t *data_rd,
		size_t size) {

	/*
	 * I2C Read Buffer without write
	 *
	 * S=Start
	 * A=Acknowledge (from slave)
	 * !A=No Acknowledge
	 * R=read bit(1)
	 * P=stop
	 *
	 * i2c Read data
	 * |S|slave-addr+R|A|data+n|A|data+n|A|...|data+x|!A|P|
	 *
	 */

	em_err ret = EM_FAIL;
	if (size == 0) {
		return EM_OK;
	}
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	if (cmd != NULL) {
		ret = i2c_master_start(cmd);	// I2C process "|S|"
		if (ret == EM_OK) {
			// I2C process "|slave-addr+R|A|"
			if (i2c_master_write_byte(cmd, (slave_addr) | READ_BIT, ACK_CHECK_EN) == EM_OK) {
//				printf("i2c_platform_master_read_slave_without_write slave_addr %x,  %x\r\n", slave_addr,(slave_addr) | READ_BIT);
				if (size > 1) {
					// I2C process "|data+n|A|data+n|A|"
					i2c_master_read(cmd, data_rd, size - 1, ACK_VAL);
				}

				// I2C process "|data+x|!A|"
				i2c_master_read_byte(cmd, data_rd + size - 1, NACK_VAL);

				//I2C prcoess "|P|"
				if (i2c_master_stop(cmd) == EM_OK) {
					ret = i2c_master_cmd_begin(i2c_port_num, cmd, 1000 / portTICK_RATE_MS);
				}
			}
		}
		i2c_cmd_link_delete(cmd);
	}

	return ret;
}

/**
 *
 */
em_err i2c_platform_slave_read_master(uint32_t i2c_port, uint8_t *data, size_t max_size, uint32_t ticks_to_wait) {
	em_err ret = EM_FAIL;

	if (max_size == 0) {
		return EM_OK;
	}

	ret = i2c_slave_read_buffer(i2c_port, data, max_size, ticks_to_wait / portTICK_RATE_MS);

	return ret;
}

/**
 *
 */
int i2c_platform_slave_write_master(uint32_t i2c_port, uint8_t *data, size_t size, uint32_t ticks_to_wait) {
	int ret = -1;
	ret = i2c_slave_write_buffer(i2c_port, data, size, ticks_to_wait / portTICK_RATE_MS);
	return ret;
}

em_err deinit_platform_i2c(I2C_CONN_DATA *i2c_conn_data) {
	em_err ret = EM_FAIL;

	if (i2c_conn_data != NULL) {
		ret = i2c_driver_delete(i2c_conn_data->i2c_port);
	} else {
		return EM_ERR_INVALID_ARG;
	}
	return ret;
}
