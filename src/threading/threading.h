/*
 * File Name: threading.h
 * File Path: /emmate/src/threading/threading.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef THREADING_H_
#define THREADING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "threading_platform.h"

#define THREADING_MAX_DELAY THREADING_MAX_DELAY_PF	/**< Maximum delay that can be set ~ infinity */

typedef BaseType_pf			BaseType;	/**< Base datatype for threading module, usually int */
typedef UBaseType_pf		UBaseType;	/**< Base unsigned datatype for threading module, usually unsigned int */

/**
 * @brief 	Return type of a task function. Whenever a task function is defined, it should
 * 			have a return type of TaskRet. This is an opaque data type which is dependant on the
 * 			underlying OS.
 *
 * 			For FreeRTOS OS: TaskRet is void
 * 			For Linux OS: TaskRet is void *
 */
typedef TaskRetPf TaskRet;

/**
 * @brief 	Function to be passed into 1st parameter of TaskCreate()
 * 			The task function must be of the following type.
 *
 * 			TaskRet mytask(void *param);
 */
typedef TaskFuncPf TaskFunc;

/**
 * @brief	Data type to store a handle of a newly created Task. Pass this as the last parameter
 * 			of TaskCreate() function as an out parameter
 */
typedef TaskHandle_pf		TaskHandle;

/**
 * @brief	Data type which denotes the system ticks
 */
typedef TickType_pf 		TickType;

/**
 * @brief	Creates a new task. In this module the words thread and task are used interchangeably
 *
 * @param[in] func 			Type: TaskFunc. Pointer to the task entry function.  Tasks
 * 							must be implemented to never return (i.e. continuous loop).
 *
 * @param[in] name 			Type: const char* const. A descriptive name for the task.  This is mainly used to
 * 							facilitate debugging.  Max length is 16.
 *
 * @param[in] stack_depth 	Type: const uint32_t. The size of the task stack specified as the number of
 * 							bytes.
 *
 * @param[in] params 		Type: void * const. Pointer that will be used as the parameter for the task
 * 							being created. Pass any data here that must be passed to the task when created
 *
 * @param[in] priority 		Type UBaseType. The priority at which the task should run.
 *
 * @param[out] task_id		Type TaskHandle *. Used to pass back a handle by which the created task
 * 							can be referenced.
 *
 * @return
 * 			- true 			Type BaseType. if the task was successfully created and added to a ready list
 * 			- error code 	Type BaseType. if failed OS specific error codes are returned
 */
#define TaskCreate(func /*TaskFunc*/, name /*const char * const*/, stack_depth /*const uint32_t*/, params /*void * const*/, priority /*int*/, task_id /* TaskHandle* */) \
		TaskPfCreate(func,name,stack_depth,params,priority,task_id)

/**
 * @brief		Create a new task with a specified affinity.
 * 				This function is similar to TaskCreate, but allows setting task affinity in SMP system.
 *
 * @note		Use this function for FreeRTOS only. This function is implemented as a stub for Linux
 * 				based platforms
 *
 * @param[in] func 			Type: TaskFunc. Pointer to the task entry function.  Tasks
 * 							must be implemented to never return (i.e. continuous loop).
 *
 * @param[in] name 			Type: const char* const. A descriptive name for the task.  This is mainly used to
 * 							facilitate debugging.  Max length is 16.
 *
 * @param[in] stack_depth 	Type: const uint32_t. The size of the task stack specified as the number of
 * 							bytes.
 *
 * @param[in] params 		Type: void * const. Pointer that will be used as the parameter for the task
 * 							being created. Pass any data here that must be passed to the task when created
 *
 * @param[in] priority 		Type UBaseType. The priority at which the task should run.
 *
 * @param[out] task_id		Type TaskHandle *. Used to pass back a handle by which the created task
 * 							can be referenced.
 *
 * @param[in] core_id 		Type BaseType. The CPU core ID. See FreeRTOS documentation for xTaskCreatePinnedToCore()
 *
 * @return
 * 			- true 			Type BaseType. if the task was successfully created and added to a ready list
 * 			- error code 	Type BaseType. if failed OS specific error codes are returned
 */
#define TaskCreatePinnedToCore(func, name, stack_depth, params, priority, task_id, core_id) \
		TaskPfCreatePinnedToCore(func, name, stack_depth, params, priority, task_id, core_id) \

/**
 * @brief	Obtain the state of any task.
 *
 * @note	Works only if the underlying OS is FreeRTOS. Other platforms return 'TH_UNKNOWN'
 *
 * @param[in] 	task_id 	Type TaskHandle. Handle of the task to be queried.
 *
 * @return
 * 			Value of enum THREAD_STATE. The state of task_id at the time the function was called.  Note the
 * 			state of the task might change between the function being called, and the
 * 			functions return value being tested by the calling task.
 */
#define TaskGetState(task_id)	TaskPfGetState(task_id) /* Returns a THREAD_STATE */

/**
 * @brief Remove a task from the OS kernel's management
 *
 * @param[in] 	task_id 	Type TaskHandle. The handle of the task to be deleted.
 * 							Passing NULL will cause the calling task to be deleted.
 *
 * @note	TaskDelete(NULL) will only work incase of FreeRTOS, it has no effect for other OS
 * 			It is advised not to use TaskDelete(NULL) when the underlying platform is not FreeRTOS
 *
 * 			Example Usage:
 * 			#if defined (CONFIG_PLATFORM_ESP_IDF)
 * 				TaskDelete(NULL);
 * 			#else
 * 				// do nothing ...
 * 			#endif
 */
#define TaskDelete(task_id)		TaskPfDelete(task_id /* TaskHandle */)

/**
 * @brief	Get tick count.
 *
 * @note	On FreeRTOS this function returns the count of ticks since scheduler was started. If a
 * 			milli seconds value is needed then divide the return value by TICK_RATE_TO_MS
 * 			On Linux this function returns the count of ticks in milli seconds since the system was started
 *
 * @return 	Type TickType. The count of ticks.
 */
#define TaskGetTickCount				TaskPfGetTickCount			/* Returns a TickType */

#if (CONFIG_PLATFORM_ESP_IDF)
/**
 * @brief 	Delay a task for a given number of ticks.
 *
 * @note	On FreeRTOS this function takes the number of ticks as input
 * 			On Linux this function takes milliseconds as input
 *
 * 			For FreeRTOS - To calculate the number of ticks equivalent to milliseconds divide the
 * 			milliseconds value by the MACRO TICK_RATE_TO_MS
 * 			Eg. for 500 ms delay
 * 			TaskDelay(500 / TICK_RATE_TO_MS);
 *
 * @param 	ticks_to_delay 	For FreeRTOS - The amount of time, in tick periods,
 * 							that the calling task should block.
 * 			msec_to_delay	For Linux - The amount of time, in milliseconds,
 * 							that the calling task should block.
 */
#define TaskDelay(ticks_to_delay)		TaskPfDelay(ticks_to_delay)
#elif (CONFIG_PLATFORM_SIMULATOR)
#define TaskDelay(msec_to_delay)		TaskPfDelay(msec_to_delay)
#endif

/**
 * @brief	Get task name
 * @return 	The text (human readable) name of the task referenced by the handle task_id
 */
#define TaskGetTaskName(task_id)		TaskPfGetTaskName(task_id)	/* Returns (char *) */

/**
 * @brief	Returns the high water mark of the stack associated with xTask.
 *
 * High water mark is the minimum free stack space there has been (in bytes
 * rather than words as found in vanilla RTOS) since the task started.
 * The smaller the returned number the closer the task has come to overflowing its stack.
 *
 * @note	Works only for FreeRTOS
 *
 * @param task_id Handle of the task associated with the stack to be checked.
 * Set task_id to NULL to check the stack of the calling task.
 *
 * @return 	For FreeRTOS - The smallest amount of free stack space there has been (in bytes
 * 			rather than words as found in vanilla RTOS) since the task referenced by
 * 			task_id was created.
 *
 * 			For Linux - This function returns 0
 */
#define TaskGetStackHighWaterMark(task_id)	TaskPfGetStackHighWaterMark(task_id)/* Returns int */

/**
 * @brief	Suspend a task. TaskSuspend() and TaskResume() is a FreeRTOS feature, it does not work
 * 			if the underlying OS is Linux
 *
 * 			When suspended, a task will never get any microcontroller processing time,
 * 			no matter what its priority.
 *
 * 			Calls to TaskSuspend are not accumulative -
 * 			i.e. calling TaskSuspend () twice on the same task still only requires one
 * 			call to TaskResume () to ready the suspended task.
 *
 * @param task_id 	Handle to the task being suspended.  Passing a NULL
 * 					handle will cause the calling task to be suspended.
 *
 */
#define TaskSuspend(task_id)		TaskPfSuspend(task_id)

/**
 * @brief	Resumes a suspended task. TaskSuspend() and TaskResume() is a FreeRTOS feature, it does not work
 * 			if the underlying OS is Linux
 *
 * 			A task that has been suspended by one or more calls to TaskSuspend ()
 * 			will be made available for running again by a single call to
 * 			TaskResume ().
 *
 * @param task_id Handle to the task being readied.
 *
 */
#define TaskResume(task_id)		TaskPfResume(task_id)

/**********************************************************************************************/
/* Queue related datatypes and macros */

typedef QueueHandle_pf QueueHandle;		/**< Datatype used to work with Queues, returned by QueueCreate */

/**
 * @brief	Creates a new queue instance. This allocates the storage required by the
 * 			new queue and returns a handle for the queue.
 *
 * @param[in] queue_len Type UBaseType. The maximum number of items that the queue can contain.
 *
 * @param[in] item_size Type UBaseType. The number of bytes each item in the queue will require.
 * 					Items are queued by copy, not by reference, so this is the number of bytes
 * 					that will be copied for each posted item.  Each item on the queue must be
 * 					the same size.
 *
 * @return 	If the queue is successfully create then a handle to the newly
 * 			created queue is returned.  If the queue cannot be created then 0 is
 * 			returned.
 */
#define QueueCreate(queue_len /*UBaseType*/, item_size /*UBaseType*/) \
		QueuePfCreate(queue_len, item_size)

/**
 * @brief	Receive an item from a queue.  The item is received by copy so a buffer of
 * 			adequate size must be provided.  The number of bytes copied into the buffer
 * 			was defined when the queue was created.
 *
 * 			Successfully received items are removed from the queue.
 *
 * @param[in] queue Type QueueHandle. The handle to the queue from which the item is to be
 * 					received.
 *
 * @param[out] buffer 	Type void*. Pointer to the buffer into which the received item will
 * 						be copied.
 *
 * @param[in] ticks_to_wait The maximum amount of time the task should block waiting for an item
 * 							to receive should the queue be empty at the time of the call.
 * 							For FreeRTOS this value must be in ticks
 * 							For Linux this value must be in milliseconds
 *
 * @return 	true 			if an item was successfully received from the queue,
 * 			false			queue is invalid
 */
#define QueueReceive(queue /*QueueHandle*/, buffer /*(void*)*/, ticks_to_wait /*uint32_t*/) \
		QueuePfReceive(queue, buffer, ticks_to_wait)

/**
 * @brief	Post an item on a queue.  The item is queued by copy, not by reference.
 * 			This function must not be called from an interrupt service routine.
 *
 * @param[in] queue Type QueueHandle. The handle to the queue on which the item is to be posted.
 *
 * @param[in] item_to_queue Type void *. A pointer to the item that is to be placed on the queue.
 * 							The size of the items the queue will hold was defined when the
 * 							queue was created, so this many bytes will be copied from item_to_queue
 * 							into the queue storage area.
 *
 * @param[in] ticks_to_wait The maximum amount of time the task should block waiting for space to
 * 							become available on the queue, should it already be full.
 * 							The call will return immediately if this is set to 0 and the queue is full.
 * 							This parameter is only used in FreeRTOS. In Linux this parameter is ignored
 *
 * @return 	true 			if the item was successfully posted
 * 			false			if queue is full or queue is invalid
 */
#define QueueSend(queue /*QueueHandle*/, item_to_queue /*const void * const*/, ticks_to_wait /*uint32_t*/) \
		QueuePfSend(queue, item_to_queue, ticks_to_wait)

/**
 * @brief	Post an item to the back of a queue.  It is safe to use this function from
 * 			within an interrupt service routine.
 *
 * 			Items are queued by copy not reference so it is preferable to only
 * 			queue small items, especially when called from an ISR.  In most cases
 * 			it would be preferable to store a pointer to the item being queued.
 *
 * @note	Works only for FreeRTOS
 *
 * @param[in] queue Type QueueHandle. The handle to the queue on which the item is to be posted.
 *
 * @param[in] item_to_queue Type void *. A pointer to the item that is to be placed on the
 * 							queue.  The size of the items the queue will hold was defined when the
 * 							queue was created, so this many bytes will be copied from item_to_queue
 * 							into the queue storage area.
 *
 * @param[out] pxHigherPriorityTaskWoken 	QueueSendFromISR() will set *pxHigherPriorityTaskWoken to pdTRUE
 * 											if sending to the queue caused a task to unblock,
 * 											and the unblocked task has a priority higher than the currently
 * 											running task.  If QueueSendFromISR() sets this value to pdTRUE then
 * 											a context switch should be requested before the interrupt is exited.
 *
 * @return pdTRUE 	if the data was successfully sent to the queue, otherwise other values
 * 					on Linux this function is implemented as a stub and returns 0
 *
 */
#define QueueSendFromISR(queue,item_to_queue,pxHigherPriorityTaskWoken)	QueuePfSendFromISR(queue,item_to_queue,pxHigherPriorityTaskWoken)

/**
 * @brief	Reset a queue back to its original empty state.  true is returned if the
 * 			queue is successfully reset.  false is returned if the queue could not be
 * 			reset because there are tasks blocked on the queue waiting to either
 * 			receive from the queue or send to the queue.
 *
 * @note	Works only for FreeRTOS
 *
 * @param[in] queue Type QueueHandle. The queue to reset
 *
 * @return 		always returns true
 * 				on Linux this function is implemented as a stub and returns 0
 */
#define QueueReset(queue /*QueueHandle*/)		QueuePfReset(queue)

/**
 * @brief	Delete a queue - freeing all the memory allocated for storing of items
 * 			placed on the queue.
 *
 * @param queue Type QueueHandle. A handle to the queue to be deleted.

 */
#define QueueDelete(queue /*QueueHandle*/)	QueuePfDelete(queue)

/**
 * @brief	Return the number of messages stored in a queue.
 *
 * @param[in] queue Type QueueHandle. A handle to the queue being queried.
 *
 * @return The number of messages available in the queue.
 */
#define QueueMessagesWaiting(queue)				QueuePfMessagesWaiting(queue)

/**********************************************************************************************/

typedef SemaphoreMutexHandle_pf	SemaphoreMutexHandle; /**< Datatype used to work with Semaphores, returned by SemaphoreCreateMutex */

/**
  * @brief	Creates a mutex type semaphore object
  *
  * @note	The semaphore function names are derived from FreeRTOS. When using these functions
  * 		with underlying Linux OS, it actually uses the pthread_mutex_xxx functions
  * 		For more information see FreeRTOS and Linux documentation
  *
  * 		Mutexes created using this function can be accessed using the SemaphoreTakeMutex()
  * 		and SemaphoreGiveMutex() macros.
  *
  * @return If the mutex was successfully created then a handle to the created
  * 		semaphore of type SemaphoreMutexHandle is returned.
  * 		If there was not enough heap to allocate the mutex data structures then NULL is returned.
  */
#define	SemaphoreCreateMutex()					SemaphorePfCreateMutex()

 /**
  * @brief 	Delete a mutex type semaphore.  This function must be used with care.
  * 		For example, do not delete a mutex type semaphore if the mutex is held by a task. If done,
  * 		undefined behaviour may occur
  *
  * @note	The semaphore function names are derived from FreeRTOS. When using these functions
  * 		with underlying Linux OS, it actually uses the pthread_mutex_xxx functions
  * 		For more information see FreeRTOS and Linux documentation
  *
  * @param semaphore Type SemaphoreMutexHandle. A handle to the semaphore to be deleted.
  */
#define	SemaphoreDeleteMutex( mutex )			SemaphorePfDeleteMutex( mutex )

 /**
  * @brief 	Call this function to release/unlock a mutex type semaphore. The mutex must have previously been
  * 		created with a call to SemaphoreCreateMutex() and obtained using SemaphoreTakeMutex().
  *
  * @note	The semaphore function names are derived from FreeRTOS. When using these functions
  * 		with underlying Linux OS, it actually uses the pthread_mutex_xxx functions
  * 		For more information see FreeRTOS and Linux documentation
  *
  * @param[in] semaphore 	A handle to the mutex type semaphore being released.  This is the
  * 						handle returned when the mutex type semaphore was created.
  *
  * @return true 	if the mutex was released/unlocked.
  * 		false	if an error occurred.
  */
#define SemaphoreGiveMutex( mutex )				SemaphorePfGiveMutex( mutex )


 /**
  * @brief	Call this function to obtain/lock a mutex type semaphore. The mutex must have previously been
  * 		created with a call to SemaphoreCreateMutex().
  *
  * @note	The semaphore function names are derived from FreeRTOS. When using these functions
  * 		with underlying Linux OS, it actually uses the pthread_mutex_xxx functions
  * 		For more information see FreeRTOS and Linux documentation
  *
  * @param[in] semaphore 		A handle to the semaphore being taken - obtained when the semaphore was created.
  *
  * @param[in] ticks_to_wait 	The time in ticks(in ms) to wait for the semaphore to become available.
  * 							This param is valid only for FreeRTOS. It is ignored on Linux
  *
  * @return true	if the mutex was obtained/locked
  * 		false	if ticks_to_wait expired without the mutex becoming available
  */
#define SemaphoreTakeMutex( mutex, ticks_to_wait )	SemaphorePfTakeMutex( mutex, ticks_to_wait )

 /**********************************************************************************************/

#ifdef __cplusplus
}
#endif
#endif /* THREADING_H_ */
