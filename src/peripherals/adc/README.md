# ADC (Analog To Digital Convertor)
## Overview
An Analog to Digital Convertor, converts a voltage value like 2.35V into a digital number like 830. So we can get a digital data from an analog data.

This module provide APIs to interface any analog device with the system.


## How to use this module from an application

##### Header(s) to include

```
adc_core_channel.h
adc_core.h
```

#### Module Specific Configurations

The following configurations are needed for this module to work.

![](img/adc-config.png)

#### Module Specific APIs

**See the examples for a better understanding**

- Initialization

```c
	/* 
	 * Initialize the ADC with the configured data and start processing.
	 * ADC Channel: ADC_POT_DETECT_AN_GPIO
	 * ADC read interval in seconds: 5secs = 500millisec
	 * ADC read data width in bit: ADC_CORE_WIDTH_BIT_10
	 * ADC voltage attenuation level: ADC_CORE_ATTEN_DB_11 (ADC read upto max Volt 3.3v)
	 */
	core_err res = init_adc_peripheral(ADC_POT_DETECT_AN_GPIO, DELAY_500_MSEC, ADC_CORE_WIDTH_BIT_10,
			ADC_CORE_ATTEN_DB_11);
	if (res != EM_OK) {
		EM_LOGI(TAG, "Failed to init 'ADC_POT_DETECT_AN_GPIO' ADC peripheral");
	}
```

- Read data from the ADC module

```c
	// Declared integer variable to capture ADC raw data
	int raw_data = 0;

	// Declared uint32 variable to capture ADC mV data (mVolt convertion from raw data)     
	uint32_t mV_data = 0;

	// Collect the ADC raw data & mV data
	get_adc_peripheral_data(ADC_POT_DETECT_AN_GPIO, &raw_data, &mV_data);

	// check whether output mV changes as of previous read mV value
	if (mV_data != prev_mV_data) {
		// if last read read mV different from the previous read mV, the this line will print. otherwise continue
		EM_LOGI(TAG, "ADC Readings: Raw Data : %d, Voltage :%.2fV", raw_data, (float )mV_data / 1000);

		// Re-initialize the lase read mV to previous read mV variable
		prev_mV_data = mV_data;
	}
```