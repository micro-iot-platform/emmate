/*
 * File Name: adc_core.h
 * File Path: /emmate/src/adc/adc_core.h
 * Description:
 *
 *  Created on: 28-May-2019
 *      Author: Noyel Seth
 */

#ifndef ADC_CORE_H_
#define ADC_CORE_H_

#include "core_config.h"
#include "core_error.h"
#include "adc_platform.h"

/* ADC attenuation factors */
#define ADC_CORE_ATTEN_0db   	ADC_PF_ATTEN_0db
#define ADC_CORE_ATTEN_2_5db 	ADC_PF_ATTEN_2_5db
#define ADC_CORE_ATTEN_6db   	ADC_PF_ATTEN_6db
#define ADC_CORE_ATTEN_11db  	ADC_PF_ATTEN_11db

/* ADC result bit widths */
#define ADC_CORE_WIDTH_9Bit  	ADC_PF_WIDTH_9Bit
#define ADC_CORE_WIDTH_10Bit 	ADC_PF_WIDTH_10Bit
#define ADC_CORE_WIDTH_11Bit 	ADC_PF_WIDTH_11Bit
#define ADC_CORE_WIDTH_12Bit 	ADC_PF_WIDTH_12Bit

/* ADC unit selection */
#define ADC_CORE_UNIT_1			ADC_PF_UNIT_1
#define ADC_CORE_UNIT_2			ADC_PF_UNIT_2

#if 0
typedef enum {
	ADC_CORE_ATTEN_DB_0 = ADC_PF_ATTEN_DB_0,		/*!<The input voltage of ADC will be reduced to about 1/1 */
	ADC_CORE_ATTEN_DB_2_5 = ADC_PF_ATTEN_DB_2_5,	/*!<The input voltage of ADC will be reduced to about 1/1.34 */
	ADC_CORE_ATTEN_DB_6 = ADC_PF_ATTEN_DB_6,		/*!<The input voltage of ADC will be reduced to about 1/2 */
	ADC_CORE_ATTEN_DB_11 = ADC_PF_ATTEN_DB_11,		/*!<The input voltage of ADC will be reduced to about 1/3.6*/
	ADC_CORE_ATTEN_MAX = ADC_PF_ATTEN_MAX,
} adc_core_atten_t;
#endif
typedef adc_pf_atten_t adc_core_atten_t;

#if 0
typedef enum {
	ADC_CORE_WIDTH_BIT_9 = ADC_PF_WIDTH_BIT_9, 		/*!< ADC capture width is 9Bit*/
	ADC_CORE_WIDTH_BIT_10 = ADC_PF_WIDTH_BIT_10, 	/*!< ADC capture width is 10Bit*/
	ADC_CORE_WIDTH_BIT_11 = ADC_PF_WIDTH_BIT_11, 	/*!< ADC capture width is 11Bit*/
	ADC_CORE_WIDTH_BIT_12= ADC_PF_WIDTH_BIT_12, 	/*!< ADC capture width is 12Bit*/
	ADC_CORE_WIDTH_MAX = ADC_PF_WIDTH_MAX,
} adc_core_bits_width_t;
#endif
typedef adc_pf_bits_width_t adc_core_bits_width_t;

#if 0
typedef enum {
	ADC1_CORE_CHANNEL_0 = ADC1_PF_CHANNEL_0, 	/*!< ADC1 channel 0 */
	ADC1_CORE_CHANNEL_1 = ADC1_PF_CHANNEL_1,    /*!< ADC1 channel 1 */
	ADC1_CORE_CHANNEL_2 = ADC1_PF_CHANNEL_2,    /*!< ADC1 channel 2 */
	ADC1_CORE_CHANNEL_3 = ADC1_PF_CHANNEL_3,    /*!< ADC1 channel 3 */
	ADC1_CORE_CHANNEL_4 = ADC1_PF_CHANNEL_4,    /*!< ADC1 channel 4 */
	ADC1_CORE_CHANNEL_5 = ADC1_PF_CHANNEL_5,    /*!< ADC1 channel 5 */
	ADC1_CORE_CHANNEL_6 = ADC1_PF_CHANNEL_6,    /*!< ADC1 channel 6 */
	ADC1_CORE_CHANNEL_7 = ADC1_PF_CHANNEL_7,    /*!< ADC1 channel 7 */
	ADC1_CORE_CHANNEL_MAX = ADC1_PF_CHANNEL_MAX,
} adc1_core_channel_t;
#endif
typedef adc1_pf_channel_t adc1_core_channel_t;

#if 0
typedef enum {
	ADC2_CORE_CHANNEL_0 = ADC2_PF_CHANNEL_0, 	/*!< ADC2 channel 0 */
    ADC2_CORE_CHANNEL_1 = ADC2_PF_CHANNEL_1,    /*!< ADC2 channel 1 */
	ADC2_CORE_CHANNEL_2 = ADC2_PF_CHANNEL_2,    /*!< ADC2 channel 2 */
	ADC2_CORE_CHANNEL_3 = ADC2_PF_CHANNEL_3,    /*!< ADC2 channel 3 */
	ADC2_CORE_CHANNEL_4 = ADC2_PF_CHANNEL_4,    /*!< ADC2 channel 4 */
	ADC2_CORE_CHANNEL_5 = ADC2_PF_CHANNEL_5,    /*!< ADC2 channel 5 */
	ADC2_CORE_CHANNEL_6 = ADC2_PF_CHANNEL_6,    /*!< ADC2 channel 6 */
	ADC2_CORE_CHANNEL_7 = ADC2_PF_CHANNEL_7,    /*!< ADC2 channel 7 */
	ADC2_CORE_CHANNEL_8 = ADC2_PF_CHANNEL_8,    /*!< ADC2 channel 8 */
	ADC2_CORE_CHANNEL_9 = ADC2_PF_CHANNEL_9,    /*!< ADC2 channel 9 */
	ADC2_CORE_CHANNEL_MAX = ADC2_PF_CHANNEL_MAX,
} adc2_core_channel_t;
#endif
typedef adc2_pf_channel_t adc2_core_channel_t;

#if 0
typedef enum {
	ADC_CORE_CHANNEL_0 = ADC_PF_CHANNEL_0, 	  /*!< ADC channel */
	ADC_CORE_CHANNEL_1 = ADC_PF_CHANNEL_1,    /*!< ADC channel */
	ADC_CORE_CHANNEL_2 = ADC_PF_CHANNEL_2,    /*!< ADC channel */
	ADC_CORE_CHANNEL_3 = ADC_PF_CHANNEL_3,    /*!< ADC channel */
	ADC_CORE_CHANNEL_4 = ADC_PF_CHANNEL_4,    /*!< ADC channel */
	ADC_CORE_CHANNEL_5 = ADC_PF_CHANNEL_5,    /*!< ADC channel */
	ADC_CORE_CHANNEL_6 = ADC_PF_CHANNEL_6,    /*!< ADC channel */
    ADC_CORE_CHANNEL_7 = ADC_PF_CHANNEL_7,    /*!< ADC channel */
    ADC_CORE_CHANNEL_8 = ADC_PF_CHANNEL_8,    /*!< ADC channel */
    ADC_CORE_CHANNEL_9 = ADC_PF_CHANNEL_9,    /*!< ADC channel */
    ADC_CORE_CHANNEL_MAX = ADC_PF_CHANNEL_MAX,
} adc_core_channel_t;
#endif
typedef adc_pf_channel_t adc_core_channel_t;

#if 0
typedef enum {
    ADC_CORE_UNIT_1 = ADC_PF_UNIT_1,        	/*!< SAR ADC 1*/
    ADC_CORE_UNIT_2 = ADC_PF_UNIT_2,          	/*!< SAR ADC 2, not supported yet*/
    ADC_CORE_UNIT_BOTH = ADC_PF_UNIT_BOTH,      /*!< SAR ADC 1 and 2, not supported yet */
    ADC_CORE_UNIT_ALTER = ADC_PF_UNIT_ALTER,    /*!< SAR ADC 1 and 2 alternative mode, not supported yet */
    ADC_CORE_UNIT_MAX = ADC_PF_UNIT_MAX,
} adc_core_unit_t;
#endif
typedef adc_pf_unit_t adc_core_unit_t;

#if 0
typedef enum {
    ADC_CORE_ENCODE_12BIT = ADC_PF_ENCODE_12BIT,  /*!< ADC to I2S data format, [15:12]-channel [11:0]-12 bits ADC data */
    ADC_CORE_ENCODE_11BIT = ADC_PF_ENCODE_11BIT,  /*!< ADC to I2S data format, [15]-1 [14:11]-channel [10:0]-11 bits ADC data */
    ADC_CORE_ENCODE_MAX = ADC_PF_ENCODE_MAX,
} adc_core_i2s_encode_t;
#endif
typedef adc_pf_i2s_encode_t adc_core_i2s_encode_t;

#if 0
typedef enum {
    ADC_CORE_I2S_DATA_SRC_IO_SIG = ADC_PF_I2S_DATA_SRC_IO_SIG, 	/*!< I2S data from GPIO matrix signal  */
    ADC_CORE_I2S_DATA_SRC_ADC = ADC_PF_I2S_DATA_SRC_ADC,    	/*!< I2S data from ADC */
    ADC_CORE_I2S_DATA_SRC_MAX=ADC_PF_I2S_DATA_SRC_MAX,
} adc_core_i2s_source_t;
#endif
typedef adc_pf_i2s_source_t adc_core_i2s_source_t;

/**
 * @brief Get the gpio number of a specific ADC1 channel.
 *
 * @param channel Channel to get the gpio number
 *
 * @param gpio_num output buffer to hold the gpio number
 *
 * @return
 *   - EM_OK if success
 *   - EM_ERR_INVALID_ARG if channal not valid
 */
#define adc_core_adc1_pad_get_io_num(channel, gpio_num)		adc_pf_adc1_pad_get_io_num(channel, gpio_num)

/**
 * @brief Configure ADC1 capture width, meanwhile enable output invert for ADC1.
 * The configuration is for all channels of ADC1
 *
 * @param width_bit Bit capture width for ADC1
 *
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_adc1_config_width(width_bit) 				adc_pf_adc1_config_width(width_bit)

/**
 * @brief Configure ADC capture width.
 * @param adc_unit ADC unit index
 * @param width_bit Bit capture width for ADC unit.
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_set_data_width(adc_unit, width_bit) 			adc_pf_set_data_width(adc_unit, width_bit)

/**
 * @brief Set the attenuation of a particular channel on ADC1, and configure its
 * associated GPIO pin mux.
 *
 * @note For any given channel, this function must be called before the first time
 * adc_core_adc1_get_raw() is called for that channel.
 *
 * @note This function can be called multiple times to configure multiple
 * ADC channels simultaneously. adc_core_adc1_get_raw() can then be called for any configured
 * channel.
 *
 * The default ADC full-scale voltage is 1.1V. To read higher voltages (up to the pin maximum voltage,
 * usually 3.3V) requires setting >0dB signal attenuation for that ADC channel.
 *
 * When VDD_A is 3.3V:
 *
 * - 0dB attenuation (ADC_CORE_ATTEN_DB_0) gives full-scale voltage 1.1V
 * - 2.5dB attenuation (ADC_CORE_ATTEN_DB_2_5) gives full-scale voltage 1.5V
 * - 6dB attenuation (ADC_CORE_ATTEN_DB_6) gives full-scale voltage 2.2V
 * - 11dB attenuation (ADC_CORE_ATTEN_DB_11) gives full-scale voltage 3.9V (see note below)
 *
 * @note The full-scale voltage is the voltage corresponding to a maximum reading (depending on ADC1 configured
 * bit width, this value is: 4095 for 12-bits, 2047 for 11-bits, 1023 for 10-bits, 511 for 9 bits.)
 *
 * @note At 11dB attenuation the maximum voltage is limited by VDD_A, not the full scale voltage.
 *
 * Due to ADC characteristics, most accurate results are obtained within the following approximate voltage ranges:
 *
 * - 0dB attenuaton (ADC_CORE_ATTEN_DB_0) between 100 and 950mV
 * - 2.5dB attenuation (ADC_CORE_ATTEN_DB_2_5) between 100 and 1250mV
 * - 6dB attenuation (ADC_CORE_ATTEN_DB_6) between 150 to 1750mV
 * - 11dB attenuation (ADC_CORE_ATTEN_DB_11) between 150 to 2450mV
 *
 * For maximum accuracy, use the ADC calibration APIs and measure voltages within these recommended ranges.
 *
 * @param channel ADC1 channel to configure
 * @param atten  Attenuation level
 *
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_adc1_config_channel_atten(channel, atten) 		adc_pf_adc1_config_channel_atten(channel, atten)

/**
 * @brief Take an ADC1 reading from a single channel.
 * @note <b>For ESP</b>, When the power switch of SARADC1, SARADC2, HALL sensor and AMP sensor is turned on,
 *       the input of GPIO36 and GPIO39 will be pulled down for about 80ns.
 *       When enabling power for any of these peripherals, ignore input from GPIO36 and GPIO39.
 *       Please refer to section 3.11 of 'ECO_and_Workarounds_for_Bugs_in_ESP32' for the description of this issue.
 *
 * @note Call adc_core_adc1_config_width() before the first time this
 * function is called.
 *
 * @note For any given channel, adc_core_adc1_config_channel_atten(channel)
 * must be called before the first time this function is called. Configuring
 * a new channel does not prevent a previously configured channel from being read.
 *
 * @param  channel ADC1 channel to read
 *
 * @return
 *     - -1: Parameter error
 *     -  Other: ADC1 channel reading.
 */
#define adc_core_adc1_get_raw(channel)					adc_pf_adc1_get_raw(channel)

/**
 * @brief Take an ADC2 reading from a single channel.
 *
 * @param[in]  	channel		ADC2 channel to read
 * @param[in]	width_bit
 * @param[out]	raw_out		Output value
 *
 * @return
 *     - -1: Parameter error
 *     -  Other: ADC2 channel reading.
 */
#define adc_core_adc2_get_raw(channel, width_bit, raw_out)		adc_pf_adc2_get_raw(channel, width_bit, raw_out)

/** @cond */    //Doxygen command to hide deprecated function from API Reference
/*
 * @note When the power switch of SARADC1, SARADC2, HALL sensor and AMP sensor is turned on,
 *       the input of GPIO36 and GPIO39 will be pulled down for about 80ns.
 *       When enabling power for any of these peripherals, ignore input from GPIO36 and GPIO39.
 *       Please refer to section 3.11 of 'ECO_and_Workarounds_for_Bugs_in_ESP32' for the description of this issue.
 *
 * @deprecated This function returns an ADC1 reading but is deprecated due to
 * a misleading name and has been changed to directly call the new function.
 * Use the new function adc_core_adc1_get_raw() instead
 */
#define adc_core_adc1_get_voltage(channel)   			adc_pf_adc1_get_voltage(channel)
/** @endcond */

/**
 * @brief Enable ADC power
 */
#define adc_core_power_on() 			adc_pf_power_on()

/**
 * @brief Power off SAR ADC
 * This function will force power down for ADC
 */
#define adc_core_power_off() 			adc_pf_power_off()

/**
 * @brief Initialize ADC pad
 * @param adc_unit ADC unit index
 * @param channel ADC channel index
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_gpio_init(adc_unit, channel)				adc_pf_gpio_init(adc_unit, channel)

/**
 * @brief Set ADC data invert
 * @param adc_unit ADC unit index
 * @param inv_en whether enable data invert
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_set_data_inv(adc_unit, inv_en)		adc_pf_set_data_inv(adc_unit, inv_en)

/**
 * @brief Set ADC source clock
 * @param clk_div ADC clock divider, ADC clock is divided from APB clock
 * @return
 *     - EM_OK success
 */
#define adc_core_set_clk_div(clk_div)			adc_pf_set_clk_div(clk_div)

/**
 * @brief Set I2S data source
 * @param src I2S DMA data source, I2S DMA can get data from digital signals or from ADC.
 * @return
 *     - EM_OK success
 */
#define adc_core_set_i2s_data_source(src)				adc_pf_set_i2s_data_source(src)

/**
 * @brief Initialize I2S ADC mode
 * @param adc_unit ADC unit index
 * @param channel ADC channel index
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_i2s_mode_init(adc_unit, channel)			adc_pf_i2s_mode_init(adc_unit, channel)

/**
 * @brief Configure ADC1 to be usable by the ULP
 *
 * This function reconfigures ADC1 to be controlled by the ULP.
 * Effect of this function can be reverted using adc_core_adc1_get_raw function.
 *
 * Note that adc1_config_channel_atten, adc_core_adc1_config_width functions need
 * to be called to configure ADC1 channels, before ADC1 is used by the ULP.
 */
#define adc_core_adc1_ulp_enable()		adc_pf_adc1_ulp_enable()

/**
 * @brief Read Hall Sensor
 *
 * @note When the power switch of SARADC1, SARADC2, HALL sensor and AMP sensor is turned on,
 *       the input of GPIO36 and GPIO39 will be pulled down for about 80ns.
 *       When enabling power for any of these peripherals, ignore input from GPIO36 and GPIO39.
 *       Please refer to section 3.11 of 'ECO_and_Workarounds_for_Bugs_in_ESP32' for the description of this issue.
 *
 * @note The Hall Sensor uses channels 0 and 3 of ADC1. Do not configure
 * these channels for use as ADC channels.
 *
 * @note The ADC1 module must be enabled by calling
 *       adc_core_adc1_config_width() before calling adc_core_hall_sensor_read(). ADC1
 *       should be configured for 12 bit readings, as the hall sensor
 *       readings are low values and do not cover the full range of the
 *       ADC.
 *
 * @return The hall sensor reading.
 */
#define adc_core_hall_sensor_read()				adc_pf_hall_sensor_read()

/**
 * @brief Get the gpio number of a specific ADC2 channel.
 *
 * @param channel Channel to get the gpio number
 *
 * @param gpio_num output buffer to hold the gpio number
 *
 * @return
 *   - EM_OK if success
 *   - EM_ERR_INVALID_ARG if channal not valid
 */
#define adc_core_adc2_pad_get_io_num(channel, gpio_num)		adc_pf_adc2_pad_get_io_num(channel, gpio_num)

/**
 * @brief Configure the ADC2 channel, including setting attenuation.
 *
 * @note This function also configures the input GPIO pin mux to
 * connect it to the ADC2 channel. It must be called before calling
 * ``adc_core_adc2_get_raw()`` for this channel.
 *
 * The default ADC full-scale voltage is 1.1V. To read higher voltages (up to the pin maximum voltage,
 * usually 3.3V) requires setting >0dB signal attenuation for that ADC channel.
 *
 * When VDD_A is 3.3V:
 *
 * - 0dB attenuaton (ADC_CORE_ATTEN_0db) gives full-scale voltage 1.1V
 * - 2.5dB attenuation (ADC_CORE_ATTEN_2_5db) gives full-scale voltage 1.5V
 * - 6dB attenuation (ADC_CORE_ATTEN_6db) gives full-scale voltage 2.2V
 * - 11dB attenuation (ADC_CORE_ATTEN_11db) gives full-scale voltage 3.9V (see note below)
 *
 * @note The full-scale voltage is the voltage corresponding to a maximum reading
 * (depending on ADC2 configured bit width, this value is: 4095 for 12-bits, 2047
 * for 11-bits, 1023 for 10-bits, 511 for 9 bits.)
 *
 * @note At 11dB attenuation the maximum voltage is limited by VDD_A, not the full scale voltage.
 *
 * @param channel ADC2 channel to configure
 * @param atten  Attenuation level
 *
 * @return
 *     - EM_OK success
 *     - EM_ERR_INVALID_ARG Parameter error
 */
#define adc_core_adc2_config_channel_atten(channel, atten)		adc_pf_adc2_config_channel_atten(channel, atten)

/**
 * @brief Take an ADC2 reading on a single channel
 *
 * @note <b>For ESP</b>, When the power switch of SARADC1, SARADC2, HALL sensor and AMP sensor is turned on,
 *       the input of GPIO36 and GPIO39 will be pulled down for about 80ns.
 *       When enabling power for any of these peripherals, ignore input from GPIO36 and GPIO39.
 *       Please refer to section 3.11 of 'ECO_and_Workarounds_for_Bugs_in_ESP32' for the description of this issue.
 *
 * @note <b>For ESP</b>, for a given channel, ``adc_core_adc2_config_channel_atten()``
 * must be called before the first time this function is called. If Wi-Fi is started via ``wifi_start``, this
 * function will always fail with ``EM_ERR_TIMEOUT``.
 *
 * @param  channel ADC2 channel to read
 *
 * @param width_bit Bit capture width for ADC2
 *
 * @param raw_out the variable to hold the output data.
 *
 * @return
 *     - EM_OK if success
 *     - EM_ERR_TIMEOUT the WIFI is started, using the ADC2
 */
#define adc_core_adc2_get_raw(channel, width_bit, raw_out)				adc_pf_adc2_get_raw(channel, width_bit, raw_out)

/**
 *  @brief Output ADC2 reference voltage to gpio 25 or 26 or 27
 *
 *  @note <b>For ESP</b>, This function utilizes the testing mux exclusive to ADC 2 to route the
 *  reference voltage one of ADC2's channels. Supported gpios are gpios
 *  25, 26, and 27. This refernce voltage can be manually read from the pin
 *  and used in the esp_adc_cal component.
 *
 *  @param[in]  gpio    GPIO number (gpios 25,26,27 supported)
 *
 *  @return
 *        - EM_OK: v_ref successfully routed to selected gpio
 *        - EM_ERR_INVALID_ARG: Unsupported gpio
 */
#define adc_core_adc2_vref_to_gpio(gpio)					adc_pf_adc2_vref_to_gpio(gpio)



/**
 * @brief	Initialize the ADC with the configured data and start processing
 *
 * @param[in]	adc_channel_gpio		ADC GPIO to read
 *
 * @param[in]	adc_read_interval_t		Time interval between ADC read in milliseconds.
 *
 * @param[in]	bits_width_t			Bit capture width for ADC
 * @note The full-scale voltage is the voltage corresponding to a maximum reading
 * (depending on ADC2 configured bit width, this value is: 4095 for 12-bits, 2047
 * for 11-bits, 1023 for 10-bits, 511 for 9 bits.)
 *
 * @param[in]	atten_t					Attenuation level
 * @note The default ADC full-scale voltage is 1.1V. To read higher voltages (up to the pin maximum voltage,
 * 										usually 3.3V) requires setting >0dB signal attenuation for that ADC channel.
 *
 * @return
 *		- EM_OK				if success
 *		- EM_FAIL				if failed to start process
 *		- EM_ERR_NO_MEM		if failed to allocate memory for ADC configuration
 *		- EM_ERR_INVALID_ARG	if adc GPIO channel not valid
 */
em_err init_adc_peripheral(uint8_t adc_channel_gpio, uint16_t adc_read_interval_t, adc_core_bits_width_t bits_width_t,
		adc_core_atten_t atten_t);

/**
 * @brief	De-initialize the ADC and stop the processing
 *
 * @param[in]	adc_channel_gpio	ADC GPIO to stop read & de-initialize
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if adc GPIO channel not valid
 */
em_err deinit_adc_peripheral(uint8_t adc_channel_gpio);

/**
 * @brief	Get the read ADC read value in RAW & mV format.
 *
 * @param[in]	adc_channel_gpio	ADC GPIO to get read ADC data
 *
 * @param[out]	raw_data			get Raw ADC data from configured ADC GPIO
 *
 * @param[out]	mV_data				get mV data from converting the read ADC Raw data.
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if adc GPIO channel not valid
 */
em_err get_adc_peripheral_data(uint8_t adc_channel_gpio, int *raw_data, uint32_t *mV_data);


/**
 * @brief	Paused the ADC read processing
 *
 * @param[in]	adc_channel_gpio	ADC GPIO to pause the adc read
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if adc GPIO channel not valid
 */
em_err pause_adc_peripheral_read(uint8_t adc_channel_gpio);


/**
 * @brief	Resumed the ADC read processing
 *
 * @param[in]	adc_channel_gpio	ADC GPIO to resume the adc read
 *
 * @return
 *		- EM_OK				if success
 *		- EM_ERR_INVALID_ARG	if adc GPIO channel not valid
 */
em_err resume_adc_peripheral_read(uint8_t adc_channel_gpio);

em_err adc_core_initialize_adc();

#endif /* ADC_CORE_H_ */
