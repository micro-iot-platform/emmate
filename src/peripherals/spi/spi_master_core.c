/*
 * File Name: spi_master_core.c
 *
 * Description:
 *
 *  Created on: 28-Sep-2019
 *      Author: Noyel Seth
 */

#include "spi_master_core.h"
#include "gll.h"
#include "threading.h"

static SemaphoreMutexHandle mutex_handle = NULL;

/**
 *
 */
em_err spi_master_bus_config(uint8_t host, int miso_io_num, int mosi_io_num, int sclk_io_num,
		int max_transfer_sz) {
	em_err ret = EM_FAIL;
	if (mutex_handle == NULL) {
		mutex_handle = SemaphoreCreateMutex();
	}
	spi_bus_config buscfg;
	buscfg.spi_host = host;
	buscfg.miso_io_num = miso_io_num;
	buscfg.mosi_io_num = mosi_io_num;
	buscfg.sclk_io_num = sclk_io_num;
	buscfg.max_transfer_sz = max_transfer_sz;
	ret = spi_master_pf_bus_config(&buscfg);
	if (ret == EM_OK) {
		buscfg.stat = SPI_BUS_READY;
	}
	return ret;
}

/**
 *
 */
em_err spi_master_device_interface_config(uint8_t host, int clock_speed_hz, int spics_io_num,
		spi_pf_device_handle_t *spi_drv_handle) {
	em_err ret = EM_FAIL;
	ret = spi_master_pf_device_interface_config(host, clock_speed_hz, spics_io_num, spi_drv_handle);
	return ret;
}

/**
 *
 */
em_err spi_master_device_transmit(spi_pf_device_handle_t spi_drv_handle, void * tx_data, void* rx_data,
		size_t tx_length_in_byte) {
	em_err ret = EM_FAIL;
	SemaphoreTakeMutex(mutex_handle, THREADING_MAX_DELAY/TICK_RATE_TO_MS);
	ret = spi_master_pf_device_transmit(spi_drv_handle, tx_data, rx_data, tx_length_in_byte);
	SemaphoreGiveMutex(mutex_handle);
	return ret;
}

/**
 *
 */
em_err spi_master_remove_device_from_bus(spi_pf_device_handle_t spi_drv_handle) {
	em_err ret = EM_FAIL;
	ret = spi_master_pf_remove_device_from_bus(spi_drv_handle);
	return ret;
}

/**
 *
 */
em_err spi_master_bus_free(uint8_t host) {
	em_err ret = EM_FAIL;
	ret = spi_master_pf_bus_free(host);
	if(ret == EM_OK){
		SemaphoreDeleteMutex(mutex_handle);
	}
	return ret;
}

