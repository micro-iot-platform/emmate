/*
 * File Name: spi_slave_core.c
 *
 * Description:
 *
 *  Created on: 30-Sep-2019
 *      Author: Noyel Seth
 */

#include "spi_slave_core.h"

/**
 *
 */
em_err spi_slave_interface_config(uint8_t host, int miso_io_num, int mosi_io_num, int sclk_io_num, int cs_io_num) {
	em_err ret = EM_FAIL;

	//Initialize SPI slave interface
	ret = spi_slave_pf_device_interface_config(host, miso_io_num, mosi_io_num, sclk_io_num, cs_io_num);

	return ret;
}

/**
 *
 */
em_err spi_slave_device_transmit(uint8_t host, void * tx_data, void* rx_data, size_t tx_length_in_byte) {
	em_err ret = EM_FAIL;

	ret = spi_slave_device_transmit(host, tx_data, rx_data, tx_length_in_byte);

	return ret;
}

/**
 *
 */
em_err spi_slave_bus_free(uint8_t host) {
	em_err ret = EM_FAIL;
	ret = spi_slave_pf_bus_free(host);
	return ret;
}
