/*
 * File Name: spi_slave_core.h
 *
 * Description:
 *
 *  Created on: 30-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef SPI_SLAVE_CORE_H_
#define SPI_SLAVE_CORE_H_

#include "spi_core_common.h"
#include "spi_slave_platform.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialize a SPI bus as a slave interface
 *
 * @warning For now, only supports HSPI and VSPI.
 *
 * @param host SPI peripheral to use as a SPI slave interface
 * @param miso_io_num	GPIO pin for Master In Slave Out
 *
 * @param mosi_io_num	GPIO pin for Master Out Slave In
 *
 * @param sclk_io_num	GPIO pin for Spi CLocK signal
 *
 * @param cs_io_num		CS GPIO pin for this device
 *
 * @return
 *         - EM_ERR_INVALID_ARG   if configuration is invalid
 *         - EM_ERR_INVALID_STATE if host already is in use
 *         - EM_ERR_NO_MEM        if out of memory
 *         - EM_OK                on success
 */
em_err spi_slave_interface_config(uint8_t host, int miso_io_num, int mosi_io_num, int sclk_io_num, int cs_io_num);

/**
 * @brief Do a SPI transaction
 *
 * Essentially does the same as spi_slave_queue_trans followed by spi_slave_get_trans_result. Do
 * not use this when there is still a transaction queued that hasn't been finalized
 * using spi_slave_get_trans_result.
 *
 * @param host SPI peripheral to that is acting as a slave
 *
 * @param tx_data	Pointer to transmit buffer, or NULL
 *
 * @param rx_data	Pointer to receive buffer, or NULL
 *
 * @param tx_length_in_byte		length of the tx-data in byte
 *
 * @return
 *         - EM_ERR_INVALID_ARG   if parameter is invalid
 *         - EM_OK                on success
 */
em_err spi_slave_device_transmit(uint8_t host, void * tx_data, void* rx_data, size_t tx_length_in_byte);

/**
 * @brief Free a SPI bus claimed as a SPI slave interface
 *
 * @param host SPI peripheral to free
 *
 * @return
 *         - EM_ERR_INVALID_ARG   if parameter is invalid
 *         - EM_ERR_INVALID_STATE if not all devices on the bus are freed
 *         - EM_OK                on success
 */
em_err spi_slave_bus_free(uint8_t host);


#ifdef __cplusplus
}
#endif

#endif /* SPI_SLAVE_CORE_H_ */
