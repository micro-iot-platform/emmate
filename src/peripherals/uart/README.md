# UART (Universal Asynchronous Receiver/Transmitter)
## Overview
UART is the basic peripheral for any embedded system. 

## How to use this module from an application

##### Header(s) to include

```
uart_helper_api.h
```

#### Module Specific Configurations

The following configurations are needed for this module to work.

![](img/uart-config.png)

#### Module Specific APIs

**See the examples for a better understanding**

- Initialization

```c
	core_err res = init_uart_driver(UART_PORT_NUM, UART_BAUD_RATE, UART_TX_PIN, UART_RX_PIN, UART_8N1,
			uart_transactor_cb);
	if (res != EM_OK) {
		EM_LOGE(TAG, "failed to initialize Uart");
		return;
	}
```

- Write Bytes

```c
		int res = uart_driver_write_bytes(UART_PORT_NUM, MESSAGE, strlen(MESSAGE));
		if (res == -1){
			EM_LOGE(TAG, "failed to write bytes via Uart");
			return;
		}
```

- Read Bytes

```
The EmMate's UART module automatically read the Rx data and stores it in a buffer and returns the data to the callback registered during initialization
```


- De-initialization

```c
			core_err res = deinit_uart_driver(UART_PORT_NUM);
			if (res != EM_OK) {
				EM_LOGE(TAG, "failed to deinitialize Uart");
			}

```