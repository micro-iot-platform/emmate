/*
 * File Name: led_pwm_driver.h
 *
 * Description:
 *
 *  Created on: 26-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef LED_PWM_DRIVER_H_
#define LED_PWM_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "led_pwm_platform_driver.h"

typedef enum {
	LED_PWM_TIMER_1_BIT = LED_PWM_PF_TIMER_1_BIT, /*!< LEDC PWM duty resolution of  1 bits */
	LED_PWM_TIMER_2_BIT = LED_PWM_PF_TIMER_2_BIT, /*!< LEDC PWM duty resolution of  2 bits */
	LED_PWM_TIMER_3_BIT = LED_PWM_PF_TIMER_3_BIT, /*!< LEDC PWM duty resolution of  3 bits */
	LED_PWM_TIMER_4_BIT = LED_PWM_PF_TIMER_4_BIT, /*!< LEDC PWM duty resolution of  4 bits */
	LED_PWM_TIMER_5_BIT = LED_PWM_PF_TIMER_5_BIT, /*!< LEDC PWM duty resolution of  5 bits */
	LED_PWM_TIMER_6_BIT = LED_PWM_PF_TIMER_6_BIT, /*!< LEDC PWM duty resolution of  6 bits */
	LED_PWM_TIMER_7_BIT = LED_PWM_PF_TIMER_7_BIT, /*!< LEDC PWM duty resolution of  7 bits */
	LED_PWM_TIMER_8_BIT = LED_PWM_PF_TIMER_8_BIT, /*!< LEDC PWM duty resolution of  8 bits */
	LED_PWM_TIMER_9_BIT = LED_PWM_PF_TIMER_9_BIT, /*!< LEDC PWM duty resolution of  9 bits */
	LED_PWM_TIMER_10_BIT = LED_PWM_PF_TIMER_10_BIT, /*!< LEDC PWM duty resolution of 10 bits */
	LED_PWM_TIMER_11_BIT = LED_PWM_PF_TIMER_11_BIT, /*!< LEDC PWM duty resolution of 11 bits */
	LED_PWM_TIMER_12_BIT = LED_PWM_PF_TIMER_12_BIT, /*!< LEDC PWM duty resolution of 12 bits */
	LED_PWM_TIMER_13_BIT = LED_PWM_PF_TIMER_13_BIT, /*!< LEDC PWM duty resolution of 13 bits */
	LED_PWM_TIMER_14_BIT = LED_PWM_PF_TIMER_14_BIT, /*!< LEDC PWM duty resolution of 14 bits */
	LED_PWM_TIMER_15_BIT = LED_PWM_PF_TIMER_15_BIT, /*!< LEDC PWM duty resolution of 15 bits */
	LED_PWM_TIMER_16_BIT = LED_PWM_PF_TIMER_16_BIT, /*!< LEDC PWM duty resolution of 16 bits */
	LED_PWM_TIMER_17_BIT = LED_PWM_PF_TIMER_17_BIT, /*!< LEDC PWM duty resolution of 17 bits */
	LED_PWM_TIMER_18_BIT = LED_PWM_PF_TIMER_18_BIT, /*!< LEDC PWM duty resolution of 18 bits */
	LED_PWM_TIMER_19_BIT = LED_PWM_PF_TIMER_19_BIT, /*!< LEDC PWM duty resolution of 19 bits */
	LED_PWM_TIMER_20_BIT = LED_PWM_PF_TIMER_20_BIT, /*!< LEDC PWM duty resolution of 20 bits */
	LED_PWM_TIMER_BIT_MAX = LED_PWM_PF_TIMER_BIT_MAX
} led_pwm_timer_bit_t;

typedef enum {
	LED_PWM_TIMER_0 = LED_PWM_PF_TIMER_0, /*!< LEDC timer 0 */
	LED_PWM_TIMER_1 = LED_PWM_PF_TIMER_1, /*!< LEDC timer 1 */
	LED_PWM_TIMER_2 = LED_PWM_PF_TIMER_2, /*!< LEDC timer 2 */
	LED_PWM_TIMER_3 = LED_PWM_PF_TIMER_3, /*!< LEDC timer 3 */
	LED_PWM_TIMER_MAX = LED_PWM_PF_TIMER_MAX,
} led_pwm_timer_t;

typedef enum {
	LED_PWM_CHANNEL_0 = LED_PWM_PF_CHANNEL_0, /*!< LEDC channel 0 */
	LED_PWM_CHANNEL_1 = LED_PWM_PF_CHANNEL_1, /*!< LEDC channel 1 */
	LED_PWM_CHANNEL_2 = LED_PWM_PF_CHANNEL_2, /*!< LEDC channel 2 */
	LED_PWM_CHANNEL_3 = LED_PWM_PF_CHANNEL_3, /*!< LEDC channel 3 */
	LED_PWM_CHANNEL_4 = LED_PWM_PF_CHANNEL_4, /*!< LEDC channel 4 */
	LED_PWM_CHANNEL_5 = LED_PWM_PF_CHANNEL_5, /*!< LEDC channel 5 */
	LED_PWM_CHANNEL_6 = LED_PWM_PF_CHANNEL_6, /*!< LEDC channel 6 */
	LED_PWM_CHANNEL_7 = LED_PWM_PF_CHANNEL_7, /*!< LEDC channel 7 */
	LED_PWM_CHANNEL_MAX = LED_PWM_PF_CHANNEL_MAX,
} led_pwm_channel_t;

/**
 * @brief LED PWM timer configuration
 *        Configure LED PWM timer with the given source timer, frequency(Hz), duty_resolution
 *
 * @param  duty_res 	LED PWM channel duty resolution
 *
 * @param  freq_hz		LED PWM frequency (Hz)
 *
 * @param  timer		LED PWM timer source of timer (0 - 3)
 *
 * @return
 *     - EM_OK Success
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_FAIL Can not find a proper pre-divider number base on the given frequency and the current duty_resolution.
 */
em_err led_pwm_driver_configure_timer(led_pwm_timer_bit_t duty_res, uint32_t freq_hz, led_pwm_timer_t timer);

/**
 * @brief LED PWM channel configuration
 *        Configure LED PWM channel with the given channel, LED PWM duty resolution, output gpio_num, source timer
 *
 * @param channel 	LED PWM channel led_pwm_channel_t (0 - 7)
 *
 * @param duty_percentage		LED PWM channel duty cycle, the range of duty setting is [0-100%]
 * @note
 * 					3.3v|``|       |``|       |``|       |``|       |``|
 * 	0% duty-cycle		|  |       |  |       |  |       |  |       |  |
 * 						|  |       |  |       |  |       |  |       |  |
 * 					0v	|---=======----=======----=======----=======----=======------------
 *
 *
 * 					3.3v|````|    |````|    |````|    |````|    |````|
 * 	50% duty-cycle		|    |    |    |    |    |    |    |    |    |
 * 						|    |    |    |    |    |    |    |    |    |
 * 					0v	|-----====------====------====------====------====-----------------
 *
 *
 * 					3.3v|```````|  |```````|  |```````|  |```````|  |```````|
 * 	100% duty-cycle		|       |  |       |  |       |  |       |  |       |
 * 						|       |  |       |  |       |  |       |  |       |
 * 					0v	|--------==---------==---------==---------==---------==---------
 *
 * @param gpio_num	LED PWM output gpio_num
 *
 * @param timer	Select the LED PWM timer source of led_pwm_timer_t (0 - 3)
 *
 * @return
 *     - EM_OK Success
 *     - EM_FAIL LED PWM timer source not configured
 *     - EM_ERR_INVALID_ARG Parameter error
 */
em_err led_pwm_driver_configure_channel(led_pwm_channel_t channel, uint32_t duty_percentage, int gpio_num,
		led_pwm_timer_t timer);

/**
 * @brief LED PWM channel configuration
 *        Configure LED PWM channel with the given channel, LED PWM duty resolution, output gpio_num, source timer
 *
 * @param channel 	LED PWM channel led_pwm_channel_t (0 - 7)
 *
 * @param decimal_code		LED PWM channel's Decimal Code, the range [0-255]
 *
 * @param gpio_num	LED PWM output gpio_num
 *
 * @param timer	Select the LED PWM timer source of led_pwm_timer_t (0 - 3)
 *
 * @return
 *     - EM_OK Success
 *     - EM_FAIL LED PWM timer source not configured
 *     - EM_ERR_INVALID_ARG Parameter error
 */
em_err led_pwm_driver_configure_channel_with_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code, int gpio_num,
		led_pwm_timer_t timer);

/**
  * @brief LED PWM stop.
  *        Disable LED PWM output, and set idle level
  *
  * @param  channel LED PWM channel (0-7), select from led_pwm_channel_t
  *
  * @return
  *     - EM_OK Success
  *     - EM_ERR_INVALID_ARG Parameter error
  */
em_err led_pwm_driver_stop(led_pwm_channel_t channel);

/**
 * @brief A thread-safe API to set duty for LED PWM channel and return when duty updated.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel 	LED PWM channel (0-7), select from led_pwm_channel_t
 *
 * @param duty_percentage	LED PWM channel duty cycle, the range of duty setting is [0-100%]
 *
 * @return
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_OK Success
 */
em_err led_pwm_driver_set_duty_cycle(led_pwm_channel_t channel, uint32_t duty_percentage);

/**
 * @brief A thread-safe API to set duty for LED PWM channel and return when duty updated.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel 	LED PWM channel (0-7), select from led_pwm_channel_t
 *
 * @param decimal_code		LED PWM channel's Decimal Code, the range [0-255]
 *
 * @return
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_OK Success
 */
em_err led_pwm_driver_set_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code);

/**
 * @brief A thread-safe API to set and start LED PWM fade function.
 * @note  Call led_pwm_driver_fade_func_install() once before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel LED PWM channel index (0-7), select from led_pwm_channel_t
 *
 * @param duty_percentage		LED PWM channel duty cycle, the range of duty setting is [0-100%]
 *
 * @param scale Controls the increase or decrease step scale.
 *
 * @param cycle_num increase or decrease the duty every cycle_num cycles

 * @return
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_OK Success
 *     - EM_ERR_INVALID_STATE Fade function not installed.
 *     - EM_FAIL Fade function init error
 */
em_err led_pwm_driver_set_fade_step_and_start(led_pwm_channel_t channel, uint32_t duty_percentage, uint32_t scale,
		uint32_t cycle_num);

/**
 * @brief A thread-safe API to set and start LED PWM fade function.
 * @note  Call led_pwm_driver_fade_func_install() once before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel LED PWM channel index (0-7), select from led_pwm_channel_t
 *
 * @param decimal_code		LED PWM channel's Decimal Code, the range [0-255]
 *
 * @param scale Controls the increase or decrease step scale.
 *
 * @param cycle_num increase or decrease the duty every cycle_num cycles

 * @return
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_OK Success
 *     - EM_ERR_INVALID_STATE Fade function not installed.
 *     - EM_FAIL Fade function init error
 */
em_err led_pwm_driver_set_fade_step_and_start_with_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code,
		uint32_t scale, uint32_t cycle_num);

/**
 * @brief A thread-safe API to set and start LED PWM fade function, with a limited time.
 * @note  Call led_pwm_driver_fade_func_install() once, before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel LED PWM channel index (0-7), select from led_pwm_channel_t
 *
 * @param duty_percentage	LED PWM channel duty cycle, the range of duty setting is [0-100%]
 *
 * @param max_fade_time_ms The maximum time of the fading ( ms ).
 *
 * @return
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_OK Success
 *     - EM_ERR_INVALID_STATE Fade function not installed.
 *     - EM_FAIL Fade function init error
 */
em_err led_pwm_driver_set_fade_time_and_start(led_pwm_channel_t channel, uint32_t duty_percentage,
		uint32_t max_fade_time_ms);

/**
 * @brief A thread-safe API to set and start LED PWM fade function, with a limited time.
 * @note  Call led_pwm_driver_fade_func_install() once, before calling this function.
 * @note  If a fade operation is running in progress on that channel, the driver would not allow it to be stopped.
 *        Other duty operations will have to wait until the fade operation has finished.
 *
 * @param channel LED PWM channel index (0-7), select from led_pwm_channel_t
 *
 * @param decimal_code		LED PWM channel's Decimal Code, the range [0-255]
 *
 * @param max_fade_time_ms The maximum time of the fading ( ms ).
 *
 * @return
 *     - EM_ERR_INVALID_ARG Parameter error
 *     - EM_OK Success
 *     - EM_ERR_INVALID_STATE Fade function not installed.
 *     - EM_FAIL Fade function init error
 */
em_err led_pwm_driver_set_fade_time_and_start_with_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code,
		uint32_t max_fade_time_ms);

/**
 * @brief Install LED PWM fade function. This function will help to execute LED PWM Fade Operation.
 *
 * @return
 *     - EM_OK Success
 *     - EM_ERR_INVALID_STATE Fade function already installed.
 */
em_err led_pwm_driver_fade_func_install();

/**
 * @brief Un-install LED PWM fade functionality.
 *
 *  @return
 *     - EM_OK Success
 */
em_err led_pwm_driver_fade_func_uninstall();

#ifdef __cplusplus
}
#endif

#endif /* LED_PWM_DRIVER_H_ */
