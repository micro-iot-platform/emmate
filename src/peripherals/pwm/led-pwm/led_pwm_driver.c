/*
 * File Name: led_pwm_driver.c
 *
 * Description:
 *
 *  Created on: 26-Sep-2019
 *      Author: Noyel Seth
 */

#include "led_pwm_driver.h"

#define MAX_DECIMAL_CODE	255
#define MIN_DECIMAL_CODE	0

/**
 *
 */
em_err led_pwm_driver_configure_timer(led_pwm_timer_bit_t duty_res, uint32_t freq_hz, led_pwm_timer_t timer) {
	em_err res = EM_FAIL;

	res = led_pwm_pf_driver_configure_timer(duty_res, freq_hz, timer);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_configure_channel(led_pwm_channel_t channel, uint32_t duty_percentage, int gpio_num,
		led_pwm_timer_t timer) {
	em_err res = EM_FAIL;

	uint32_t max_duty = led_pwm_pf_driver_get_max_duty_by_timer(timer);
	if(max_duty != 0){

		uint32_t target_duty = ((float)max_duty *  ((float)duty_percentage / 100));

		res = led_pwm_pf_driver_configure_channel(channel, target_duty, gpio_num, timer);
	}

	return res;
}

/**
 *
 */
em_err led_pwm_driver_configure_channel_with_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code, int gpio_num,
		led_pwm_timer_t timer) {
	em_err res = EM_FAIL;

	uint32_t duty_percentage = 0;

	duty_percentage = ((float)decimal_code / MAX_DECIMAL_CODE) * (float)100;

	res = led_pwm_driver_configure_channel(channel, duty_percentage, gpio_num, timer);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_stop(led_pwm_channel_t channel) {
	em_err res = EM_FAIL;
	res = led_pwm_pf_driver_stop(channel);
	return res;
}

/**
 *
 */
em_err led_pwm_driver_set_duty_cycle(led_pwm_channel_t channel, uint32_t duty_percentage) {
	em_err res = EM_FAIL;

	uint32_t max_duty = led_pwm_pf_driver_get_max_duty_by_channel(channel);

	uint32_t target_duty = ((float)max_duty *  ((float)duty_percentage / 100));

	res = led_pwm_pf_driver_set_duty_cycle(channel, target_duty);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_set_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code) {
	em_err res = EM_FAIL;

	uint32_t duty_percentage = 0;

	duty_percentage = ((float)decimal_code / MAX_DECIMAL_CODE) * (float)100;

	res = led_pwm_driver_set_duty_cycle(channel, duty_percentage);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_set_fade_step_and_start(led_pwm_channel_t channel, uint32_t duty_percentage, uint32_t scale,
		uint32_t cycle_num) {
	em_err res = EM_FAIL;

	uint32_t max_duty = led_pwm_pf_driver_get_max_duty_by_channel(channel);

	uint32_t target_duty = ((float)max_duty *  ((float)duty_percentage / 100));

	res = led_pwm_pf_driver_set_fade_step_and_start(channel, target_duty, scale, cycle_num);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_set_fade_step_and_start_with_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code,
		uint32_t scale, uint32_t cycle_num) {

	em_err res = EM_FAIL;

	uint32_t duty_percentage = 0;

	duty_percentage = ((float) decimal_code / MAX_DECIMAL_CODE) * (float) 100;

	res = led_pwm_driver_set_fade_step_and_start(channel, duty_percentage, scale, cycle_num);

	return res;

}

/**
 *
 */
em_err led_pwm_driver_set_fade_time_and_start(led_pwm_channel_t channel, uint32_t duty_percentage,
		uint32_t max_fade_time_ms) {
	em_err res = EM_FAIL;

	uint32_t max_duty = led_pwm_pf_driver_get_max_duty_by_channel(channel);

	uint32_t target_duty = ((float)max_duty *  ((float)duty_percentage / 100));

	res = led_pwm_pf_driver_set_fade_time_and_start(channel, target_duty, max_fade_time_ms);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_set_fade_time_and_start_with_decimal_code(led_pwm_channel_t channel, uint8_t decimal_code,
		uint32_t max_fade_time_ms) {

	em_err res = EM_FAIL;

	uint32_t duty_percentage = 0;

	duty_percentage = ((float) decimal_code / MAX_DECIMAL_CODE) * (float) 100;

	res = led_pwm_driver_set_fade_time_and_start(channel, duty_percentage, max_fade_time_ms);

	return res;
}

/**
 *
 */
em_err led_pwm_driver_fade_func_install() {
	em_err res = EM_FAIL;

	res = led_pwm_pf_driver_fade_func_install();

	return res;
}

/**
 *
 */
em_err led_pwm_driver_fade_func_uninstall() {
	em_err res = EM_FAIL;

	res = led_pwm_pf_driver_fade_func_uninstall();

	return res;
}

