# LED PWM (Pulse Width Modulation for LED)
## Overview
This module is explit for ESP32. It internally uses `ESP IDF ledc component`

This module provides simple API to dimm, fade and change color of LEDs


## How to use this module from an application

##### Header(s) to include

```
led_pwm_driver.h
```

#### Module Specific Configurations

The following configurations are needed for this module to work.

![](img/led-pwm-config.png)

#### Module Specific APIs

**See the examples for a better understanding**

- Initialization

```c
	/* initializations LED PWM*/
	core_err res = led_pwm_driver_configure_timer(LED_PWM_TIMER_13_BIT, 5000, LED_PWM_TIMER_0);
	if (res != EM_OK) {
		EM_LOGE(TAG, "failed to initialize LED PWM Timer driver");
	}

	/*
	 * Install & activate LED PWM functionality
	 */
	res = led_pwm_driver_fade_func_install();
	if (res != EM_OK) {
		EM_LOGE(TAG, "failed to initialize LED PWM fade functionality");
	}

	/*
	 * Configure LED PWM Channel 0 for SYSTEM_HMI_LED_MONO_RED
	 */
	res = led_pwm_driver_configure_channel(LED_PWM_CHANNEL_0, 0, SYSTEM_HMI_LED_MONO_RED, LED_PWM_TIMER_0);
	if (res != EM_OK) {
		EM_LOGE(TAG, "failed to initialize LED PWM Channel");
	}
```

- Fade LED

```c
	/*
	 * Set & Start LED PWM fade Out with PWM Duty-cycle for LED_PWM_CHANNEL_0 for 1sec
	 */
	core_err res = led_pwm_driver_set_fade_time_and_start(LED_PWM_CHANNEL_0, 100, DELAY_1_SEC);
	if (res != EM_OK) {
		EM_LOGE(TAG, "failed to set fade_time to configured LED PWM Channel-0");
	}
```