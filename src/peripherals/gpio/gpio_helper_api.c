/*
 * File Name: gpio_helper_api.c
 *
 * Description:
 *
 *  Created on: 24-Aug-2019
 *      Author: Noyel Seth
 */

#include "gpio_helper_api.h"
#include "string.h"
#include "som.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG  LTAG_GPIO

em_err gpio_api_initialize_gpio() {
	em_err ret = gpio_core_initialize_gpio();
	if (ret != EM_OK) {
		EM_LOGE(TAG, "GPIO init failed!");
	}
	return ret;
}
/*
 *
 */
em_err configure_gpio(uint32_t gpio_num, gpio_io_mode direction, gpio_io_pull_mode pull_mode) {
	em_err ret = EM_FAIL;

	if (UNASSIGNED_PIN == gpio_num) {
		return EM_ERR_INVALID_ARG;
	}

	gpio_io_config io_confg;
	bzero(&io_confg, sizeof(gpio_io_config));

	// set interrupt (Any edge, Pos-edge, Neg-edge, High-edge, Low-edge)
	io_confg.intr_type = GPIO_INTERRUPT_DISABLE;
#if (CONFIG_PLATFORM_SIMULATOR)
	// Use GPIO Number for the Simulator Platform
	io_confg.pin_bit_mask = gpio_num;
#else
	//bit mask of the pins, use INPUT GPIO here
	io_confg.pin_bit_mask = 1ULL << gpio_num;
#endif
	//set as input mode
	io_confg.mode = direction;

	switch (pull_mode) {
	case GPIO_IO_PULLUP_ONLY: {
		// disable pull-down mode
		io_confg.pull_down_en = GPIO_IO_PULLDOWN_DISABLE;
		// enable pull-up mode
		io_confg.pull_up_en = GPIO_IO_PULLUP_ENABLE;
		break;
	}
	case GPIO_IO_PULLDOWN_ONLY: {
		// enable pull-down mode
		io_confg.pull_down_en = GPIO_IO_PULLDOWN_ENABLE;
		// disable pull-up mode
		io_confg.pull_up_en = GPIO_IO_PULLUP_DISABLE;
		break;
	}
	case GPIO_IO_PULLUP_PULLDOWN: {
		// enable pull-down mode
		io_confg.pull_down_en = GPIO_IO_PULLDOWN_ENABLE;
		// enable pull-up mode
		io_confg.pull_up_en = GPIO_IO_PULLUP_ENABLE;
		break;
	}
	case GPIO_IO_FLOATING: {
		// disable pull-down mode
		io_confg.pull_down_en = GPIO_IO_PULLDOWN_DISABLE;
		// disable pull-up mode
		io_confg.pull_up_en = GPIO_IO_PULLUP_DISABLE;
		break;
	}

	}

	ret = gpio_io_config(&io_confg);
	return ret;
}

/*
 *
 */
em_err add_gpio_isr(uint32_t gpio_num, gpio_interrupt_type intr_type, gpio_io_isr intr_isr_handler, void *arg) {
	em_err ret = EM_FAIL;

	if (UNASSIGNED_PIN == gpio_num) {
		return EM_ERR_INVALID_ARG;
	}

	if (intr_type != GPIO_INTERRUPT_DISABLE) {
		// if ISR already set then ignore the return of 'gpio_io_install_isr_service' function
		gpio_io_install_isr_service(GPIO_IO_INTR_FLAG_IRAM);

		// set Interrupt type
		ret = gpio_io_set_intr_type(gpio_num, intr_type);
		if (ret == EM_OK) {
			if (intr_isr_handler != NULL) {
				ret = gpio_io_isr_handler_add(gpio_num, intr_isr_handler, arg);
				if (ret == EM_OK) {
					EM_LOGD(TAG, "(%d) Hook ISR handler for specific gpio %d\n", ret, gpio_num);
				} else {
					EM_LOGE(TAG, "Error: (%d) Failed to Hook ISR handler for specific gpio %d\n", ret, gpio_num);
				}
			}
		}
	}

	return ret;
}

/*
 *
 */
em_err remove_gpio_isr(uint32_t gpio_num) {
	em_err ret = EM_FAIL;

	if (UNASSIGNED_PIN == gpio_num) {
		return EM_ERR_INVALID_ARG;
	}

	ret = gpio_io_isr_handler_remove(gpio_num);
	if(ret == EM_OK){
		ret = gpio_io_set_intr_type(gpio_num, GPIO_INTERRUPT_DISABLE);
	}
	return ret;
}

/*
 *
 */
uint8_t get_gpio_value(uint32_t gpio_num) {
	if (UNASSIGNED_PIN == gpio_num) {
		return 0;
	}
	return gpio_io_get_level(gpio_num);
}

/*
 *
 */
em_err set_gpio_value(uint32_t gpio_num, uint8_t level) {
	em_err ret = EM_FAIL;

	if(UNASSIGNED_PIN == gpio_num){
		return EM_ERR_INVALID_ARG;
	}

	ret = gpio_io_set_level(gpio_num, level);
	return ret;
}
