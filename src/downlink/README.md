# Downlink Module

## Overview

Downlinks are those networks which having inter-connectivity in a local area. These networks are not directly connected to the internet. Examples of downlink networks are OneWire, Bluetooth Mesh, Zigbee, MODBUS etc.

Possible applications of downlink networks are:

- Data acquisition from multiple sensors located at a distance in a room/building
- Controlling multiple actuators
- Controlling multiple HMI devices

## Features

The EmMate's downlink module is planned support the following Network Interfaces/Protocols

1. OneWire - For more details see [Details of OneWire Module](onewire/README.md)
2. MODBUS - *(not yet implemented)*
3. Bluetooth Mesh - *(not yet implemented)*
4. Zigbee - *(not yet implemented)*

**For more details of each interface please see its Readme, API Documentation and Examples**