set(srcs
	core_utils.c
	delay_utils.c
	)

add_library(utils STATIC ${srcs})

list(APPEND EXTRA_LIBS emmate_config)

if(CONFIG_USE_COMMON)
	list(APPEND EXTRA_LIBS common)
	if(CONFIG_USE_ERRORS)
		list(APPEND EXTRA_LIBS errors)
	endif()
endif()

if(CONFIG_USE_LOGGING)
	list(APPEND EXTRA_LIBS logging)
endif()

if(CONFIG_USE_SYSTEM)
	list(APPEND EXTRA_LIBS system)
endif()

target_link_libraries(utils PRIVATE ${EXTRA_LIBS})

target_include_directories(utils 
							INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
							)