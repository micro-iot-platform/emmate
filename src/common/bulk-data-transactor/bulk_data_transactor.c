/*
 * File Name: stream_transactor.c
 * File Path: /emmate/src/common/data-stream-transactor/stream_transactor.c
 * Description:
 *
 *  Created on: 12-Jun-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include "bulk_data_transactor.h"
#include "bd_transactor_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG	LTAG_BDTRANSACTOR

static TaskRet bulkdata_transactor(void * params) {
	TransactionData *trans_data = (TransactionData *) params;
}

static em_err start_bulkdata_transactor(TransactionData *trans_data) {
	em_err ret = EM_FAIL;

	/* Create a Data Provider Queue to send Transaction Data to other modules */
	// TODO: to change the QueueCreate parameters
	trans_data->transact_queue = QueueCreate(1, sizeof(TransactionData));
	if (trans_data->transact_queue == 0) {
		EM_LOGE(TAG, "Failed to create Bulk Data Transaction Queue!");
		return EM_FAIL;
	} else {
		ret = EM_OK;
	}

	/* Create a thread to perform the Bulk Data Transactions */
	BaseType thread_stat;
	thread_stat = TaskCreate(bulkdata_transactor, "bulkdata_transactor", TASK_STACK_SIZE_6K, trans_data,
			THREAD_PRIORITY_2, &trans_data->transact_thread);
	if (thread_stat == false) {
		EM_LOGE(TAG, "Failed to create Bulk Data Transaction thread!");
		ret = EM_FAIL;
	} else {
		ret = EM_OK;
	}
	return ret;
}

static em_err init_bulk_data_peripheral(BULKDATA_PERIPHERALS peripheral) {
	em_err ret = EM_FAIL;

	switch (peripheral) {
	case UART_BULK_DATA:
		break;
	case USB_BULK_DATA:
		break;
	case BLE_BULK_DATA:
		break;
	}
	return ret;
}

static TransactionData *m_tdata = NULL;

em_err init_bulkdata_transactor(BULKDATA_TRANSACTION_MODE mode, BULKDATA_TRANSACTION_TYPE type, TransactionEnd *end,
		BULKDATA_PERIPHERALS peripheral, bulkdata_transactor_cb cb) {
	em_err ret = EM_FAIL;

	/* Allocate memory for TransactionData. This is to be used for the entire transaction and freed at the end */
	m_tdata = (TransactionData*) calloc(1, sizeof(TransactionData));
	if (m_tdata == NULL) {
		EM_LOGE(TAG, "Failed to allocate memory for TransactionData!");
		return EM_ERR_NO_MEM;
	}

	/* Copy the caller settings */
	m_tdata->mode = mode;
	m_tdata->type = type;
	memcpy(&m_tdata->stream_end, end, sizeof(TransactionEnd));
	m_tdata->peripheral = peripheral;
	m_tdata->transaction_cb = cb;

	/* Validate the Transaction input data provided by the caller */
	ret = validate_bd_transaction_input(m_tdata);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Invalid input data sent to the Bulk Data Transactor!");
		ret = EM_FAIL;
		goto free_mem;
	}

	/* Initialize/open the peripheral to read/write the data stream */
	ret = init_bulk_data_peripheral(m_tdata->peripheral);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to initialize the Bulk Data Peripheral!");
		ret = EM_FAIL;
		goto free_mem;
	}

	/* Start the Bulk Data Transactor to start the transaction process */
	ret = start_bulkdata_transactor(m_tdata);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "Failed to start the Bulk Data Transactor!");
		ret = EM_FAIL;
		goto free_mem;
	}

	free_mem:
	EM_LOGD(TAG, "Freeing allocated memory of stream transactor");
	free(m_tdata);

	return ret;
}

void deinit_bulkdata_transactor() {

}

BULKDATA_TRANSACTION_STATUS get_transaction_status() {
	return m_tdata->transaction_status;
}

void* get_transaction_data() {
	void *data = NULL;

	switch (m_tdata->transaction_status) {
	case TRANSACTION_NOT_STARTED:
		data = NULL;
		break;
	case TRANSACTION_ONGOING:
		data = NULL;
		break;
	case TRANSACTION_DONE:
		data = m_tdata->transaction_data;
		break;
	}
	return data;
}
