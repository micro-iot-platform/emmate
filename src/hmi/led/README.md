# LED Module
## Overview
The LED Module provides APIs to create and show LED notifications. Here, the application can initialize a LED notification, define the number of LEDs that will be used in the notification, provide characteristics of each LED, make one or more patterns to be used in the notification and finally start the LED notification.

## Features
- Define a LED notification which will run for a particular duration
- Define a LED notification which will run forever
- Use monochrome or RGB leds
- Use 1 to 'n' number of LEDs in a notification
- Use 1 to 'n' number of patterns in a notification
- Support various LED connections:
  - LED connected directly to a microcontroller pin
  - LED connected through a shift register *(not yet implemented)*
- PWM for generating any color *(not yet implemented)*
	

## How to use the LED Module from an Application
The below code snippet shows a LED notification with the following configurations:
- Number of LED = 1
- LED Type = `MONO_LED`
- LED pin = see any LED Example
- LED pattern = see any LED Example

```c
#include "hmi.h"
#include "led_helper.h"
#include "led_patterns.h"
#include "core_logger.h"
#include "thing.h"

static void show_my_led_notification() {
	core_err ret;
	int num_leds = 1;

	/* Creating and starting an LED notification consists of 4 steps. These steps must be followed
	 * in sequential manner, else behaviour of the library is undefied.
	 *
	 * Note: If you have multiple LEDs, then please follow Step 2 & 3, in the order as shown below.
	 *
	 * */

	/* Step 1: init_led_notification(num_led, period); */
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != EM_OK) {
		EM_LOGE(TAG, "%s init_led_notification failed!", __func__);
		return;
	}

	/* Step 2: set_led_type_idx(led no., led_type, m_r_idx, g_idx, b_idx) */
	/* Step 3: map_patterns_to_led(led no., num_patterns, pattern1, pattern2, ...) */
	set_led_type_idx(num_leds, MONO_LED, BLINK_GPIO, 0, 0);
	map_patterns_to_led(num_leds, 1, PATTERN_1);

	/* Step 4: Start the led notification */
	start_led_notification();
}

void emmate_main(void * param) {
	init_hmi_led();
	show_my_led_notification();
	
	while(1){
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
```

**Please see the LED Examples for more info**

##### Header(s) to include

```
hmi.h
led_helper.h
led_patterns.h
```
#### Module Specific Configurations

The following configurations are needed for the LED Helper module to work.

![](img/led-config.png)