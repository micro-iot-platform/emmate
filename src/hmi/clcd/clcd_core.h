/*
 * File Name: clcd_core.h
 * 
 * Description:
 *
 *  Created on: 02-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef CLCD_CORE_H_
#define CLCD_CORE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_error.h"
#include "thing.h"


// LCD_Config_CMD
#define TWO_LINE_FOUR_BIT_DISPLAY_CMD      0X28            /**  0x001010XX      (RB7-RB0)
                                                                (0x001 DL N F X X)
                                                                DL = 0 (4 BIT_MODE DATA_BUS)
                                                                N = 1   (2-LINE DISPLAY)
                                                                F = 0 (5x8 DOT FORMAT CHAR DISPLAY)
                                                                Note: F = 1 (5x11 DOT FORMAT Char Display)
                                                            */

#define TWO_LINE_EIGHT_BIT_DISPLAY_CMD      0X38            /**  0x001110XX      (RB7-RB0)
                                                                (0x001 DL N F X X)
                                                                DL = 1 (8 BIT_MODE DATA_BUS)
                                                                N = 1   (2-LINE DISPLAY)
                                                                F = 0 (5x8 DOT FORMAT CHAR DISPLAY)
                                                                Note: F = 1 (5x11 DOT FORMAT Char Display)
                                                            */
//#define TWO_LINE_ELEVEN_BIT_DISPLAY_CMD      0X3C           // 0x001111XX

#define CLEAR_DISPLAY_CMD       0x01		/** CLCD Clear command */
#define RETURN_HOME_CMD     0x02			/** CLCD Set cursor at home command */

#define DECREMENT_CURSOR_CMD    0x04    /** Cursor move on input shift to left */
#define INCREMET_CURSOR_CMD     0x06    /** Cursor move on input shift to right */

#define SHIFT_DISPLAY_RIGHT_CMD 0x05	/** Display shift right command  */
#define SHIFT_DISPLAY_LEFT_CMD  0x07	/** Display shift left command  */

#define DISPLAY_OFF_CURSOR_OFF_CMD  	0x08	/** Display off cursor off  */
#define DISPLAY_OFF_CURSOR_ON_CMD   	0x0A	/** Display off cursor on  */
#define DISPLAY_ON_CURSOR_OFF_CMD   	0x0C	/** Display on cursor off  */
#define DISPLAY_ON_CURSOR_ON_CMD    	0x0E	/** Display on cursor on  */
#define DISPLAY_ON_CURSOR_BLINK_CMD 	0x0F	/** Display On cursor On blink  */

#define SHIFT_CURSOR_POSITION_TO_LEFT_CMD   0X10	/** Shift cursor to left  */
#define SHIFT_CURSOR_POSITION_TO_RIGHT_CMD   0X14	/** Shift cursor to right  */

#define SHIFT_ENTIRE_DISPLAY_TO_LEFT_CMD    0x18	/** Shift entire display to left  */
#define SHIFT_ENTIRE_DISPLAY_TO_RIGHT_CMD    0x1C	/** Shift entire display to right  */

#define MAX_CORDINATE_OF_ROW_1      0x93	/** Row 1 Max coordinate  */
#define MAX_CORDINATE_OF_ROW_2      0xD3	/** Row 2 Max coordinate  */
#define MAX_CORDINATE_OF_ROW_3      0xA7	/** Row 3 Max coordinate  */
#define MAX_CORDINATE_OF_ROW_4      0xE7	/** Row 4 Max coordinate  */

#define LINE1_HOME						0x80		/** Line 1 Home Max coordinate  */
#define LINE2_HOME						0xC0		/** Line 2 Home Max coordinate  */
#define LINE3_HOME						0x94		/** Line 3 Home Max coordinate  */
#define LINE4_HOME						0xD4		/** Line 4 Home Max coordinate  */

/**
 *	@brief CLCD configuration
 *
 *	@return
 *     - EM_OK on success
 *     - EM_FAIL on fail
 *
 */
em_err init_clcd_core(void);

/**
 *	@brief write data to CLCD
 *
 */
void write_lcd_data(unsigned char data);

/**
 *	@brief write CMD to CLCD
 *
 */
void write_lcd_cmd(unsigned char cmd);

/**
 *	@brief CLCD is-busy Check
 *
 */
void is_busy(void);


#ifdef __cplusplus
}
#endif

#endif /* CLCD_CORE_H_ */
