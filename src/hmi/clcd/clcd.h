/*
 * File Name: clcd.h
 * 
 * Description:
 *
 *  Created on: 30-Aug-2019
 *      Author: Noyel Seth
 */

#ifndef CLCD_CLCD_H_
#define CLCD_CLCD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include "core_error.h"
#include "core_config.h"

#define MAX_ROW     CONFIG_CLCD_MAX_ROW					/** CLCD Max Supported Row*/
#define MAX_COL     CONFIG_CLCD_MAX_COLUMN				/** CLCD Max Supported Column*/

#define FIRST_ROW       0				/** CLCD First Row*/
#define SECOND_ROW      1				/** CLCD Second Row*/
#define THIRD_ROW       2				/** CLCD Third Row*/
#define FOURTH_ROW      3				/** CLCD Fourth Row*/

#define DEFAULT_STRING_DIVIDER          4		/** Display String's Display divider */

#define START_UID       'A'		/** Set Display String's UID */

#define MAX_DISPLAY_STRUCT     20		/** Max Display Structure  */

#define MAX_DISPLAY_DATA_LEN    40		/** Max Display Data Length  */

#define MAX_DATA_LEN    40			/** Max Data Length  */

typedef enum {
	NOT_OVERWRITE = 0, 					/** Display string's Not-Overwrite Option  */
	OVERWRITE,							/** Display string's Overwrite Option  */
} CLCD_STRING_OVERWRITE_STAT;

typedef enum {
	SCROLL_OFF = 0, 				/** Display string's Scroll-Off Option  */
	SCROLL_RIGHT_TO_LEFT = 1, 		/** Display string's Scroll right-left Option  */
	SCROLL_LEFT_TO_RIGHT = 2,		/** Display string's Scroll left-right Option  */
} CLCD_STRING_SCROLL_OPTION;

typedef enum {
	CONFLICT_STRING = 200,							/** set Display string's Conflict status */
	CONFLICT_STRING_CHECK_AND_OVERWRITE = 201,		/** set Display string's check & OVERWRITE status */
	CONFLICT_STRING_OVERWRITE = 202,				/** set Display string's OVERWRITE status */
	CONFLICT_STRING_NOT_CONFLICT_WITH_OVERWRITE_ARGUMENT = 203,		/** set Display string's not Conflict when Overwrite argument set status */
}CLCD_DISPLAY_STRING_CONFLICT_STATUS;


/**
 *	@brief Get Max Supported Row by CLCD Lib
 */
int get_clcd_max_supported_row();

/**
 *	@brief Get Max Supported Column by CLCD Lib
 */
int get_clcd_max_supported_col();

/**
 *	@brief Initialize the Character LCD
 *
 *	@param	clcd_init_row		CLCD Row range
 *
 *	@param	clcd_init_col		CLCD Column range
 *
 *	@return
 *     - EM_OK on success
 *     - EM_FAIL on fail
 *     - EM_ERR_INVALID_SIZE	If CLCD ROW and Column size greater then the CLCD MAX Row & Column size
 *
 */
em_err init_clcd(uint8_t clcd_init_row, uint8_t clcd_init_col);

/**
 *	@brief De-Intilize the Character LCD
 *
 *	@return
 *     - EM_OK on success
 *     - EM_FAIL on fail
 *
 */
em_err deinit_clcd();

/**
 *	@brief Set string to Character LCD (multi-line not supported)
 *
 *	@param	str				Display String
 *
 *	@param	start_row		String's Start Row number (0 to MAX_ROW-1)
 *
 *	@param	end_row			String's End Row number	(0 to MAX_ROW-1)
 *
 *	@param	start_col		String's Start Column number	(0 to MAX_COL-1)
 *
 *	@param	end_col			String's End Column number	(0 to MAX_COL-1)
 *
 *	@param	scroll_opt		String's Scroll direction
 *
 *	@param	scroll_frq		String's Scroll time(ms)
 *
 *	@param	blink_frq		String's Blink time(ms)
 *
 *	@param	over_write		Overwrite the conflicted strings
 *
 *
 *	@return
 *		- EM_OK 				on success
 *		- CONFLICT_STRING		If set string getting conflict with other configured string.
 *		- EM_ERR_INVALID_ARG 	If invalid string, column and row number.
 *
 */
em_err set_string_to_clcd(char *str, uint8_t start_row, uint8_t end_row, uint8_t start_col, uint8_t end_col,
		CLCD_STRING_SCROLL_OPTION scroll_opt, uint16_t scroll_frq, uint16_t blink_frq,
		CLCD_STRING_OVERWRITE_STAT over_write);

/**
 * @brief get Configured CLCD Max Row Size
 *
 * @return
 * 		- (-1) if CLCD not configured
 *		- CONFIGURED_ROW
 *
 */
int get_clcd_max_row();

/**
 * @brief get Configured CLCD Max Column Size
 *
 * @return
 * 		- (-1) if CLCD not configured
 *		- CONFIGURED_COLUMN
 */
int get_clcd_max_col();


#ifdef __cplusplus
}
#endif

#endif /* CLCD_CLCD_H_ */
