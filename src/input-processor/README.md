# Input Processor Module
## Overview
The Input Processor module provides API to convert data to/from a data transmission object like a JSON. 

Currenly this module support only JSON. It uses the parson open source library. To know more please see the [parson library documentation](parson/README.md)


## How to use this module from an application

##### Header(s) to include

```
input_processor.h
```

#### Module Specific Configurations

The following configurations are needed for this module to work.

![](img/input-processor-config.png)

#### Module Specific APIs
For JSON data convertion the parson library APIs have to be used directly. To know more please see the [parson library documentation](parson/README.md)