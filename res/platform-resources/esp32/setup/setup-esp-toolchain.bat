@ECHO OFF

rem ######## Setup ESP-Toolchain

set CURR_DIR=%cd%
rem echo %CURR_DIR%
set PWD=%cd%

echo "### Starting ESP-Toolchain Setup #########################""

if "%IDF_TOOLS_PATH%"=="" (

	echo IDF_TOOLS_PATH enviroment variable is not set
	echo "### Failed to Setup ESP-Toolchain #########################"
	exit /b 1
)


if "%IDF_PATH%"=="" (
	echo IDF_PATH enviroment variable is not set
	echo "### Failed to Setup ESP-Toolchain #########################"
	exit /b 1
)


if not exist "%IDF_PATH%\install.bat" (
	echo IDF_PATH is invalid.
	echo "### Failed to Setup ESP-Toolchain #########################"
	exit /b 1
)

call %IDF_PATH%\install.bat

if %ERRORLEVEL% NEQ 0 (
	echo "### Failed to Setup ESP-Toolchain Unkonwn Operating System #########################"
)

echo "### Completed ESP-Toolchain Setup #########################"