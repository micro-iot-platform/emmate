#!/bin/bash

#echo --------------------- EmMate Build Help ---------------------

echo "Usage: ./build.sh [options]"

echo "options"
echo 

echo "-menuconfig				Open project configuration tool"
echo "					Example usage from command line: ./build.sh -menuconfig"
echo

echo "-help					Show help information of EmMate Build CMD"
echo

echo "-make					Build the EmMate project"
echo "					Example usage from command line: ./build.sh -make"
echo

echo "-clean					Clean the EmMate project. Deletes all object files and binaries"
echo "					Example usage from command line: ./build.sh -clean"
echo

echo "-flash -p[port] -b[baud]		Flash the binaries to the device"
echo "					If -p and -b options are not provided, default values will be used from configuration tool"
echo "					Example usage from command line: ./build.sh -flash -p/dev/ttyUSB0 -b115200"
echo

echo "-erase-flash -p[port] -b[baud]		Erase the device memory"
echo "					If -p and -b options are not provided, default values will be used from configuration tool"
echo "					Example usage from command line: ./build.sh -erase-flash -p/dev/ttyUSB0 -b115200"
echo

echo "-log -p[port]				Monitor the output log messages of the device"
echo "					If -p option is not provided, default value will be used from configuration tool"
echo "					Example usage from command line: ./build.sh -log -p/dev/ttyUSB0"
echo "					Default baud rate at which the log messages are printed out is 115200 and it cannot be changed"
echo