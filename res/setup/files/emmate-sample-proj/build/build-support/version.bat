@ECHO OFF

echo.
echo ### Application Version #############################################

echo Reading version conf file...
set VER_CONF_FILE=%APP_SRC_PATH%\include\app_version.conf
echo %VER_CONF_FILE%

rem #reading the existing version numbers from conf file
rem eol stops comments from being parsed
rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%VER_CONF_FILE%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    if NOT "%%a"=="" set /A %%a=%%b
)

set "APP_VERSION_NUMBER=%APP_VERSION_MAJOR%.%APP_VERSION_MINOR%.%APP_VERSION_PATCH%.%APP_VERSION_HOTFIX%"

set "APP_DEV_VERSION_NUMBER=%APP_VERSION_MAJOR%.%APP_VERSION_MINOR%.%APP_VERSION_PATCH%.%APP_VERSION_HOTFIX%.%APP_VERSION_BUILD_NUMBER%"

echo .....................................................................
echo Existing versions: %APP_VERSION_NUMBER%
echo Existing Dev versions: %APP_DEV_VERSION_NUMBER%
echo .....................................................................


echo.
echo Generating new version number

rem #incrementing the build number by 1(one)
set /A NEW_APP_VERSION_BUILD_NUMBER=%APP_VERSION_BUILD_NUMBER%+1

set BUILD_VERSION_NUMBER=%APP_VERSION_NUMBER%
set BUILD_DEV_VERSION_NUMBER=%APP_VERSION_MAJOR%.%APP_VERSION_MINOR%.%APP_VERSION_PATCH%.%APP_VERSION_HOTFIX%.%NEW_APP_VERSION_BUILD_NUMBER%

echo .....................................................................
echo Build Version: %BUILD_VERSION_NUMBER%
echo Build Dev Version: %BUILD_DEV_VERSION_NUMBER%
echo .....................................................................
echo.

rem #updating the version conf file
echo Updating the version conf file

rem #the below redirection must be one > because we want to create a new header file everytime
echo # Last Updated: %CURRENT_DATE% %CURRENT_TIME% >  %VER_CONF_FILE%
echo APP_VERSION_MAJOR=%APP_VERSION_MAJOR% >>  %VER_CONF_FILE%
echo APP_VERSION_MINOR=%APP_VERSION_MINOR% >>  %VER_CONF_FILE%
echo APP_VERSION_PATCH=%APP_VERSION_PATCH% >>  %VER_CONF_FILE%
echo APP_VERSION_HOTFIX=%APP_VERSION_HOTFIX% >>  %VER_CONF_FILE%
rem #the build number will have the NEW build number
echo APP_VERSION_BUILD_NUMBER=%NEW_APP_VERSION_BUILD_NUMBER% >>  %VER_CONF_FILE%


rem #generating the version header file
echo Generating the version header file

set VER_HEADER_FILE=%APP_SRC_PATH%\include\app_version.h

rem #the below redirection must be one > because we want to create a new header file everytime
echo #define APP_VERSION_NUMBER  "%BUILD_VERSION_NUMBER%"  >  %VER_HEADER_FILE%
rem echo -e "\n"
echo #define APP_DEV_VERSION_NUMBER "%BUILD_DEV_VERSION_NUMBER%" >>  %VER_HEADER_FILE%

echo "#####################################################################"
echo.
