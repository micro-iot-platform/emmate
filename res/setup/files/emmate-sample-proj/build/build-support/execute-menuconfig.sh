#!/bin/bash

#exporting MenuConfiguration Helper Files
export MENUCONFIG_PY_PATH=$CURR_DIR/core-menuconfig/menuconf_gen.py

export BASE_KCONFIG_FILE_PATH=$CURR_DIR/core-menuconfig/Kconfig

export PLATFORM_KCONFIG_FILE_PATH=$CURR_DIR/build-support/esp32/Kconfig/Kconfig.platform
export SOM_KCONFIG_FILE_PATH=$CURR_DIR/build-support/esp32/Kconfig/Kconfig.som
export PORT_KCONFIG_FILE_PATH=$CURR_DIR/build-support/esp32/Kconfig/Kconfig.port


export TEMP_KCONFIG_FILE_PATH=$CURR_DIR/core-menuconfig/Kconfig.tmp

export MENUCONF_CONFIG_FILE_PATH=$CURR_DIR/../emmate_config

export MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=$CURR_DIR/menuconfig-gen-execution-status

export AUTO_CONFIG_H_FILE_PATH=$CURR_DIR/../emmate/emmate_config/core_config.h


#exporting Module's Kconfigs File path
export CORE_EMBEDDED_KCONFIG_FILE_PATH=$CURR_DIR/../emmate/

#export CORE_EMBEDDED_CORE_SRC_FILE_PATH=$CURR_DIR/../platform/core/
#export CORE_EMBEDDED_ESP32_SRC_FILE_PATH=$CURR_DIR/../platform/esp/

#exporting Configured Build File/Directory location mapping CSV
CSV_FILE_PATH="$CURR_DIR/build-cpy.csv"
export CSV_FILE_PATH=`realpath $CSV_FILE_PATH`
#echo $CSV_FILE_PATH


export PLATFORM_IDF_VERSION_ENV=$(cat $EMMATE_PATH/sdk/esp-idf/version)
echo ESP_IDF_VERSION = $PLATFORM_IDF_VERSION_ENV



## Script Functions ###############################################################################
run_menuconf_gen(){
	GET_MENUCONF_CONFIG_FILE_OLD_CREATION_TIME_IN_SECS=`date -r $MENUCONF_CONFIG_FILE_PATH +%s`
	echo "GET_MENUCONF_CONFIG_FILE_CREATION_TIME_IN_SECS(OLD) = $GET_MENUCONF_CONFIG_FILE_OLD_CREATION_TIME_IN_SECS"
	# execute menuconf_gen.py
	echo .....................................................................
	echo execute menuconf_gen.py...
	
	if [[ $PROJ_BUILD_MODE = "gui-config" ]]
	then
		if [[ $OS_TYPE == "Linux" ]]
		then
			python3 $MENUCONFIG_PY_PATH --config $MENUCONF_CONFIG_FILE_PATH --kconfig $TEMP_KCONFIG_FILE_PATH --output autoconf $AUTO_CONFIG_H_FILE_PATH --build-mode $PROJ_BUILD_MODE
		elif [[ $OS_TYPE == "Windows" ]]
		then 
			python $MENUCONFIG_PY_PATH --config $MENUCONF_CONFIG_FILE_PATH --kconfig $TEMP_KCONFIG_FILE_PATH --output autoconf $AUTO_CONFIG_H_FILE_PATH --build-mode $PROJ_BUILD_MODE
		fi
	elif [[ $PROJ_BUILD_MODE = "cli-config" ]]
	then
		if [[ $OS_TYPE == "Linux" ]]
		then
			python3 $MENUCONFIG_PY_PATH --config $MENUCONF_CONFIG_FILE_PATH --kconfig $TEMP_KCONFIG_FILE_PATH --output autoconf $AUTO_CONFIG_H_FILE_PATH --output config $MENUCONF_CONFIG_FILE_PATH --build-mode $PROJ_BUILD_MODE
		elif [[ $OS_TYPE == "Windows" ]]
		then 
			python $MENUCONFIG_PY_PATH --config $MENUCONF_CONFIG_FILE_PATH --kconfig $TEMP_KCONFIG_FILE_PATH --output autoconf $AUTO_CONFIG_H_FILE_PATH --output config $MENUCONF_CONFIG_FILE_PATH --build-mode $PROJ_BUILD_MODE
		fi
	else
		echo "No BUILD_MODE is set, exiting the build!"
		exit
	fi
	
	EXECUTION_STAT=$?
	if [[ $EXECUTION_STAT -eq 0 ]]
	then
		GET_MENUCONF_CONFIG_FILE_NEW_CREATION_TIME_IN_SECS=`date -r $MENUCONF_CONFIG_FILE_PATH +%s`
		echo "GET_MENUCONF_CONFIG_FILE_NEW_CREATION_TIME_IN_SECS(NEW) = $GET_MENUCONF_CONFIG_FILE_NEW_CREATION_TIME_IN_SECS"
		TIME_DIFF="$(($GET_MENUCONF_CONFIG_FILE_NEW_CREATION_TIME_IN_SECS-$GET_MENUCONF_CONFIG_FILE_OLD_CREATION_TIME_IN_SECS))"
		echo "TIME_DIFF = $TIME_DIFF"
		if [[ TIME_DIFF -ne 0 ]]
		then
			echo "MENUCONFIG_EXECUTION_STATUS=y" > $MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH
		fi
		
	fi

	echo .....................................................................
	echo
}

find_kconfig_files(){
	
export KCONFIG_FILE_NAME="Kconfig"
export KCONFIG_PROBUILD_FILE_NAME="Kconfig.projbuild"

if [[ $1 = "-all" ]]
then
	
	# find Kconfig file for 2level depth of the $CORE_EMBEDDED_KCONFIG_FILE_PATH 
	CORE_KCONFIG_FILES_ARR=($(find $CORE_EMBEDDED_KCONFIG_FILE_PATH -name "$KCONFIG_FILE_NAME" | sort))
	#echo CORE_KCONFIG_FILES_ARR = ${#CORE_KCONFIG_FILES_ARR[@]}
	
else
	
	# find Kconfig file for 2level depth of the $CORE_EMBEDDED_KCONFIG_FILE_PATH 
	CORE_KCONFIG_FILES_ARR=($(find $CORE_EMBEDDED_KCONFIG_FILE_PATH -maxdepth 2 -mindepth 2 -name "$KCONFIG_FILE_NAME" | sort))
	#echo CORE_KCONFIG_FILES_ARR = ${#CORE_KCONFIG_FILES_ARR[@]}

	# find Kconfig.probuild file for 2level depth of the $CORE_EMBEDDED_KCONFIG_FILE_PATH 
	#CORE_KCONFIG_PROBUILD_FILES_ARR=($(find $CORE_EMBEDDED_KCONFIG_FILE_PATH -maxdepth 2 -mindepth 2 -name "$KCONFIG_PROBUILD_FILE_NAME"))
	#echo CORE_KCONFIG_PROBUILD = ${#CORE_KCONFIG_PROBUILD_FILES_ARR[@]}
	
fi


}


### Execute Core Menuconfig #############################################
echo
echo
echo "### Execute Core Menuconfig #############################################"


echo .....................................................................
echo Create $TEMP_KCONFIG_FILE_PATH...
# find Base Kconfig file to start the Menuconfig window
find_kconfig_files
# create $TEMP_KCONFIG_FILE_PATH

cat $BASE_KCONFIG_FILE_PATH > $TEMP_KCONFIG_FILE_PATH

ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH=`realpath $SOM_KCONFIG_FILE_PATH` 
ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH=`realpath $PLATFORM_KCONFIG_FILE_PATH`
ABSOLUTE_PATH_PORT_KCONFIG_FILE_PATH=`realpath $PORT_KCONFIG_FILE_PATH`
if [[ $OS_TYPE == "Linux" ]]
then

	echo "	source \""$ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH"\""  >> $TEMP_KCONFIG_FILE_PATH
	echo "	source \""$ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH"\""  >> $TEMP_KCONFIG_FILE_PATH
	echo "	source \""$ABSOLUTE_PATH_PORT_KCONFIG_FILE_PATH"\"" >> $TEMP_KCONFIG_FILE_PATH
	
elif [[ $OS_TYPE == "Windows" ]]
then
	DRIVE_CHAR=${ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH:1:1}
	#echo $DRIVE_CHAR
	ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH=`echo $ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH | sed -e "s/\/${DRIVE_CHAR}/${DRIVE_CHAR}:/g"`
	echo "	source \""$ABSOLUTE_PATH_SOM_KCONFIG_FILE_PATH"\""  >> $TEMP_KCONFIG_FILE_PATH
	
	DRIVE_CHAR=${ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH:1:1}
	#echo $DRIVE_CHAR
	ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH=`echo $ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH | sed -e "s/\/${DRIVE_CHAR}/${DRIVE_CHAR}:/g"`
	echo "	source \""$ABSOLUTE_PATH_PLATFORM_KCONFIG_FILE_PATH"\""  >> $TEMP_KCONFIG_FILE_PATH
fi

echo
echo "	menu \"EmMate Module Configurations\"" >> $TEMP_KCONFIG_FILE_PATH

count=${#CORE_KCONFIG_FILES_ARR[@]}
for ((i=0; i<$count; i++))
do
	if [[ $OS_TYPE == "Linux" ]]
	then
		elem=${CORE_KCONFIG_FILES_ARR[$i]}
		echo "		source \""`realpath $elem`"\"" >> $TEMP_KCONFIG_FILE_PATH
	elif [[ $OS_TYPE == "Windows" ]]
	then
		elem=${CORE_KCONFIG_FILES_ARR[$i]}
		ABSOLUTE_KCONFIG_FILE_PATH=`realpath $elem`
		DRIVE_CHAR=${ABSOLUTE_KCONFIG_FILE_PATH:1:1}
		
		ABSOLUTE_KCONFIG_FILE_PATH=`echo $ABSOLUTE_KCONFIG_FILE_PATH | sed -e "s/\/${DRIVE_CHAR}\//${DRIVE_CHAR}:\//g"`
		echo "	source \""$ABSOLUTE_KCONFIG_FILE_PATH"\""  >> $TEMP_KCONFIG_FILE_PATH
	fi
done


echo "	endmenu" >> $TEMP_KCONFIG_FILE_PATH

echo .....................................................................
echo

echo



if [[ $1 = "-menuconfig" ]]
then 
	# run menuconf_gen.py for execute menuconfig application
	run_menuconf_gen
else
	if [ -f "$MENUCONF_CONFIG_FILE_PATH" ] && [ -f "$AUTO_CONFIG_H_FILE_PATH" ]
	then
		:	
	else
		# run menuconf_gen.py for execute menuconfig application
		run_menuconf_gen
	fi
fi

if [[ $EXECUTION_STAT -eq 0 ]]
then
	:
else
	echo "#####################################################################"
	echo
	exit $EXECUTION_STAT
fi


echo 

echo .....................................................................
echo Create $CSV_FILE_PATH..
source $MENUCONF_CONFIG_FILE_PATH
export $(grep --regexp ^[A-Z] $MENUCONF_CONFIG_FILE_PATH | cut -d= -f1)

find_kconfig_files -all
count=${#CORE_KCONFIG_FILES_ARR[@]}
echo "# Directory Paths of Kconfig files" > $CSV_FILE_PATH
for ((i=0; i<$count; i++))
do
	elem=${CORE_KCONFIG_FILES_ARR[$i]}
	BASENAME=$(basename $(dirname $elem))
	
#	if [ $BASENAME = "wifi" ] || [ $BASENAME = "ble" ] || [ $BASENAME = "eth" ] || [ $BASENAME = "gsm" ]
#	then
#		CONFIG_VER_NAME=CONFIG_USE_IFACE_${BASENAME^^}
#		CONFIG_VER_NAME=$(echo "$CONFIG_VER_NAME" | sed "s/\b-\b/_/g")
#	
#	elif [ $BASENAME = "http" ] || [ $BASENAME = "sntp" ] || [ $BASENAME = "mqtt" ]
#	then
#		CONFIG_VER_NAME=CONFIG_USE_PROTOCOL_${BASENAME^^}
#		CONFIG_VER_NAME=$(echo "$CONFIG_VER_NAME" | sed "s/\b-\b/_/g")
#	else
		CONFIG_VER_NAME=CONFIG_USE_${BASENAME^^}
		CONFIG_VER_NAME=$(echo "$CONFIG_VER_NAME" | sed "s/\b-\b/_/g")
#	fi
	CONFIG_VAR_VALUE=${!CONFIG_VER_NAME}
	
	CONFIG_VAR_CORE_MODULE_PATH=`realpath $(dirname $elem)`
	
	
	STRIPPED_CORE_PATH=${CONFIG_VAR_CORE_MODULE_PATH#"$PROJECT_DIR"}
#	echo $STRIPPED_CORE_PATH

	STRIPPED_ESP32_PATH=$(echo "$STRIPPED_CORE_PATH" | sed "s/\bcore\b/esp/g")
#	echo $STRIPPED_ESP32_PATH

	CONFIG_VAR_ESP32_MODULE_PATH=$PROJECT_DIR$STRIPPED_ESP32_PATH
#	echo $CONFIG_VAR_ESP32_MODULE_PATH
	
#	CONFIG_VAR_ESP32_MODULE_PATH=$(echo "$CONFIG_VAR_CORE_MODULE_PATH" | sed "s/\bcore\b/esp/g")
	
#	echo .................................................................................
#	echo $i
#	echo $elem
#	echo $CONFIG_VAR_CORE_MODULE_PATH
#	echo $CONFIG_VAR_ESP32_MODULE_PATH
#	echo .................................................................................
	
	if [ $CONFIG_VER_NAME = "CONFIG_USE_ONEWIRE" ] || [ $CONFIG_VER_NAME = "CONFIG_USE_SOM" ]
	then
		echo $CONFIG_VER_NAME,$CONFIG_VAR_VALUE,-r,$CONFIG_VAR_CORE_MODULE_PATH,$CONFIG_VAR_ESP32_MODULE_PATH  >> $CSV_FILE_PATH
	else
		echo $CONFIG_VER_NAME,$CONFIG_VAR_VALUE,,$CONFIG_VAR_CORE_MODULE_PATH,$CONFIG_VAR_ESP32_MODULE_PATH  >> $CSV_FILE_PATH
	fi
done
echo .....................................................................
echo

echo

echo .....................................................................
echo Remove $TEMP_KCONFIG_FILE_PATH...
rm -f -v $TEMP_KCONFIG_FILE_PATH
echo .....................................................................
echo

echo "#####################################################################"
echo

exit 0

## run menuconf_gen.py for execute menuconfig application
#run_menuconf_gen


