@ECHO OFF

rem  ---------- get the OS type ----------
echo EMMATE_PATH = %EMMATE_PATH%

rem if exist %SOMTHING_SIM_PATH% (
rem	echo SOMTHING_SIM_PATH = %SOMTHING_SIM_PATH%
rem )

rem set OS_TYPE=< python %EMMATE_PATH%\resource\getOSPlatform.py
python %EMMATE_PATH%\resource\getOSPlatform.py > tmpFile
rem set /p OS_TYPE= < tmpFile 
for /f "delims=" %%i in (tmpFile) do (
 		rem echo %%i
 		set OS_TYPE=%%i
)
del tmpFile
echo OS_TYPE = %OS_TYPE%

rem  ---------- setting the directories and paths ----------
set CURR_DIR=%cd%
set PWD=%cd%
rem ECHO %CURR_DIR%

rem  ---------- setting the configuration file paths ----------
set CONFIG_PATH=%CURR_DIR%\build.conf
set EMMATE_CONFIG_FILE_PATH=%CURR_DIR%\..\emmate_config
set PREV_BUILD_PLATFORM_PATH=%CURR_DIR%\build-support\prev_build

rem ---------- get the build config variables ----------
rem eol stops comments from being parsed
rem otherwise split lines at the = char into two tokens
for /F "eol=# delims== tokens=1,*" %%a in (%CONFIG_PATH%) do (
    rem proper lines have both a and b set
    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
    if NOT "%%a"=="" set %%a=%%b
)
rem ECHO %BUILD_MODE%
rem pause

rem ---------- set the current date and time ----------
set CURRENT_DATE=%date%
set CURRENT_TIME=%time%


rem ---------- importing the build cmd option helper script ---------- 
set BUILD_OPTION_HELPER_PATH=%CURR_DIR%\build-support\build-helps.bat

rem ---------- importing the menu configuration script ----------
set MENUCONFIG_EXECUTION_SCRIPT=%CURR_DIR%\build-support\execute-menuconfig.bat
set MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH=%CURR_DIR%\menuconfig-gen-execution-status

rem ---------- set project name, src and other variables ----------
rem go to Project Directory
cd ..
set PROJECT_DIR=%cd%
for /f "delims=" %%A in ('cd') do (
     set foldername=%%~nxA
)
set PROJECT_NAME=%foldername%
set APP_SRC_PATH=%PROJECT_DIR%\src
set BIN_DIRECTORY=%PROJECT_DIR%\bin
set CMAKE_BIN_DIR=%BIN_DIRECTORY%\cmake_build
rem pause

rem ---------- return to current directory ----------
cd %CURR_DIR%

rem ---------- start running the build script ---------- 
ECHO .....................................................................
ECHO Initiating build for %PROJECT_NAME%
ECHO .....................................................................
ECHO.
rem # if BUILD_MODEis not set then set it to gui-config
rem if "%BUILD_MODE%"==""(
rem 	set BUILD_MODE="gui-config"
rem )else(
rem 	echo "all set"
rem )
rem pause
echo.
echo .....................................................................
echo Build Mode             : %BUILD_MODE%
echo Build Date             : %CURRENT_DATE%
echo Build Time             : %CURRENT_TIME%
echo Build By               : %USERNAME%
echo .....................................................................
echo Project Location : %PROJECT_DIR%
echo .....................................................................
echo Distribution Location : %BIN_DIRECTORY%
echo .....................................................................
echo.
rem echo %cd%

rem TODO: remove the PROJ_BUILD_MODE related code from here if not required
rem ---------- setting the build mode ----------
set PROJ_BUILD_MODE=%BUILD_MODE%
rem echo PROJ_BUILD_MODE = %PROJ_BUILD_MODE%

@setlocal EnableDelayedExpansion

rem ---------- read menuconfig tool's execution status ----------
if exist %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% (
	echo MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH = %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
	rem eol stops comments from being parsed
	rem otherwise split lines at the = char into two tokens
	for /F "eol=# tokens=1,* delims==" %%a in (%MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%) do (
	    rem proper lines have both a and b set
	    rem if NOT "%%a"=="" if NOT "%%b"=="" set %%a=%%b
	    if NOT "%%a"=="" set %%a=%%b
	    
	 	if "%%a" == "MENUCONFIG_EXECUTION_STATUS" (
			rem echo inside MENUCONFIG_EXECUTION_STATUS=%%b..
			if "%%b" == "y " (
				if "%1"=="-make" (
			 		rem #echo "If menuconfig execute then Deleting BIN_DIRECTORY/.PROJECT_NAME if present" >> $LOG_FILE
			 		rem # delete hidden .PROJECT_NAME/* files
			 		rem rd /s /q %BIN_DIRECTORY%
			 		rem rd /s /q %CMAKE_BIN_DIR%
			 		rem del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH%
					IF EXIST %BIN_DIRECTORY% (
						rd /s /q %BIN_DIRECTORY%
					) 
					rem ------------- CLEAN CMAKE BIN DIRECTORY -------------
					IF EXIST %CMAKE_BIN_DIR% (
						rd /s /q %CMAKE_BIN_DIR%
					)
					rem -----------------------------------------------------
					IF EXIST %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% (
						del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% 
					)
		 		)
	 		)
		)	
	)
)

rem ---------- set the build log directory and file ----------
set LOG_DIR=%BIN_DIRECTORY%\log
set LOG_FILE=%LOG_DIR%\build.log

if not exist %LOG_DIR% (
	mkdir %LOG_DIR%
)

rem ---------- start the emmate build ----------
rem --------------------------------------------------
echo Setting up the build...
call %CURR_DIR%\build-support\build_setup.bat > %LOG_FILE%
echo.
echo.

rem ---------- creating binary output directory ----------
if not exist %BIN_DIRECTORY% (
	mkdir %BIN_DIRECTORY%
)


rem ################################################################### Execute Menuconfig operation
IF "%1" ==  "-menuconfig"  (
	echo. 
	echo ## EmMate Project Menuconfig ################################
	echo.
	call %MENUCONFIG_EXECUTION_SCRIPT% -menuconfig >> %LOG_FILE%
	
	rem echo errorlevel: !errorlevel!
	echo.
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Menuconfig Failed ################################ 
		GOTO ERROR_EXIT
	) 
		
	echo ## EmMate Project Menuconfig Successful ################################
	GOTO SUCCESS_EXIT
) 



rem ################################################################### Execute Make operation
if "%1"=="-make" (
	
	echo ## EmMate Project Build ################################
	echo.
	
	rem echo ## Executing EmMate Menuconfig ################################
	rem execute menuconfig if emmate_config or core_config.h is not present before calling make
	call %MENUCONFIG_EXECUTION_SCRIPT% >> %LOG_FILE%
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Project Build Failed ################################ 
		GOTO ERROR_EXIT
	)
	
	IF EXIST %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% (
		del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% 
	)
	
	rem echo ## EmMate Menuconfig Successful ################################
	echo.

	rem #Creating Application Version
	echo Updating the version ...
	call %CURR_DIR%\build-support\version.bat >> %LOG_FILE%
	
	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	rem echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	rem echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		)
	)
	
	if exist %PREV_BUILD_PLATFORM_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%PREV_BUILD_PLATFORM_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
			rem echo %%a=!val!
		    if "%%a" == "PREV_BUILD_PLATFORM" (
		    	set PREV_BUILD=!val!
		    	rem echo PREV_BUILD_PLATFORM = !PREV_BUILD!
		    )
		)
	)
	
	rem ---------- call the cmake config generation script ----------
	call %CURR_DIR%\build-support\platform-process.bat  >> %LOG_FILE%
	
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Build
		
		if "!PREV_BUILD!" NEQ "esp32" (
		
			mkdir %PROJECT_DIR%\emmate_config_bkp >> %LOG_FILE%
			move %PROJECT_DIR%\emmate\emmate_config\core_config.h  %PROJECT_DIR%\emmate_config_bkp\ >> %LOG_FILE%
			rd /s /q %PROJECT_DIR%\emmate
			mkdir %PROJECT_DIR%\emmate
			
			set EMMATE_SOURCE=%CURR_DIR%\..\emmate
			
			echo Copying the emmate core-src specific code to the emmate directory
			echo.
			set EMMATE_RESOURCE=%EMMATE_PATH%\src
			xcopy /S /I /E /H /Y /F !EMMATE_RESOURCE!\* %PROJECT_DIR%\emmate >> %LOG_FILE%
			move %PROJECT_DIR%\emmate\*.c	%PROJECT_DIR%\src >> %LOG_FILE%
			rem rm -fv %PROJECT_DIR%\emmate\*.c
		
			echo Copying the esp32 platform specific code to the emmate directory
			echo.
			set EMMATE_RESOURCE=%EMMATE_PATH%\platform
			xcopy /s /i /e /h /y /f !EMMATE_RESOURCE!\* !EMMATE_SOURCE! >> %LOG_FILE%
			echo PREV_BUILD_PLATFORM="esp32"> !PREV_BUILD_PLATFORM_PATH!
			
			
			xcopy /s /i /e /h /y /f %PROJECT_DIR%\emmate_config_bkp\* %PROJECT_DIR%\emmate\emmate_config >> %LOG_FILE%
			rd /s /q %PROJECT_DIR%\emmate_config_bkp
			
		)
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		set IDF_EXPORT=!IDF_PATH!\export.bat
		rem echo IDF_EXPORT = !IDF_EXPORT!
		call !IDF_EXPORT!
		
		if not exist %CMAKE_BIN_DIR%\NUL mkdir %CMAKE_BIN_DIR%
		cd %CMAKE_BIN_DIR%
		
		rem cmake "project_src_path" -D"toolchain" -G"Specify a build system generator"
		call cmake %PROJECT_DIR% -DCMAKE_TOOLCHAIN_FILE=!IDF_PATH!\tools\cmake\toolchain-esp32.cmake -GNinja
		call cmake --build .
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Build

		if "!PREV_BUILD!" NEQ "simulator" (
		
			mkdir %PROJECT_DIR%\emmate_config_bkp >> %LOG_FILE%
			move %PROJECT_DIR%\emmate\emmate_config\core_config.h  %PROJECT_DIR%\emmate_config_bkp\ >> %LOG_FILE%
			rd /s /q %PROJECT_DIR%\emmate
			mkdir %PROJECT_DIR%\emmate
			
			set EMMATE_SOURCE=%CURR_DIR%\..\emmate
			
			echo Copying the emmate core-src specific code to the emmate directory
			echo.
			set EMMATE_RESOURCE=%EMMATE_PATH%\src
			xcopy /S /I /D /E /H /Y /F !EMMATE_RESOURCE!\* %PROJECT_DIR%\emmate >> %LOG_FILE%
			move %PROJECT_DIR%\emmate\*.c	%PROJECT_DIR%\src >> %LOG_FILE%
			rem rm -fv %PROJECT_DIR%\emmate\*.c
		
			echo Copying the simulator code to the emmate directory
			echo.
			set EMMATE_RESOURCE=%EMMATE_PATH%\simulator
			xcopy /s /i /d /e /h /y /f !EMMATE_RESOURCE!\* !EMMATE_SOURCE! >> %LOG_FILE%
			echo PREV_BUILD_PLATFORM="simulator"> !PREV_BUILD_PLATFORM_PATH!
			
			
			xcopy /s /i /e /h /y /f %PROJECT_DIR%\emmate_config_bkp\* %PROJECT_DIR%\emmate\emmate_config >> %LOG_FILE%
			rd /s /q %PROJECT_DIR%\emmate_config_bkp
		)
		
		set EMMATE_EXPORT=!EMMATE_PATH!\export_emmate.bat
		echo EMMATE_EXPORT = !EMMATE_EXPORT!
		call !EMMATE_EXPORT!
				
		if not exist %CMAKE_BIN_DIR%\NUL mkdir %CMAKE_BIN_DIR%
		cd %CMAKE_BIN_DIR%
		
		call cmake %PROJECT_DIR% -GNinja
		call cmake --build .
	)

	rem echo errorlevel: !errorlevel!
	echo.
	
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Project Build Failed ################################ 
		GOTO ERROR_EXIT
	) 
	
	
	if "!TARGET_PLATFORM!"=="esp32" (
		rem make a copy of %PROJECT_NAME%.bin into %BIN_DIRECTORY% form %CMAKE_BIN_DIR%
		xcopy /i /h /y /f %CMAKE_BIN_DIR%\%PROJECT_NAME%.bin	%BIN_DIRECTORY% >> %LOG_FILE%
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		rem make a copy of %PROJECT_NAME%.exe into %BIN_DIRECTORY% form %CMAKE_BIN_DIR%
		xcopy /i /h /y /f %CMAKE_BIN_DIR%\%PROJECT_NAME%.elf.exe	%BIN_DIRECTORY% >> %LOG_FILE%
	)
			
	echo ## EmMate Project Build Successful ################################ 
	GOTO SUCCESS_EXIT
)


rem ################################################################### Execute Erase-Flash operation
if "%1"=="-erase-flash" (

	echo ## EmMate Project Erase-Flash ################################
	echo.

	call set com_port=%2
	call set com_baud=%3
	
	rem set flash command line arguments COM port & baud rate 
	echo !com_port!|find "-p" >nul
	if !errorlevel! EQU 0 ( 
		call set com_port=%2
		call set com_port=!com_port:-p=!
		echo com_port = !com_port!
	) else (
		call set "com_port="
	)
	
	echo !com_baud!|find "-b" >nul
	if !errorlevel! EQU 0 ( 
		call set com_baud=%3
		call set com_baud=!com_baud:-b=!
		echo com_baud = !com_baud!
	) else (
		call set "com_baud="
	)

	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		    if "%%a" == "CONFIG_SERIAL_COM_PORT" (
		    	if "!com_port!"=="" set com_port=!val!
		    	echo com_port = !com_port!
		    )
			if "%%a" == "CONFIG_SERIAL_BAUD_RATE" (
		    	if "!com_baud!"=="" set com_baud=!val!
		    	echo com_baud = !com_baud!
		    )
		)
	)
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Erase Flash
		
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		set IDF_EXPORT=!IDF_PATH!\export.bat
		rem echo IDF_EXPORT = !IDF_EXPORT!
		call !IDF_EXPORT!		

		cd %CMAKE_BIN_DIR%
		python !IDF_PATH!\components\esptool_py\esptool\esptool.py -p !com_port! -b !com_baud! erase_flash
		
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Erase Flash
		del /f /q "!SOMTHING_SIM_PATH!"\*.*  >> %LOG_FILE%
		del /f /q "!SOMTHING_SIM_PATH!"\EMMATE_NVS  >> %LOG_FILE%
	)
	
	rem echo errorlevel: !errorlevel!
	echo.
	
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Project Erase-Flash Failed ################################ 
		GOTO ERROR_EXIT
	) 
	
	echo ## EmMate Project Erase-FLash Successful ################################ 
	GOTO SUCCESS_EXIT
)



rem ################################################################### Execute Flash operation
if "%1"=="-flash" (

	echo ## EmMate Project Flash ################################
	echo.

	call set com_port=%2
	call set com_baud=%3
	
	rem set flash command line arguments COM port & baud rate 
	echo !com_port!|find "-p" >nul
	if !errorlevel! EQU 0 ( 
		call set com_port=%2
		call set com_port=!com_port:-p=!
		echo com_port = !com_port!
	) else (
		call set "com_port="
	)
	
	echo !com_baud!|find "-b" >nul
	if !errorlevel! EQU 0 ( 
		call set com_baud=%3
		call set com_baud=!com_baud:-b=!
		echo com_baud = !com_baud!
	) else (
		call set "com_baud="
	)

	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		    if "%%a" == "CONFIG_SERIAL_COM_PORT" (
		    	if "!com_port!"=="" set com_port=!val!
		    	echo com_port = !com_port!
		    )
			if "%%a" == "CONFIG_SERIAL_BAUD_RATE" (
		    	if "!com_baud!"=="" set com_baud=!val!
		    	echo com_baud = !com_baud!
		    )
		)
	)
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Flash
		
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		set IDF_EXPORT=!IDF_PATH!\export.bat
		rem echo IDF_EXPORT = !IDF_EXPORT!
		call !IDF_EXPORT!		

		cd %CMAKE_BIN_DIR%
		python !IDF_PATH!\components\esptool_py\esptool\esptool.py -p !com_port! -b !com_baud! write_flash @flash_project_args

	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Flash
		rem echo SOMTHING_SIM_PATH = %SOMTHING_SIM_PATH%
		del /f /q "!SOMTHING_SIM_PATH!"\*.* >> %LOG_FILE%
		xcopy /i /h /y /f %BIN_DIRECTORY%\%PROJECT_NAME%.elf.exe "!SOMTHING_SIM_PATH!" >> %LOG_FILE%
	)
	
	rem echo errorlevel: !errorlevel!
	echo.
	
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Project Flash Failed ################################ 
		GOTO ERROR_EXIT
	) 
	
	echo ## EmMate Project Flash Successful ################################ 
	GOTO SUCCESS_EXIT
)



rem ################################################################### Execute Log operation
if "%1"=="-log" (
	
	echo ## EmMate Project Log ################################
	echo.
	
	call set com_port=%2
	
	rem set flash command line arguments COM port & baud rate 
	echo !com_port!|find "-p" >nul
	if !errorlevel! EQU 0 ( 
		call set com_port=%2
		call set com_port=!com_port:-p=!
		echo com_port = !com_port!
	) else (
		call set "com_port="
	)

	rem get the target platform from CONFIG_PLATFORM_TARGET KConfig variable
	echo EMMATE_CONFIG_FILE_PATH = %EMMATE_CONFIG_FILE_PATH%
	if exist %EMMATE_CONFIG_FILE_PATH% (
		for /F "eol=# delims== tokens=1,*" %%a in (%EMMATE_CONFIG_FILE_PATH%) do (
		    call set val=%%b
			set val=!val:"=!
		    rem echo %%a=!val!
		    if "%%a" == "CONFIG_PLATFORM_TARGET" (
		    	set TARGET_PLATFORM=!val!
		    	echo TARGET_PLATFORM = !TARGET_PLATFORM!
		    )
		    if "%%a" == "CONFIG_SERIAL_COM_PORT" (
		    	if "!com_port!"=="" set com_port=!val!
		    	echo com_port = !com_port!
		    )
		)
	)
	if "!TARGET_PLATFORM!"=="esp32" (
		echo.
		echo Executing ESP32 Platform Monitor
		
		set IDF_PATH=%EMMATE_PATH%\sdk\esp-idf
		set IDF_EXPORT=!IDF_PATH!\export.bat
		rem echo IDF_EXPORT = !IDF_EXPORT!
		call !IDF_EXPORT!		

		cd %CMAKE_BIN_DIR%
		rem set PROJECT_ELF=%PROJECT_NAME%.elf
		rem echo PROJECT_NAME = %PROJECT_NAME%
		rem echo PROJECT_ELF = %PROJECT_ELF%
		python !IDF_PATH!\tools\idf_monitor.py -p !com_port! %PROJECT_NAME%.elf
		
	)
	if "!TARGET_PLATFORM!"=="simulator" (
		echo.
		echo Executing Simulator Platform Monitor
	)
	
	
	
	rem echo errorlevel: !errorlevel!....
	echo.
	
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Project Log Failed ################################ 
		GOTO ERROR_EXIT
	) 
	
	echo ## EmMate Project Log Successful ################################ 
	GOTO SUCCESS_EXIT
)


rem ################################################################### Execute Clean operation
if "%1"=="-clean" (

	echo ## EmMate Project Clean ################################
	echo.
	
	IF EXIST %BIN_DIRECTORY% (
		rd /s /q %BIN_DIRECTORY%
	) 
rem ------------- CLEAN CMAKE BIN DIRECTORY -------------
	IF EXIST %CMAKE_BIN_DIR% (
		rd /s /q %CMAKE_BIN_DIR%
	)
rem -----------------------------------------------------
	IF EXIST %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% (
		del /f /q %MENUCONFIG_GEN_EXECUTION_STATUS_FILE_PATH% 
	)
	
	rem echo errorlevel: !errorlevel!
	echo.
	
	if "!errorlevel!" NEQ "0" ( 
		echo ## EmMate Project Clean Failed ################################ 
		GOTO ERROR_EXIT
	) 
	
	echo ## EmMate Project Clean Successful ################################ 
	GOTO SUCCESS_EXIT
)

call %BUILD_OPTION_HELPER_PATH%
GOTO SUCCESS_EXIT

:ERROR_EXIT
	exit /b 1
	
:SUCCESS_EXIT
	exit /b 0