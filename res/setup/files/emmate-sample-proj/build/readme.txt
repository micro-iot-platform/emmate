#--------------------------------------------------------------------
# BUILD DIRECTORY CONTENTS
#--------------------------------------------------------------------
The build directory must contain the following directories and files:

1. build-support						<DIR>
2. core-menuconfig						<DIR>
3. build.bat							<FILE>
4. build.sh								<FILE>
5. build.conf							<FILE>
6. readme.txt							<FILE>

1. build-support						<DIR>
The build-support directory will contain the supporting files/scripts to
build / compile an EmMate project. These files/scripts are used by
the main build script - build.sh / builds.bat


2. core-menuconfig						<DIR>
The core-menuconfig directory will contain the supporting files/scripts
for the EmMate Menuconfig GUI Tool.


3. build.bat							<FILE>
The build.bat file is the script (for Windows) using which the following tasks can be
done on an EmMate project. Details of each option are given below
configure, make, clean, flash, log erase-flash


4. build.sh								<FILE>
The build.sh file is the script (for Ubuntu) using which the following tasks can be
done on an EmMate project. Details of each option are given below
configure, make, clean, flash, log erase-flash


5. build.conf							<FILE>
The build.conf file is a configuration file for the scripts build.bat and build.sh
This file is for EmMate Framework Developers and should not be touched by the user


6. readme.txt							<FILE>
The readme.txt file is the file you are reading now.


#--------------------------------------------------------------------
# OPTIONS FOR BUILD SCRIPT - BUILD.BAT & BUILD.SH
#--------------------------------------------------------------------

Build script usage from command line

For Windows:
Open a command prompt, cd to the build directory of your project, 
run the below command - build.bat [options]


For Ubuntu:
Open a terminal, cd to the build directory of your project, 
run the below command - build.sh [options]


Options
#--------------------------------------------------------------------
-menuconfig
Open project configuration tool
Example usage from command line: ./build.bat -menuconfig OR ./build.sh -menuconfig


-help
Show help information of EmMate Build CMD


-make
Build (compile) the EmMate project
Example usage from command line: ./build.bat -make OR ./build.sh -make


-clean
Clean the EmMate project. Deletes all object files and binaries
Example usage from command line: ./build.bat -clean OR ./build.sh -clean


-flash -p[port] -b[baud]
Flash the binaries to the device
If -p and -b options are not provided, default values will be used from configuration tool
Example usage from command line: ./build.bat -flash -pCOM1 -b115200 OR
./build.sh -flash -p/dev/ttyUSB0 -b115200


-erase-flash -p[port] -b[baud]
Erase the device memory
If -p and -b options are not provided, default values will be used from configuration tool
Example usage from command line: ./build.bat -flash -pCOM1 -b115200 OR
./build.sh -flash -p/dev/ttyUSB0 -b115200


-log -p[port]
Monitor the output log messages of the device
If -p option is not provided, default value will be used from configuration tool
Example usage from command line: ./build.bat -log -pCOM1 OR ./build.sh -log -p/dev/ttyUSB0
Default baud rate at which the log messages are printed out is 115200 and it cannot be changed
