
#include "your-module/your_module.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	EM_LOGI(TAG, "Calling your_module_init() in your_module.c in your-module directory ...");
	your_module_init();
	EM_LOGI(TAG, "Returned from your_module_init()");

	while(1){
		EM_LOGI(TAG, "Calling your_module_loop() in your_module.c in your-module directory ...");
		your_module_loop();
		EM_LOGI(TAG, "Sleeping for %d ms before looping ...", DELAY_10_SEC);
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
