
@ECHO OFF

set "cmake_path=%userprofile%\.espressif\tools\cmake"
set "ninja_path=%userprofile%\.espressif\tools\ninja"

rem echo cmake_path %cmake_path%
rem echo ninja_path %ninja_path%

echo.
rem only directory in name descending 
for /f "tokens=*" %%a in ('dir "%cmake_path%" /d /b /o-n') do set latest_cmake_ver=%%a& goto set_cmake_path


:set_cmake_path
 	echo Latest cmake ver : %latest_cmake_ver%
 	set cmake_path=%cmake_path%\%latest_cmake_ver%\bin
 	echo cmake_path : %cmake_path%.
 	

echo. 	
rem only directory in name descending 
for /f "tokens=*" %%a in ('dir "%ninja_path%" /d /b /o-n') do set latest_ninja_ver=%%a& goto set_ninja_path

:set_ninja_path
	echo Latest ninja ver = %latest_ninja_ver%
 	set ninja_path=%ninja_path%\%latest_ninja_ver%
	echo ninja_path = %ninja_path%
	

echo.
set PATH=%cmake_path%;%ninja_path%;%PATH%


echo.
rem for %%a in ("%path:;=";"%") do (
rem       echo %%a
rem )

rem echo Update Path enviroment variable: 
rem ECHO.%PATH:;= & ECHO.%

