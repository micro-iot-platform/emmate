@ECHO OFF

:: setting current directory
set CURR_DIR=%cd%
::ECHO %CURR_DIR%

set CURRENT_DATE=%date%
set CURRENT_TIME=%time%

set TOOLS_DIR=%CURR_DIR%\tools\
set DIST_DIR=%TOOLS_DIR%\dist\

rem 7zip related variable
set INSTALLER_7Z_FILE_NAME="7z-installer.exe"
set EXECUTABLE_7ZIP_PATH=""

if not exist %DIST_DIR%\NUL mkdir %DIST_DIR%

echo.
echo #####################################################################
echo Starting Emmate Installation
echo #####################################################################
echo.


rem Get EmMate-Framework Version
for /F "eol=# delims== tokens=1,*" %%a in (%CURR_DIR%\src\version\core_version.conf) do (
	if NOT "%%a"=="" set %%a=%%b
)

if "%PROCESSOR_ARCHITECTURE%" == "AMD64" (
	echo Installing EmMate Framework v%CORE_VERSION_MAJOR%.%CORE_VERSION_MINOR%
	echo.
) else (
	echo Windows x64bit and all applicable updates are required to install EmMate Framework v%CORE_VERSION_MAJOR%.%CORE_VERSION_MINOR%
	goto FAILED_STATUS
)


echo Set The Environment variable 'EMMATE_PATH' = %CURR_DIR%
setx EMMATE_PATH "%CURR_DIR%"

echo.
rem Checking The Python3 is present or not #############################################################################################################

py -3 -V >nul 2>nul

if %errorlevel% NEQ 0 (
	if "%PROCESSOR_ARCHITECTURE%" == "AMD64" (
		echo Python3 not installed, Install Python3 for Windows 64bit
	) else (
		echo Python3 not installed, Install Python3 for Windows 32bit
	)
	goto FAILED_STATUS
)

for /f "delims=" %%i in ('py -3 -V') do echo Current Python Version: '%%i'

echo.
rem install Python3 required Libs/Modules for EmMate..
echo install Python3 required Libs/Modules for EmMate..

py -3 -m pip install --upgrade pip

py -3 -m pip install -r %CURR_DIR%\requirements.txt

rem #####################################################################################################################################

echo.
echo.
rem Set 7z.exe path  #############################################################################################################

if "%PROCESSOR_ARCHITECTURE%" == "AMD64" (
	set EXECUTABLE_7ZIP_PATH="%TOOLS_DIR%\7zip_x64"
) else (
	set EXECUTABLE_7ZIP_PATH="%TOOLS_DIR%\7zip_x86"
)

echo EXECUTABLE_7ZIP_PATH = %EXECUTABLE_7ZIP_PATH%

rem #####################################################################################################################################

rem Unzip sdk\esp-idf.zip #############################################################################################################

echo.
echo ............................
echo Unzip sdk\esp-idf.zip to %CURR_DIR%\sdk
echo ............................

rem unzip at destination folder/directory and overwrite if exist
"%EXECUTABLE_7ZIP_PATH%\7z.exe" x -o"%CURR_DIR%\sdk\" "%CURR_DIR%\sdk\esp-idf.zip" -aoa

rem echo Unzip_sdk errorlevel = %errorlevel%

if %errorlevel% NEQ 0 (
 	echo Failed to Unzip sdk\esp-idf.zip.
 	goto FAILED_STATUS
)

rem #####################################################################################################################################



rem Unzip tools\dist\msys2.zip #############################################################################################################

echo.
echo ............................
echo Unzip tools\dist\msys2.zip to %CURR_DIR%\tools\msys2
echo ............................

rem unzip at destination folder/directory and overwrite if exist
"%EXECUTABLE_7ZIP_PATH%\7z.exe" x -o"%CURR_DIR%\tools" "%CURR_DIR%\tools\dist\msys2.zip" -aoa

rem echo Unzip_sdk errorlevel = %errorlevel%

if %errorlevel% NEQ 0 (
 	echo Failed to tools\dist\msys2.zip
 	goto FAILED_STATUS
)

echo. 
echo setting up MSYS2 for User: %USERNAME%
echo.
start /wait /b /min %CURR_DIR%\tools\msys2\msys2.exe -exit

rem echo.
rem echo --------- Set MSYS2_PATH and MSYS2_BIN_PATH
rem setx MSYS2_PATH "%CURR_DIR%\tools\msys2"
rem echo MSYS2_PATH = %CURR_DIR%\tools\msys2

rem setx MSYS2_BIN_PATH "%CURR_DIR%\tools\msys2\usr\bin"
rem echo MSYS2_BIN_PATH = %CURR_DIR%\tools\msys2\usr\bin

echo --------- Adding MSYS2 Tools to PATH (User) environment variable
rem get exising user path env variable
powershell -NoProfile -ExecutionPolicy Bypass -Command "&{ ([System.Environment]::GetEnvironmentVariable('PATH','user')) > tmpFile;}" 

for /f "tokens=*" %%i in ('type tmpFile') do (
 		rem echo %%i
 		set OLD_USER_PATH=%%i
)
rem echo OLD_USER_PATH = %OLD_USER_PATH%

set "ADDITIONAL_PATH=%CURR_DIR%\tools\msys2;%CURR_DIR%\tools\msys2\usr\bin;"
echo.%ADDITIONAL_PATH:;= & ECHO.%

rem replace additional path with blank, if they are exist.
call set NEW_USER_PATH=%%OLD_USER_PATH:%ADDITIONAL_PATH%=%%

rem echo.
rem echo NEW_USER_PATH = %NEW_USER_PATH%
rem echo.
rem echo Update NEW_ADDITIONS path enviroment variable: 
rem echo.%NEW_USER_PATH:;= & ECHO.%

powershell -NoProfile -ExecutionPolicy Bypass -Command "&{ $user_path = ([System.Environment]::GetEnvironmentVariable('PATH','user')); [System.Environment]::SetEnvironmentVariable('path','%ADDITIONAL_PATH%%NEW_USER_PATH%',[System.EnvironmentVariableTarget]::User); }"

del tmpFile

rem #####################################################################################################################################


echo.
set IDF_PATH=%CURR_DIR%\sdk\esp-idf
rem echo %IDF_PATH%
echo.
echo ............................
echo Start SDK installation.
echo ............................

call %IDF_PATH%\install.bat

if "%errorlevel%" EQU "0" (

	call %IDF_PATH%\export.bat
	
	echo.
	echo #####################################################################
	echo Emmate Installation Complete
	echo #####################################################################
	exit /b 0
	
) else (
	echo Failed to Install the SDK..
	goto FAILED_STATUS
)


:FAILED_STATUS
	echo.
	echo #####################################################################
	echo Emmate Installation Failed
	echo #####################################################################
	exit /b 1 
