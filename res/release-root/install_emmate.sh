#!/bin/bash

#setting current directory
export CURR_DIR=`pwd`

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`


if [ -z "$EMMATE_PATH" ]
then
	echo "Environment variable 'EMMATE_PATH' is not set"
	echo "You need to update your 'EMMATE_PATH' environment variable in the ~/.profile file."
	echo "export EMMATE_PATH=$CURR_DIR"
	exit 1
fi



end(){
	echo
	echo #####################################################################
	echo Emmate Installation Failed
	echo #####################################################################
	exit 1 
}


echo
echo "#####################################################################"
echo Starting Emmate Installation
echo "#####################################################################"

echo
echo The Environment variable 'EMMATE_PATH' = $CURR_DIR

echo
# Checking The Python2/3 is present or not #############################################################################################################

# hiding-output-of-a-command link: https://askubuntu.com/questions/474556/hiding-output-of-a-command
python3 -V > /dev/null 2>&1

ret_stat=$?

if [ $ret_stat -ne 0 ]
then 
	echo "Python3 not installed, Install Python3 for Ubuntu"
	end
fi

python3_ver=$(python3 -V)
if [[ $python3_ver != "" ]]
then
	echo "Current Python3 Version: $python3_ver"
fi

python_ver=$(python2 -V 2>&1)
if [[ $python_ver != "" ]]
then
	echo "Current Python/Python2 Version: $python_ver"
fi

echo
# install Python3 required Libs/Modules for EmMate..
echo ........................................................
echo "Install Python3 required Libs/Modules for EmMate"
echo ........................................................
python3 -m pip install -r $CURR_DIR/requirements.txt

######################################################################################################################################



echo
echo ............................
echo Unzip SDK to $CURR_DIR/sdk
echo ............................

# unzip at destination folder/directory and overwrite if exist 
unzip -o $CURR_DIR/sdk/esp-idf.zip -d $CURR_DIR/sdk/

ret_stat=$?

if [ $ret_stat -eq 0 ]
then
	export IDF_PATH=$CURR_DIR/sdk/esp-idf/
	echo
	echo ............................
	echo Start SDK installation.
	echo ............................
	
	$IDF_PATH/install.sh
	
	ret_stat=$?
	if [ $ret_stat -eq 0 ]
	then
		. $IDF_PATH/export.sh
		
		echo
		echo "#####################################################################"
		echo Emmate Installation Complete
		echo "#####################################################################"
		exit 0
	else
		echo "Failed to Install the SDK.."
		echo
		echo "#####################################################################"
		echo Emmate Installation Failed
		echo "#####################################################################"
		exit 1
	fi
	
else
	echo "Failed to Unzip the SDK.."
	echo
	echo "#####################################################################"
	echo Emmate Installation Failed
	echo "#####################################################################"
	exit 1
fi

