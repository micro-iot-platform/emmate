# EmMate Framework

The EmMate Framework is a Open Source multi-architecture, platform-independant middleware that can be used for any micro to small scale embedded application development.

The prime objective of the EmMate Framework is to provide an abstraction between an application and the hardware. The EmMate Framework provides an unified interface for application development, code compilation, application deployment and debugging for multiple embedded platforms.

## EmMate Release Distribution Package Contents

The EmMate Release Distribution Package must contain the following directories and files:
1.  examples			<DIR>	Directory containing numerous example codes for learning and using the EmMate framework
2.  getting-started		<DIR>	Directory containing the Getting Started guide(s)
3.  platform			<DIR>	Directory containing the source code for the specific hardware platform you have chosen
5.  resource			<DIR>	Directory containing EmMate Project setup files and scripts
6.  sdk					<DIR>	Directory containing the sdk of the specific hardware platform you have chosen
7.  simulator			<DIR>	Directory containing the source code for SoMThings simulator
8.  src					<DIR>	Directory containing the source code for the EmMate framework
9.  tools				<DIR>	Directory containing tools required for installing and running EmMate
10. export_emmate.bat	<FILE>	This file is used to setup emmate build environment on Windows. This is used internally from the build scripts
11. install_emmate.bat	<FILE>	This file is used to install the EmMate Development environment on Windows 
12. install_emmate.sh	<FILE>	This file is used to install the EmMate Development environment on Ubuntu
13. README.md			<FILE>	The file you are reading now
14. requirements.txt	<FILE>	This file is used by the installation scripts to check and fulfill python package requirements

## Getting Started with EmMate

The quickest way to get started with the EmMate Framework is to follow the getting started guide located at:
`getting-started`

Select the file `getting_started_with_emmate_<OS>.md` based on your choice of OS

The contents of this file can also be viewed on the web at: [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=documentation)
Be sure to select the correct documentation version on the web
