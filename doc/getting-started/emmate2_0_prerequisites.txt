Prerequisites for Emmate 2.0

################################################################################################################
For Ubuntu
################################################################################################################
sudo apt-get install cmake
sudo apt-get install make

sudo apt-get install python3-pip
python2 -m pip install --upgrade pip

sudo apt-get install python-pip
python -m pip install --upgrade pip


sudo apt-get install python3-tk
sudo apt-get install python-tk

sudo apt-get install cmake  (Note: camke 3.5 will install)
sudo apt-get install make


//in case Unzip not present in Ubuntu system
sudo apt-get install unzip


UART Setup

ESP32 - Device Setup

After completing the above installations, you are now ready setup your ESP32 based device/board. So, connect your ESP32 based device/board to the computer and check under what serial port it is visible

Serial ports have the following patterns in their names:

Linux: starting with /dev/tty

To check the serial port name in Linux run the following command in a terminal

$ ls /dev/tty*

Adding user to dialout on Linux

The currently logged user should have read and write access the serial port over USB. So we need to run the following command on our Ubuntu Linux terminal

$ sudo usermod -a -G dialout $USER

Log off and log in back to make this change effective.






################################################################################################################
for Windows
################################################################################################################
Install Python3.8 or else with he default installation with Enabling the Add to PATH(Enviroment variable) option