# Getting Started with the EmMate Framework

## Introduction

Welcome to EmMate!! The one stop solution for all your embedded requirements.

You are about to build a **hardware platform agnostic embedded application**

The EmMate framework is built with the intent to **support and work with multiple hardware platforms**

Here is your chance to be a part of **build once and flash anywhere** application ecosystem

# Getting Started

This guide will help you to set up, build and run an embedded application based on the EmMate framework.

## Prerequisites

There are few prior requirements.

### First thing first

You will need a Computer running Windows. Currently only 64-bit versions of Windows are supported.

### Hardware

You will need a development board from the MIG Platform's Hardware ecosystem. Please visit [The MIG SoMThing Page](https://mig.iquesters.com/?s=somthing&p=resources) to get your choice of hardware.

If you are a beginner, it is highly recommended that you use a development kit from the [MIG Platform](https://mig.iquesters.com/?s=somthing&p=resources). Using a MIG SoMThing kit will ensure a quick and easy way to start with EmMate.

Alternatively, if you are moderately advanced user, you may use any ESP32 based development board.

*Currently the EmMate Framework supports ESP32 based SOCs only*

**Supported Boards**
1. Team Thing with ESP32 SoM - [Micro IoT Gateway](https://mig.iquesters.com/?s=somthing&p=resources)
2. Team Thing with ESP32 ETH SoM - [Micro IoT Gateway](https://mig.iquesters.com/?s=somthing&p=resources)
3. MIG Starter - ESP32 Board - [Micro IoT Gateway](https://mig.iquesters.com/?s=somthing&p=resources)
4. ESP32-DevKit-C - Espressif
5. ESP-WROVER-Kit - Espressif

### Software

First you need to download a Release Package of the EmMate Framework. You will be directed to download it in the next section of the document.

You will need to download the [Eclipse CDT IDE](https://www.eclipse.org/)

Please download and install **Eclipse CDT** before proceeding as it will be required later on. You may also need to install **a java runtime environment (jre)** for Eclipse. The details for downloading and installing Eclipse can be found in [Eclipse Website](https://www.eclipse.org/)

*Note: If you are an advanced user and want a Command Line Interface (CLI), you can skip the Eclipse IDE steps. EmMate can be complete used from CLI. However, CLI usage is undocumented*

## Downloading the Required Files

Now that you have,

- Chosen your preferred hardware
- Installed **Eclipse CDT**

You may download a release package of the EmMate framework.

### Download Package
**NOTE:**

- *It is recommended to download the latest version under the 'Releases' Section from the link below as they have been tested and are stable.

- *Alternatively, if you are an advanced user, you may want to clone the source files from the git. You will get the link to EmMate's source in the link below.*

- *Please note that this document only covers the steps for application development using the release packages of EmMate. If you have cloned the git repository then you need to take additional steps (coming soon) before following this document.*

To download please visit [Downloads](https://mig.iquesters.com/?s=embedded&p=downloads) 

Assuming that you have successfully downloaded a release package, we would proceed further.
The file name of the release package would be `emmate-v2.1-esp32.zip`. Make sure you have downloaded this file.
You may keep the release package anywhere in your machine. We will keep it in `d:\emmate` for this guide.

### Distribution package Contents

Unzip the `emmate-v2.1-esp32.zip` file and you will get a folder called - `emmate-v2.1-esp32`

The folder `emmate-v2.1-esp32` must contain the following directories and files:

```
1.  examples            <DIR>    Directory containing numerous example codes for learning and using the EmMate framework
2.  getting-started        <DIR>    Directory containing the Getting Started documents (including this doc)
3.  platform            <DIR>    Directory containing EmMate source code for the specific hardware platform you have chosen
5.  sdk                    <DIR>    Directory containing the sdk of the specific hardware platform you have chosen
6.  resource            <DIR>    Directory containing EmMate Project setup files and scripts
7.  simulator            <DIR>    Directory containing the source code for SoMThing Simulator
8.  src                    <DIR>    Directory containing the source code for the EmMate APIs
9.  tools                <DIR>    Directory containing tools required for installing and running EmMate
10. export_emmate.bat    <FILE>    This file is used to setup emmate build environment on Windows. This is used internally from the build scripts
11. install_emmate.bat    <FILE>    This file is used to install the EmMate Development environment on Windows 
12. install_emmate.sh    <FILE>    This file is used to install the EmMate Development environment on Ubuntu
13. README.md            <FILE>    The main readme file
14. requirements.txt    <FILE>    This file is used by the installation scripts to check and fulfill python package requirements
```

## Installing EmMate Development Environment - Requires internet

Now that you have, downloaded and extracted the release package of the EmMate framework.

You are ready to install the EmMate Development Environment on your computer.

### Installing Python3

First, you must install the latest version of `python 3` in your machine from the official [python website](https://www.python.org/downloads/)

Be sure to install "**Python Launcher for Windows**" by following the instructions provided [here](https://docs.python.org/3/using/windows.html#the-full-installer). Also **Add Python to PATH** must be checked during installation.

### Installing EmMate Development Environment

Now that you have, successfully installed `python 3` from the above steps

You can install the EmMate Development Environment.

*Note: The installation process is going to download approx 300MB of files from the internet. Make sure you have a strong network connection.*

The steps for installation are:
- Go into the folder `emmate-v2.1-esp32`
- Open a command prompt in here. To do so press `Alt+d`, then type `cmd` and press `enter`. A command prompt window will open.
- Now run the file `install_emmate.bat`. To do so in the command prompt type `install_emmate.bat` and press `enter`
- This batch script will download and install all necessary files

## Device Setup - Requires internet

After completing the above installation, you can connect your development board to your PC. The same steps apply for all the supported boards listed above. So, connect your board to the computer via USB. Your Windows OS should automatically download and install the required device driver.

If the driver does not install automatically, then install the driver manually from the links given in the below table.

| Sl # | Development Board                                                | USB Driver                                                                                          |
|:----:|------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
|   1  | [Team Thing](https://mig.iquesters.com/?s=somthing&p=resources)  | [CP210x](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers) |
|   2  | [MIG Starter](https://mig.iquesters.com/?s=somthing&p=resources) | [CP210x](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers) |
|   3  | ESP32-DevKit-C                                                   | [CP210x](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers) |
|   4  | ESP-WROVER-Kit                                                   | [FTDI](https://www.ftdichip.com/Drivers/VCP.htm) |


Your Windows PC will detect the board as a COM Port.

Once the driver is successfully installed, you may confirm it by opening the device manager. To do so,
- Open the `Run` dialog box by pressing and holding the `Windows` key, then press the R key (Run).
- Type `devmgmt.msc`
- Click OK
- After the device manager opens, you may verify the COM Port as per the below image. Please take a note of the COM Port number as this will be required later on. The image show **COM3**

<img src="img/win/win-device-manager.png">

# Setup, Build & Run EmMate Projects

Now that you have,

- Successfully installed the EmMate Development Environment
- Successfully setup and verified your Device

You may now start developing actual applications. The best way to learn EmMate is to try the examples first. You may also try out the open source EmMate projects provided in the [MIG Projects Webpage](https://mig.iquesters.com/?p=projects). Follow this guide to setup, build and run EmMate examples, EmMate existing projects and EmMate new projects.

*Note: The below steps require usage of the Eclipse CDT IDE. So, it is assumed that you have successfully installed the latest version of Eclipse CDT on your machine.* If you have not installed Eclipse, please [click here](https://www.eclipse.org/) to do so.

## Steps to Setup a Project

There are 2 types of EmMate Projects which can be setup:

1. Empty Project (new project)
2. Existing EmMate Project, i.e. examples

### Setup an Empty Project (New Project)

1. To create an empty project first create a new folder in your Eclipse's workspace.

**This folder must be in the same drive where your EmMate Release Package resides. The folder name must not contain any spaces. The name you provide to this new folder becomes the name of your project.**

2. Inside this project folder copy the folder called 'setup' from `d:\emmate\emmate-v2.1-esp32\resource`

3. Now, go to `setup` folder. Open a command prompt here and run the file `setup.bat`. To do so press `Alt+d`, then type `cmd` and press `enter`. A command prompt window will open. In the command prompt type `setup.bat` and press `enter`. This will create the required folder structure and copy other dependency files in your project.

4. Your empty project setup is done. Read ahead to use Eclipse to build and run your new EmMate Project. The below documentation uses the `hello-world` example, but the same steps apply for your new project also.

### Setup an Existing EmMate Project (Examples & MIG Projects)

For setting up an existing EmMate Project, we will use the `hello-world` example located in `d:\emmate\emmate-v2.1-esp32\examples\getting-started\`. All the other examples can be setup, built and run in the same process. Please refer each example's README.md file to know more about it.

1. To setup an example project simply copy the example from the examples folder of the EmMate release package and paste it in your Eclipse's workspace. In our case we will copy the `hello-world` example from `d:\emmate\emmate-v2.1-esp32\examples\getting-started\`

2. Now, go to `setup` folder inside `hello-world`. Open a command prompt here and run the file `setup.bat`. To do so press `Alt+d`, then type `cmd` and press `enter`. A command prompt window will open. In the command prompt type `setup.bat` and press `enter`. This will create the required folder structure and copy other dependency files in your project.

3. Your example setup is done. Read ahead to use Eclipse to build and run your new EmMate Project.

### Import this project into Eclipse

Open Eclipse and click on `File -> Import`

<img src="img/win/import-emmate-project.png" width="1080">

Select `General -> Existing Projects into Workspace` and click Next

<img src="img/win/import-existing-proj.png" width="1080">

Then, click Browse and select the `hello-world` project from the file explorer and click Ok

The `hello-world` project will be automatically shown in the Import Projects window. Ensure the `hello-world` project is checked and the click on `Finish`

<img src="img/win/select-eclipse-project-and-finish.png" width="1080">

Now the EmMate `hello-world` project will be shown in the Eclipse Project Explorer

<img src="img/win/hello-world-project-explorer.png" width="1080">

## Steps to Build a Project

1. Click on the Project Name i.e. `hello-world`, then select the External Tool `build` from the dropdown as shown below and finally click on Launch button 

<img src="img/win/build-project.png" width="1080">

2. You will start seeing log message on the Eclipse Console and the **EmMate Development Framework Configuration** Tool will open. Here you need to set the serial port name as per your system. See the **Device Setup** section to know your COM Port number. See the below image to set this configuration. Once done you can, click on Save and close this tool.

<img src="img/win/emmate-configuration-tool.png" width="1080">

Now the `hello-world` project will continue the build process and finally you will be able to see the log:

```
## EmMate Project Build Successful ################################
```

<img src="img/win/build-complete.png" width="1080">  

## Steps to Flash and View Log Messages

1. First open the README.md file inside the example folder and follow the section **Prepare the Hardware**. In this case `hello-world`

2. Once your hardware is ready and connected, select the Eclipse External Tool `flash` from the dropdown as shown below and click on Launch button.

<img src="img/win/flash-project.png" width="1080">

If all goes well you will see the following log message in the console.

```
## EmMate Project Flash Successful ################################
```

<img src="img/win/flash-complete.png" width="1080">

3. To view log messages you need to open a **Serial Terminal**. Eclipse provides a Serial Terminal and we are going to use that one. So click on the Terminal button as shown below. The keyboard shortcut is `Shift+Ctrl+Alt+T`

<img src="img/win/terminal-open.png" width="1080">

From the `Choose Terminal` dropdown select `Serial Terminal`, and keep the settings as per the below image and click Ok

*Note: the baud rate for viewing log messages over serial port is fixed at 115200 and cannot be changed*

<img src="img/win/terminal-settings.png" width="1080">

Now you will be able to see the following.

<img src="img/win/log-messages.png" width="1080">

And that's all, you have **successfully completed the required steps**

4. If you want to completely erase the flash memory of the hardware, select the **Eclipse External Tool** `erase flash` from the dropdown as shown below and click on **Launch button**.

<img src="img/win/erase-flash-project.png" width="1080">

You will see the following log message in the console.

```
## EmMate Project Erase-FLash Successful ################################
```

## Known Issues

1. At any point in time if you get the following error in Eclipse while running any of the `External Tools` for building, flash etc. click on the project name in the `Project Explorer` and try again. The reason for this issue is; the launcher utility of Eclipse external tools depends on the selected context. Here we need to select the project as a context for the launcher.

<img src="img/win/eclipse-problem.png" width="1080">


# Understanding the EmMate Project Structure

To know the EmMate Project Structure and the purpose of each file, please go to the `README.md` file of the examples folder in your EmMate Release Package.

# API Documentation

The API Documentation for the **EmMate** framework is a documentation generated using **Doxygen**

[EmMate Version 2.1.0.0 API Documentation](https://assets.iquesters.com/emmate/api-docs/v2.1.0.0)

