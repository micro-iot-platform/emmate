# EmMate Release v2.1

Documentation for this release can be found at: https://mig.iquesters.com/?s=embedded&p=documentation

API Documentation for this release can be found at: https://assets.iquesters.com/emmate/api-docs/v2.1.0.0

- Supported Hardware - ESP32, SoMThing Simulator
- Supported Platform - ESP IDF, SoMThing Simulator

## Changes
1. Bugs fixed
	- Resolved menuconfig warnings
	
2. Refactoring
	- Renamed all CORE_LOGx to EM_LOGx
	- Renamed core_err to em_err
	- Rename all CORE_xx error macros to EM_xx
	- Renamed core_app_main() to emmate_main()
	- Renamed core_app_main.c to emmate_main.c

3. New Simulator Platform added
	- Support for threading, HTTP, HMI LED & Button, GPIO and ADC
	- No support for - I2C, SPI, UART, PWM and HMI CLCD yet

## Features
1. Connectivity:
* BLE SPP
* Wi-Fi STA
* Collect network info
* Internet availability checker

2. Network Protocols:
* HTTP(S)
* SNTP

3. IoT Cloud Support (migCloud):
* migCloud - https://mig.iquesters.com/?s=cloud
* Heartbeat
* Send network info in hearbeat
* Firmware Over the Air (FOTA)
* Get application configurations from migCloud server
* Post application data to migCloud server
* Release SoMThing
* Network Reset from server
 

4. System Services:
* Device Configuration via BLE
* System Time synced via SNTP

5. Downlink:
* OneWire

6. Peripherals:
* ADC
* GPIO
* I2C
* UART
* PWM (only for LED)

7. Persistent Memory Support:
* Non-Volatile Memory
* SD/MMC Card

8. File System
* FAT File System

9. HMI Support:
* LED notifications - Now supports system and application simultaneously
* Push button configuration
* Character LCD helper library

10. Logging
* Log levels
* Console Log
* Network Log via UDP (in experimental stage)

11. RTOS features:
* FreeRTOS Tasks
* FreeRTOS Queues
* FreeRTOS Event Groups
* FreeRTOS Mutex
* FreeRTOS Semaphores

12. Helper Libraries:
* Generic Doubly Linked List - https://github.com/philbot9/generic-linked-list
* Parson - https://github.com/kgabis/parson

13. Framework Configurations:
* The EmMate framework can be configured via Kconfig menu based configuration tool

